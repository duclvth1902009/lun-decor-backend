package com.lun.decor.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * @author duclai on 20/07/2021
 */
public class ClientAppInterceptor implements HandlerInterceptor {

    private final String clientKey;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getRequestURI().contains("api/app/")) {
            String token = request.getHeader("apikey");
            if (!clientKey.equals(token)) {
                throw new AccessDeniedException("not allow");
            }
        }
        return true;
    }

    public ClientAppInterceptor(String clientKey) {
        this.clientKey = clientKey;
    }
}
