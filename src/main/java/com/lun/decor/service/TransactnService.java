package com.lun.decor.service;

import com.lun.decor.service.dto.TransactnDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lun.decor.domain.Transactn}.
 */
public interface TransactnService {
    /**
     * Save a transactn.
     *
     * @param transactnDTO the entity to save.
     * @return the persisted entity.
     */
    TransactnDTO save(TransactnDTO transactnDTO);

    /**
     * Partially updates a transactn.
     *
     * @param transactnDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TransactnDTO> partialUpdate(TransactnDTO transactnDTO);

    /**
     * Get all the transactns.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TransactnDTO> findAll(Pageable pageable);

    /**
     * Get the "id" transactn.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TransactnDTO> findOne(String id);

    /**
     * Delete the "id" transactn.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
