package com.lun.decor.service;

import com.lun.decor.service.dto.RequestOrderDTO;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lun.decor.domain.RequestOrder}.
 */
public interface RequestOrderService {
    /**
     * Save a requestOrder.
     *
     * @param requestOrderDTO the entity to save.
     * @return the persisted entity.
     */
    RequestOrderDTO save(RequestOrderDTO requestOrderDTO);

    /**
     * Partially updates a requestOrder.
     *
     * @param requestOrderDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RequestOrderDTO> partialUpdate(RequestOrderDTO requestOrderDTO);

    /**
     * Get all the requestOrders.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RequestOrderDTO> findAll(Pageable pageable);

    /**
     * Get all the requestOrders with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RequestOrderDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" requestOrder.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RequestOrderDTO> findOne(String id);

    /**
     * Delete the "id" requestOrder.
     *
     * @param id the id of the entity.
     */
    void delete(String id);

    Page<RequestOrderDTO> findAllByStatus(Pageable pageable, String status);

    List<RequestOrderDTO> findAllByUpdateDate(String user);
}
