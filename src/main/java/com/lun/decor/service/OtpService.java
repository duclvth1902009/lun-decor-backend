package com.lun.decor.service;

import com.lun.decor.app.controller.RequestOrderController;
import com.lun.decor.domain.Otp;
import com.lun.decor.repository.OtpRepository;
import com.lun.decor.service.dto.BaseMessage;
import com.lun.decor.service.dto.RequestOrderDTO;
import com.lun.decor.service.dto.SendOtpResponseDTO;
import java.time.ZonedDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author duclai on 02/09/2021
 */
@Service
public class OtpService {

    private final Logger log = LoggerFactory.getLogger(OtpService.class);

    @Value("${send_otp.api_key}")
    public String API_KEY;

    @Value("${send_otp.secret_key}")
    public String SECRET_KEY;

    @Value("${send_otp.enabled}")
    public boolean ENABLE_CALL;

    private final MailService mailService;

    private final OtpRepository otpRepository;

    public OtpService(OtpRepository otpRepository, MailService mailService) {
        this.otpRepository = otpRepository;
        this.mailService = mailService;
    }

    public Otp verifyOtp(String otpId, String otpCode) throws Exception {
        log.debug("otp_id: " + otpId + "\notp code: " + otpCode);
        Otp otp = otpRepository.findById(otpId).orElse(null);
        if (otp == null) {
            log.error("otp.verify.not.exist");
            throw new Exception("otp.verify.not.exist");
        }
        if (ZonedDateTime.now().isAfter(otp.getExpired_time())) {
            log.error("otp.verify.expired");
            throw new Exception("otp.verify.expired");
        }
        if (!otp.getOtp_code().equals(otpCode)) {
            log.error("otp.verify.not.correct");
            throw new Exception("otp.verify.not.correct");
        }

        log.info("Verify OTP success! return OTP object: " + otp.toString());
        return otp;
    }

    public BaseMessage sendOtpSMSorEmail(@Nullable String email, @Nullable String phone) {
        BaseMessage result = new BaseMessage();
        boolean isSentMail = false;

        try {
            String otpCode = RandomStringUtils.randomNumeric(6);
            log.info("\n-------------------otp------------------\n" + otpCode + "\n-------------------otp------------------");

            if (email != null) {
                Pattern pattern = Pattern.compile("^.+@.+\\..+$");
                Matcher matcher = pattern.matcher(email);
                if (matcher.matches()) {
                    log.debug("Sending Email");
                    mailService.sendOTPEmail(email, otpCode);
                    isSentMail = true;
                }
            }
            if (ENABLE_CALL) {
                if (phone != null && phone.length() == 10) {
                    RestTemplate restTemplate = new RestTemplate();
                    String url =
                        "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?" +
                        "Phone=" +
                        phone +
                        "&Content=" +
                        otpCode +
                        "&ApiKey=" +
                        API_KEY +
                        "&SecretKey=" +
                        SECRET_KEY +
                        "&SmsType=2&Brandname=Verify";
                    ResponseEntity<SendOtpResponseDTO> response = restTemplate.getForEntity(url, SendOtpResponseDTO.class);
                    if ((response.getBody() == null || !"100".equals(response.getBody().CodeResult)) && !isSentMail) {
                        log.error("otp.sent.call.error");
                        log.error("Otp server response: " + response.toString());
                        throw new Exception("otp.sent.call.error");
                    }
                }
            }
            Otp otp = new Otp(otpCode);
            otp = otpRepository.save(otp);
            result.setDescription(otp.getId());
        } catch (Exception e) {
            result.setErrorCode("1");
            result.setSuccess(false);
            result.setDescription(e.getMessage());
        }
        return result;
    }
}
