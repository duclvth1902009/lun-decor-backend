package com.lun.decor.service;

import com.lun.decor.domain.Projct;
import com.lun.decor.service.dto.CustomerDTO;
import com.lun.decor.service.dto.PackDTO;
import com.lun.decor.service.dto.ProjctDTO;
import com.lun.decor.service.dto.RequestOrderDTO;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lun.decor.domain.Projct}.
 */
public interface ProjctService {
    /**
     * Save a projct.
     *
     * @param projctDTO the entity to save.
     * @return the persisted entity.
     */
    ProjctDTO save(ProjctDTO projctDTO);

    /**
     * Partially updates a projct.
     *
     * @param projctDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ProjctDTO> partialUpdate(ProjctDTO projctDTO);

    /**
     * Get all the projcts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProjctDTO> findAll(Pageable pageable);

    /**
     * Get all the projcts with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProjctDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" projct.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProjctDTO> findOne(String id);

    /**
     * Delete the "id" projct.
     *
     * @param id the id of the entity.
     */
    void delete(String id);

    ProjctDTO findProjctByRequestId(String requestId);

    List<ProjctDTO> findProjctsByCustomer(String phone, String email);

    Page<ProjctDTO> findByStatus(Pageable pageable, String status);

    Page<ProjctDTO> findForManage(Pageable pageable);

    Page<ProjctDTO> searchProjct(Pageable pageable, String status, Date start, Date end);

    Page<ProjctDTO> searchProjctFromTo(Pageable pageable, Date start, Date end);

    ProjctDTO generateProjctFromReqOrd(RequestOrderDTO requestOrderDTO, CustomerDTO customerDTO, PackDTO packDTO);
}
