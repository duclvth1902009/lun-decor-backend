package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.lun.decor.domain.Pack} entity.
 */
public class PackDTO implements Serializable {

    private String id;

    private byte[] file;

    private String fileContentType;
    private String name;

    private Long price_from;

    private Long price_to;

    private String description;

    private Set<ImageDTO> images = new HashSet<>();

    private Set<ProductDTO> products = new HashSet<>();

    private Set<ServicePkgDTO> services = new HashSet<>();

    private EventTypeDTO type;

    private Set<ImageTransfer> files = new HashSet<>();

    public Set<ImageTransfer> getFiles() {
        return files;
    }

    public void setFiles(Set<ImageTransfer> files) {
        this.files = files;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice_from() {
        return price_from;
    }

    public void setPrice_from(Long price_from) {
        this.price_from = price_from;
    }

    public Long getPrice_to() {
        return price_to;
    }

    public void setPrice_to(Long price_to) {
        this.price_to = price_to;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    public Set<ServicePkgDTO> getServices() {
        return services;
    }

    public void setServices(Set<ServicePkgDTO> services) {
        this.services = services;
    }

    public EventTypeDTO getType() {
        return type;
    }

    public void setType(EventTypeDTO type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PackDTO)) {
            return false;
        }

        PackDTO packDTO = (PackDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, packDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PackDTO{" +
            "id='" + getId() + "'" +
            ", file='" + getFile() + "'" +
            ", name='" + getName() + "'" +
            ", price_from=" + getPrice_from() +
            ", price_to=" + getPrice_to() +
            ", description='" + getDescription() + "'" +
            ", images='" + getImages() + "'" +
            ", products='" + getProducts() + "'" +
            ", services='" + getServices() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
