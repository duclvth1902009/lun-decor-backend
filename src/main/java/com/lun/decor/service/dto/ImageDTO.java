package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.lun.decor.domain.Image} entity.
 */
public class ImageDTO implements Serializable {

    private String id;

    private byte[] file;

    private String fileContentType;
    private String url;

    private String type;

    private String feature;

    private String note;

    private EventVenueDTO eventVenue;

    private ProjctDTO projct;

    private TrackProgressDTO trackProgress;

    private TaskDTO task;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public EventVenueDTO getEventVenue() {
        return eventVenue;
    }

    public void setEventVenue(EventVenueDTO eventVenue) {
        this.eventVenue = eventVenue;
    }

    public ProjctDTO getProjct() {
        return projct;
    }

    public void setProjct(ProjctDTO projct) {
        this.projct = projct;
    }

    public TrackProgressDTO getTrackProgress() {
        return trackProgress;
    }

    public void setTrackProgress(TrackProgressDTO trackProgress) {
        this.trackProgress = trackProgress;
    }

    public TaskDTO getTask() {
        return task;
    }

    public void setTask(TaskDTO task) {
        this.task = task;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageDTO)) {
            return false;
        }

        ImageDTO imageDTO = (ImageDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, imageDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageDTO{" +
            "id='" + getId() + "'" +
            ", file='" + getFile() + "'" +
            ", url='" + getUrl() + "'" +
            ", type='" + getType() + "'" +
            ", feature='" + getFeature() + "'" +
            ", note='" + getNote() + "'" +
            ", eventVenue='" + getEventVenue() + "'" +
            ", projct='" + getProjct() + "'" +
            ", trackProgress='" + getTrackProgress() + "'" +
            ", task='" + getTask() + "'" +
            "}";
    }
}
