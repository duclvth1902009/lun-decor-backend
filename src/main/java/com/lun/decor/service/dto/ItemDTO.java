package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.lun.decor.domain.Item} entity.
 */
public class ItemDTO implements Serializable {

    private String id;

    private String name;

    @NotNull
    private String code;

    private Integer type;

    private Long price;

    private String description;

    private String unit;

    private Set<ImageDTO> images = new HashSet<>();

    private Set<PartnerDTO> providers = new HashSet<>();

    private Set<ImageTransfer> files = new HashSet<>();

    public Set<ImageTransfer> getFiles() {
        return files;
    }

    public void setFiles(Set<ImageTransfer> files) {
        this.files = files;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    public Set<PartnerDTO> getProviders() {
        return providers;
    }

    public void setProviders(Set<PartnerDTO> providers) {
        this.providers = providers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ItemDTO)) {
            return false;
        }

        ItemDTO itemDTO = (ItemDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, itemDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ItemDTO{" +
            "id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", type=" + getType() +
            ", price=" + getPrice() +
            ", description='" + getDescription() + "'" +
            ", unit='" + getUnit() + "'" +
            ", images='" + getImages() + "'" +
            ", providers='" + getProviders() + "'" +
            "}";
    }
}
