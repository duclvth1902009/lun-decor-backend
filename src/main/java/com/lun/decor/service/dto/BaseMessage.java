package com.lun.decor.service.dto;

public class BaseMessage {

    private String errorCode;
    private Boolean success;
    private String description;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BaseMessage() {
        this.success = true;
        this.errorCode = "0";
        this.description = "";
    }
}
