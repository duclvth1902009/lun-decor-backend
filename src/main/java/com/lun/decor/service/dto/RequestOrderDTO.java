package com.lun.decor.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.lun.decor.domain.RequestOrder} entity.
 */
public class RequestOrderDTO extends AbstractHistoryDTO implements Serializable {

    private String id;

    private byte[] file;

    private String fileContentType;

    @NotNull(message = "request.order.phone.required")
    @Pattern(regexp = "[0-9]+")
    private String phone;

    private String email;

    @NotNull(message = "request.order.name.required")
    private String cusName;

    private String discount;

    private String about;

    private ZonedDateTime holdTime;

    private String budget;

    private String address;

    private String guestNum;

    private String description;

    private String status;

    private PackDTO pack;

    private Set<ImageDTO> images = new HashSet<>();

    private Set<TopicDTO> topics = new HashSet<>();

    private Set<ProductDTO> add_products = new HashSet<>();

    private Set<ProductDTO> sub_products = new HashSet<>();

    private Set<ServicePkgDTO> add_services = new HashSet<>();

    private Set<ServicePkgDTO> sub_services = new HashSet<>();

    private EventTypeDTO type;

    private EventVenueDTO eventVenue;

    private Set<ImageTransfer> files = new HashSet<>();

    public Set<ImageTransfer> getFiles() {
        return files;
    }

    public void setFiles(Set<ImageTransfer> files) {
        this.files = files;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public ZonedDateTime getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(ZonedDateTime holdTime) {
        this.holdTime = holdTime;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGuestNum() {
        return guestNum;
    }

    public void setGuestNum(String guestNum) {
        this.guestNum = guestNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PackDTO getPack() {
        return pack;
    }

    public void setPack(PackDTO pack) {
        this.pack = pack;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    public Set<TopicDTO> getTopics() {
        return topics;
    }

    public void setTopics(Set<TopicDTO> topics) {
        this.topics = topics;
    }

    public Set<ProductDTO> getAdd_products() {
        return add_products;
    }

    public void setAdd_products(Set<ProductDTO> add_products) {
        this.add_products = add_products;
    }

    public Set<ProductDTO> getSub_products() {
        return sub_products;
    }

    public void setSub_products(Set<ProductDTO> sub_products) {
        this.sub_products = sub_products;
    }

    public Set<ServicePkgDTO> getAdd_services() {
        return add_services;
    }

    public void setAdd_services(Set<ServicePkgDTO> add_services) {
        this.add_services = add_services;
    }

    public Set<ServicePkgDTO> getSub_services() {
        return sub_services;
    }

    public void setSub_services(Set<ServicePkgDTO> sub_services) {
        this.sub_services = sub_services;
    }

    public EventTypeDTO getType() {
        return type;
    }

    public void setType(EventTypeDTO type) {
        this.type = type;
    }

    public EventVenueDTO getEventVenue() {
        return eventVenue;
    }

    public void setEventVenue(EventVenueDTO eventVenue) {
        this.eventVenue = eventVenue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequestOrderDTO)) {
            return false;
        }

        RequestOrderDTO requestOrderDTO = (RequestOrderDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, requestOrderDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequestOrderDTO{" +
            "id='" + getId() + "'" +
            ", file='" + getFile() + "'" +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", cusName='" + getCusName() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", about='" + getAbout() + "'" +
            ", holdTime='" + getHoldTime() + "'" +
            ", budget='" + getBudget() + "'" +
            ", address='" + getAddress() + "'" +
            ", guestNum='" + getGuestNum() + "'" +
            ", description='" + getDescription() + "'" +
            ", status='" + getStatus() + "'" +
            ", pack='" + getPack() + "'" +
            ", images='" + getImages() + "'" +
            ", topics='" + getTopics() + "'" +
            ", add_products='" + getAdd_products() + "'" +
            ", sub_products='" + getSub_products() + "'" +
            ", add_services='" + getAdd_services() + "'" +
            ", sub_services='" + getSub_services() + "'" +
            ", type='" + getType() + "'" +
            ", eventVenue='" + getEventVenue() + "'" +
            "}";
    }
}
