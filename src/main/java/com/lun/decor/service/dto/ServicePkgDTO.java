package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.lun.decor.domain.ServicePkg} entity.
 */
public class ServicePkgDTO implements Serializable {

    private String id;

    private byte[] file;

    private String fileContentType;
    private String name;

    @NotNull
    private String code;

    private Long price;

    private String description;

    private Set<ImageDTO> images = new HashSet<>();

    private Set<PartnerDTO> providers = new HashSet<>();

    private Set<ImageTransfer> files = new HashSet<>();

    public Set<ImageTransfer> getFiles() {
        return files;
    }

    public void setFiles(Set<ImageTransfer> files) {
        this.files = files;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    public Set<PartnerDTO> getProviders() {
        return providers;
    }

    public void setProviders(Set<PartnerDTO> providers) {
        this.providers = providers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ServicePkgDTO)) {
            return false;
        }

        ServicePkgDTO servicePkgDTO = (ServicePkgDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, servicePkgDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ServicePkgDTO{" +
            "id='" + getId() + "'" +
            ", file='" + getFile() + "'" +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", price=" + getPrice() +
            ", description='" + getDescription() + "'" +
            ", images='" + getImages() + "'" +
            ", providers='" + getProviders() + "'" +
            "}";
    }
}
