package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.lun.decor.domain.Comment} entity.
 */
public class CommentDTO implements Serializable {

    private String id;

    private String content;

    private String parent_id;

    private CustomerDTO from;

    private CommentDTO parent;

    private ProjctDTO projct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public CustomerDTO getFrom() {
        return from;
    }

    public void setFrom(CustomerDTO from) {
        this.from = from;
    }

    public CommentDTO getParent() {
        return parent;
    }

    public void setParent(CommentDTO parent) {
        this.parent = parent;
    }

    public ProjctDTO getProjct() {
        return projct;
    }

    public void setProjct(ProjctDTO projct) {
        this.projct = projct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommentDTO)) {
            return false;
        }

        CommentDTO commentDTO = (CommentDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commentDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommentDTO{" +
            "id='" + getId() + "'" +
            ", content='" + getContent() + "'" +
            ", parent_id='" + getParent_id() + "'" +
            ", from='" + getFrom() + "'" +
            ", parent='" + getParent() + "'" +
            ", projct='" + getProjct() + "'" +
            "}";
    }
}
