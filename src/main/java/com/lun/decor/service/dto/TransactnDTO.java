package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.lun.decor.domain.Transactn} entity.
 */
public class TransactnDTO implements Serializable {

    private String id;

    private byte[] file;

    private String fileContentType;
    private String detail;

    private Long total;

    private String note;

    private String status;

    private ProjctDTO projct;

    private TrackProgressDTO stage;

    private TaskDTO atTask;

    private PartnerDTO provider;

    private CustomerDTO customer;

    private ImageDTO image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProjctDTO getProjct() {
        return projct;
    }

    public void setProjct(ProjctDTO projct) {
        this.projct = projct;
    }

    public TrackProgressDTO getStage() {
        return stage;
    }

    public void setStage(TrackProgressDTO stage) {
        this.stage = stage;
    }

    public TaskDTO getAtTask() {
        return atTask;
    }

    public void setAtTask(TaskDTO atTask) {
        this.atTask = atTask;
    }

    public PartnerDTO getProvider() {
        return provider;
    }

    public void setProvider(PartnerDTO provider) {
        this.provider = provider;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public ImageDTO getImage() {
        return image;
    }

    public void setImage(ImageDTO image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactnDTO)) {
            return false;
        }

        TransactnDTO transactnDTO = (TransactnDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, transactnDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TransactnDTO{" +
            "id='" + getId() + "'" +
            ", file='" + getFile() + "'" +
            ", detail='" + getDetail() + "'" +
            ", total=" + getTotal() +
            ", note='" + getNote() + "'" +
            ", status='" + getStatus() + "'" +
            ", projct='" + getProjct() + "'" +
            ", stage='" + getStage() + "'" +
            ", atTask='" + getAtTask() + "'" +
            ", provider='" + getProvider() + "'" +
            ", customer='" + getCustomer() + "'" +
            ", image='" + getImage() + "'" +
            "}";
    }
}
