package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.lun.decor.domain.EventVenue} entity.
 */
public class EventVenueDTO implements Serializable {

    private String id;

    private byte[] file;

    private String fileContentType;
    private String name;

    private String address;

    private String location;

    private Long price_from;

    private Long price_to;

    @NotNull
    @Pattern(regexp = "[0-9]+")
    private String phone;

    private String email;

    private String description;

    private Long quantity_max;

    private String note;

    private Set<ImageDTO> images = new HashSet<>();

    private Set<ImageTransfer> files = new HashSet<>();

    public Set<ImageTransfer> getFiles() {
        return files;
    }

    public void setFiles(Set<ImageTransfer> files) {
        this.files = files;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getPrice_from() {
        return price_from;
    }

    public void setPrice_from(Long price_from) {
        this.price_from = price_from;
    }

    public Long getPrice_to() {
        return price_to;
    }

    public void setPrice_to(Long price_to) {
        this.price_to = price_to;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getQuantity_max() {
        return quantity_max;
    }

    public void setQuantity_max(Long quantity_max) {
        this.quantity_max = quantity_max;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EventVenueDTO)) {
            return false;
        }

        EventVenueDTO eventVenueDTO = (EventVenueDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, eventVenueDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EventVenueDTO{" +
            "id='" + getId() + "'" +
            ", file='" + getFile() + "'" +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", location='" + getLocation() + "'" +
            ", price_from=" + getPrice_from() +
            ", price_to=" + getPrice_to() +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", description='" + getDescription() + "'" +
            ", quantity_max=" + getQuantity_max() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
