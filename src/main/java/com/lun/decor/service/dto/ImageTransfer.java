package com.lun.decor.service.dto;

/**
 * @author duclai on 14/08/2021
 */
public class ImageTransfer {

    private byte[] file;

    private String fileContentType;

    public ImageTransfer() {}

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }
}
