package com.lun.decor.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.lun.decor.domain.TrackProgress} entity.
 */
public class TrackProgressDTO extends AbstractHistoryDTO implements Serializable {

    private String id;

    private byte[] file;

    private String fileContentType;
    private String name;

    private String description;

    private Boolean clientEnable;

    private Integer step;

    private ZonedDateTime deadLine;

    private String status;

    private ProjctDTO projct;

    private Set<ImageDTO> images = new HashSet<>();

    private Set<ImageTransfer> files = new HashSet<>();

    public Set<ImageTransfer> getFiles() {
        return files;
    }

    public void setFiles(Set<ImageTransfer> files) {
        this.files = files;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getClientEnable() {
        return clientEnable;
    }

    public void setClientEnable(Boolean clientEnable) {
        this.clientEnable = clientEnable;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public ZonedDateTime getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(ZonedDateTime deadLine) {
        this.deadLine = deadLine;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProjctDTO getProjct() {
        return projct;
    }

    public void setProjct(ProjctDTO projct) {
        this.projct = projct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrackProgressDTO)) {
            return false;
        }

        TrackProgressDTO trackProgressDTO = (TrackProgressDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, trackProgressDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TrackProgressDTO{" +
            "id='" + getId() + "'" +
            ", file='" + getFile() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", clientEnable='" + getClientEnable() + "'" +
            ", step=" + getStep() +
            ", deadLine='" + getDeadLine() + "'" +
            ", status='" + getStatus() + "'" +
            ", projct='" + getProjct() + "'" +
            "}";
    }
}
