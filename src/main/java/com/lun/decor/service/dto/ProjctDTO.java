package com.lun.decor.service.dto;

import com.lun.decor.domain.TrackProgress;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.lun.decor.domain.Projct} entity.
 */
public class ProjctDTO extends AbstractHistoryDTO implements Serializable {

    private String id;

    private ZonedDateTime holdTime;

    private String location;

    private byte[] contract;

    private String contractContentType;

    private Double rate;

    private Long preCapital;

    private Long preTotal;

    private Long finCapital;

    private Long finTotal;

    private String rschangeCap;

    private String rschangeTot;

    private String status;

    private String name;

    private Double progress;

    public Double getProgress() {
        return progress;
    }

    public void setProgress(Double progress) {
        this.progress = progress;
    }

    private Set<TopicDTO> topics = new HashSet<>();

    private EventTypeDTO type;

    private Set<ImageTransfer> files = new HashSet<>();

    public Set<ImageTransfer> getFiles() {
        return files;
    }

    public void setFiles(Set<ImageTransfer> files) {
        this.files = files;
    }

    private EventVenueDTO eventVenue;

    private RequestOrderDTO requestOrder;

    private CustomerDTO customer;

    private Set<PackDTO> packs = new HashSet<>();

    private DesignDTO final_design;

    private Set<TrackProgressDTO> stages = new HashSet<>();

    public Set<TrackProgressDTO> getStages() {
        return stages;
    }

    public void setStages(Set<TrackProgressDTO> stages) {
        this.stages = stages;
    }

    private Set<ImageDTO> images = new HashSet<>();

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(ZonedDateTime holdTime) {
        this.holdTime = holdTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public byte[] getContract() {
        return contract;
    }

    public void setContract(byte[] contract) {
        this.contract = contract;
    }

    public String getContractContentType() {
        return contractContentType;
    }

    public void setContractContentType(String contractContentType) {
        this.contractContentType = contractContentType;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Long getPreCapital() {
        return preCapital;
    }

    public void setPreCapital(Long preCapital) {
        this.preCapital = preCapital;
    }

    public Long getPreTotal() {
        return preTotal;
    }

    public void setPreTotal(Long preTotal) {
        this.preTotal = preTotal;
    }

    public Long getFinCapital() {
        return finCapital;
    }

    public void setFinCapital(Long finCapital) {
        this.finCapital = finCapital;
    }

    public Long getFinTotal() {
        return finTotal;
    }

    public void setFinTotal(Long finTotal) {
        this.finTotal = finTotal;
    }

    public String getRschangeCap() {
        return rschangeCap;
    }

    public void setRschangeCap(String rschangeCap) {
        this.rschangeCap = rschangeCap;
    }

    public String getRschangeTot() {
        return rschangeTot;
    }

    public void setRschangeTot(String rschangeTot) {
        this.rschangeTot = rschangeTot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TopicDTO> getTopics() {
        return topics;
    }

    public void setTopics(Set<TopicDTO> topics) {
        this.topics = topics;
    }

    public EventTypeDTO getType() {
        return type;
    }

    public void setType(EventTypeDTO type) {
        this.type = type;
    }

    public EventVenueDTO getEventVenue() {
        return eventVenue;
    }

    public void setEventVenue(EventVenueDTO eventVenue) {
        this.eventVenue = eventVenue;
    }

    public RequestOrderDTO getRequestOrder() {
        return requestOrder;
    }

    public void setRequestOrder(RequestOrderDTO requestOrder) {
        this.requestOrder = requestOrder;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public Set<PackDTO> getPacks() {
        return packs;
    }

    public void setPacks(Set<PackDTO> packs) {
        this.packs = packs;
    }

    public DesignDTO getFinal_design() {
        return final_design;
    }

    public void setFinal_design(DesignDTO final_design) {
        this.final_design = final_design;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProjctDTO)) {
            return false;
        }

        ProjctDTO projctDTO = (ProjctDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, projctDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProjctDTO{" +
            "id='" + getId() + "'" +
            ", holdTime='" + getHoldTime() + "'" +
            ", location='" + getLocation() + "'" +
            ", contract='" + getContract() + "'" +
            ", rate=" + getRate() +
            ", preCapital=" + getPreCapital() +
            ", preTotal=" + getPreTotal() +
            ", finCapital=" + getFinCapital() +
            ", finTotal=" + getFinTotal() +
            ", rschangeCap='" + getRschangeCap() + "'" +
            ", rschangeTot='" + getRschangeTot() + "'" +
            ", status='" + getStatus() + "'" +
            ", name='" + getName() + "'" +
            ", topics='" + getTopics() + "'" +
            ", type='" + getType() + "'" +
            ", eventVenue='" + getEventVenue() + "'" +
            ", requestOrder='" + getRequestOrder() + "'" +
            ", customer='" + getCustomer() + "'" +
            ", packs='" + getPacks() + "'" +
            ", final_design='" + getFinal_design() + "'" +
            "}";
    }
}
