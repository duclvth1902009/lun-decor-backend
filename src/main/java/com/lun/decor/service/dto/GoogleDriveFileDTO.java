package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.Objects;

public class GoogleDriveFileDTO implements Serializable {

    private String id;
    private String name;
    private String link;
    private String size;
    private String thumbnailLink;
    private boolean shared;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoogleDriveFileDTO that = (GoogleDriveFileDTO) o;
        return (
            shared == that.shared &&
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(link, that.link) &&
            Objects.equals(size, that.size) &&
            Objects.equals(thumbnailLink, that.thumbnailLink)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, link, size, thumbnailLink, shared);
    }
}
