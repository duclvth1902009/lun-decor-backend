package com.lun.decor.service.dto;

/**
 * @author duclai on 02/08/2021
 */
public class RequestOrderResponseDTO {

    private String requestOrderId;
    private Boolean success;
    private String description;
    private String estimateCost;

    public String getRequestOrderId() {
        return requestOrderId;
    }

    public void setRequestOrderId(String requestOrderId) {
        this.requestOrderId = requestOrderId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEstimateCost() {
        return estimateCost;
    }

    public void setEstimateCost(String estimateCost) {
        this.estimateCost = estimateCost;
    }

    public RequestOrderResponseDTO() {
        this.setSuccess(true);
        this.setDescription("Gửi biểu mẫu thành công");
    }
}
