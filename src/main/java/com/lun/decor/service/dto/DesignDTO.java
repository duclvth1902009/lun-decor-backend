package com.lun.decor.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.lun.decor.domain.Design} entity.
 */
public class DesignDTO implements Serializable {

    private String id;

    private byte[] file;

    private String fileContentType;

    private byte[] imgQuickView;

    private String imgQuickViewContentType;

    private String quickView;

    private String designFile;

    private Long width;

    private Long height;

    private String note;

    private Set<ImageDTO> images = new HashSet<>();

    private Set<ProductDTO> products = new HashSet<>();

    private Set<TopicDTO> topics = new HashSet<>();

    private ProjctDTO of_prjct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public byte[] getImgQuickView() {
        return imgQuickView;
    }

    public void setImgQuickView(byte[] imgQuickView) {
        this.imgQuickView = imgQuickView;
    }

    public String getImgQuickViewContentType() {
        return imgQuickViewContentType;
    }

    public void setImgQuickViewContentType(String imgQuickViewContentType) {
        this.imgQuickViewContentType = imgQuickViewContentType;
    }

    public String getQuickView() {
        return quickView;
    }

    public void setQuickView(String quickView) {
        this.quickView = quickView;
    }

    public String getDesignFile() {
        return designFile;
    }

    public void setDesignFile(String designFile) {
        this.designFile = designFile;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    public Set<TopicDTO> getTopics() {
        return topics;
    }

    public void setTopics(Set<TopicDTO> topics) {
        this.topics = topics;
    }

    public ProjctDTO getOf_prjct() {
        return of_prjct;
    }

    public void setOf_prjct(ProjctDTO of_prjct) {
        this.of_prjct = of_prjct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DesignDTO)) {
            return false;
        }

        DesignDTO designDTO = (DesignDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, designDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DesignDTO{" +
            "id='" + getId() + "'" +
            ", file='" + getFile() + "'" +
            ", imgQuickView='" + getImgQuickView() + "'" +
            ", quickView='" + getQuickView() + "'" +
            ", width=" + getWidth() +
            ", height=" + getHeight() +
            ", note='" + getNote() + "'" +
            ", images='" + getImages() + "'" +
            ", products='" + getProducts() + "'" +
            ", topics='" + getTopics() + "'" +
            ", of_prjct='" + getOf_prjct() + "'" +
            "}";
    }
}
