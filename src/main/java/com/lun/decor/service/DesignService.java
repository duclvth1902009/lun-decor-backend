package com.lun.decor.service;

import com.lun.decor.service.dto.DesignDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lun.decor.domain.Design}.
 */
public interface DesignService {
    /**
     * Save a design.
     *
     * @param designDTO the entity to save.
     * @return the persisted entity.
     */
    DesignDTO save(DesignDTO designDTO);

    /**
     * Partially updates a design.
     *
     * @param designDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DesignDTO> partialUpdate(DesignDTO designDTO);

    /**
     * Get all the designs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DesignDTO> findAll(Pageable pageable);

    /**
     * Get all the designs with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DesignDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" design.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DesignDTO> findOne(String id);

    /**
     * Delete the "id" design.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
