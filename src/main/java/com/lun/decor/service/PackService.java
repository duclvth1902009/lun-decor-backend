package com.lun.decor.service;

import com.lun.decor.service.dto.PackDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lun.decor.domain.Pack}.
 */
public interface PackService {
    /**
     * Save a pack.
     *
     * @param packDTO the entity to save.
     * @return the persisted entity.
     */
    PackDTO save(PackDTO packDTO);

    /**
     * Partially updates a pack.
     *
     * @param packDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PackDTO> partialUpdate(PackDTO packDTO);

    /**
     * Get all the packs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PackDTO> findAll(Pageable pageable);

    /**
     * Get all the packs with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PackDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" pack.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PackDTO> findOne(String id);

    /**
     * Delete the "id" pack.
     *
     * @param id the id of the entity.
     */
    void delete(String id);

    Page<PackDTO> findByTypeId(String eventType, Pageable pageable);
}
