package com.lun.decor.service;

import com.lun.decor.service.dto.ServicePkgDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lun.decor.domain.ServicePkg}.
 */
public interface ServicePkgService {
    /**
     * Save a servicePkg.
     *
     * @param servicePkgDTO the entity to save.
     * @return the persisted entity.
     */
    ServicePkgDTO save(ServicePkgDTO servicePkgDTO);

    /**
     * Partially updates a servicePkg.
     *
     * @param servicePkgDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ServicePkgDTO> partialUpdate(ServicePkgDTO servicePkgDTO);

    /**
     * Get all the servicePkgs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ServicePkgDTO> findAll(Pageable pageable);

    /**
     * Get all the servicePkgs with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ServicePkgDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" servicePkg.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ServicePkgDTO> findOne(String id);

    /**
     * Delete the "id" servicePkg.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
