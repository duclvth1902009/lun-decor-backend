package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.RequestOrderDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RequestOrder} and its DTO {@link RequestOrderDTO}.
 */
@Mapper(
    componentModel = "spring",
    uses = {
        PackMapper.class,
        ImageMapper.class,
        TopicMapper.class,
        ProductMapper.class,
        ServicePkgMapper.class,
        EventTypeMapper.class,
        EventVenueMapper.class,
    }
)
public interface RequestOrderMapper extends EntityMapper<RequestOrderDTO, RequestOrder> {
    @Mapping(target = "pack", source = "pack", qualifiedByName = "name")
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    @Mapping(target = "topics", source = "topics", qualifiedByName = "nameSet")
    @Mapping(target = "add_products", source = "add_products", qualifiedByName = "nameSet")
    @Mapping(target = "sub_products", source = "sub_products", qualifiedByName = "nameSet")
    @Mapping(target = "add_services", source = "add_services", qualifiedByName = "nameSet")
    @Mapping(target = "sub_services", source = "sub_services", qualifiedByName = "nameSet")
    @Mapping(target = "type", source = "type", qualifiedByName = "name")
    @Mapping(target = "eventVenue", source = "eventVenue", qualifiedByName = "name")
    RequestOrderDTO toDto(RequestOrder s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    RequestOrderDTO toDtoId(RequestOrder requestOrder);

    @Mapping(target = "removeImages", ignore = true)
    @Mapping(target = "removeTopics", ignore = true)
    @Mapping(target = "removeAdd_products", ignore = true)
    @Mapping(target = "removeSub_products", ignore = true)
    @Mapping(target = "removeAdd_services", ignore = true)
    @Mapping(target = "removeSub_services", ignore = true)
    RequestOrder toEntity(RequestOrderDTO requestOrderDTO);
}
