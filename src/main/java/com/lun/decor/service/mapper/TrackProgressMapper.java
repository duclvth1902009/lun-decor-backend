package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.TaskDTO;
import com.lun.decor.service.dto.TrackProgressDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TrackProgress} and its DTO {@link TrackProgressDTO}.
 */
@Mapper(componentModel = "spring", uses = { ProjctMapper.class, ImageMapper.class })
public interface TrackProgressMapper extends EntityMapper<TrackProgressDTO, TrackProgress> {
    @Mapping(target = "projct", source = "projct", qualifiedByName = "id")
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    TrackProgressDTO toDto(TrackProgress s);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    TrackProgressDTO toDtoName(TrackProgress trackProgress);

    @Mapping(target = "removeImages", ignore = true)
    TrackProgress toEntity(TrackProgressDTO trackProgressDTO);
}
