package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.ProductDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = { ImageMapper.class, ItemMapper.class })
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    @Mapping(target = "items", source = "items", qualifiedByName = "nameSet")
    ProductDTO toDto(Product s);

    @Mapping(target = "removeImages", ignore = true)
    @Mapping(target = "removeItems", ignore = true)
    Product toEntity(ProductDTO productDTO);

    @Named("nameSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    Set<ProductDTO> toDtoNameSet(Set<Product> product);
}
