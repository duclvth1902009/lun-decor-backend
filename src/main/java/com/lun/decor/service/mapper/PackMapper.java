package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.PackDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pack} and its DTO {@link PackDTO}.
 */
@Mapper(componentModel = "spring", uses = { ImageMapper.class, ProductMapper.class, ServicePkgMapper.class, EventTypeMapper.class })
public interface PackMapper extends EntityMapper<PackDTO, Pack> {
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    @Mapping(target = "products", source = "products", qualifiedByName = "nameSet")
    @Mapping(target = "services", source = "services", qualifiedByName = "nameSet")
    @Mapping(target = "type", source = "type", qualifiedByName = "name")
    PackDTO toDto(Pack s);

    @Mapping(target = "removeImages", ignore = true)
    @Mapping(target = "removeProducts", ignore = true)
    @Mapping(target = "removeServices", ignore = true)
    Pack toEntity(PackDTO packDTO);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    PackDTO toDtoName(Pack pack);

    @Named("nameSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    Set<PackDTO> toDtoNameSet(Set<Pack> pack);
}
