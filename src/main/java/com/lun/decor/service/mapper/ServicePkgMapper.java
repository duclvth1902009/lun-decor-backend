package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.ServicePkgDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ServicePkg} and its DTO {@link ServicePkgDTO}.
 */
@Mapper(componentModel = "spring", uses = { ImageMapper.class, PartnerMapper.class })
public interface ServicePkgMapper extends EntityMapper<ServicePkgDTO, ServicePkg> {
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    @Mapping(target = "providers", source = "providers", qualifiedByName = "nameSet")
    ServicePkgDTO toDto(ServicePkg s);

    @Mapping(target = "removeImages", ignore = true)
    @Mapping(target = "removeProviders", ignore = true)
    ServicePkg toEntity(ServicePkgDTO servicePkgDTO);

    @Named("nameSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    Set<ServicePkgDTO> toDtoNameSet(Set<ServicePkg> servicePkg);
}
