package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.TaskDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Task} and its DTO {@link TaskDTO}.
 */
@Mapper(componentModel = "spring", uses = { ProjctMapper.class, TrackProgressMapper.class, ImageMapper.class })
public interface TaskMapper extends EntityMapper<TaskDTO, Task> {
    @Mapping(target = "projct", source = "projct", qualifiedByName = "id")
    @Mapping(target = "stage", source = "stage", qualifiedByName = "name")
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    TaskDTO toDto(Task s);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    TaskDTO toDtoName(Task task);

    @Mapping(target = "removeImages", ignore = true)
    Task toEntity(TaskDTO taskDTO);
}
