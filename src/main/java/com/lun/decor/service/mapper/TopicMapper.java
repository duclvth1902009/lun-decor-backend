package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.TopicDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Topic} and its DTO {@link TopicDTO}.
 */
@Mapper(componentModel = "spring", uses = { ImageMapper.class, EventTypeMapper.class })
public interface TopicMapper extends EntityMapper<TopicDTO, Topic> {
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    @Mapping(target = "type", source = "type", qualifiedByName = "name")
    TopicDTO toDto(Topic s);

    @Mapping(target = "removeImages", ignore = true)
    Topic toEntity(TopicDTO topicDTO);

    @Named("nameSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    Set<TopicDTO> toDtoNameSet(Set<Topic> topic);
}
