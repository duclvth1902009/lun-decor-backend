package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.CommentDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Comment} and its DTO {@link CommentDTO}.
 */
@Mapper(componentModel = "spring", uses = { CustomerMapper.class, ProjctMapper.class })
public interface CommentMapper extends EntityMapper<CommentDTO, Comment> {
    @Mapping(target = "from", source = "from", qualifiedByName = "name")
    @Mapping(target = "parent", source = "parent", qualifiedByName = "content")
    @Mapping(target = "projct", source = "projct", qualifiedByName = "id")
    CommentDTO toDto(Comment s);

    @Named("content")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "content", source = "content")
    CommentDTO toDtoContent(Comment comment);
}
