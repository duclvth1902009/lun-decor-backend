package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.EventTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link EventType} and its DTO {@link EventTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EventTypeMapper extends EntityMapper<EventTypeDTO, EventType> {
    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    EventTypeDTO toDtoName(EventType eventType);
}
