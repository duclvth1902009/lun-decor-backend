package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.DesignDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Design} and its DTO {@link DesignDTO}.
 */
@Mapper(componentModel = "spring", uses = { ImageMapper.class, ProductMapper.class, TopicMapper.class, ProjctMapper.class })
public interface DesignMapper extends EntityMapper<DesignDTO, Design> {
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    @Mapping(target = "products", source = "products", qualifiedByName = "nameSet")
    @Mapping(target = "topics", source = "topics", qualifiedByName = "nameSet")
    @Mapping(target = "of_prjct", source = "of_prjct", qualifiedByName = "id")
    DesignDTO toDto(Design s);

    @Mapping(target = "removeImages", ignore = true)
    @Mapping(target = "removeProducts", ignore = true)
    @Mapping(target = "removeTopics", ignore = true)
    Design toEntity(DesignDTO designDTO);

    @Named("quickView")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "quickView", source = "quickView")
    DesignDTO toDtoQuickView(Design design);
}
