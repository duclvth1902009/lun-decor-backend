package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.ImageDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Image} and its DTO {@link ImageDTO}.
 */
@Mapper(componentModel = "spring", uses = { EventVenueMapper.class, ProjctMapper.class, TrackProgressMapper.class, TaskMapper.class })
public interface ImageMapper extends EntityMapper<ImageDTO, Image> {
    @Mapping(target = "eventVenue", source = "eventVenue", qualifiedByName = "name")
    @Mapping(target = "projct", source = "projct", qualifiedByName = "id")
    @Mapping(target = "trackProgress", source = "trackProgress", qualifiedByName = "name")
    @Mapping(target = "task", source = "task", qualifiedByName = "name")
    ImageDTO toDto(Image s);

    @Named("url")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "url", source = "url")
    ImageDTO toDtoUrl(Image image);

    @Named("urlSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "url", source = "url")
    Set<ImageDTO> toDtoUrlSet(Set<Image> image);
}
