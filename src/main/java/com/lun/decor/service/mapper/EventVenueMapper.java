package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.EventVenueDTO;
import com.lun.decor.service.dto.ItemDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link EventVenue} and its DTO {@link EventVenueDTO}.
 */
@Mapper(componentModel = "spring", uses = { ImageMapper.class })
public interface EventVenueMapper extends EntityMapper<EventVenueDTO, EventVenue> {
    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    EventVenueDTO toDtoName(EventVenue eventVenue);

    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    EventVenueDTO toDto(EventVenue s);

    @Mapping(target = "removeImages", ignore = true)
    EventVenue toEntity(EventVenueDTO venueDTO);
}
