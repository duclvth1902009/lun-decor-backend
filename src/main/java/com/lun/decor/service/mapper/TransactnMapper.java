package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.TransactnDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Transactn} and its DTO {@link TransactnDTO}.
 */
@Mapper(
    componentModel = "spring",
    uses = { ProjctMapper.class, TrackProgressMapper.class, TaskMapper.class, PartnerMapper.class, CustomerMapper.class, ImageMapper.class }
)
public interface TransactnMapper extends EntityMapper<TransactnDTO, Transactn> {
    @Mapping(target = "projct", source = "projct", qualifiedByName = "id")
    @Mapping(target = "stage", source = "stage", qualifiedByName = "name")
    @Mapping(target = "atTask", source = "atTask", qualifiedByName = "name")
    @Mapping(target = "provider", source = "provider", qualifiedByName = "name")
    @Mapping(target = "customer", source = "customer", qualifiedByName = "name")
    @Mapping(target = "image", source = "image", qualifiedByName = "url")
    TransactnDTO toDto(Transactn s);
}
