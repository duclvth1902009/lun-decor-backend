package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.ProjctDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Projct} and its DTO {@link ProjctDTO}.
 */
@Mapper(
    componentModel = "spring",
    uses = {
        TopicMapper.class,
        EventTypeMapper.class,
        EventVenueMapper.class,
        RequestOrderMapper.class,
        CustomerMapper.class,
        PackMapper.class,
        DesignMapper.class,
        ImageMapper.class,
    }
)
public interface ProjctMapper extends EntityMapper<ProjctDTO, Projct> {
    @Mapping(target = "topics", source = "topics", qualifiedByName = "nameSet")
    @Mapping(target = "type", source = "type", qualifiedByName = "name")
    @Mapping(target = "eventVenue", source = "eventVenue", qualifiedByName = "name")
    @Mapping(target = "requestOrder", source = "requestOrder", qualifiedByName = "id")
    @Mapping(target = "customer", source = "customer", qualifiedByName = "name")
    @Mapping(target = "packs", source = "packs", qualifiedByName = "nameSet")
    @Mapping(target = "final_design", source = "final_design", qualifiedByName = "quickView")
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    @Mapping(target = "stages", ignore = true)
    @Mapping(target = "progress")
    ProjctDTO toDto(Projct s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProjctDTO toDtoId(Projct projct);

    @Mapping(target = "removeImages", ignore = true)
    @Mapping(target = "removeTopics", ignore = true)
    @Mapping(target = "removePacks", ignore = true)
    Projct toEntity(ProjctDTO projctDTO);
}
