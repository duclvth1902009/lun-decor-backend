package com.lun.decor.service.mapper;

import com.lun.decor.domain.*;
import com.lun.decor.service.dto.ItemDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Item} and its DTO {@link ItemDTO}.
 */
@Mapper(componentModel = "spring", uses = { ImageMapper.class, PartnerMapper.class })
public interface ItemMapper extends EntityMapper<ItemDTO, Item> {
    @Mapping(target = "images", source = "images", qualifiedByName = "urlSet")
    @Mapping(target = "providers", source = "providers", qualifiedByName = "nameSet")
    ItemDTO toDto(Item s);

    @Mapping(target = "removeImages", ignore = true)
    @Mapping(target = "removeProviders", ignore = true)
    Item toEntity(ItemDTO itemDTO);

    @Named("nameSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    Set<ItemDTO> toDtoNameSet(Set<Item> item);
}
