package com.lun.decor.service.impl;

import com.lun.decor.config.Constants;
import com.lun.decor.domain.Customer;
import com.lun.decor.domain.Projct;
import com.lun.decor.domain.RequestOrder;
import com.lun.decor.domain.TrackProgress;
import com.lun.decor.repository.CustomerRepository;
import com.lun.decor.repository.ProjctRepository;
import com.lun.decor.repository.RequestOrderRepository;
import com.lun.decor.service.ProjctService;
import com.lun.decor.service.dto.*;
import com.lun.decor.service.mapper.CustomerMapper;
import com.lun.decor.service.mapper.ProjctMapper;
import com.lun.decor.service.mapper.TrackProgressMapper;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Projct}.
 */
@Service
public class ProjctServiceImpl implements ProjctService {

    private final Logger log = LoggerFactory.getLogger(ProjctServiceImpl.class);

    private final ProjctRepository projctRepository;

    private final ProjctMapper projctMapper;

    private final TrackProgressMapper trackProgressMapper;

    private final RequestOrderRepository requestOrderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerMapper customerMapper;

    public ProjctServiceImpl(
        ProjctRepository projctRepository,
        ProjctMapper projctMapper,
        RequestOrderRepository requestOrderRepository,
        TrackProgressMapper trackProgressMapper
    ) {
        this.projctRepository = projctRepository;
        this.projctMapper = projctMapper;
        this.requestOrderRepository = requestOrderRepository;
        this.trackProgressMapper = trackProgressMapper;
    }

    @Override
    public ProjctDTO save(ProjctDTO projctDTO) {
        log.debug("Request to save Projct : {}", projctDTO);
        Projct projct = projctMapper.toEntity(projctDTO);
        List<TrackProgressDTO> lstTrackDTO = new ArrayList<>(projctDTO.getStages());
        List<TrackProgress> lstTrack = trackProgressMapper.toEntity(lstTrackDTO);
        projct.setStages(new HashSet<>(lstTrack));
        projct = projctRepository.save(projct);
        return projctMapper.toDto(projct);
    }

    @Override
    public Optional<ProjctDTO> partialUpdate(ProjctDTO projctDTO) {
        log.debug("Request to partially update Projct : {}", projctDTO);

        return projctRepository
            .findById(projctDTO.getId())
            .map(
                existingProjct -> {
                    projctMapper.partialUpdate(existingProjct, projctDTO);
                    return existingProjct;
                }
            )
            .map(projctRepository::save)
            .map(projctMapper::toDto);
    }

    @Override
    public Page<ProjctDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Projcts");
        return projctRepository.findAll(pageable).map(projctMapper::toDto);
    }

    public Page<ProjctDTO> findAllWithEagerRelationships(Pageable pageable) {
        return projctRepository.findAllWithEagerRelationships(pageable).map(projctMapper::toDto);
    }

    @Override
    public Optional<ProjctDTO> findOne(String id) {
        log.debug("Request to get Projct : {}", id);
        Optional<Projct> projct = projctRepository.findOneWithEagerRelationships(id);
        if (!projct.isPresent()) {
            return Optional.empty();
        }
        Optional<ProjctDTO> projctDTO = projct.map(projctMapper::toDto);
        List<TrackProgress> lstTrack = new ArrayList<>(projct.get().getStages());

        List<TrackProgressDTO> lstTrackDTO = trackProgressMapper.toDto(lstTrack);
        projctDTO.ifPresent(dto -> dto.setStages(new HashSet<>(lstTrackDTO)));
        return projctDTO;
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Projct : {}", id);
        projctRepository.deleteById(id);
    }

    @Override
    public ProjctDTO findProjctByRequestId(String requestId) {
        Optional<RequestOrder> requestOrderOptional = requestOrderRepository.findOneWithEagerRelationships(requestId);
        if (requestOrderOptional.isEmpty()) {
            return null;
        }
        RequestOrder requestOrder = requestOrderOptional.get();
        return projctMapper.toDto(projctRepository.findProjctByRequestOrder(requestOrder));
    }

    @Override
    public List<ProjctDTO> findProjctsByCustomer(String phone, String email) {
        List<Customer> customerList = customerRepository.findAllByPhoneOrEmail(phone, email);
        if (!customerList.isEmpty()) {
            List<Projct> projctList = projctRepository.findAllByCustomer(customerList.get(0));
            if (!projctList.isEmpty()) {
                List<ProjctDTO> rspList = projctRepository
                    .findAllByCustomer(customerList.get(0))
                    .stream()
                    .map(projctMapper::toDto)
                    .collect(Collectors.toList());
                rspList.get(0).setCustomer(customerMapper.toDto(customerList.get(0)));
                return rspList;
            }
        }
        return null;
    }

    @Override
    public Page<ProjctDTO> findByStatus(Pageable pageable, String status) {
        return projctRepository.findAllByStatus(pageable, status).map(projctMapper::toDto);
    }

    @Override
    public Page<ProjctDTO> findForManage(Pageable pageable) {
        return projctRepository.findForManage(pageable, Constants.STATUS_ACTIVE, Constants.STATUS_PENDING).map(projctMapper::toDto);
    }

    @Override
    public Page<ProjctDTO> searchProjct(Pageable pageable, String status, Date start, Date end) {
        return projctRepository.searchProjct(pageable, status, start, end).map(projctMapper::toDto);
    }

    @Override
    public Page<ProjctDTO> searchProjctFromTo(Pageable pageable, Date start, Date end) {
        return projctRepository.searchProjctFromTo(pageable, start, end).map(projctMapper::toDto);
    }

    @Override
    public ProjctDTO generateProjctFromReqOrd(RequestOrderDTO requestOrderDTO, CustomerDTO customerDTO, PackDTO packDTO) {
        // tao project
        ProjctDTO projctDTO = new ProjctDTO();
        projctDTO.setImages(packDTO.getImages());
        projctDTO.setHoldTime(requestOrderDTO.getHoldTime());
        projctDTO.setEventVenue(requestOrderDTO.getEventVenue());
        projctDTO.setRequestOrder(requestOrderDTO);
        projctDTO.setCustomer(customerDTO);
        projctDTO.setType(requestOrderDTO.getType());
        projctDTO.setTopics(requestOrderDTO.getTopics());
        projctDTO.getPacks().add(packDTO);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("HH:mm 'ngày' dd/MM/yyyy");
        projctDTO.setStatus(Constants.STATUS_ACTIVE);
        String projectName =
            projctDTO.getCustomer().getName() +
            "-" +
            projctDTO.getCustomer().getPhone() +
            "-" +
            packDTO.getName() +
            "-" +
            requestOrderDTO.getHoldTime().format(dateFormat);
        projctDTO.setName(projectName);
        if (requestOrderDTO.getEventVenue() != null) {
            projctDTO.setLocation(requestOrderDTO.getEventVenue().getAddress());
            projctDTO.setEventVenue(requestOrderDTO.getEventVenue());
        } else if (requestOrderDTO.getAddress() != null) {
            projctDTO.setLocation(requestOrderDTO.getAddress());
        } else {
            projctDTO.setLocation("Chưa rõ địa điểm");
        }

        projctDTO.setFinTotal(packDTO.getPrice_to());
        projctDTO.setPreTotal(packDTO.getPrice_to());
        return this.save(projctDTO);
    }
}
