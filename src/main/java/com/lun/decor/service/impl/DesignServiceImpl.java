package com.lun.decor.service.impl;

import com.lun.decor.domain.Design;
import com.lun.decor.repository.DesignRepository;
import com.lun.decor.service.DesignService;
import com.lun.decor.service.dto.DesignDTO;
import com.lun.decor.service.mapper.DesignMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Design}.
 */
@Service
public class DesignServiceImpl implements DesignService {

    private final Logger log = LoggerFactory.getLogger(DesignServiceImpl.class);

    private final DesignRepository designRepository;

    private final DesignMapper designMapper;

    public DesignServiceImpl(DesignRepository designRepository, DesignMapper designMapper) {
        this.designRepository = designRepository;
        this.designMapper = designMapper;
    }

    @Override
    public DesignDTO save(DesignDTO designDTO) {
        log.debug("Request to save Design : {}", designDTO);
        Design design = designMapper.toEntity(designDTO);
        design = designRepository.save(design);
        return designMapper.toDto(design);
    }

    @Override
    public Optional<DesignDTO> partialUpdate(DesignDTO designDTO) {
        log.debug("Request to partially update Design : {}", designDTO);

        return designRepository
            .findById(designDTO.getId())
            .map(
                existingDesign -> {
                    designMapper.partialUpdate(existingDesign, designDTO);
                    return existingDesign;
                }
            )
            .map(designRepository::save)
            .map(designMapper::toDto);
    }

    @Override
    public Page<DesignDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Designs");
        return designRepository.findAll(pageable).map(designMapper::toDto);
    }

    public Page<DesignDTO> findAllWithEagerRelationships(Pageable pageable) {
        return designRepository.findAllWithEagerRelationships(pageable).map(designMapper::toDto);
    }

    @Override
    public Optional<DesignDTO> findOne(String id) {
        log.debug("Request to get Design : {}", id);
        return designRepository.findOneWithEagerRelationships(id).map(designMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Design : {}", id);
        designRepository.deleteById(id);
    }
}
