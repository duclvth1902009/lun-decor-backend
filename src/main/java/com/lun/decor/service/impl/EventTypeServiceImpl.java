package com.lun.decor.service.impl;

import com.lun.decor.domain.EventType;
import com.lun.decor.repository.EventTypeRepository;
import com.lun.decor.service.EventTypeService;
import com.lun.decor.service.dto.EventTypeDTO;
import com.lun.decor.service.mapper.EventTypeMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link EventType}.
 */
@Service
public class EventTypeServiceImpl implements EventTypeService {

    private final Logger log = LoggerFactory.getLogger(EventTypeServiceImpl.class);

    private final EventTypeRepository eventTypeRepository;

    private final EventTypeMapper eventTypeMapper;

    public EventTypeServiceImpl(EventTypeRepository eventTypeRepository, EventTypeMapper eventTypeMapper) {
        this.eventTypeRepository = eventTypeRepository;
        this.eventTypeMapper = eventTypeMapper;
    }

    @Override
    public EventTypeDTO save(EventTypeDTO eventTypeDTO) {
        log.debug("Request to save EventType : {}", eventTypeDTO);
        EventType eventType = eventTypeMapper.toEntity(eventTypeDTO);
        eventType = eventTypeRepository.save(eventType);
        return eventTypeMapper.toDto(eventType);
    }

    @Override
    public Optional<EventTypeDTO> partialUpdate(EventTypeDTO eventTypeDTO) {
        log.debug("Request to partially update EventType : {}", eventTypeDTO);

        return eventTypeRepository
            .findById(eventTypeDTO.getId())
            .map(
                existingEventType -> {
                    eventTypeMapper.partialUpdate(existingEventType, eventTypeDTO);
                    return existingEventType;
                }
            )
            .map(eventTypeRepository::save)
            .map(eventTypeMapper::toDto);
    }

    @Override
    public List<EventTypeDTO> findAll() {
        log.debug("Request to get all EventTypes");
        return eventTypeRepository.findAll().stream().map(eventTypeMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<EventTypeDTO> findOne(String id) {
        log.debug("Request to get EventType : {}", id);
        return eventTypeRepository.findById(id).map(eventTypeMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete EventType : {}", id);
        eventTypeRepository.deleteById(id);
    }
}
