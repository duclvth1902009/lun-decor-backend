package com.lun.decor.service.impl;

import com.lun.decor.domain.Transactn;
import com.lun.decor.repository.TransactnRepository;
import com.lun.decor.service.TransactnService;
import com.lun.decor.service.dto.TransactnDTO;
import com.lun.decor.service.mapper.TransactnMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Transactn}.
 */
@Service
public class TransactnServiceImpl implements TransactnService {

    private final Logger log = LoggerFactory.getLogger(TransactnServiceImpl.class);

    private final TransactnRepository transactnRepository;

    private final TransactnMapper transactnMapper;

    public TransactnServiceImpl(TransactnRepository transactnRepository, TransactnMapper transactnMapper) {
        this.transactnRepository = transactnRepository;
        this.transactnMapper = transactnMapper;
    }

    @Override
    public TransactnDTO save(TransactnDTO transactnDTO) {
        log.debug("Request to save Transactn : {}", transactnDTO);
        Transactn transactn = transactnMapper.toEntity(transactnDTO);
        transactn = transactnRepository.save(transactn);
        return transactnMapper.toDto(transactn);
    }

    @Override
    public Optional<TransactnDTO> partialUpdate(TransactnDTO transactnDTO) {
        log.debug("Request to partially update Transactn : {}", transactnDTO);

        return transactnRepository
            .findById(transactnDTO.getId())
            .map(
                existingTransactn -> {
                    transactnMapper.partialUpdate(existingTransactn, transactnDTO);
                    return existingTransactn;
                }
            )
            .map(transactnRepository::save)
            .map(transactnMapper::toDto);
    }

    @Override
    public Page<TransactnDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Transactns");
        return transactnRepository.findAll(pageable).map(transactnMapper::toDto);
    }

    @Override
    public Optional<TransactnDTO> findOne(String id) {
        log.debug("Request to get Transactn : {}", id);
        return transactnRepository.findById(id).map(transactnMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Transactn : {}", id);
        transactnRepository.deleteById(id);
    }
}
