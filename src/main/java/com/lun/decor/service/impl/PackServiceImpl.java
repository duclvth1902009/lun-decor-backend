package com.lun.decor.service.impl;

import com.lun.decor.domain.Pack;
import com.lun.decor.repository.PackRepository;
import com.lun.decor.service.PackService;
import com.lun.decor.service.dto.PackDTO;
import com.lun.decor.service.mapper.PackMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Pack}.
 */
@Service
public class PackServiceImpl implements PackService {

    private final Logger log = LoggerFactory.getLogger(PackServiceImpl.class);

    private final PackRepository packRepository;

    private final PackMapper packMapper;

    public PackServiceImpl(PackRepository packRepository, PackMapper packMapper) {
        this.packRepository = packRepository;
        this.packMapper = packMapper;
    }

    @Override
    public PackDTO save(PackDTO packDTO) {
        log.debug("Request to save Pack : {}", packDTO);
        Pack pack = packMapper.toEntity(packDTO);
        pack = packRepository.save(pack);
        return packMapper.toDto(pack);
    }

    @Override
    public Optional<PackDTO> partialUpdate(PackDTO packDTO) {
        log.debug("Request to partially update Pack : {}", packDTO);

        return packRepository
            .findById(packDTO.getId())
            .map(
                existingPack -> {
                    packMapper.partialUpdate(existingPack, packDTO);
                    return existingPack;
                }
            )
            .map(packRepository::save)
            .map(packMapper::toDto);
    }

    @Override
    public Page<PackDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Packs");
        return packRepository.findAll(pageable).map(packMapper::toDto);
    }

    public Page<PackDTO> findAllWithEagerRelationships(Pageable pageable) {
        return packRepository.findAllWithEagerRelationships(pageable).map(packMapper::toDto);
    }

    @Override
    public Optional<PackDTO> findOne(String id) {
        log.debug("Request to get Pack : {}", id);
        return packRepository.findOneWithEagerRelationships(id).map(packMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Pack : {}", id);
        packRepository.deleteById(id);
    }

    @Override
    public Page<PackDTO> findByTypeId(String id, Pageable pageable) {
        return packRepository.findByTypeId(pageable, id).map(packMapper::toDto);
    }
}
