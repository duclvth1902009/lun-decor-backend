package com.lun.decor.service.impl;

import com.lun.decor.domain.EventVenue;
import com.lun.decor.repository.EventVenueRepository;
import com.lun.decor.service.EventVenueService;
import com.lun.decor.service.dto.EventVenueDTO;
import com.lun.decor.service.mapper.EventVenueMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link EventVenue}.
 */
@Service
public class EventVenueServiceImpl implements EventVenueService {

    private final Logger log = LoggerFactory.getLogger(EventVenueServiceImpl.class);

    private final EventVenueRepository eventVenueRepository;

    private final EventVenueMapper eventVenueMapper;

    public EventVenueServiceImpl(EventVenueRepository eventVenueRepository, EventVenueMapper eventVenueMapper) {
        this.eventVenueRepository = eventVenueRepository;
        this.eventVenueMapper = eventVenueMapper;
    }

    @Override
    public EventVenueDTO save(EventVenueDTO eventVenueDTO) {
        log.debug("Request to save EventVenue : {}", eventVenueDTO);
        EventVenue eventVenue = eventVenueMapper.toEntity(eventVenueDTO);
        eventVenue = eventVenueRepository.save(eventVenue);
        return eventVenueMapper.toDto(eventVenue);
    }

    @Override
    public Optional<EventVenueDTO> partialUpdate(EventVenueDTO eventVenueDTO) {
        log.debug("Request to partially update EventVenue : {}", eventVenueDTO);

        return eventVenueRepository
            .findById(eventVenueDTO.getId())
            .map(
                existingEventVenue -> {
                    eventVenueMapper.partialUpdate(existingEventVenue, eventVenueDTO);
                    return existingEventVenue;
                }
            )
            .map(eventVenueRepository::save)
            .map(eventVenueMapper::toDto);
    }

    @Override
    public Page<EventVenueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EventVenues");
        return eventVenueRepository.findAll(pageable).map(eventVenueMapper::toDto);
    }

    @Override
    public Optional<EventVenueDTO> findOne(String id) {
        log.debug("Request to get EventVenue : {}", id);
        return eventVenueRepository.findById(id).map(eventVenueMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete EventVenue : {}", id);
        eventVenueRepository.deleteById(id);
    }
}
