package com.lun.decor.service.impl;

import com.lun.decor.domain.ServicePkg;
import com.lun.decor.repository.ServicePkgRepository;
import com.lun.decor.service.ServicePkgService;
import com.lun.decor.service.dto.ServicePkgDTO;
import com.lun.decor.service.mapper.ServicePkgMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link ServicePkg}.
 */
@Service
public class ServicePkgServiceImpl implements ServicePkgService {

    private final Logger log = LoggerFactory.getLogger(ServicePkgServiceImpl.class);

    private final ServicePkgRepository servicePkgRepository;

    private final ServicePkgMapper servicePkgMapper;

    public ServicePkgServiceImpl(ServicePkgRepository servicePkgRepository, ServicePkgMapper servicePkgMapper) {
        this.servicePkgRepository = servicePkgRepository;
        this.servicePkgMapper = servicePkgMapper;
    }

    @Override
    public ServicePkgDTO save(ServicePkgDTO servicePkgDTO) {
        log.debug("Request to save ServicePkg : {}", servicePkgDTO);
        ServicePkg servicePkg = servicePkgMapper.toEntity(servicePkgDTO);
        servicePkg = servicePkgRepository.save(servicePkg);
        return servicePkgMapper.toDto(servicePkg);
    }

    @Override
    public Optional<ServicePkgDTO> partialUpdate(ServicePkgDTO servicePkgDTO) {
        log.debug("Request to partially update ServicePkg : {}", servicePkgDTO);

        return servicePkgRepository
            .findById(servicePkgDTO.getId())
            .map(
                existingServicePkg -> {
                    servicePkgMapper.partialUpdate(existingServicePkg, servicePkgDTO);
                    return existingServicePkg;
                }
            )
            .map(servicePkgRepository::save)
            .map(servicePkgMapper::toDto);
    }

    @Override
    public Page<ServicePkgDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ServicePkgs");
        return servicePkgRepository.findAll(pageable).map(servicePkgMapper::toDto);
    }

    public Page<ServicePkgDTO> findAllWithEagerRelationships(Pageable pageable) {
        return servicePkgRepository.findAllWithEagerRelationships(pageable).map(servicePkgMapper::toDto);
    }

    @Override
    public Optional<ServicePkgDTO> findOne(String id) {
        log.debug("Request to get ServicePkg : {}", id);
        return servicePkgRepository.findOneWithEagerRelationships(id).map(servicePkgMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete ServicePkg : {}", id);
        servicePkgRepository.deleteById(id);
    }
}
