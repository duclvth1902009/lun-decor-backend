package com.lun.decor.service.impl;

import com.lun.decor.domain.RequestOrder;
import com.lun.decor.repository.RequestOrderRepository;
import com.lun.decor.service.RequestOrderService;
import com.lun.decor.service.dto.RequestOrderDTO;
import com.lun.decor.service.mapper.RequestOrderMapper;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link RequestOrder}.
 */
@Service
public class RequestOrderServiceImpl implements RequestOrderService {

    private final Logger log = LoggerFactory.getLogger(RequestOrderServiceImpl.class);

    private final RequestOrderRepository requestOrderRepository;

    private final RequestOrderMapper requestOrderMapper;

    public RequestOrderServiceImpl(RequestOrderRepository requestOrderRepository, RequestOrderMapper requestOrderMapper) {
        this.requestOrderRepository = requestOrderRepository;
        this.requestOrderMapper = requestOrderMapper;
    }

    @Override
    public RequestOrderDTO save(RequestOrderDTO requestOrderDTO) {
        log.debug("Request to save RequestOrder : {}", requestOrderDTO);
        RequestOrder requestOrder = requestOrderMapper.toEntity(requestOrderDTO);
        requestOrder = requestOrderRepository.save(requestOrder);
        return requestOrderMapper.toDto(requestOrder);
    }

    @Override
    public Optional<RequestOrderDTO> partialUpdate(RequestOrderDTO requestOrderDTO) {
        log.debug("Request to partially update RequestOrder : {}", requestOrderDTO);

        return requestOrderRepository
            .findById(requestOrderDTO.getId())
            .map(
                existingRequestOrder -> {
                    requestOrderMapper.partialUpdate(existingRequestOrder, requestOrderDTO);
                    return existingRequestOrder;
                }
            )
            .map(requestOrderRepository::save)
            .map(requestOrderMapper::toDto);
    }

    @Override
    public Page<RequestOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequestOrders");
        return requestOrderRepository.findAll(pageable).map(requestOrderMapper::toDto);
    }

    public Page<RequestOrderDTO> findAllWithEagerRelationships(Pageable pageable) {
        return requestOrderRepository.findAllWithEagerRelationships(pageable).map(requestOrderMapper::toDto);
    }

    @Override
    public Optional<RequestOrderDTO> findOne(String id) {
        log.debug("Request to get RequestOrder : {}", id);
        return requestOrderRepository.findOneWithEagerRelationships(id).map(requestOrderMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete RequestOrder : {}", id);
        requestOrderRepository.deleteById(id);
    }

    @Override
    public Page<RequestOrderDTO> findAllByStatus(Pageable pageable, String status) {
        return requestOrderRepository.findAllByStatus(status, pageable).map(requestOrderMapper::toDto);
    }

    @Override
    public List<RequestOrderDTO> findAllByUpdateDate(String user) {

        return requestOrderMapper.toDto(requestOrderRepository.findAllByLastModifiedBy(user));
    }
}
