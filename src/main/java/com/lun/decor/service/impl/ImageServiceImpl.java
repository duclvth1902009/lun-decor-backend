package com.lun.decor.service.impl;

import com.lun.decor.domain.Image;
import com.lun.decor.googledrive.GoogleDriveFileService;
import com.lun.decor.repository.ImageRepository;
import com.lun.decor.repository.UploadImageRepo;
import com.lun.decor.service.ImageService;
import com.lun.decor.service.dto.ImageDTO;
import com.lun.decor.service.dto.ImageTransfer;
import com.lun.decor.service.mapper.ImageMapper;
import java.io.File;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Image}.
 */
@Service
public class ImageServiceImpl implements ImageService {

    private final Logger log = LoggerFactory.getLogger(ImageServiceImpl.class);

    private final ImageRepository imageRepository;

    private final ImageMapper imageMapper;

    @Autowired
    private UploadImageRepo uploadImageRepo;

    @Autowired
    GoogleDriveFileService googleDriveFileService;

    public ImageServiceImpl(ImageRepository imageRepository, ImageMapper imageMapper) {
        this.imageRepository = imageRepository;
        this.imageMapper = imageMapper;
    }

    @Override
    public ImageDTO save(ImageDTO imageDTO) {
        log.debug("Request to save Image : {}", imageDTO);
        Image image = imageMapper.toEntity(imageDTO);
        image = imageRepository.save(image);
        return imageMapper.toDto(image);
    }

    @Override
    public Optional<ImageDTO> partialUpdate(ImageDTO imageDTO) {
        log.debug("Request to partially update Image : {}", imageDTO);

        return imageRepository
            .findById(imageDTO.getId())
            .map(
                existingImage -> {
                    imageMapper.partialUpdate(existingImage, imageDTO);
                    return existingImage;
                }
            )
            .map(imageRepository::save)
            .map(imageMapper::toDto);
    }

    @Override
    public Page<ImageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Images");
        return imageRepository.findAll(pageable).map(imageMapper::toDto);
    }

    @Override
    public Optional<ImageDTO> findOne(String id) {
        log.debug("Request to get Image : {}", id);
        return imageRepository.findById(id).map(imageMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Image : {}", id);
        Optional<ImageDTO> imageDTO = this.findOne(id);
        if (imageDTO.isPresent()) {
            String[] fileId = imageDTO.get().getUrl().split("=");
            try {
                googleDriveFileService.deleteFile(fileId[2]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        imageRepository.deleteById(id);
    }

    @Override
    public String uploadImage(byte[] data, String fileContentType) {
        File file = uploadImageRepo.getImageFromBase64(data, fileContentType);
        String link = googleDriveFileService.uploadFile(file, "image", fileContentType, true);
        file.delete();
        return link;
    }

    @Override
    public Set<ImageDTO> createListImage(Set<ImageTransfer> setFile) {
        Set<ImageDTO> rspList = new HashSet<>();
        for (ImageTransfer base64 : setFile) {
            File file = uploadImageRepo.getImageFromBase64(base64.getFile(), base64.getFileContentType());
            String link = googleDriveFileService.uploadFile(file, "image", base64.getFileContentType(), true);
            ImageDTO imageDTO = new ImageDTO();
            imageDTO.setFile(null);
            imageDTO.setUrl(link);
            ImageDTO img = this.save(imageDTO);
            rspList.add(img);
            file.delete();
        }
        return rspList;
    }
}
