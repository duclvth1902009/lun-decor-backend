package com.lun.decor.service.impl;

import com.lun.decor.domain.Customer;
import com.lun.decor.repository.CustomerRepository;
import com.lun.decor.service.CustomerService;
import com.lun.decor.service.dto.CustomerDTO;
import com.lun.decor.service.dto.RequestOrderDTO;
import com.lun.decor.service.mapper.CustomerMapper;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Customer}.
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    @Override
    public CustomerDTO save(CustomerDTO customerDTO) {
        log.debug("Request to save Customer : {}", customerDTO);
        Customer customer = customerMapper.toEntity(customerDTO);
        customer = customerRepository.save(customer);
        return customerMapper.toDto(customer);
    }

    @Override
    public Optional<CustomerDTO> partialUpdate(CustomerDTO customerDTO) {
        log.debug("Request to partially update Customer : {}", customerDTO);

        return customerRepository
            .findById(customerDTO.getId())
            .map(
                existingCustomer -> {
                    customerMapper.partialUpdate(existingCustomer, customerDTO);
                    return existingCustomer;
                }
            )
            .map(customerRepository::save)
            .map(customerMapper::toDto);
    }

    @Override
    public Page<CustomerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Customers");
        return customerRepository.findAll(pageable).map(customerMapper::toDto);
    }

    @Override
    public Optional<CustomerDTO> findOne(String id) {
        log.debug("Request to get Customer : {}", id);
        return customerRepository.findById(id).map(customerMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Customer : {}", id);
        customerRepository.deleteById(id);
    }

    @Override
    public List<CustomerDTO> getCustomerDTOsByPhoneOrEmail(String phone, String email) {
        return customerMapper.toDto(customerRepository.findAllByPhoneOrEmail(phone, email));
    }

    @Override
    public CustomerDTO getIfNullCreateCustomerDTOByPhoneOrEmail(String phone, String email, RequestOrderDTO requestOrderDTO) {
        List<CustomerDTO> customers = this.getCustomerDTOsByPhoneOrEmail(phone, email);
        if (customers.isEmpty()) {
            CustomerDTO customerDTO = new CustomerDTO();
            customerDTO.setEmail(requestOrderDTO.getEmail());
            customerDTO.setName(requestOrderDTO.getCusName());
            customerDTO.setEmail(requestOrderDTO.getEmail());
            customerDTO.setPhone(requestOrderDTO.getPhone());
            return this.save(customerDTO);
        } else {
            return customers.get(0);
        }
    }
}
