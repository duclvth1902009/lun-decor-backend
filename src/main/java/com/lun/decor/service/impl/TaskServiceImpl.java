package com.lun.decor.service.impl;

import com.lun.decor.config.Constants;
import com.lun.decor.domain.Task;
import com.lun.decor.repository.TaskRepository;
import com.lun.decor.service.TaskService;
import com.lun.decor.service.dto.*;
import com.lun.decor.service.mapper.TaskMapper;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Task}.
 */
@Service
public class TaskServiceImpl implements TaskService {

    private final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

    private final TaskRepository taskRepository;

    private final TaskMapper taskMapper;

    public TaskServiceImpl(TaskRepository taskRepository, TaskMapper taskMapper) {
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
    }

    @Override
    public TaskDTO save(TaskDTO taskDTO) {
        log.debug("Request to save Task : {}", taskDTO);
        Task task = taskMapper.toEntity(taskDTO);
        task = taskRepository.save(task);
        return taskMapper.toDto(task);
    }

    @Override
    public Optional<TaskDTO> partialUpdate(TaskDTO taskDTO) {
        log.debug("Request to partially update Task : {}", taskDTO);

        return taskRepository
            .findById(taskDTO.getId())
            .map(
                existingTask -> {
                    taskMapper.partialUpdate(existingTask, taskDTO);
                    return existingTask;
                }
            )
            .map(taskRepository::save)
            .map(taskMapper::toDto);
    }

    @Override
    public Page<TaskDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Tasks");
        return taskRepository.findAll(pageable).map(taskMapper::toDto);
    }

    @Override
    public Optional<TaskDTO> findOne(String id) {
        log.debug("Request to get Task : {}", id);
        return taskRepository.findById(id).map(taskMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Task : {}", id);
        taskRepository.deleteById(id);
    }

    @Override
    public TaskDTO createByTrack(TrackProgressDTO trackProgressDTO) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setProjct(trackProgressDTO.getProjct());
        taskDTO.setName(Constants.NAME_TASK_TRACK);
        taskDTO.setDetail("Track :" + trackProgressDTO.getName());
        taskDTO.setStatus(Constants.STATUS_TODO);
        return this.save(taskDTO);
    }

    @Override
    public Set<TaskDTO> generateByPackInfo(PackDTO packDTO, RequestOrderDTO requestOrderDTO, ProjctDTO projctDTO) {
        Set<TaskDTO> rtTasks = new HashSet<>();
        List<ProductDTO> products = new ArrayList<>(packDTO.getProducts());
        if (requestOrderDTO.getSub_products() != null && requestOrderDTO.getSub_products().size() > 0) {
            products.removeAll(requestOrderDTO.getSub_products());
        }

        if (requestOrderDTO.getAdd_products() != null && requestOrderDTO.getAdd_products().size() > 0) {
            products.addAll(requestOrderDTO.getAdd_products());
        }
        for (ProductDTO productDTO : products) {
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.setName(Constants.NAME_TASK_PRODUCT);
            taskDTO.setStatus(Constants.STATUS_TODO);
            taskDTO.setDetail("Product : " + productDTO.getName());
            taskDTO.setProjct(projctDTO);
            rtTasks.add(this.save(taskDTO));
        }

        //tao task theo service
        List<ServicePkgDTO> servicePkgDTOS = new ArrayList<>();

        if (packDTO.getServices() != null && packDTO.getServices().size() > 0) {
            servicePkgDTOS.addAll(packDTO.getServices());
        }
        if (requestOrderDTO.getSub_services() != null && requestOrderDTO.getSub_services().size() > 0) {
            servicePkgDTOS.removeAll(requestOrderDTO.getAdd_services());
        }
        if (requestOrderDTO.getAdd_services() != null && requestOrderDTO.getAdd_services().size() > 0) {
            servicePkgDTOS.addAll(requestOrderDTO.getAdd_services());
        }

        for (ServicePkgDTO servicePkgDTO : servicePkgDTOS) {
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.setName(Constants.NAME_TASK_SERVICE);
            taskDTO.setStatus(Constants.STATUS_TODO);
            taskDTO.setDetail("Service : " + servicePkgDTO.getName());
            taskDTO.setProjct(projctDTO);
            rtTasks.add(this.save(taskDTO));
        }
        return rtTasks;
    }
}
