package com.lun.decor.service.impl;

import com.lun.decor.config.Constants;
import com.lun.decor.domain.TrackProgress;
import com.lun.decor.repository.TrackProgressRepository;
import com.lun.decor.service.TaskService;
import com.lun.decor.service.TrackProgressService;
import com.lun.decor.service.dto.ProjctDTO;
import com.lun.decor.service.dto.TaskDTO;
import com.lun.decor.service.dto.TrackProgressDTO;
import com.lun.decor.service.mapper.TrackProgressMapper;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link TrackProgress}.
 */
@Service
public class TrackProgressServiceImpl implements TrackProgressService {

    private final Logger log = LoggerFactory.getLogger(TrackProgressServiceImpl.class);

    private final TrackProgressRepository trackProgressRepository;

    private final TrackProgressMapper trackProgressMapper;

    private final TaskService taskService;

    public TrackProgressServiceImpl(
        TrackProgressRepository trackProgressRepository,
        TrackProgressMapper trackProgressMapper,
        TaskService taskService
    ) {
        this.trackProgressRepository = trackProgressRepository;
        this.trackProgressMapper = trackProgressMapper;
        this.taskService = taskService;
    }

    @Override
    public TrackProgressDTO save(TrackProgressDTO trackProgressDTO) {
        log.debug("Request to save TrackProgress : {}", trackProgressDTO);
        TrackProgress trackProgress = trackProgressMapper.toEntity(trackProgressDTO);
        trackProgress = trackProgressRepository.save(trackProgress);
        return trackProgressMapper.toDto(trackProgress);
    }

    @Override
    public Optional<TrackProgressDTO> partialUpdate(TrackProgressDTO trackProgressDTO) {
        log.debug("Request to partially update TrackProgress : {}", trackProgressDTO);

        return trackProgressRepository
            .findById(trackProgressDTO.getId())
            .map(
                existingTrackProgress -> {
                    trackProgressMapper.partialUpdate(existingTrackProgress, trackProgressDTO);
                    return existingTrackProgress;
                }
            )
            .map(trackProgressRepository::save)
            .map(trackProgressMapper::toDto);
    }

    @Override
    public Page<TrackProgressDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TrackProgresses");
        return trackProgressRepository.findAll(pageable).map(trackProgressMapper::toDto);
    }

    @Override
    public Optional<TrackProgressDTO> findOne(String id) {
        log.debug("Request to get TrackProgress : {}", id);
        return trackProgressRepository.findById(id).map(trackProgressMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete TrackProgress : {}", id);
        trackProgressRepository.deleteById(id);
    }

    @Override
    public List<TrackProgress> findALlByProjectId(String prjId) {
        return trackProgressRepository.findByProjctId(prjId);
    }

    @Override
    public List<TrackProgressDTO> findALlByProjectIdAndClientEnable(String prjId) {
        return trackProgressRepository
            .findByProjctIdAndClient(prjId, true)
            .stream()
            .map(trackProgressMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public Set<TrackProgressDTO> generateDefaultTracks(ProjctDTO projctDTO) {
        Set<TrackProgressDTO> lstTrack = new HashSet<>();
        for (int i = 0; i < 6; i++) {
            TrackProgressDTO trackProgressDTO = new TrackProgressDTO();
            trackProgressDTO.setName(Constants.TRACK_PROGESS_NAME.get(i));
            trackProgressDTO.setStatus(Constants.STATUS_TODO);
            trackProgressDTO.setProjct(projctDTO);
            trackProgressDTO.setStep(i + 1);
            trackProgressDTO = this.save(trackProgressDTO);

            //todo: check again the tasks
            taskService.createByTrack(trackProgressDTO);
            lstTrack.add(trackProgressDTO);
        }

        return lstTrack;
    }
}
