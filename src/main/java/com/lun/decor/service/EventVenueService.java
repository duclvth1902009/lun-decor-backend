package com.lun.decor.service;

import com.lun.decor.service.dto.EventVenueDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lun.decor.domain.EventVenue}.
 */
public interface EventVenueService {
    /**
     * Save a eventVenue.
     *
     * @param eventVenueDTO the entity to save.
     * @return the persisted entity.
     */
    EventVenueDTO save(EventVenueDTO eventVenueDTO);

    /**
     * Partially updates a eventVenue.
     *
     * @param eventVenueDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EventVenueDTO> partialUpdate(EventVenueDTO eventVenueDTO);

    /**
     * Get all the eventVenues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EventVenueDTO> findAll(Pageable pageable);

    /**
     * Get the "id" eventVenue.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EventVenueDTO> findOne(String id);

    /**
     * Delete the "id" eventVenue.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
