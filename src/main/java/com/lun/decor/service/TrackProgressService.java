package com.lun.decor.service;

import com.lun.decor.domain.TrackProgress;
import com.lun.decor.service.dto.ProjctDTO;
import com.lun.decor.service.dto.TrackProgressDTO;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lun.decor.domain.TrackProgress}.
 */
public interface TrackProgressService {
    /**
     * Save a trackProgress.
     *
     * @param trackProgressDTO the entity to save.
     * @return the persisted entity.
     */
    TrackProgressDTO save(TrackProgressDTO trackProgressDTO);

    /**
     * Partially updates a trackProgress.
     *
     * @param trackProgressDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TrackProgressDTO> partialUpdate(TrackProgressDTO trackProgressDTO);

    /**
     * Get all the trackProgresses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TrackProgressDTO> findAll(Pageable pageable);

    /**
     * Get the "id" trackProgress.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TrackProgressDTO> findOne(String id);

    /**
     * Delete the "id" trackProgress.
     *
     * @param id the id of the entity.
     */
    void delete(String id);

    List<TrackProgress> findALlByProjectId(String prjId);

    List<TrackProgressDTO> findALlByProjectIdAndClientEnable(String prjId);

    Set<TrackProgressDTO> generateDefaultTracks(ProjctDTO projctDTO);
}
