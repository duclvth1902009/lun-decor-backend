package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A RequestOrder.
 */
@Document(collection = "request_order")
public class RequestOrder extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //    @Field("file")
    //    private byte[] file;
    //
    //    @Field("file_content_type")
    //    private String fileContentType;

    @NotNull
    @Pattern(regexp = "[0-9]+")
    @Field("phone")
    private String phone;

    @Field("email")
    private String email;

    @Field("cus_name")
    private String cusName;

    @Field("discount")
    private String discount;

    @Field("about")
    private String about;

    @Field("hold_time")
    private ZonedDateTime holdTime;

    @Field("budget")
    private String budget;

    @Field("address")
    private String address;

    @Field("guest_num")
    private String guestNum;

    @Field("description")
    private String description;

    @Field("status")
    private String status;

    @DBRef
    @Field("pack")
    @JsonIgnoreProperties(value = { "images", "products", "services", "type" }, allowSetters = true)
    private Pack pack;

    @DBRef
    @Field("images")
    @JsonIgnoreProperties(
        value = { "eventVenue", "items", "products", "servicePkgs", "packs", "designs", "orders", "projct", "trackProgress", "task" },
        allowSetters = true
    )
    private Set<Image> images = new HashSet<>();

    @DBRef
    @Field("topics")
    @JsonIgnoreProperties(value = { "images", "type" }, allowSetters = true)
    private Set<Topic> topics = new HashSet<>();

    @DBRef
    @Field("add_products")
    @JsonIgnoreProperties(value = { "images", "items" }, allowSetters = true)
    private Set<Product> add_products = new HashSet<>();

    @DBRef
    @Field("sub_products")
    @JsonIgnoreProperties(value = { "images", "items" }, allowSetters = true)
    private Set<Product> sub_products = new HashSet<>();

    @DBRef
    @Field("add_services")
    @JsonIgnoreProperties(value = { "images", "providers" }, allowSetters = true)
    private Set<ServicePkg> add_services = new HashSet<>();

    @DBRef
    @Field("sub_services")
    @JsonIgnoreProperties(value = { "images", "providers" }, allowSetters = true)
    private Set<ServicePkg> sub_services = new HashSet<>();

    @DBRef
    @Field("type")
    private EventType type;

    @DBRef
    @Field("eventVenue")
    @JsonIgnoreProperties(value = { "images" }, allowSetters = true)
    private EventVenue eventVenue;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RequestOrder id(String id) {
        this.id = id;
        return this;
    }

    public String getPhone() {
        return this.phone;
    }

    public RequestOrder phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public RequestOrder email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCusName() {
        return this.cusName;
    }

    public RequestOrder cusName(String cusName) {
        this.cusName = cusName;
        return this;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDiscount() {
        return this.discount;
    }

    public RequestOrder discount(String discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAbout() {
        return this.about;
    }

    public RequestOrder about(String about) {
        this.about = about;
        return this;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public ZonedDateTime getHoldTime() {
        return this.holdTime;
    }

    public RequestOrder holdTime(ZonedDateTime holdTime) {
        this.holdTime = holdTime;
        return this;
    }

    public void setHoldTime(ZonedDateTime holdTime) {
        this.holdTime = holdTime;
    }

    public String getBudget() {
        return this.budget;
    }

    public RequestOrder budget(String budget) {
        this.budget = budget;
        return this;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getAddress() {
        return this.address;
    }

    public RequestOrder address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGuestNum() {
        return this.guestNum;
    }

    public RequestOrder guestNum(String guestNum) {
        this.guestNum = guestNum;
        return this;
    }

    public void setGuestNum(String guestNum) {
        this.guestNum = guestNum;
    }

    public String getDescription() {
        return this.description;
    }

    public RequestOrder description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return this.status;
    }

    public RequestOrder status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Pack getPack() {
        return this.pack;
    }

    public RequestOrder pack(Pack pack) {
        this.setPack(pack);
        return this;
    }

    public void setPack(Pack pack) {
        this.pack = pack;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public RequestOrder images(Set<Image> images) {
        this.setImages(images);
        return this;
    }

    public RequestOrder addImages(Image image) {
        this.images.add(image);
        image.getOrders().add(this);
        return this;
    }

    public RequestOrder removeImages(Image image) {
        this.images.remove(image);
        image.getOrders().remove(this);
        return this;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public Set<Topic> getTopics() {
        return this.topics;
    }

    public RequestOrder topics(Set<Topic> topics) {
        this.setTopics(topics);
        return this;
    }

    public RequestOrder addTopics(Topic topic) {
        this.topics.add(topic);
        return this;
    }

    public RequestOrder removeTopics(Topic topic) {
        this.topics.remove(topic);
        return this;
    }

    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

    public Set<Product> getAdd_products() {
        return this.add_products;
    }

    public RequestOrder add_products(Set<Product> products) {
        this.setAdd_products(products);
        return this;
    }

    public RequestOrder addAdd_products(Product product) {
        this.add_products.add(product);
        return this;
    }

    public RequestOrder removeAdd_products(Product product) {
        this.add_products.remove(product);
        return this;
    }

    public void setAdd_products(Set<Product> products) {
        this.add_products = products;
    }

    public Set<Product> getSub_products() {
        return this.sub_products;
    }

    public RequestOrder sub_products(Set<Product> products) {
        this.setSub_products(products);
        return this;
    }

    public RequestOrder addSub_products(Product product) {
        this.sub_products.add(product);
        return this;
    }

    public RequestOrder removeSub_products(Product product) {
        this.sub_products.remove(product);
        return this;
    }

    public void setSub_products(Set<Product> products) {
        this.sub_products = products;
    }

    public Set<ServicePkg> getAdd_services() {
        return this.add_services;
    }

    public RequestOrder add_services(Set<ServicePkg> servicePkgs) {
        this.setAdd_services(servicePkgs);
        return this;
    }

    public RequestOrder addAdd_services(ServicePkg servicePkg) {
        this.add_services.add(servicePkg);
        return this;
    }

    public RequestOrder removeAdd_services(ServicePkg servicePkg) {
        this.add_services.remove(servicePkg);
        return this;
    }

    public void setAdd_services(Set<ServicePkg> servicePkgs) {
        this.add_services = servicePkgs;
    }

    public Set<ServicePkg> getSub_services() {
        return this.sub_services;
    }

    public RequestOrder sub_services(Set<ServicePkg> servicePkgs) {
        this.setSub_services(servicePkgs);
        return this;
    }

    public RequestOrder addSub_services(ServicePkg servicePkg) {
        this.sub_services.add(servicePkg);
        return this;
    }

    public RequestOrder removeSub_services(ServicePkg servicePkg) {
        this.sub_services.remove(servicePkg);
        return this;
    }

    public void setSub_services(Set<ServicePkg> servicePkgs) {
        this.sub_services = servicePkgs;
    }

    public EventType getType() {
        return this.type;
    }

    public RequestOrder type(EventType eventType) {
        this.setType(eventType);
        return this;
    }

    public void setType(EventType eventType) {
        this.type = eventType;
    }

    public EventVenue getEventVenue() {
        return this.eventVenue;
    }

    public RequestOrder eventVenue(EventVenue eventVenue) {
        this.setEventVenue(eventVenue);
        return this;
    }

    public void setEventVenue(EventVenue eventVenue) {
        this.eventVenue = eventVenue;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequestOrder)) {
            return false;
        }
        return id != null && id.equals(((RequestOrder) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequestOrder{" +
            "id=" + getId() +
//            ", file='" + getFile() + "'" +
//            ", fileContentType='" + getFileContentType() + "'" +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", cusName='" + getCusName() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", about='" + getAbout() + "'" +
            ", holdTime='" + getHoldTime() + "'" +
            ", budget='" + getBudget() + "'" +
            ", address='" + getAddress() + "'" +
            ", guestNum='" + getGuestNum() + "'" +
            ", description='" + getDescription() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
