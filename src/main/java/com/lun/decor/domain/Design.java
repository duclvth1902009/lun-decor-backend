package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Design.
 */
@Document(collection = "design")
public class Design extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("file")
    private byte[] file;

    @Field("file_content_type")
    private String fileContentType;

    @Field("img_quick_view")
    private byte[] imgQuickView;

    @Field("img_quick_view_content_type")
    private String imgQuickViewContentType;

    @NotNull
    @Field("quick_view")
    private String quickView;

    @Field("design_file")
    private String designFile;

    @Field("width")
    private Long width;

    @Field("height")
    private Long height;

    @Field("note")
    private String note;

    @DBRef
    @Field("images")
    @JsonIgnoreProperties(
        value = { "eventVenue", "items", "products", "servicePkgs", "packs", "designs", "orders", "projct", "trackProgress", "task" },
        allowSetters = true
    )
    private Set<Image> images = new HashSet<>();

    @DBRef
    @Field("products")
    @JsonIgnoreProperties(value = { "images", "items" }, allowSetters = true)
    private Set<Product> products = new HashSet<>();

    @DBRef
    @Field("topics")
    @JsonIgnoreProperties(value = { "images", "type" }, allowSetters = true)
    private Set<Topic> topics = new HashSet<>();

    @DBRef
    @Field("of_prjct")
    @JsonIgnoreProperties(
        value = {
            "topics",
            "type",
            "eventVenue",
            "requestOrder",
            "customer",
            "packs",
            "final_design",
            "designs",
            "comments",
            "images",
            "stages",
            "tasks",
        },
        allowSetters = true
    )
    private Projct of_prjct;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Design id(String id) {
        this.id = id;
        return this;
    }

    public byte[] getFile() {
        return this.file;
    }

    public Design file(byte[] file) {
        this.file = file;
        return this;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return this.fileContentType;
    }

    public Design fileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
        return this;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public byte[] getImgQuickView() {
        return this.imgQuickView;
    }

    public Design imgQuickView(byte[] imgQuickView) {
        this.imgQuickView = imgQuickView;
        return this;
    }

    public void setImgQuickView(byte[] imgQuickView) {
        this.imgQuickView = imgQuickView;
    }

    public String getImgQuickViewContentType() {
        return this.imgQuickViewContentType;
    }

    public Design imgQuickViewContentType(String imgQuickViewContentType) {
        this.imgQuickViewContentType = imgQuickViewContentType;
        return this;
    }

    public void setImgQuickViewContentType(String imgQuickViewContentType) {
        this.imgQuickViewContentType = imgQuickViewContentType;
    }

    public String getQuickView() {
        return this.quickView;
    }

    public Design quickView(String quickView) {
        this.quickView = quickView;
        return this;
    }

    public void setQuickView(String quickView) {
        this.quickView = quickView;
    }

    public Long getWidth() {
        return this.width;
    }

    public Design width(Long width) {
        this.width = width;
        return this;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return this.height;
    }

    public Design height(Long height) {
        this.height = height;
        return this;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public String getNote() {
        return this.note;
    }

    public Design note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public Design images(Set<Image> images) {
        this.setImages(images);
        return this;
    }

    public Design addImages(Image image) {
        this.images.add(image);
        image.getDesigns().add(this);
        return this;
    }

    public Design removeImages(Image image) {
        this.images.remove(image);
        image.getDesigns().remove(this);
        return this;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public Set<Product> getProducts() {
        return this.products;
    }

    public Design products(Set<Product> products) {
        this.setProducts(products);
        return this;
    }

    public String getDesignFile() {
        return designFile;
    }

    public void setDesignFile(String designFile) {
        this.designFile = designFile;
    }

    public Design addProducts(Product product) {
        this.products.add(product);
        return this;
    }

    public Design removeProducts(Product product) {
        this.products.remove(product);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Set<Topic> getTopics() {
        return this.topics;
    }

    public Design topics(Set<Topic> topics) {
        this.setTopics(topics);
        return this;
    }

    public Design addTopics(Topic topic) {
        this.topics.add(topic);
        return this;
    }

    public Design removeTopics(Topic topic) {
        this.topics.remove(topic);
        return this;
    }

    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

    public Projct getOf_prjct() {
        return this.of_prjct;
    }

    public Design of_prjct(Projct projct) {
        this.setOf_prjct(projct);
        return this;
    }

    public void setOf_prjct(Projct projct) {
        this.of_prjct = projct;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Design)) {
            return false;
        }
        return id != null && id.equals(((Design) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Design{" +
            "id=" + getId() +
            ", file='" + getFile() + "'" +
            ", fileContentType='" + getFileContentType() + "'" +
            ", imgQuickView='" + getImgQuickView() + "'" +
            ", imgQuickViewContentType='" + getImgQuickViewContentType() + "'" +
            ", quickView='" + getQuickView() + "'" +
            ", width=" + getWidth() +
            ", height=" + getHeight() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
