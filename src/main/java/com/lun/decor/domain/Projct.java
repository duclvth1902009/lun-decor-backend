package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Projct.
 */
@Document(collection = "projct")
public class Projct extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //    @Field("file")
    //    private byte[] file;
    //
    //    @Field("file_content_type")
    //    private String fileContentType;

    @Field("hold_time")
    private ZonedDateTime holdTime;

    @Field("location")
    private String location;

    @Field("contract")
    private byte[] contract;

    @Field("contract_content_type")
    private String contractContentType;

    @Field("rate")
    private Double rate;

    @Field("pre_capital")
    private Long preCapital;

    @Field("pre_total")
    private Long preTotal;

    @Field("fin_capital")
    private Long finCapital;

    @Field("fin_total")
    private Long finTotal;

    @Field("rschange_cap")
    private String rschangeCap;

    @Field("rschange_tot")
    private String rschangeTot;

    @Field("status")
    private String status;

    @Field("name")
    private String name;

    @Field("progress")
    private Double progress;

    @DBRef
    @Field("topics")
    @JsonIgnoreProperties(value = { "images", "type" }, allowSetters = true)
    private Set<Topic> topics = new HashSet<>();

    @DBRef
    @Field("type")
    private EventType type;

    @DBRef
    @Field("eventVenue")
    @JsonIgnoreProperties(value = { "images" }, allowSetters = true)
    private EventVenue eventVenue;

    @DBRef
    @Field("requestOrder")
    private RequestOrder requestOrder;

    @DBRef
    @Field("customer")
    private Customer customer;

    @DBRef
    @Field("packs")
    @JsonIgnoreProperties(value = { "images", "products", "services", "type" }, allowSetters = true)
    private Set<Pack> packs = new HashSet<>();

    @DBRef
    @Field("final_design")
    private Design final_design;

    @DBRef
    @Field("designs")
    @JsonIgnoreProperties(value = { "images", "products", "topics", "of_prjct" }, allowSetters = true)
    private Set<Design> designs = new HashSet<>();

    @DBRef
    @Field("comments")
    @JsonIgnoreProperties(value = { "from", "parent", "projct" }, allowSetters = true)
    private Set<Comment> comments = new HashSet<>();

    @DBRef
    @Field("images")
    @JsonIgnoreProperties(
        value = { "eventVenue", "items", "products", "servicePkgs", "packs", "designs", "orders", "projct", "trackProgress", "task" },
        allowSetters = true
    )
    private Set<Image> images = new HashSet<>();

    @DBRef
    @Field("stages")
    @JsonIgnoreProperties(value = { "projct", "tasks", "images" }, allowSetters = true)
    private Set<TrackProgress> stages = new HashSet<>();

    @DBRef
    @Field("tasks")
    @JsonIgnoreProperties(value = { "projct", "stage", "images" }, allowSetters = true)
    private Set<Task> tasks = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Projct id(String id) {
        this.id = id;
        return this;
    }

    //    public byte[] getFile() {
    //        return this.file;
    //    }
    //
    //    public Projct file(byte[] file) {
    //        this.file = file;
    //        return this;
    //    }
    //
    //    public void setFile(byte[] file) {
    //        this.file = file;
    //    }
    //
    //    public String getFileContentType() {
    //        return this.fileContentType;
    //    }
    //
    //    public Projct fileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //        return this;
    //    }
    //
    //    public void setFileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //    }

    public ZonedDateTime getHoldTime() {
        return this.holdTime;
    }

    public Projct holdTime(ZonedDateTime holdTime) {
        this.holdTime = holdTime;
        return this;
    }

    public void setHoldTime(ZonedDateTime holdTime) {
        this.holdTime = holdTime;
    }

    public String getLocation() {
        return this.location;
    }

    public Projct location(String location) {
        this.location = location;
        return this;
    }

    public Double getProgress() {
        return progress;
    }

    public void setProgress(Double progress) {
        this.progress = progress;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public byte[] getContract() {
        return this.contract;
    }

    public Projct contract(byte[] contract) {
        this.contract = contract;
        return this;
    }

    public void setContract(byte[] contract) {
        this.contract = contract;
    }

    public String getContractContentType() {
        return this.contractContentType;
    }

    public Projct contractContentType(String contractContentType) {
        this.contractContentType = contractContentType;
        return this;
    }

    public void setContractContentType(String contractContentType) {
        this.contractContentType = contractContentType;
    }

    public Double getRate() {
        return this.rate;
    }

    public Projct rate(Double rate) {
        this.rate = rate;
        return this;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Long getPreCapital() {
        return this.preCapital;
    }

    public Projct preCapital(Long preCapital) {
        this.preCapital = preCapital;
        return this;
    }

    public void setPreCapital(Long preCapital) {
        this.preCapital = preCapital;
    }

    public Long getPreTotal() {
        return this.preTotal;
    }

    public Projct preTotal(Long preTotal) {
        this.preTotal = preTotal;
        return this;
    }

    public void setPreTotal(Long preTotal) {
        this.preTotal = preTotal;
    }

    public Long getFinCapital() {
        return this.finCapital;
    }

    public Projct finCapital(Long finCapital) {
        this.finCapital = finCapital;
        return this;
    }

    public void setFinCapital(Long finCapital) {
        this.finCapital = finCapital;
    }

    public Long getFinTotal() {
        return this.finTotal;
    }

    public Projct finTotal(Long finTotal) {
        this.finTotal = finTotal;
        return this;
    }

    public void setFinTotal(Long finTotal) {
        this.finTotal = finTotal;
    }

    public String getRschangeCap() {
        return this.rschangeCap;
    }

    public Projct rschangeCap(String rschangeCap) {
        this.rschangeCap = rschangeCap;
        return this;
    }

    public void setRschangeCap(String rschangeCap) {
        this.rschangeCap = rschangeCap;
    }

    public String getRschangeTot() {
        return this.rschangeTot;
    }

    public Projct rschangeTot(String rschangeTot) {
        this.rschangeTot = rschangeTot;
        return this;
    }

    public void setRschangeTot(String rschangeTot) {
        this.rschangeTot = rschangeTot;
    }

    public String getStatus() {
        return this.status;
    }

    public Projct status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return this.name;
    }

    public Projct name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Topic> getTopics() {
        return this.topics;
    }

    public Projct topics(Set<Topic> topics) {
        this.setTopics(topics);
        return this;
    }

    public Projct addTopics(Topic topic) {
        this.topics.add(topic);
        return this;
    }

    public Projct removeTopics(Topic topic) {
        this.topics.remove(topic);
        return this;
    }

    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

    public EventType getType() {
        return this.type;
    }

    public Projct type(EventType eventType) {
        this.setType(eventType);
        return this;
    }

    public void setType(EventType eventType) {
        this.type = eventType;
    }

    public EventVenue getEventVenue() {
        return this.eventVenue;
    }

    public Projct eventVenue(EventVenue eventVenue) {
        this.setEventVenue(eventVenue);
        return this;
    }

    public void setEventVenue(EventVenue eventVenue) {
        this.eventVenue = eventVenue;
    }

    public RequestOrder getRequestOrder() {
        return this.requestOrder;
    }

    public Projct requestOrder(RequestOrder requestOrder) {
        this.setRequestOrder(requestOrder);
        return this;
    }

    public void setRequestOrder(RequestOrder requestOrder) {
        this.requestOrder = requestOrder;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public Projct customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<Pack> getPacks() {
        return this.packs;
    }

    public Projct packs(Set<Pack> packs) {
        this.setPacks(packs);
        return this;
    }

    public Projct addPacks(Pack pack) {
        this.packs.add(pack);
        return this;
    }

    public Projct removePacks(Pack pack) {
        this.packs.remove(pack);
        return this;
    }

    public void setPacks(Set<Pack> packs) {
        this.packs = packs;
    }

    public Design getFinal_design() {
        return this.final_design;
    }

    public Projct final_design(Design design) {
        this.setFinal_design(design);
        return this;
    }

    public void setFinal_design(Design design) {
        this.final_design = design;
    }

    public Set<Design> getDesigns() {
        return this.designs;
    }

    public Projct designs(Set<Design> designs) {
        this.setDesigns(designs);
        return this;
    }

    public Projct addDesigns(Design design) {
        this.designs.add(design);
        design.setOf_prjct(this);
        return this;
    }

    public Projct removeDesigns(Design design) {
        this.designs.remove(design);
        design.setOf_prjct(null);
        return this;
    }

    public void setDesigns(Set<Design> designs) {
        if (this.designs != null) {
            this.designs.forEach(i -> i.setOf_prjct(null));
        }
        if (designs != null) {
            designs.forEach(i -> i.setOf_prjct(this));
        }
        this.designs = designs;
    }

    public Set<Comment> getComments() {
        return this.comments;
    }

    public Projct comments(Set<Comment> comments) {
        this.setComments(comments);
        return this;
    }

    public Projct addComments(Comment comment) {
        this.comments.add(comment);
        comment.setProjct(this);
        return this;
    }

    public Projct removeComments(Comment comment) {
        this.comments.remove(comment);
        comment.setProjct(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        if (this.comments != null) {
            this.comments.forEach(i -> i.setProjct(null));
        }
        if (comments != null) {
            comments.forEach(i -> i.setProjct(this));
        }
        this.comments = comments;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public Projct images(Set<Image> images) {
        this.setImages(images);
        return this;
    }

    public Projct addImages(Image image) {
        this.images.add(image);
        image.setProjct(this);
        return this;
    }

    public Projct removeImages(Image image) {
        this.images.remove(image);
        image.setProjct(null);
        return this;
    }

    public void setImages(Set<Image> images) {
        if (this.images != null) {
            this.images.forEach(i -> i.setProjct(null));
        }
        if (images != null) {
            images.forEach(i -> i.setProjct(this));
        }
        this.images = images;
    }

    public Set<TrackProgress> getStages() {
        return this.stages;
    }

    public Projct stages(Set<TrackProgress> trackProgresses) {
        this.setStages(trackProgresses);
        return this;
    }

    public Projct addStages(TrackProgress trackProgress) {
        this.stages.add(trackProgress);
        trackProgress.setProjct(this);
        return this;
    }

    public Projct removeStages(TrackProgress trackProgress) {
        this.stages.remove(trackProgress);
        trackProgress.setProjct(null);
        return this;
    }

    public void setStages(Set<TrackProgress> trackProgresses) {
        if (this.stages != null) {
            this.stages.forEach(i -> i.setProjct(null));
        }
        if (trackProgresses != null) {
            trackProgresses.forEach(i -> i.setProjct(this));
        }
        this.stages = trackProgresses;
    }

    public Set<Task> getTasks() {
        return this.tasks;
    }

    public Projct tasks(Set<Task> tasks) {
        this.setTasks(tasks);
        return this;
    }

    public Projct addTasks(Task task) {
        this.tasks.add(task);
        task.setProjct(this);
        return this;
    }

    public Projct removeTasks(Task task) {
        this.tasks.remove(task);
        task.setProjct(null);
        return this;
    }

    public void setTasks(Set<Task> tasks) {
        if (this.tasks != null) {
            this.tasks.forEach(i -> i.setProjct(null));
        }
        if (tasks != null) {
            tasks.forEach(i -> i.setProjct(this));
        }
        this.tasks = tasks;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Projct)) {
            return false;
        }
        return id != null && id.equals(((Projct) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Projct{" +
            "id=" + getId() +
//            ", file='" + getFile() + "'" +
//            ", fileContentType='" + getFileContentType() + "'" +
            ", holdTime='" + getHoldTime() + "'" +
            ", location='" + getLocation() + "'" +
            ", contract='" + getContract() + "'" +
            ", contractContentType='" + getContractContentType() + "'" +
            ", rate=" + getRate() +
            ", preCapital=" + getPreCapital() +
            ", preTotal=" + getPreTotal() +
            ", finCapital=" + getFinCapital() +
            ", finTotal=" + getFinTotal() +
            ", rschangeCap='" + getRschangeCap() + "'" +
            ", rschangeTot='" + getRschangeTot() + "'" +
            ", status='" + getStatus() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
