package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Task.
 */
@Document(collection = "task")
public class Task extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //    @Field("file")
    //    private byte[] file;
    //
    //    @Field("file_content_type")
    //    private String fileContentType;

    @Field("name")
    private String name;

    @Field("detail")
    private String detail;

    @Field("total")
    private Long total;

    @Field("note")
    private String note;

    @Field("scope")
    private String scope;

    @Field("status")
    private String status;

    @Field("start_time")
    private ZonedDateTime startTime;

    @Field("dead_line")
    private ZonedDateTime deadLine;

    @DBRef
    @Field("projct")
    @JsonIgnoreProperties(
        value = {
            "topics",
            "type",
            "eventVenue",
            "requestOrder",
            "customer",
            "packs",
            "final_design",
            "designs",
            "comments",
            "images",
            "stages",
            "tasks",
        },
        allowSetters = true
    )
    private Projct projct;

    @DBRef
    @Field("stage")
    @JsonIgnoreProperties(value = { "projct", "tasks", "images" }, allowSetters = true)
    private TrackProgress stage;

    @DBRef
    @Field("images")
    @JsonIgnoreProperties(
        value = { "eventVenue", "items", "products", "servicePkgs", "packs", "designs", "orders", "projct", "trackProgress", "task" },
        allowSetters = true
    )
    private Set<Image> images = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Task id(String id) {
        this.id = id;
        return this;
    }

    //    public byte[] getFile() {
    //        return this.file;
    //    }
    //
    //    public Task file(byte[] file) {
    //        this.file = file;
    //        return this;
    //    }
    //
    //    public void setFile(byte[] file) {
    //        this.file = file;
    //    }
    //
    //    public String getFileContentType() {
    //        return this.fileContentType;
    //    }
    //
    //    public Task fileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //        return this;
    //    }
    //
    //    public void setFileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //    }

    public String getName() {
        return this.name;
    }

    public Task name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return this.detail;
    }

    public Task detail(String detail) {
        this.detail = detail;
        return this;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Long getTotal() {
        return this.total;
    }

    public Task total(Long total) {
        this.total = total;
        return this;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getNote() {
        return this.note;
    }

    public Task note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getScope() {
        return this.scope;
    }

    public Task scope(String scope) {
        this.scope = scope;
        return this;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getStatus() {
        return this.status;
    }

    public Task status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ZonedDateTime getStartTime() {
        return this.startTime;
    }

    public Task startTime(ZonedDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getDeadLine() {
        return this.deadLine;
    }

    public Task deadLine(ZonedDateTime deadLine) {
        this.deadLine = deadLine;
        return this;
    }

    public void setDeadLine(ZonedDateTime deadLine) {
        this.deadLine = deadLine;
    }

    public Projct getProjct() {
        return this.projct;
    }

    public Task projct(Projct projct) {
        this.setProjct(projct);
        return this;
    }

    public void setProjct(Projct projct) {
        this.projct = projct;
    }

    public TrackProgress getStage() {
        return this.stage;
    }

    public Task stage(TrackProgress trackProgress) {
        this.setStage(trackProgress);
        return this;
    }

    public void setStage(TrackProgress trackProgress) {
        this.stage = trackProgress;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public Task images(Set<Image> images) {
        this.setImages(images);
        return this;
    }

    public Task addImages(Image image) {
        this.images.add(image);
        image.setTask(this);
        return this;
    }

    public Task removeImages(Image image) {
        this.images.remove(image);
        image.setTask(null);
        return this;
    }

    public void setImages(Set<Image> images) {
        if (this.images != null) {
            this.images.forEach(i -> i.setTask(null));
        }
        if (images != null) {
            images.forEach(i -> i.setTask(this));
        }
        this.images = images;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Task)) {
            return false;
        }
        return id != null && id.equals(((Task) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Task{" +
            "id=" + getId() +
//            ", file='" + getFile() + "'" +
//            ", fileContentType='" + getFileContentType() + "'" +
            ", name='" + getName() + "'" +
            ", detail='" + getDetail() + "'" +
            ", total=" + getTotal() +
            ", note='" + getNote() + "'" +
            ", scope='" + getScope() + "'" +
            ", status='" + getStatus() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", deadLine='" + getDeadLine() + "'" +
            "}";
    }
}
