package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Topic.
 */
@Document(collection = "topic")
public class Topic extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //    @Field("file")
    //    private byte[] file;
    //
    //    @Field("file_content_type")
    //    private String fileContentType;

    @Field("name")
    private String name;

    @Field("tags")
    private String tags;

    @DBRef
    @Field("images")
    @JsonIgnoreProperties(
        value = { "eventVenue", "items", "products", "servicePkgs", "packs", "designs", "orders", "projct", "trackProgress", "task" },
        allowSetters = true
    )
    private Set<Image> images = new HashSet<>();

    @DBRef
    @Field("type")
    private EventType type;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Topic id(String id) {
        this.id = id;
        return this;
    }

    //    public byte[] getFile() {
    //        return this.file;
    //    }
    //
    //    public Topic file(byte[] file) {
    //        this.file = file;
    //        return this;
    //    }
    //
    //    public void setFile(byte[] file) {
    //        this.file = file;
    //    }
    //
    //    public String getFileContentType() {
    //        return this.fileContentType;
    //    }
    //
    //    public Topic fileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //        return this;
    //    }
    //
    //    public void setFileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //    }

    public String getName() {
        return this.name;
    }

    public Topic name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTags() {
        return this.tags;
    }

    public Topic tags(String tags) {
        this.tags = tags;
        return this;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public Topic images(Set<Image> images) {
        this.setImages(images);
        return this;
    }

    public Topic addImages(Image image) {
        this.images.add(image);
        return this;
    }

    public Topic removeImages(Image image) {
        this.images.remove(image);
        return this;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public EventType getType() {
        return this.type;
    }

    public Topic type(EventType eventType) {
        this.setType(eventType);
        return this;
    }

    public void setType(EventType eventType) {
        this.type = eventType;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Topic)) {
            return false;
        }
        return id != null && id.equals(((Topic) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Topic{" +
            "id=" + getId() +
//            ", file='" + getFile() + "'" +
//            ", fileContentType='" + getFileContentType() + "'" +
            ", name='" + getName() + "'" +
            ", tags='" + getTags() + "'" +
            "}";
    }
}
