package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Comment.
 */
@Document(collection = "comment")
public class Comment extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("content")
    private String content;

    @Field("parent_id")
    private String parent_id;

    @DBRef
    @Field("from")
    private Customer from;

    @DBRef
    @Field("parent")
    @JsonIgnoreProperties(value = { "from", "parent", "projct" }, allowSetters = true)
    private Comment parent;

    @DBRef
    @Field("projct")
    @JsonIgnoreProperties(
        value = {
            "topics",
            "type",
            "eventVenue",
            "requestOrder",
            "customer",
            "packs",
            "final_design",
            "designs",
            "comments",
            "images",
            "stages",
            "tasks",
        },
        allowSetters = true
    )
    private Projct projct;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Comment id(String id) {
        this.id = id;
        return this;
    }

    public String getContent() {
        return this.content;
    }

    public Comment content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getParent_id() {
        return this.parent_id;
    }

    public Comment parent_id(String parent_id) {
        this.parent_id = parent_id;
        return this;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public Customer getFrom() {
        return this.from;
    }

    public Comment from(Customer customer) {
        this.setFrom(customer);
        return this;
    }

    public void setFrom(Customer customer) {
        this.from = customer;
    }

    public Comment getParent() {
        return this.parent;
    }

    public Comment parent(Comment comment) {
        this.setParent(comment);
        return this;
    }

    public void setParent(Comment comment) {
        this.parent = comment;
    }

    public Projct getProjct() {
        return this.projct;
    }

    public Comment projct(Projct projct) {
        this.setProjct(projct);
        return this;
    }

    public void setProjct(Projct projct) {
        this.projct = projct;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Comment)) {
            return false;
        }
        return id != null && id.equals(((Comment) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Comment{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", parent_id='" + getParent_id() + "'" +
            "}";
    }
}
