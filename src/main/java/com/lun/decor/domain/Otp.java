package com.lun.decor.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Otp.
 */
@Document(collection = "otp")
public class Otp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("otp_code")
    private String otp_code;

    @Field("expired_time")
    private ZonedDateTime expired_time;

    @Field("status")
    private Long status;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Otp id(String id) {
        this.id = id;
        return this;
    }

    public String getOtp_code() {
        return this.otp_code;
    }

    public Otp otp_code(String otp_code) {
        this.otp_code = otp_code;
        return this;
    }

    public void setOtp_code(String otp_code) {
        this.otp_code = otp_code;
    }

    public ZonedDateTime getExpired_time() {
        return this.expired_time;
    }

    public Otp expired_time(ZonedDateTime expired_time) {
        this.expired_time = expired_time;
        return this;
    }

    public void setExpired_time(ZonedDateTime expired_time) {
        this.expired_time = expired_time;
    }

    public Long getStatus() {
        return this.status;
    }

    public Otp status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Otp)) {
            return false;
        }
        return id != null && id.equals(((Otp) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Otp{" +
            "id=" + getId() +
            ", otp_code='" + getOtp_code() + "'" +
            ", expired_time='" + getExpired_time() + "'" +
            ", status=" + getStatus() +
            "}";
    }

    public Otp() {}

    public Otp(String otp_code) {
        this.setStatus(1L);
        this.setOtp_code(otp_code);
        ZonedDateTime expiredDate = ZonedDateTime.now();
        this.setExpired_time(expiredDate.plusMinutes(10));
    }
}
