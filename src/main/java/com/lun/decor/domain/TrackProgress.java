package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A TrackProgress.
 */
@Document(collection = "track_progress")
public class TrackProgress extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //    @Field("file")
    //    private byte[] file;
    //
    //    @Field("file_content_type")
    //    private String fileContentType;

    @Field("name")
    private String name;

    @Field("description")
    private String description;

    @Field("client_enable")
    private Boolean clientEnable;

    @Field("step")
    private Integer step;

    @Field("dead_line")
    private ZonedDateTime deadLine;

    @Field("status")
    private String status;

    @DBRef
    @Field("projct")
    @JsonIgnoreProperties(
        value = {
            "topics",
            "type",
            "eventVenue",
            "requestOrder",
            "customer",
            "packs",
            "final_design",
            "designs",
            "comments",
            "images",
            "stages",
            "tasks",
        },
        allowSetters = true
    )
    private Projct projct;

    @DBRef
    @Field("tasks")
    @JsonIgnoreProperties(value = { "projct", "stage", "images" }, allowSetters = true)
    private Set<Task> tasks = new HashSet<>();

    @DBRef
    @Field("images")
    @JsonIgnoreProperties(
        value = { "eventVenue", "items", "products", "servicePkgs", "packs", "designs", "orders", "projct", "trackProgress", "task" },
        allowSetters = true
    )
    private Set<Image> images = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TrackProgress id(String id) {
        this.id = id;
        return this;
    }

    //    public byte[] getFile() {
    //        return this.file;
    //    }
    //
    //    public TrackProgress file(byte[] file) {
    //        this.file = file;
    //        return this;
    //    }
    //
    //    public void setFile(byte[] file) {
    //        this.file = file;
    //    }
    //
    //    public String getFileContentType() {
    //        return this.fileContentType;
    //    }
    //
    //    public TrackProgress fileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //        return this;
    //    }
    //
    //    public void setFileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //    }

    public String getName() {
        return this.name;
    }

    public TrackProgress name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public TrackProgress description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getClientEnable() {
        return this.clientEnable;
    }

    public TrackProgress clientEnable(Boolean clientEnable) {
        this.clientEnable = clientEnable;
        return this;
    }

    public void setClientEnable(Boolean clientEnable) {
        this.clientEnable = clientEnable;
    }

    public Integer getStep() {
        return this.step;
    }

    public TrackProgress step(Integer step) {
        this.step = step;
        return this;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public ZonedDateTime getDeadLine() {
        return this.deadLine;
    }

    public TrackProgress deadLine(ZonedDateTime deadLine) {
        this.deadLine = deadLine;
        return this;
    }

    public void setDeadLine(ZonedDateTime deadLine) {
        this.deadLine = deadLine;
    }

    public String getStatus() {
        return this.status;
    }

    public TrackProgress status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Projct getProjct() {
        return this.projct;
    }

    public TrackProgress projct(Projct projct) {
        this.setProjct(projct);
        return this;
    }

    public void setProjct(Projct projct) {
        this.projct = projct;
    }

    public Set<Task> getTasks() {
        return this.tasks;
    }

    public TrackProgress tasks(Set<Task> tasks) {
        this.setTasks(tasks);
        return this;
    }

    public TrackProgress addTasks(Task task) {
        this.tasks.add(task);
        task.setStage(this);
        return this;
    }

    public TrackProgress removeTasks(Task task) {
        this.tasks.remove(task);
        task.setStage(null);
        return this;
    }

    public void setTasks(Set<Task> tasks) {
        if (this.tasks != null) {
            this.tasks.forEach(i -> i.setStage(null));
        }
        if (tasks != null) {
            tasks.forEach(i -> i.setStage(this));
        }
        this.tasks = tasks;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public TrackProgress images(Set<Image> images) {
        this.setImages(images);
        return this;
    }

    public TrackProgress addImages(Image image) {
        this.images.add(image);
        image.setTrackProgress(this);
        return this;
    }

    public TrackProgress removeImages(Image image) {
        this.images.remove(image);
        image.setTrackProgress(null);
        return this;
    }

    public void setImages(Set<Image> images) {
        if (this.images != null) {
            this.images.forEach(i -> i.setTrackProgress(null));
        }
        if (images != null) {
            images.forEach(i -> i.setTrackProgress(this));
        }
        this.images = images;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrackProgress)) {
            return false;
        }
        return id != null && id.equals(((TrackProgress) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TrackProgress{" +
            "id=" + getId() +
//            ", file='" + getFile() + "'" +
//            ", fileContentType='" + getFileContentType() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", clientEnable='" + getClientEnable() + "'" +
            ", step=" + getStep() +
            ", deadLine='" + getDeadLine() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
