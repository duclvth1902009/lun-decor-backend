package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Transactn.
 */
@Document(collection = "transactn")
public class Transactn extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("file")
    private byte[] file;

    @Field("file_content_type")
    private String fileContentType;

    @Field("detail")
    private String detail;

    @Field("total")
    private Long total;

    @Field("note")
    private String note;

    @Field("status")
    private String status;

    @DBRef
    @Field("projct")
    @JsonIgnoreProperties(
        value = {
            "topics",
            "type",
            "eventVenue",
            "requestOrder",
            "customer",
            "packs",
            "final_design",
            "designs",
            "comments",
            "images",
            "stages",
            "tasks",
        },
        allowSetters = true
    )
    private Projct projct;

    @DBRef
    @Field("stage")
    @JsonIgnoreProperties(value = { "projct", "tasks", "images" }, allowSetters = true)
    private TrackProgress stage;

    @DBRef
    @Field("atTask")
    @JsonIgnoreProperties(value = { "projct", "stage", "images" }, allowSetters = true)
    private Task atTask;

    @DBRef
    @Field("provider")
    private Partner provider;

    @DBRef
    @Field("customer")
    private Customer customer;

    @DBRef
    @Field("image")
    private Image image;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Transactn id(String id) {
        this.id = id;
        return this;
    }

    public byte[] getFile() {
        return this.file;
    }

    public Transactn file(byte[] file) {
        this.file = file;
        return this;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return this.fileContentType;
    }

    public Transactn fileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
        return this;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getDetail() {
        return this.detail;
    }

    public Transactn detail(String detail) {
        this.detail = detail;
        return this;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Long getTotal() {
        return this.total;
    }

    public Transactn total(Long total) {
        this.total = total;
        return this;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getNote() {
        return this.note;
    }

    public Transactn note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return this.status;
    }

    public Transactn status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Projct getProjct() {
        return this.projct;
    }

    public Transactn projct(Projct projct) {
        this.setProjct(projct);
        return this;
    }

    public void setProjct(Projct projct) {
        this.projct = projct;
    }

    public TrackProgress getStage() {
        return this.stage;
    }

    public Transactn stage(TrackProgress trackProgress) {
        this.setStage(trackProgress);
        return this;
    }

    public void setStage(TrackProgress trackProgress) {
        this.stage = trackProgress;
    }

    public Task getAtTask() {
        return this.atTask;
    }

    public Transactn atTask(Task task) {
        this.setAtTask(task);
        return this;
    }

    public void setAtTask(Task task) {
        this.atTask = task;
    }

    public Partner getProvider() {
        return this.provider;
    }

    public Transactn provider(Partner partner) {
        this.setProvider(partner);
        return this;
    }

    public void setProvider(Partner partner) {
        this.provider = partner;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public Transactn customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Image getImage() {
        return this.image;
    }

    public Transactn image(Image image) {
        this.setImage(image);
        return this;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transactn)) {
            return false;
        }
        return id != null && id.equals(((Transactn) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Transactn{" +
            "id=" + getId() +
            ", file='" + getFile() + "'" +
            ", fileContentType='" + getFileContentType() + "'" +
            ", detail='" + getDetail() + "'" +
            ", total=" + getTotal() +
            ", note='" + getNote() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
