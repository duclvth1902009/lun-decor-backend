package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Image.
 */
@Document(collection = "image")
public class Image extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //    @Field("file")
    //    private byte[] file;
    //
    //    @Field("file_content_type")
    //    private String fileContentType;

    @Field("url")
    private String url;

    @Field("type")
    private String type;

    @Field("feature")
    private String feature;

    @Field("note")
    private String note;

    @DBRef
    @Field("eventVenue")
    @JsonIgnoreProperties(value = { "images" }, allowSetters = true)
    private EventVenue eventVenue;

    @DBRef
    @Field("items")
    @JsonIgnoreProperties(value = { "images", "providers" }, allowSetters = true)
    private Set<Item> items = new HashSet<>();

    @DBRef
    @Field("products")
    @JsonIgnoreProperties(value = { "images", "items" }, allowSetters = true)
    private Set<Product> products = new HashSet<>();

    @DBRef
    @Field("servicePkgs")
    @JsonIgnoreProperties(value = { "images", "providers" }, allowSetters = true)
    private Set<ServicePkg> servicePkgs = new HashSet<>();

    @DBRef
    @Field("packs")
    @JsonIgnoreProperties(value = { "images", "products", "services", "type" }, allowSetters = true)
    private Set<Pack> packs = new HashSet<>();

    @DBRef
    @Field("designs")
    @JsonIgnoreProperties(value = { "images", "products", "topics", "of_prjct" }, allowSetters = true)
    private Set<Design> designs = new HashSet<>();

    @DBRef
    @Field("orders")
    @JsonIgnoreProperties(
        value = { "pack", "images", "topics", "add_products", "sub_products", "add_services", "sub_services", "type", "eventVenue" },
        allowSetters = true
    )
    private Set<RequestOrder> orders = new HashSet<>();

    @DBRef
    @Field("projct")
    @JsonIgnoreProperties(
        value = {
            "topics",
            "type",
            "eventVenue",
            "requestOrder",
            "customer",
            "packs",
            "final_design",
            "designs",
            "comments",
            "images",
            "stages",
            "tasks",
        },
        allowSetters = true
    )
    private Projct projct;

    @DBRef
    @Field("trackProgress")
    @JsonIgnoreProperties(value = { "projct", "tasks", "images" }, allowSetters = true)
    private TrackProgress trackProgress;

    @DBRef
    @Field("task")
    @JsonIgnoreProperties(value = { "projct", "stage", "images" }, allowSetters = true)
    private Task task;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Image id(String id) {
        this.id = id;
        return this;
    }

    //    public byte[] getFile() {
    //        return this.file;
    //    }
    //
    //    public Image file(byte[] file) {
    //        this.file = file;
    //        return this;
    //    }
    //
    //    public void setFile(byte[] file) {
    //        this.file = file;
    //    }
    //
    //    public String getFileContentType() {
    //        return this.fileContentType;
    //    }
    //
    //    public Image fileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //        return this;
    //    }
    //
    //    public void setFileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //    }

    public String getUrl() {
        return this.url;
    }

    public Image url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return this.type;
    }

    public Image type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFeature() {
        return this.feature;
    }

    public Image feature(String feature) {
        this.feature = feature;
        return this;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getNote() {
        return this.note;
    }

    public Image note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public EventVenue getEventVenue() {
        return this.eventVenue;
    }

    public Image eventVenue(EventVenue eventVenue) {
        this.setEventVenue(eventVenue);
        return this;
    }

    public void setEventVenue(EventVenue eventVenue) {
        this.eventVenue = eventVenue;
    }

    public Set<Item> getItems() {
        return this.items;
    }

    public Image items(Set<Item> items) {
        this.setItems(items);
        return this;
    }

    public Image addItems(Item item) {
        this.items.add(item);
        item.getImages().add(this);
        return this;
    }

    public Image removeItems(Item item) {
        this.items.remove(item);
        item.getImages().remove(this);
        return this;
    }

    public void setItems(Set<Item> items) {
        if (this.items != null) {
            this.items.forEach(i -> i.removeImages(this));
        }
        if (items != null) {
            items.forEach(i -> i.addImages(this));
        }
        this.items = items;
    }

    public Set<Product> getProducts() {
        return this.products;
    }

    public Image products(Set<Product> products) {
        this.setProducts(products);
        return this;
    }

    public Image addProducts(Product product) {
        this.products.add(product);
        product.getImages().add(this);
        return this;
    }

    public Image removeProducts(Product product) {
        this.products.remove(product);
        product.getImages().remove(this);
        return this;
    }

    public void setProducts(Set<Product> products) {
        if (this.products != null) {
            this.products.forEach(i -> i.removeImages(this));
        }
        if (products != null) {
            products.forEach(i -> i.addImages(this));
        }
        this.products = products;
    }

    public Set<ServicePkg> getServicePkgs() {
        return this.servicePkgs;
    }

    public Image servicePkgs(Set<ServicePkg> servicePkgs) {
        this.setServicePkgs(servicePkgs);
        return this;
    }

    public Image addServicePkgs(ServicePkg servicePkg) {
        this.servicePkgs.add(servicePkg);
        servicePkg.getImages().add(this);
        return this;
    }

    public Image removeServicePkgs(ServicePkg servicePkg) {
        this.servicePkgs.remove(servicePkg);
        servicePkg.getImages().remove(this);
        return this;
    }

    public void setServicePkgs(Set<ServicePkg> servicePkgs) {
        if (this.servicePkgs != null) {
            this.servicePkgs.forEach(i -> i.removeImages(this));
        }
        if (servicePkgs != null) {
            servicePkgs.forEach(i -> i.addImages(this));
        }
        this.servicePkgs = servicePkgs;
    }

    public Set<Pack> getPacks() {
        return this.packs;
    }

    public Image packs(Set<Pack> packs) {
        this.setPacks(packs);
        return this;
    }

    public Image addPacks(Pack pack) {
        this.packs.add(pack);
        pack.getImages().add(this);
        return this;
    }

    public Image removePacks(Pack pack) {
        this.packs.remove(pack);
        pack.getImages().remove(this);
        return this;
    }

    public void setPacks(Set<Pack> packs) {
        if (this.packs != null) {
            this.packs.forEach(i -> i.removeImages(this));
        }
        if (packs != null) {
            packs.forEach(i -> i.addImages(this));
        }
        this.packs = packs;
    }

    public Set<Design> getDesigns() {
        return this.designs;
    }

    public Image designs(Set<Design> designs) {
        this.setDesigns(designs);
        return this;
    }

    public Image addDesigns(Design design) {
        this.designs.add(design);
        design.getImages().add(this);
        return this;
    }

    public Image removeDesigns(Design design) {
        this.designs.remove(design);
        design.getImages().remove(this);
        return this;
    }

    public void setDesigns(Set<Design> designs) {
        if (this.designs != null) {
            this.designs.forEach(i -> i.removeImages(this));
        }
        if (designs != null) {
            designs.forEach(i -> i.addImages(this));
        }
        this.designs = designs;
    }

    public Set<RequestOrder> getOrders() {
        return this.orders;
    }

    public Image orders(Set<RequestOrder> requestOrders) {
        this.setOrders(requestOrders);
        return this;
    }

    public Image addOrders(RequestOrder requestOrder) {
        this.orders.add(requestOrder);
        requestOrder.getImages().add(this);
        return this;
    }

    public Image removeOrders(RequestOrder requestOrder) {
        this.orders.remove(requestOrder);
        requestOrder.getImages().remove(this);
        return this;
    }

    public void setOrders(Set<RequestOrder> requestOrders) {
        if (this.orders != null) {
            this.orders.forEach(i -> i.removeImages(this));
        }
        if (requestOrders != null) {
            requestOrders.forEach(i -> i.addImages(this));
        }
        this.orders = requestOrders;
    }

    public Projct getProjct() {
        return this.projct;
    }

    public Image projct(Projct projct) {
        this.setProjct(projct);
        return this;
    }

    public void setProjct(Projct projct) {
        this.projct = projct;
    }

    public TrackProgress getTrackProgress() {
        return this.trackProgress;
    }

    public Image trackProgress(TrackProgress trackProgress) {
        this.setTrackProgress(trackProgress);
        return this;
    }

    public void setTrackProgress(TrackProgress trackProgress) {
        this.trackProgress = trackProgress;
    }

    public Task getTask() {
        return this.task;
    }

    public Image task(Task task) {
        this.setTask(task);
        return this;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Image)) {
            return false;
        }
        return id != null && id.equals(((Image) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Image{" +
            "id=" + getId() +
//            ", file='" + getFile() + "'" +
//            ", fileContentType='" + getFileContentType() + "'" +
            ", url='" + getUrl() + "'" +
            ", type='" + getType() + "'" +
            ", feature='" + getFeature() + "'" +
            ", note='" + getNote() + "'" +
            "}";
    }
}
