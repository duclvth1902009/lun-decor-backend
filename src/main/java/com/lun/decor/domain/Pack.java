package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Pack.
 */
@Document(collection = "pack")
public class Pack extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //    @Field("file")
    //    private byte[] file;
    //
    //    @Field("file_content_type")
    //    private String fileContentType;

    @Field("name")
    private String name;

    @Field("price_from")
    private Long price_from;

    @Field("price_to")
    private Long price_to;

    @Field("description")
    private String description;

    @DBRef
    @Field("images")
    @JsonIgnoreProperties(
        value = { "eventVenue", "items", "products", "servicePkgs", "packs", "designs", "orders", "projct", "trackProgress", "task" },
        allowSetters = true
    )
    private Set<Image> images = new HashSet<>();

    @DBRef
    @Field("products")
    @JsonIgnoreProperties(value = { "images", "items" }, allowSetters = true)
    private Set<Product> products = new HashSet<>();

    @DBRef
    @Field("services")
    @JsonIgnoreProperties(value = { "images", "providers" }, allowSetters = true)
    private Set<ServicePkg> services = new HashSet<>();

    @DBRef
    @Field("type")
    private EventType type;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Pack id(String id) {
        this.id = id;
        return this;
    }

    //    public byte[] getFile() {
    //        return this.file;
    //    }
    //
    //    public Pack file(byte[] file) {
    //        this.file = file;
    //        return this;
    //    }
    //
    //    public void setFile(byte[] file) {
    //        this.file = file;
    //    }
    //
    //    public String getFileContentType() {
    //        return this.fileContentType;
    //    }
    //
    //    public Pack fileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //        return this;
    //    }
    //
    //    public void setFileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //    }

    public String getName() {
        return this.name;
    }

    public Pack name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice_from() {
        return this.price_from;
    }

    public Pack price_from(Long price_from) {
        this.price_from = price_from;
        return this;
    }

    public void setPrice_from(Long price_from) {
        this.price_from = price_from;
    }

    public Long getPrice_to() {
        return this.price_to;
    }

    public Pack price_to(Long price_to) {
        this.price_to = price_to;
        return this;
    }

    public void setPrice_to(Long price_to) {
        this.price_to = price_to;
    }

    public String getDescription() {
        return this.description;
    }

    public Pack description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public Pack images(Set<Image> images) {
        this.setImages(images);
        return this;
    }

    public Pack addImages(Image image) {
        this.images.add(image);
        image.getPacks().add(this);
        return this;
    }

    public Pack removeImages(Image image) {
        this.images.remove(image);
        image.getPacks().remove(this);
        return this;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public Set<Product> getProducts() {
        return this.products;
    }

    public Pack products(Set<Product> products) {
        this.setProducts(products);
        return this;
    }

    public Pack addProducts(Product product) {
        this.products.add(product);
        return this;
    }

    public Pack removeProducts(Product product) {
        this.products.remove(product);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Set<ServicePkg> getServices() {
        return this.services;
    }

    public Pack services(Set<ServicePkg> servicePkgs) {
        this.setServices(servicePkgs);
        return this;
    }

    public Pack addServices(ServicePkg servicePkg) {
        this.services.add(servicePkg);
        return this;
    }

    public Pack removeServices(ServicePkg servicePkg) {
        this.services.remove(servicePkg);
        return this;
    }

    public void setServices(Set<ServicePkg> servicePkgs) {
        this.services = servicePkgs;
    }

    public EventType getType() {
        return this.type;
    }

    public Pack type(EventType eventType) {
        this.setType(eventType);
        return this;
    }

    public void setType(EventType eventType) {
        this.type = eventType;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pack)) {
            return false;
        }
        return id != null && id.equals(((Pack) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pack{" +
            "id=" + getId() +
//            ", file='" + getFile() + "'" +
//            ", fileContentType='" + getFileContentType() + "'" +
            ", name='" + getName() + "'" +
            ", price_from=" + getPrice_from() +
            ", price_to=" + getPrice_to() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
