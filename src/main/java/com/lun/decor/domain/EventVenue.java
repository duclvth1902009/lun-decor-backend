package com.lun.decor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A EventVenue.
 */
@Document(collection = "event_venue")
public class EventVenue extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //    @Field("file")
    //    private byte[] file;
    //
    //    @Field("file_content_type")
    //    private String fileContentType;

    @Field("name")
    private String name;

    @Field("address")
    private String address;

    @Field("location")
    private String location;

    @Field("price_from")
    private Long price_from;

    @Field("price_to")
    private Long price_to;

    @NotNull
    @Pattern(regexp = "[0-9]+")
    @Field("phone")
    private String phone;

    @Field("email")
    private String email;

    @Field("description")
    private String description;

    @Field("quantity_max")
    private Long quantity_max;

    @Field("note")
    private String note;

    @DBRef
    @Field("images")
    @JsonIgnoreProperties(
        value = { "eventVenue", "items", "products", "servicePkgs", "packs", "designs", "orders", "projct", "trackProgress", "task" },
        allowSetters = true
    )
    private Set<Image> images = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public EventVenue id(String id) {
        this.id = id;
        return this;
    }

    //    public byte[] getFile() {
    //        return this.file;
    //    }
    //
    //    public EventVenue file(byte[] file) {
    //        this.file = file;
    //        return this;
    //    }
    //
    //    public void setFile(byte[] file) {
    //        this.file = file;
    //    }
    //
    //    public String getFileContentType() {
    //        return this.fileContentType;
    //    }
    //
    //    public EventVenue fileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //        return this;
    //    }
    //
    //    public void setFileContentType(String fileContentType) {
    //        this.fileContentType = fileContentType;
    //    }

    public String getName() {
        return this.name;
    }

    public EventVenue name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public EventVenue address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return this.location;
    }

    public EventVenue location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getPrice_from() {
        return this.price_from;
    }

    public EventVenue price_from(Long price_from) {
        this.price_from = price_from;
        return this;
    }

    public void setPrice_from(Long price_from) {
        this.price_from = price_from;
    }

    public Long getPrice_to() {
        return this.price_to;
    }

    public EventVenue price_to(Long price_to) {
        this.price_to = price_to;
        return this;
    }

    public void setPrice_to(Long price_to) {
        this.price_to = price_to;
    }

    public String getPhone() {
        return this.phone;
    }

    public EventVenue phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public EventVenue email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return this.description;
    }

    public EventVenue description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getQuantity_max() {
        return this.quantity_max;
    }

    public EventVenue quantity_max(Long quantity_max) {
        this.quantity_max = quantity_max;
        return this;
    }

    public void setQuantity_max(Long quantity_max) {
        this.quantity_max = quantity_max;
    }

    public String getNote() {
        return this.note;
    }

    public EventVenue note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public EventVenue images(Set<Image> images) {
        this.setImages(images);
        return this;
    }

    public EventVenue addImages(Image image) {
        this.images.add(image);
        image.setEventVenue(this);
        return this;
    }

    public EventVenue removeImages(Image image) {
        this.images.remove(image);
        image.setEventVenue(null);
        return this;
    }

    public void setImages(Set<Image> images) {
        if (this.images != null) {
            this.images.forEach(i -> i.setEventVenue(null));
        }
        if (images != null) {
            images.forEach(i -> i.setEventVenue(this));
        }
        this.images = images;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EventVenue)) {
            return false;
        }
        return id != null && id.equals(((EventVenue) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EventVenue{" +
            "id=" + getId() +
//            ", file='" + getFile() + "'" +
//            ", fileContentType='" + getFileContentType() + "'" +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", location='" + getLocation() + "'" +
            ", price_from=" + getPrice_from() +
            ", price_to=" + getPrice_to() +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", description='" + getDescription() + "'" +
            ", quantity_max=" + getQuantity_max() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
