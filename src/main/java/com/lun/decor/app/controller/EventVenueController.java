package com.lun.decor.app.controller;

import com.lun.decor.service.EventTypeService;
import com.lun.decor.service.EventVenueService;
import com.lun.decor.service.dto.EventTypeDTO;
import com.lun.decor.service.dto.EventVenueDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author duclai on 15/08/2021
 */
@RestController
@RequestMapping("api/app/eventVenue")
public class EventVenueController {

    @Autowired
    private EventVenueService eventVenueService;

    @GetMapping
    public ResponseEntity<List<EventVenueDTO>> getAllEventVenue() {
        Page<EventVenueDTO> results = eventVenueService.findAll(PageRequest.of(0, 100));
        return ResponseEntity.ok().body(results.getContent());
    }
}
