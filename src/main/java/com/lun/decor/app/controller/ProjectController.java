package com.lun.decor.app.controller;

import com.lun.decor.domain.Otp;
import com.lun.decor.repository.OtpRepository;
import com.lun.decor.service.CustomerService;
import com.lun.decor.service.ProjctService;
import com.lun.decor.service.TrackProgressService;
import com.lun.decor.service.dto.CustomerDTO;
import com.lun.decor.service.dto.ProjctDTO;
import com.lun.decor.service.dto.TrackProgressDTO;
import java.time.ZonedDateTime;
import java.util.*;
import javax.annotation.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author duclai on 05/08/2021
 */
@RestController
@RequestMapping("api/app/project")
public class ProjectController {

    private final ProjctService projctService;

    private final TrackProgressService trackProgressService;

    private final CustomerService customerService;

    private final OtpRepository otpRepository;

    public ProjectController(
        ProjctService projctService,
        TrackProgressService trackProgressService,
        CustomerService customerService,
        OtpRepository otpRepository
    ) {
        this.projctService = projctService;
        this.trackProgressService = trackProgressService;
        this.customerService = customerService;
        this.otpRepository = otpRepository;
    }

    @GetMapping
    ResponseEntity<List<ProjctDTO>> getProjectByPhoneOrEmail(
        @Nullable @RequestParam(name = "phone") String phone,
        @Nullable @RequestParam(name = "email") String email,
        @RequestParam(name = "otp_id") String otp_id,
        @RequestParam(name = "otp_code") String otp_code
    ) throws Exception {
        Otp otp = otpRepository.findById(otp_id).orElse(null);
        if (otp == null) {
            throw new Exception("Không tìm thấy mã xác thực tương ứng");
        }
        if (ZonedDateTime.now().isAfter(otp.getExpired_time())) {
            throw new Exception("Mã xác thực hết hạn.");
        }
        if (!otp.getOtp_code().equals(otp_code)) {
            throw new Exception("Mã xác thực không chính xác.");
        }
        List<ProjctDTO> list = projctService.findProjctsByCustomer(phone, email);
        return ResponseEntity.ok(list);
    }

    @GetMapping("/progress")
    ResponseEntity<ProjctDTO> getListTrackProgressByRequestIdF(
        @RequestParam("id") String requestId,
        @RequestParam("phone") String phone,
        @RequestParam("otp") String otp,
        @RequestParam("otp_id") String otp_id
    ) {
        if (phone == null || otp == null || requestId == null) {
            return null;
        }
        List<CustomerDTO> listCus = customerService.getCustomerDTOsByPhoneOrEmail(phone, "null");
        if (listCus == null || listCus.isEmpty()) {
            return null;
        }
        Otp otpObj = otpRepository.findById(otp_id).orElse(null);
        if (otpObj == null) {
            return null;
        }
        if (!otp.equals(otpObj.getOtp_code())) {
            return null;
        }
        ProjctDTO projctDTO = projctService.findProjctByRequestId(requestId);
        projctDTO.setCustomer(listCus.get(0));
        List<TrackProgressDTO> list = trackProgressService.findALlByProjectIdAndClientEnable(projctDTO.getId());
        projctDTO.setStages(new HashSet<>(list));
        return ResponseEntity.ok(projctDTO);
    }
}
