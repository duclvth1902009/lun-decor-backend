package com.lun.decor.app.controller;

import com.lun.decor.service.PackService;
import com.lun.decor.service.TopicService;
import com.lun.decor.service.dto.TopicDTO;
import com.lun.decor.web.rest.helper.Translator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

/**
 * @author duclai on 18/07/2021
 */
@RestController
@RequestMapping("api/app/topic")
public class TopicController {

    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping
    public ResponseEntity<List<TopicDTO>> getAllTopic() {
        System.out.println(Translator.toLocale("email.creation.text1"));
        return ResponseEntity.ok().body(topicService.getAll());
    }
}
