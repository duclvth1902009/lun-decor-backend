package com.lun.decor.app.controller;

import com.lun.decor.service.EventTypeService;
import com.lun.decor.service.TopicService;
import com.lun.decor.service.dto.EventTypeDTO;
import com.lun.decor.service.dto.TopicDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

/**
 * @author duclai on 18/07/2021
 */
@RestController
@RequestMapping("api/app/eventType")
public class EventTypeController {

    @Autowired
    private EventTypeService eventTypeService;

    @GetMapping
    public ResponseEntity<List<EventTypeDTO>> getAllEventType() {
        List<EventTypeDTO> results = eventTypeService.findAll();
        return ResponseEntity.ok().body(results);
    }
}
