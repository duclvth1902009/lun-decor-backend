package com.lun.decor.app.controller;

import com.lun.decor.service.PackService;
import com.lun.decor.service.dto.PackDTO;
import com.lun.decor.web.rest.PackResource;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

/**
 * @author duclai on 18/07/2021
 */
@RestController
@RequestMapping("api/app/package")
public class PackageController {

    private final Logger log = LoggerFactory.getLogger(PackageController.class);

    private final PackService packService;

    public PackageController(PackService packService) {
        this.packService = packService;
    }

    @GetMapping
    public ResponseEntity<List<PackDTO>> getAllPacks(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of package");
        Page<PackDTO> page;
        if (eagerload) {
            page = packService.findAllWithEagerRelationships(pageable);
        } else {
            page = packService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PackDTO> getPack(@PathVariable("id") String id) {
        log.debug("REST request to get a package");
        return ResponseEntity.ok().body(packService.findOne(id).get());
    }

    @GetMapping(value = "/eventType")
    public ResponseEntity<List<PackDTO>> getPacksByEventType(Pageable pageable, @RequestParam(name = "eventType") String eventType) {
        log.debug("REST request to get a page of package");
        Page<PackDTO> page = packService.findByTypeId(eventType, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        //        return ResponseEntity.ok().body(lstResult);
    }
}
