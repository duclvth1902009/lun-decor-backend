package com.lun.decor.app.controller;

import com.lun.decor.config.Constants;
import com.lun.decor.domain.Otp;
import com.lun.decor.domain.TrackProgress;
import com.lun.decor.service.*;
import com.lun.decor.service.dto.*;
import com.lun.decor.service.mapper.TrackProgressMapper;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/app/requestOrder")
public class RequestOrderController {

    private final Logger log = LoggerFactory.getLogger(RequestOrderController.class);

    private final OtpService otpService;

    private final RequestOrderService requestOrderService;

    private final CustomerService customerService;

    private final ProjctService projctService;

    private final TrackProgressService trackProgressService;

    private final TaskService taskService;

    private final MailService mailService;

    private final PackService packService;

    @Value("${send_otp.api_key}")
    public String API_KEY;

    @Value("${send_otp.secret_key}")
    public String SECRET_KEY;

    @Value("${client.web.baseURL}")
    private String clientUrl;

    public RequestOrderController(
        RequestOrderService requestOrderService,
        CustomerService customerService,
        ProjctService projctService,
        TrackProgressService trackProgressService,
        TaskService taskService,
        MailService mailService,
        PackService packService,
        OtpService otpService
    ) {
        this.requestOrderService = requestOrderService;
        this.customerService = customerService;
        this.projctService = projctService;
        this.trackProgressService = trackProgressService;
        this.taskService = taskService;
        this.mailService = mailService;
        this.packService = packService;
        this.otpService = otpService;
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<RequestOrderResponseDTO> createRequest(@RequestBody @Valid RequestOrderDTO requestOrderDTO) throws Exception {
        RequestOrderResponseDTO result = new RequestOrderResponseDTO();
        try {
            requestOrderDTO.setStatus(Constants.STATUS_PENDING);
            if (requestOrderDTO.getDiscount() == null) {
                requestOrderDTO.setDiscount("0");
            }
            requestOrderDTO = requestOrderService.save(requestOrderDTO);
            result.setRequestOrderId(requestOrderDTO.getId());
        } catch (Exception e) {
            result.setSuccess(false);
            result.setDescription("success");
        }

        return ResponseEntity.ok().body(result);
    }

    @GetMapping("sendOtp")
    public ResponseEntity<BaseMessage> sendOtpSMSorEmail(
        @Nullable @RequestParam(name = "phone") String phone,
        @Nullable @RequestParam(name = "email") String email
    ) {
        return ResponseEntity.ok().body(otpService.sendOtpSMSorEmail(email, phone));
    }

    @GetMapping("checkOtpForRequestOrder")
    public ResponseEntity<BaseMessage> checkOtpForRequestOrder(
        @RequestParam(name = "order_id") String order_id,
        @RequestParam(name = "otp_id") String otp_id,
        @RequestParam(name = "otp_code") String otp_code
    ) {
        BaseMessage result = new BaseMessage();
        try {
            Otp otp = otpService.verifyOtp(otp_id, otp_code);
            RequestOrderDTO requestOrderDTO = requestOrderService.findOne(order_id).orElse(null);
            if (requestOrderDTO == null) {
                throw new Exception("Không tìm thấy thông tin tiệc đã đặt");
            }
            requestOrderDTO.setStatus("active");
            requestOrderService.save(requestOrderDTO);
            PackDTO packDTO = packService.findOne(requestOrderDTO.getPack().getId()).orElse(new PackDTO());
            String url =
                clientUrl +
                "/tracking-order?id=" +
                requestOrderDTO.getId() +
                "&phone=" +
                requestOrderDTO.getPhone() +
                "&otp=" +
                otp_code +
                "&otp_id=" +
                otp_id;
            mailService.sendConfirmEmail(requestOrderDTO.getEmail(), requestOrderDTO, url);
            CustomerDTO customerDTO = customerService.getIfNullCreateCustomerDTOByPhoneOrEmail(
                requestOrderDTO.getPhone(),
                requestOrderDTO.getEmail(),
                requestOrderDTO
            );

            // tao project
            ProjctDTO projctDTO = projctService.generateProjctFromReqOrd(requestOrderDTO, customerDTO, packDTO);

            // tao trackProcess
            projctDTO.setStages(trackProgressService.generateDefaultTracks(projctDTO));
            projctService.save(projctDTO);

            //tao task theo product
            taskService.generateByPackInfo(packDTO, requestOrderDTO, projctDTO);
        } catch (Exception e) {
            result.setErrorCode("1");
            result.setSuccess(false);
            result.setDescription(e.getMessage());
        }
        return ResponseEntity.ok().body(result);
    }
    //    public void createTrackProcess(ProjctDTO projctDTO) {
    //        Set<TrackProgress> lstTrack = new HashSet<>();
    //        for (int i = 0; i < 6; i++) {
    //            TrackProgressDTO trackProgressDTO = new TrackProgressDTO();
    //            trackProgressDTO.setName(Constants.TRACK_PROGESS_NAME.get(i));
    //            trackProgressDTO.setStatus(Constants.STATUS_TODO);
    //            trackProgressDTO.setProjct(projctDTO);
    //            trackProgressDTO.setStep(i + 1);
    //            trackProgressDTO = trackProgressService.save(trackProgressDTO);
    //            TrackProgress trackProgress = trackProgressMapper.toEntity(trackProgressDTO);
    //            TaskDTO taskDTO = new TaskDTO();
    //            taskDTO.setProjct(projctDTO);
    //            taskDTO.setName(Constants.NAME_TASK_TRACK);
    //            taskDTO.setDetail("Track :" + trackProgressDTO.getName());
    //            taskDTO.setStatus(Constants.STATUS_TODO);
    //            taskService.save(taskDTO);
    //            lstTrack.add(trackProgress);
    //        }
    //        List<TrackProgressDTO> lstTrackDTO = trackProgressMapper.toDto(new ArrayList<>(lstTrack));
    //        projctDTO.setStages(new HashSet<>(lstTrackDTO));
    //        projctService.save(projctDTO);
    //    }
}
