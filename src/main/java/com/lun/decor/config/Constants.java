package com.lun.decor.config;

import java.util.Arrays;
import java.util.List;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";

    public static final String SYSTEM = "system";
    public static final String DEFAULT_LANGUAGE = "vi";

    public static final String STATUS_WAIT_ACTIVE = "waitActive";
    public static final String STATUS_ACTIVE = "active";
    public static final String STATUS_INACTIVE = "inActive";
    public static final String STATUS_TODO = "todo";
    public static final String STATUS_DOING = "doing";
    public static final String STATUS_DONE = "done";

    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_CANCEL = "cancel";

    public static final String NAME_TASK_SERVICE = "service";
    public static final String NAME_TASK_PRODUCT = "product";
    public static final String NAME_TASK_TRACK = "track";

    public static final List<String> TRACK_PROGESS_NAME = Arrays.asList(
        "Thiết kế",
        "Hoàn thiện thiết kế với khách hàng",
        "Deal giá",
        "Chuẩn bị và liên hệ đối tác",
        "Triển khai",
        "Dọn dẹp"
    );

    private Constants() {}
}
