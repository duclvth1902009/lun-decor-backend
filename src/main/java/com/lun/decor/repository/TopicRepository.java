package com.lun.decor.repository;

import com.lun.decor.domain.Topic;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Topic entity.
 */
@Repository
public interface TopicRepository extends MongoRepository<Topic, String> {
    @Query("{}")
    Page<Topic> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Topic> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Topic> findOneWithEagerRelationships(String id);
}
