package com.lun.decor.repository;

import com.lun.decor.domain.ServicePkg;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ServicePkg entity.
 */
@Repository
public interface ServicePkgRepository extends MongoRepository<ServicePkg, String> {
    @Query("{}")
    Page<ServicePkg> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<ServicePkg> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<ServicePkg> findOneWithEagerRelationships(String id);
}
