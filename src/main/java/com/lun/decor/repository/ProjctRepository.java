package com.lun.decor.repository;

import com.lun.decor.domain.Customer;
import com.lun.decor.domain.Projct;
import com.lun.decor.domain.RequestOrder;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Projct entity.
 */
@Repository
public interface ProjctRepository extends MongoRepository<Projct, String> {
    @Query("{}")
    Page<Projct> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Projct> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Projct> findOneWithEagerRelationships(String id);

    Projct findProjctByRequestOrder(RequestOrder requestOrder);

    List<Projct> findAllByCustomer(Customer customer);

    Page<Projct> findAllByStatus(Pageable pageable, String status);

    @Query("{'hold_time': {$gte: ?1, $lte:?2 }, 'status': ?0}")
    Page<Projct> searchProjct(Pageable pageable, String status, Date start, Date end);

    @Query("{'hold_time': {$gte: ?0, $lte:?1 }}")
    Page<Projct> searchProjctFromTo(Pageable pageable, Date start, Date end);

    @Query("{'$or':[ {'status':?0}, {'status':?1} ]}")
    Page<Projct> findForManage(Pageable pageable, String statusActive, String statusPending);
}
