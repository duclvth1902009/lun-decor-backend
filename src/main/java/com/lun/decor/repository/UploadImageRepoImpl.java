package com.lun.decor.repository;

import java.io.*;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public class UploadImageRepoImpl implements UploadImageRepo {

    @Override
    public File getImageFromBase64(byte[] data, String contentType) {
        String fileName = UUID.randomUUID().toString();
        String extension;
        switch (contentType) { // check image's extension
            case "image/jpeg":
                extension = "jpeg";
                break;
            case "image/png":
                extension = "png";
                break;
            default: // should write cases for more images types
                extension = "jpg";
                break;
        }
        // convert base64 string to binary data
        File file = new File(fileName + "." + extension);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public File getFileFromBase64(byte[] data, String contentType) {
        String fileName = UUID.randomUUID().toString();
        String extension = contentType;

        File file = new File(fileName + "." + extension);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
