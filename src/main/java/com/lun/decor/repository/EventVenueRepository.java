package com.lun.decor.repository;

import com.lun.decor.domain.EventVenue;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the EventVenue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventVenueRepository extends MongoRepository<EventVenue, String> {}
