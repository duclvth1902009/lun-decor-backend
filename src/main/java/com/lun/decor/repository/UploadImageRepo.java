package com.lun.decor.repository;

import java.io.File;
import org.springframework.stereotype.Repository;

public interface UploadImageRepo {
    File getImageFromBase64(byte[] data, String contentType);
    File getFileFromBase64(byte[] data, String contentType);
}
