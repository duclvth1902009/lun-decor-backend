package com.lun.decor.repository;

import com.lun.decor.domain.Transactn;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Transactn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactnRepository extends MongoRepository<Transactn, String> {}
