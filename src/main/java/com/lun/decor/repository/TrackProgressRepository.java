package com.lun.decor.repository;

import com.lun.decor.domain.TrackProgress;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the TrackProgress entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrackProgressRepository extends MongoRepository<TrackProgress, String> {
    @Query(value = "{'projct.id' : ?0}")
    List<TrackProgress> findByProjctId(String id);

    @Query(value = "{'projct.id' : ?0, 'client_enable' : ?1}")
    List<TrackProgress> findByProjctIdAndClient(String id, boolean isClientEnable);
}
