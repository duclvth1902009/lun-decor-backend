package com.lun.decor.repository;

import com.lun.decor.domain.RequestOrder;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the RequestOrder entity.
 */
@Repository
public interface RequestOrderRepository extends MongoRepository<RequestOrder, String> {
    @Query("{}")
    Page<RequestOrder> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<RequestOrder> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<RequestOrder> findOneWithEagerRelationships(String id);

    @Query("{'status': ?0}")
    Page<RequestOrder> findAllByStatus(String status, Pageable pageable);

    List<RequestOrder> findAllByLastModifiedBy(String user);
}
