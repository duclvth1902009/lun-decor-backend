package com.lun.decor.repository;

import com.lun.decor.domain.Pack;
import com.sun.el.stream.Stream;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Pack entity.
 */
@Repository
public interface PackRepository extends MongoRepository<Pack, String> {
    @Query("{}")
    Page<Pack> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Pack> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Pack> findOneWithEagerRelationships(String id);

    @Query(value = "{'type.id' : ?0}")
    Page<Pack> findByTypeId(Pageable pageable, String id);
}
