package com.lun.decor.repository;

import com.lun.decor.domain.Design;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Design entity.
 */
@Repository
public interface DesignRepository extends MongoRepository<Design, String> {
    @Query("{}")
    Page<Design> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Design> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Design> findOneWithEagerRelationships(String id);
}
