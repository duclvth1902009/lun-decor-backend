package com.lun.decor.repository;

import com.lun.decor.domain.Otp;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Otp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtpRepository extends MongoRepository<Otp, String> {}
