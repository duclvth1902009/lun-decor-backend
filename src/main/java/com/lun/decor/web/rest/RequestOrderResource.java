package com.lun.decor.web.rest;

import com.lun.decor.config.Constants;
import com.lun.decor.domain.TrackProgress;
import com.lun.decor.googledrive.GoogleDriveFileService;
import com.lun.decor.repository.RequestOrderRepository;
import com.lun.decor.repository.UploadImageRepo;
import com.lun.decor.service.*;
import com.lun.decor.service.MailService;
import com.lun.decor.service.RequestOrderService;
import com.lun.decor.service.dto.*;
import com.lun.decor.service.dto.RequestOrderDTO;
import com.lun.decor.service.mapper.TrackProgressMapper;
import com.lun.decor.web.rest.errors.BadRequestAlertException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.lun.decor.domain.RequestOrder}.
 */
@RestController
@RequestMapping("/api")
public class RequestOrderResource {

    private final Logger log = LoggerFactory.getLogger(RequestOrderResource.class);

    private static final String ENTITY_NAME = "requestOrder";

    @Autowired
    private ProjctService projctService;

    @Autowired
    private TrackProgressService trackProgressService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ImageService imageService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RequestOrderService requestOrderService;

    private final RequestOrderRepository requestOrderRepository;
    private final TrackProgressMapper trackProgressMapper;

    @Autowired
    private MailService mailService;

    public RequestOrderResource(
        RequestOrderService requestOrderService,
        RequestOrderRepository requestOrderRepository,
        TrackProgressMapper trackProgressMapper
    ) {
        this.requestOrderService = requestOrderService;
        this.requestOrderRepository = requestOrderRepository;
        this.trackProgressMapper = trackProgressMapper;
    }

    /**
     * {@code POST  /request-orders} : Create a new requestOrder.
     *
     * @param requestOrderDTO the requestOrderDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new requestOrderDTO, or with status {@code 400 (Bad Request)} if the requestOrder has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/request-orders")
    public ResponseEntity<RequestOrderDTO> createRequestOrder(@Valid @RequestBody RequestOrderDTO requestOrderDTO)
        throws URISyntaxException {
        log.debug("REST request to save RequestOrder : {}", requestOrderDTO);
        if (requestOrderDTO.getId() != null) {
            throw new BadRequestAlertException("A new requestOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if (!requestOrderDTO.getFiles().isEmpty()) {
            requestOrderDTO.getImages().addAll(imageService.createListImage(requestOrderDTO.getFiles()));
        }

        RequestOrderDTO result = requestOrderService.save(requestOrderDTO);

        //mailService.sendConfirmEmail(requestOrderDTO.getEmail(), decodeBase64(result.getId()));

        return ResponseEntity
            .created(new URI("/api/request-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /request-orders/:id} : Updates an existing requestOrder.
     *
     * @param id the id of the requestOrderDTO to save.
     * @param requestOrderDTO the requestOrderDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated requestOrderDTO,
     * or with status {@code 400 (Bad Request)} if the requestOrderDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the requestOrderDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/request-orders/{id}")
    public ResponseEntity<RequestOrderDTO> updateRequestOrder(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody RequestOrderDTO requestOrderDTO
    ) throws URISyntaxException {
        log.debug("REST request to update RequestOrder : {}, {}", id, requestOrderDTO);
        if (requestOrderDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, requestOrderDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }
        if (!requestOrderDTO.getFiles().isEmpty()) {
            requestOrderDTO.getImages().addAll(imageService.createListImage(requestOrderDTO.getFiles()));
        }
        RequestOrderDTO oldRequest = requestOrderService.findOne(id).orElse(null);

        if (oldRequest == null) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RequestOrderDTO result = requestOrderService.save(requestOrderDTO);

        if (Constants.STATUS_ACTIVE.equals(result.getStatus()) && !Constants.STATUS_ACTIVE.equals(oldRequest.getStatus())) {
            // tao project
            ProjctDTO projctDTO = new ProjctDTO();
            projctDTO.setHoldTime(result.getHoldTime());
            projctDTO.setEventVenue(projctDTO.getEventVenue());
            projctDTO.setRequestOrder(result);
            CustomerDTO customerDTO = new CustomerDTO();
            customerDTO.setEmail(result.getEmail());
            customerDTO.setName(result.getCusName());
            customerDTO.setEmail(result.getEmail());
            customerDTO.setPhone(result.getPhone());
            customerDTO = customerService.save(customerDTO);
            projctDTO.setCustomer(customerDTO);
            projctDTO.setType(requestOrderDTO.getType());
            projctDTO.setTopics(requestOrderDTO.getTopics());
            projctDTO.setStatus("active");
            projctDTO = projctService.save(projctDTO);

            // tao trackProcess
            createTrackProcess(projctDTO);

            //tao task
            PackDTO packDTO = requestOrderDTO.getPack();

            //tao task theo product
            List<ProductDTO> products = new ArrayList<>();
            if (packDTO != null) {
                products.addAll(packDTO.getProducts());
                if (requestOrderDTO.getSub_products() != null && requestOrderDTO.getSub_products().size() > 0) {
                    products.removeAll(requestOrderDTO.getSub_products());
                }
            }

            if (requestOrderDTO.getAdd_products() != null && requestOrderDTO.getAdd_products().size() > 0) {
                products.addAll(requestOrderDTO.getAdd_products());
            }
            for (ProductDTO productDTO : products) {
                TaskDTO taskDTO = new TaskDTO();
                taskDTO.setName(Constants.NAME_TASK_PRODUCT);
                taskDTO.setStatus(Constants.STATUS_TODO);
                taskDTO.setDetail("Product : " + productDTO.getName());
                taskDTO.setProjct(projctDTO);
                taskService.save(taskDTO);
            }

            //tao task theo service
            List<ServicePkgDTO> servicePkgDTOS = new ArrayList<>();

            if (packDTO != null && packDTO.getServices() != null && packDTO.getServices().size() > 0) {
                servicePkgDTOS.addAll(packDTO.getServices());
            }
            if (requestOrderDTO.getSub_services() != null && requestOrderDTO.getSub_services().size() > 0) {
                servicePkgDTOS.removeAll(requestOrderDTO.getAdd_services());
            }
            if (requestOrderDTO.getAdd_services() != null && requestOrderDTO.getAdd_services().size() > 0) {
                servicePkgDTOS.addAll(requestOrderDTO.getAdd_services());
            }

            for (ServicePkgDTO servicePkgDTO : servicePkgDTOS) {
                TaskDTO taskDTO = new TaskDTO();
                taskDTO.setName(Constants.NAME_TASK_SERVICE);
                taskDTO.setStatus(Constants.STATUS_TODO);
                taskDTO.setDetail("Service : " + servicePkgDTO.getName());
                taskDTO.setProjct(projctDTO);
                taskService.save(taskDTO);
            }
        }

        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, requestOrderDTO.getId()))
            .body(result);
    }

    public void createTrackProcess(ProjctDTO projctDTO) {
        Set<TrackProgress> lstTrack = new HashSet<>();
        for (int i = 0; i < 6; i++) {
            TrackProgressDTO trackProgressDTO = new TrackProgressDTO();
            trackProgressDTO.setName(Constants.TRACK_PROGESS_NAME.get(i));
            trackProgressDTO.setStatus(Constants.STATUS_PENDING);
            trackProgressDTO.setProjct(projctDTO);
            trackProgressDTO.setStep(i + 1);
            trackProgressDTO = trackProgressService.save(trackProgressDTO);
            TrackProgress trackProgress = trackProgressMapper.toEntity(trackProgressDTO);
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.setProjct(projctDTO);
            taskDTO.setName(Constants.NAME_TASK_TRACK);
            taskDTO.setDetail("Track :" + trackProgressDTO.getName());
            taskDTO.setStatus(Constants.STATUS_TODO);
            taskService.save(taskDTO);
            lstTrack.add(trackProgress);
        }
        List<TrackProgressDTO> lstTrackDTO = trackProgressMapper.toDto(new ArrayList<>(lstTrack));
        projctDTO.setStages(new HashSet<>(lstTrackDTO));
        projctService.save(projctDTO);
    }

    /**
     * {@code PATCH  /request-orders/:id} : Partial updates given fields of an existing requestOrder, field will ignore if it is null
     *
     * @param id the id of the requestOrderDTO to save.
     * @param requestOrderDTO the requestOrderDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated requestOrderDTO,
     * or with status {@code 400 (Bad Request)} if the requestOrderDTO is not valid,
     * or with status {@code 404 (Not Found)} if the requestOrderDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the requestOrderDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/request-orders/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<RequestOrderDTO> partialUpdateRequestOrder(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody RequestOrderDTO requestOrderDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update RequestOrder partially : {}, {}", id, requestOrderDTO);
        if (requestOrderDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, requestOrderDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!requestOrderRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        //        ImageDTO imageDTO = new ImageDTO();
        //        imageDTO
        Optional<RequestOrderDTO> result = requestOrderService.partialUpdate(requestOrderDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, requestOrderDTO.getId())
        );
    }

    /**
     * {@code GET  /request-orders} : get all the requestOrders.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requestOrders in body.
     */
    @GetMapping("/request-orders")
    public ResponseEntity<List<RequestOrderDTO>> getAllRequestOrders(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of RequestOrders");
        Page<RequestOrderDTO> page;
        if (eagerload) {
            page = requestOrderService.findAllWithEagerRelationships(pageable);
        } else {
            page = requestOrderService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/request-orders-waitActive")
    public ResponseEntity<List<RequestOrderDTO>> getWaitActiveRequestOrders(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of RequestOrders wait active");
        Page<RequestOrderDTO> page;

        page = requestOrderService.findAllByStatus(pageable, Constants.STATUS_WAIT_ACTIVE);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /request-orders/:id} : get the "id" requestOrder.
     *
     * @param id the id of the requestOrderDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requestOrderDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/request-orders/{id}")
    public ResponseEntity<RequestOrderDTO> getRequestOrder(@PathVariable String id) {
        log.debug("REST request to get RequestOrder : {}", id);
        Optional<RequestOrderDTO> requestOrderDTO = requestOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requestOrderDTO);
    }

    /**
     * {@code DELETE  /request-orders/:id} : delete the "id" requestOrder.
     *
     * @param id the id of the requestOrderDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/request-orders/{id}")
    public ResponseEntity<Void> deleteRequestOrder(@PathVariable String id) {
        log.debug("REST request to delete RequestOrder : {}", id);
        requestOrderService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    @GetMapping("/request-orders/status/{status}")
    public ResponseEntity<List<RequestOrderDTO>> getReqquestByStatus(Pageable pageable, @PathVariable String status) {
        Page<RequestOrderDTO> page;
        if (!status.isEmpty() && !status.isBlank()) {
            page = requestOrderService.findAllByStatus(pageable, status);
        } else {
            page = requestOrderService.findAll(pageable);
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/request-orders/waiting")
    public ResponseEntity<List<RequestOrderDTO>> getRequestOrderWaiting() {
        log.debug("REST request to get RequestOrder waiting");
        List<RequestOrderDTO> lst = requestOrderService.findAllByUpdateDate(null);
        return ResponseEntity.ok().body(lst);
    }
}
