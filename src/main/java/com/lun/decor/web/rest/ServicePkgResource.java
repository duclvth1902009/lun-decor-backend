package com.lun.decor.web.rest;

import com.lun.decor.googledrive.GoogleDriveFileService;
import com.lun.decor.repository.ServicePkgRepository;
import com.lun.decor.repository.UploadImageRepo;
import com.lun.decor.service.ImageService;
import com.lun.decor.service.ServicePkgService;
import com.lun.decor.service.dto.ImageDTO;
import com.lun.decor.service.dto.ProductDTO;
import com.lun.decor.service.dto.ServicePkgDTO;
import com.lun.decor.web.rest.errors.BadRequestAlertException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.lun.decor.domain.ServicePkg}.
 */
@RestController
@RequestMapping("/api")
public class ServicePkgResource {

    private final Logger log = LoggerFactory.getLogger(ServicePkgResource.class);

    private static final String ENTITY_NAME = "servicePkg";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ServicePkgService servicePkgService;

    private final ServicePkgRepository servicePkgRepository;

    @Autowired
    private ImageService imageService;

    public ServicePkgResource(ServicePkgService servicePkgService, ServicePkgRepository servicePkgRepository) {
        this.servicePkgService = servicePkgService;
        this.servicePkgRepository = servicePkgRepository;
    }

    /**
     * {@code POST  /service-pkgs} : Create a new servicePkg.
     *
     * @param servicePkgDTO the servicePkgDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new servicePkgDTO, or with status {@code 400 (Bad Request)} if the servicePkg has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/service-pkgs")
    public ResponseEntity<ServicePkgDTO> createServicePkg(@Valid @RequestBody ServicePkgDTO servicePkgDTO) throws URISyntaxException {
        log.debug("REST request to save ServicePkg : {}", servicePkgDTO);
        if (servicePkgDTO.getId() != null) {
            throw new BadRequestAlertException("A new servicePkg cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (!servicePkgDTO.getFiles().isEmpty()) {
            servicePkgDTO.setImages(imageService.createListImage(servicePkgDTO.getFiles()));
        }
        ServicePkgDTO result = servicePkgService.save(servicePkgDTO);
        return ResponseEntity
            .created(new URI("/api/service-pkgs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /service-pkgs/:id} : Updates an existing servicePkg.
     *
     * @param id the id of the servicePkgDTO to save.
     * @param servicePkgDTO the servicePkgDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated servicePkgDTO,
     * or with status {@code 400 (Bad Request)} if the servicePkgDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the servicePkgDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/service-pkgs/{id}")
    public ResponseEntity<ServicePkgDTO> updateServicePkg(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody ServicePkgDTO servicePkgDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ServicePkg : {}, {}", id, servicePkgDTO);
        if (servicePkgDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, servicePkgDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!servicePkgDTO.getFiles().isEmpty()) {
            servicePkgDTO.getImages().addAll(imageService.createListImage(servicePkgDTO.getFiles()));
        }

        if (!servicePkgRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ServicePkgDTO result = servicePkgService.save(servicePkgDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, servicePkgDTO.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /service-pkgs/:id} : Partial updates given fields of an existing servicePkg, field will ignore if it is null
     *
     * @param id the id of the servicePkgDTO to save.
     * @param servicePkgDTO the servicePkgDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated servicePkgDTO,
     * or with status {@code 400 (Bad Request)} if the servicePkgDTO is not valid,
     * or with status {@code 404 (Not Found)} if the servicePkgDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the servicePkgDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/service-pkgs/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ServicePkgDTO> partialUpdateServicePkg(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody ServicePkgDTO servicePkgDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ServicePkg partially : {}, {}", id, servicePkgDTO);
        if (servicePkgDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, servicePkgDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!servicePkgRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ServicePkgDTO> result = servicePkgService.partialUpdate(servicePkgDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, servicePkgDTO.getId())
        );
    }

    /**
     * {@code GET  /service-pkgs} : get all the servicePkgs.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of servicePkgs in body.
     */
    @GetMapping("/service-pkgs")
    public ResponseEntity<List<ServicePkgDTO>> getAllServicePkgs(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of ServicePkgs");
        Page<ServicePkgDTO> page;
        if (eagerload) {
            page = servicePkgService.findAllWithEagerRelationships(pageable);
        } else {
            page = servicePkgService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /service-pkgs/:id} : get the "id" servicePkg.
     *
     * @param id the id of the servicePkgDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the servicePkgDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/service-pkgs/{id}")
    public ResponseEntity<ServicePkgDTO> getServicePkg(@PathVariable String id) {
        log.debug("REST request to get ServicePkg : {}", id);
        Optional<ServicePkgDTO> servicePkgDTO = servicePkgService.findOne(id);
        return ResponseUtil.wrapOrNotFound(servicePkgDTO);
    }

    /**
     * {@code DELETE  /service-pkgs/:id} : delete the "id" servicePkg.
     *
     * @param id the id of the servicePkgDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/service-pkgs/{id}")
    public ResponseEntity<Void> deleteServicePkg(@PathVariable String id) {
        log.debug("REST request to delete ServicePkg : {}", id);
        servicePkgService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
