/**
 * View Models used by Spring MVC REST controllers.
 */
package com.lun.decor.web.rest.vm;
