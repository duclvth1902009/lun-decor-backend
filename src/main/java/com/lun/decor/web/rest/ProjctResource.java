package com.lun.decor.web.rest;

import com.lun.decor.config.Constants;
import com.lun.decor.domain.Projct;
import com.lun.decor.domain.TrackProgress;
import com.lun.decor.googledrive.GoogleDriveFileService;
import com.lun.decor.repository.ProjctRepository;
import com.lun.decor.repository.UploadImageRepo;
import com.lun.decor.service.ImageService;
import com.lun.decor.service.MailService;
import com.lun.decor.service.ProjctService;
import com.lun.decor.service.TrackProgressService;
import com.lun.decor.service.dto.*;
import com.lun.decor.service.mapper.ProjctMapper;
import com.lun.decor.web.rest.errors.BadRequestAlertException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.lun.decor.domain.Projct}.
 */
@RestController
@RequestMapping("/api")
public class ProjctResource {

    private final Logger log = LoggerFactory.getLogger(ProjctResource.class);

    private static final String ENTITY_NAME = "projct";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProjctService projctService;

    private final TrackProgressService trackProgressService;

    private final ProjctRepository projctRepository;

    @Autowired
    private ImageService imageService;

    public ProjctResource(ProjctService projctService, ProjctRepository projctRepository, TrackProgressService trackProgressService) {
        this.projctService = projctService;
        this.projctRepository = projctRepository;
        this.trackProgressService = trackProgressService;
        //        this.projctMapper = projctMapper;
    }

    /**
     * {@code POST  /projcts} : Create a new projct.
     *
     * @param projctDTO the projctDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new projctDTO, or with status {@code 400 (Bad Request)} if the projct has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/projcts")
    public ResponseEntity<ProjctDTO> createProjct(@RequestBody ProjctDTO projctDTO) throws URISyntaxException {
        log.debug("REST request to save Projct : {}", projctDTO);
        if (projctDTO.getId() != null) {
            throw new BadRequestAlertException("A new projct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (!projctDTO.getFiles().isEmpty()) {
            projctDTO.setImages(imageService.createListImage(projctDTO.getFiles()));
        }
        projctDTO.setProgress(0D);
        ProjctDTO result = projctService.save(projctDTO);

        return ResponseEntity
            .created(new URI("/api/projcts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /projcts/:id} : Updates an existing projct.
     *
     * @param id        the id of the projctDTO to save.
     * @param projctDTO the projctDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated projctDTO,
     * or with status {@code 400 (Bad Request)} if the projctDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the projctDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/projcts/{id}")
    public ResponseEntity<ProjctDTO> updateProjct(
        @PathVariable(value = "id", required = false) final String id,
        @RequestBody ProjctDTO projctDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Projct : {}, {}", id, projctDTO);
        if (projctDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, projctDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        //        if (!projctRepository.existsById(id)) {
        //            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        //        }
        Projct oldProjct = projctRepository.findById(id).orElse(null);
        if (oldProjct == null) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        if (Constants.STATUS_DONE.equals(oldProjct.getStatus())) {
            throw new BadRequestAlertException("Dự án đã hoàn thành, không thể cập nhật", ENTITY_NAME, "projectIsDone");
        }
        if (Constants.STATUS_CANCEL.equals(oldProjct.getStatus())) {
            throw new BadRequestAlertException("Dự án đã hủy, không thể cập nhật", ENTITY_NAME, "projectIsCancel");
        }
        if (Constants.STATUS_DONE.equals(projctDTO.getStatus())) {
            List<TrackProgress> lstTrack = trackProgressService.findALlByProjectId(id);
            for (TrackProgress trackProgress : lstTrack) {
                if (!Constants.STATUS_DONE.equals(trackProgress.getStatus())) {
                    throw new BadRequestAlertException("Có bước triển khai chưa hoàn thành", ENTITY_NAME, "trackIsNotDone");
                }
            }
        }

        if (!projctDTO.getFiles().isEmpty()) {
            projctDTO.getImages().addAll(imageService.createListImage(projctDTO.getFiles()));
        }
        ProjctDTO result = projctService.save(projctDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, projctDTO.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /projcts/:id} : Partial updates given fields of an existing projct, field will ignore if it is null
     *
     * @param id        the id of the projctDTO to save.
     * @param projctDTO the projctDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated projctDTO,
     * or with status {@code 400 (Bad Request)} if the projctDTO is not valid,
     * or with status {@code 404 (Not Found)} if the projctDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the projctDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/projcts/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ProjctDTO> partialUpdateProjct(
        @PathVariable(value = "id", required = false) final String id,
        @RequestBody ProjctDTO projctDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Projct partially : {}, {}", id, projctDTO);
        if (projctDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, projctDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!projctRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ProjctDTO> result = projctService.partialUpdate(projctDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, projctDTO.getId())
        );
    }

    /**
     * {@code GET  /projcts} : get all the projcts.
     *
     * @param pageable  the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of projcts in body.
     */
    @GetMapping("/projcts")
    public ResponseEntity<List<ProjctDTO>> getAllProjcts(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of Projcts");
        Page<ProjctDTO> page;
        if (eagerload) {
            page = projctService.findAllWithEagerRelationships(pageable);
        } else {
            page = projctService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /projcts/:id} : get the "id" projct.
     *
     * @param id the id of the projctDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the projctDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/projcts/{id}")
    public ResponseEntity<ProjctDTO> getProjct(@PathVariable String id) {
        log.debug("REST request to get Projct : {}", id);
        Optional<ProjctDTO> projctDTO = projctService.findOne(id);
        return ResponseUtil.wrapOrNotFound(projctDTO);
    }

    /**
     * {@code DELETE  /projcts/:id} : delete the "id" projct.
     *
     * @param id the id of the projctDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/projcts/{id}")
    public ResponseEntity<Void> deleteProjct(@PathVariable String id) {
        log.debug("REST request to delete Projct : {}", id);
        projctService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    @GetMapping("/projcts/status/{status}")
    public ResponseEntity<List<ProjctDTO>> getProjctByStatus(Pageable pageable, @PathVariable String status) {
        Page<ProjctDTO> page;
        if (!status.isEmpty() && !status.isBlank()) {
            page = projctService.findByStatus(pageable, status);
        } else {
            page = projctService.findAll(pageable);
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/projcts/manage")
    public ResponseEntity<List<ProjctDTO>> getProjctPendingAndActive(Pageable pageable) {
        Page<ProjctDTO> page = projctService.findForManage(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/projcts/search/{status}/{start}/{end}")
    public ResponseEntity<List<ProjctDTO>> searchProjct(
        Pageable pageable,
        @PathVariable(name = "status") String status,
        @PathVariable(name = "start") String startDate,
        @PathVariable(name = "end") String endDate
    ) {
        Page<ProjctDTO> page = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

        try {
            Date start = df.parse(startDate);
            Date end = df.parse(endDate);
            if ((!status.isEmpty() && !status.isBlank())) {
                page = projctService.searchProjct(pageable, status, start, end);
            } else {
                page = projctService.searchProjctFromTo(pageable, start, end);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
