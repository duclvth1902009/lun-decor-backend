package com.lun.decor.web.rest;

import com.lun.decor.domain.Otp;
import com.lun.decor.repository.OtpRepository;
import com.lun.decor.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.lun.decor.domain.Otp}.
 */
@RestController
@RequestMapping("/api")
public class OtpResource {

    private final Logger log = LoggerFactory.getLogger(OtpResource.class);

    private static final String ENTITY_NAME = "otp";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtpRepository otpRepository;

    public OtpResource(OtpRepository otpRepository) {
        this.otpRepository = otpRepository;
    }

    /**
     * {@code POST  /otps} : Create a new otp.
     *
     * @param otp the otp to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otp, or with status {@code 400 (Bad Request)} if the otp has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/otps")
    public ResponseEntity<Otp> createOtp(@RequestBody Otp otp) throws URISyntaxException {
        log.debug("REST request to save Otp : {}", otp);
        if (otp.getId() != null) {
            throw new BadRequestAlertException("A new otp cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Otp result = otpRepository.save(otp);
        return ResponseEntity
            .created(new URI("/api/otps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /otps/:id} : Updates an existing otp.
     *
     * @param id the id of the otp to save.
     * @param otp the otp to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otp,
     * or with status {@code 400 (Bad Request)} if the otp is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otp couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/otps/{id}")
    public ResponseEntity<Otp> updateOtp(@PathVariable(value = "id", required = false) final String id, @RequestBody Otp otp)
        throws URISyntaxException {
        log.debug("REST request to update Otp : {}, {}", id, otp);
        if (otp.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, otp.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!otpRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Otp result = otpRepository.save(otp);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otp.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /otps/:id} : Partial updates given fields of an existing otp, field will ignore if it is null
     *
     * @param id the id of the otp to save.
     * @param otp the otp to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otp,
     * or with status {@code 400 (Bad Request)} if the otp is not valid,
     * or with status {@code 404 (Not Found)} if the otp is not found,
     * or with status {@code 500 (Internal Server Error)} if the otp couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/otps/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Otp> partialUpdateOtp(@PathVariable(value = "id", required = false) final String id, @RequestBody Otp otp)
        throws URISyntaxException {
        log.debug("REST request to partial update Otp partially : {}, {}", id, otp);
        if (otp.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, otp.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!otpRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Otp> result = otpRepository
            .findById(otp.getId())
            .map(
                existingOtp -> {
                    if (otp.getOtp_code() != null) {
                        existingOtp.setOtp_code(otp.getOtp_code());
                    }
                    if (otp.getExpired_time() != null) {
                        existingOtp.setExpired_time(otp.getExpired_time());
                    }
                    if (otp.getStatus() != null) {
                        existingOtp.setStatus(otp.getStatus());
                    }

                    return existingOtp;
                }
            )
            .map(otpRepository::save);

        return ResponseUtil.wrapOrNotFound(result, HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otp.getId()));
    }

    /**
     * {@code GET  /otps} : get all the otps.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otps in body.
     */
    @GetMapping("/otps")
    public List<Otp> getAllOtps() {
        log.debug("REST request to get all Otps");
        return otpRepository.findAll();
    }

    /**
     * {@code GET  /otps/:id} : get the "id" otp.
     *
     * @param id the id of the otp to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otp, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/otps/{id}")
    public ResponseEntity<Otp> getOtp(@PathVariable String id) {
        log.debug("REST request to get Otp : {}", id);
        Optional<Otp> otp = otpRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(otp);
    }

    /**
     * {@code DELETE  /otps/:id} : delete the "id" otp.
     *
     * @param id the id of the otp to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/otps/{id}")
    public ResponseEntity<Void> deleteOtp(@PathVariable String id) {
        log.debug("REST request to delete Otp : {}", id);
        otpRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
