package com.lun.decor.web.rest;

import com.lun.decor.repository.TransactnRepository;
import com.lun.decor.service.TransactnService;
import com.lun.decor.service.dto.TransactnDTO;
import com.lun.decor.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.lun.decor.domain.Transactn}.
 */
@RestController
@RequestMapping("/api")
public class TransactnResource {

    private final Logger log = LoggerFactory.getLogger(TransactnResource.class);

    private static final String ENTITY_NAME = "transactn";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TransactnService transactnService;

    private final TransactnRepository transactnRepository;

    public TransactnResource(TransactnService transactnService, TransactnRepository transactnRepository) {
        this.transactnService = transactnService;
        this.transactnRepository = transactnRepository;
    }

    /**
     * {@code POST  /transactns} : Create a new transactn.
     *
     * @param transactnDTO the transactnDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transactnDTO, or with status {@code 400 (Bad Request)} if the transactn has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transactns")
    public ResponseEntity<TransactnDTO> createTransactn(@RequestBody TransactnDTO transactnDTO) throws URISyntaxException {
        log.debug("REST request to save Transactn : {}", transactnDTO);
        if (transactnDTO.getId() != null) {
            throw new BadRequestAlertException("A new transactn cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TransactnDTO result = transactnService.save(transactnDTO);
        return ResponseEntity
            .created(new URI("/api/transactns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /transactns/:id} : Updates an existing transactn.
     *
     * @param id the id of the transactnDTO to save.
     * @param transactnDTO the transactnDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactnDTO,
     * or with status {@code 400 (Bad Request)} if the transactnDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transactnDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/transactns/{id}")
    public ResponseEntity<TransactnDTO> updateTransactn(
        @PathVariable(value = "id", required = false) final String id,
        @RequestBody TransactnDTO transactnDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Transactn : {}, {}", id, transactnDTO);
        if (transactnDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, transactnDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!transactnRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TransactnDTO result = transactnService.save(transactnDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transactnDTO.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /transactns/:id} : Partial updates given fields of an existing transactn, field will ignore if it is null
     *
     * @param id the id of the transactnDTO to save.
     * @param transactnDTO the transactnDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactnDTO,
     * or with status {@code 400 (Bad Request)} if the transactnDTO is not valid,
     * or with status {@code 404 (Not Found)} if the transactnDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the transactnDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/transactns/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<TransactnDTO> partialUpdateTransactn(
        @PathVariable(value = "id", required = false) final String id,
        @RequestBody TransactnDTO transactnDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Transactn partially : {}, {}", id, transactnDTO);
        if (transactnDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, transactnDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!transactnRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TransactnDTO> result = transactnService.partialUpdate(transactnDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transactnDTO.getId())
        );
    }

    /**
     * {@code GET  /transactns} : get all the transactns.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactns in body.
     */
    @GetMapping("/transactns")
    public ResponseEntity<List<TransactnDTO>> getAllTransactns(Pageable pageable) {
        log.debug("REST request to get a page of Transactns");
        Page<TransactnDTO> page = transactnService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /transactns/:id} : get the "id" transactn.
     *
     * @param id the id of the transactnDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transactnDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transactns/{id}")
    public ResponseEntity<TransactnDTO> getTransactn(@PathVariable String id) {
        log.debug("REST request to get Transactn : {}", id);
        Optional<TransactnDTO> transactnDTO = transactnService.findOne(id);
        return ResponseUtil.wrapOrNotFound(transactnDTO);
    }

    /**
     * {@code DELETE  /transactns/:id} : delete the "id" transactn.
     *
     * @param id the id of the transactnDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transactns/{id}")
    public ResponseEntity<Void> deleteTransactn(@PathVariable String id) {
        log.debug("REST request to delete Transactn : {}", id);
        transactnService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
