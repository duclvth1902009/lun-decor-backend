package com.lun.decor.web.rest;

import com.lun.decor.googledrive.GoogleDriveFileService;
import com.lun.decor.repository.DesignRepository;
import com.lun.decor.repository.UploadImageRepo;
import com.lun.decor.service.DesignService;
import com.lun.decor.service.ImageService;
import com.lun.decor.service.dto.DesignDTO;
import com.lun.decor.service.dto.ImageDTO;
import com.lun.decor.web.rest.errors.BadRequestAlertException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.lun.decor.domain.Design}.
 */
@RestController
@RequestMapping("/api")
public class DesignResource {

    private static final String ENTITY_NAME = "design";
    private final Logger log = LoggerFactory.getLogger(DesignResource.class);
    private final DesignService designService;
    private final DesignRepository designRepository;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    private ImageService imageService;

    @Autowired
    private GoogleDriveFileService driveFileService;

    @Autowired
    private UploadImageRepo uploadImageRepo;

    public DesignResource(DesignService designService, DesignRepository designRepository) {
        this.designService = designService;
        this.designRepository = designRepository;
    }

    /**
     * {@code POST  /designs} : Create a new design.
     *
     * @param designDTO the designDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new designDTO, or with status {@code 400 (Bad Request)} if the design has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/designs")
    public ResponseEntity<DesignDTO> createDesign(@Valid @RequestBody DesignDTO designDTO) throws URISyntaxException {
        log.debug("REST request to save Design : {}", designDTO);
        if (designDTO.getId() != null) {
            throw new BadRequestAlertException("A new design cannot already have an ID", ENTITY_NAME, "idexists");
        }
        uploadFile(designDTO);
        DesignDTO result = designService.save(designDTO);
        return ResponseEntity
            .created(new URI("/api/designs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /designs/:id} : Updates an existing design.
     *
     * @param id        the id of the designDTO to save.
     * @param designDTO the designDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated designDTO,
     * or with status {@code 400 (Bad Request)} if the designDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the designDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/designs/{id}")
    public ResponseEntity<DesignDTO> updateDesign(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody DesignDTO designDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Design : {}, {}", id, designDTO);
        if (designDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, designDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!designRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        uploadFile(designDTO);
        DesignDTO result = designService.save(designDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, designDTO.getId()))
            .body(result);
    }

    private void uploadFile(@RequestBody @Valid DesignDTO designDTO) {
        if (designDTO.getFile() != null && designDTO.getFile().length > 0) {
            String fileType = designDTO.getNote();
            String mimeType = "";
            switch (fileType) {
                case "pdf":
                case "ai":
                    mimeType = "application/pdf";
                    break;
                case "psd":
                    mimeType = "application/x-photoshop";
                    break;
                default:
                    {
                        throw new BadRequestAlertException("Invalid file type", ENTITY_NAME, "invalidFileType");
                    }
            }
            File file = uploadImageRepo.getFileFromBase64(designDTO.getFile(), fileType);
            String fileUrl = driveFileService.uploadFile(file, "design", mimeType, true);
            designDTO.setNote(fileType + "-" + fileUrl);
            designDTO.setFile(null);
            file.delete();
        }
        if (designDTO.getImgQuickView() != null && designDTO.getImgQuickView().length > 0) {
            String url = imageService.uploadImage(designDTO.getImgQuickView(), designDTO.getImgQuickViewContentType());
            ImageDTO imageDTO = new ImageDTO();
            imageDTO.setFile(null);
            imageDTO.setUrl(url);
            imageService.save(imageDTO);
            designDTO.setQuickView(url);
            designDTO.setImgQuickView(null);
        }
    }

    /**
     * {@code PATCH  /designs/:id} : Partial updates given fields of an existing design, field will ignore if it is null
     *
     * @param id        the id of the designDTO to save.
     * @param designDTO the designDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated designDTO,
     * or with status {@code 400 (Bad Request)} if the designDTO is not valid,
     * or with status {@code 404 (Not Found)} if the designDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the designDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/designs/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DesignDTO> partialUpdateDesign(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody DesignDTO designDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Design partially : {}, {}", id, designDTO);
        if (designDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, designDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!designRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DesignDTO> result = designService.partialUpdate(designDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, designDTO.getId())
        );
    }

    /**
     * {@code GET  /designs} : get all the designs.
     *
     * @param pageable  the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of designs in body.
     */
    @GetMapping("/designs")
    public ResponseEntity<List<DesignDTO>> getAllDesigns(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of Designs");
        Page<DesignDTO> page;
        if (eagerload) {
            page = designService.findAllWithEagerRelationships(pageable);
        } else {
            page = designService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /designs/:id} : get the "id" design.
     *
     * @param id the id of the designDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the designDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/designs/{id}")
    public ResponseEntity<DesignDTO> getDesign(@PathVariable String id) {
        log.debug("REST request to get Design : {}", id);
        Optional<DesignDTO> designDTO = designService.findOne(id);
        return ResponseUtil.wrapOrNotFound(designDTO);
    }

    /**
     * {@code DELETE  /designs/:id} : delete the "id" design.
     *
     * @param id the id of the designDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/designs/{id}")
    public ResponseEntity<Void> deleteDesign(@PathVariable String id) {
        log.debug("REST request to delete Design : {}", id);
        designService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
