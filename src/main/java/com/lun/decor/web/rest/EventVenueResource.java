package com.lun.decor.web.rest;

import com.lun.decor.googledrive.GoogleDriveFileService;
import com.lun.decor.repository.EventVenueRepository;
import com.lun.decor.repository.UploadImageRepo;
import com.lun.decor.service.EventVenueService;
import com.lun.decor.service.ImageService;
import com.lun.decor.service.dto.EventVenueDTO;
import com.lun.decor.service.dto.ImageDTO;
import com.lun.decor.service.mapper.EventVenueMapper;
import com.lun.decor.web.rest.errors.BadRequestAlertException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.lun.decor.domain.EventVenue}.
 */
@RestController
@RequestMapping("/api")
public class EventVenueResource {

    private final Logger log = LoggerFactory.getLogger(EventVenueResource.class);

    private static final String ENTITY_NAME = "eventVenue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EventVenueService eventVenueService;

    private final EventVenueRepository eventVenueRepository;

    private final EventVenueMapper eventVenueMapper;

    @Autowired
    private ImageService imageService;

    public EventVenueResource(
        EventVenueService eventVenueService,
        EventVenueRepository eventVenueRepository,
        EventVenueMapper eventVenueMapper
    ) {
        this.eventVenueService = eventVenueService;
        this.eventVenueRepository = eventVenueRepository;
        this.eventVenueMapper = eventVenueMapper;
    }

    /**
     * {@code POST  /event-venues} : Create a new eventVenue.
     *
     * @param eventVenueDTO the eventVenueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new eventVenueDTO, or with status {@code 400 (Bad Request)} if the eventVenue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/event-venues")
    public ResponseEntity<EventVenueDTO> createEventVenue(@Valid @RequestBody EventVenueDTO eventVenueDTO) throws URISyntaxException {
        log.debug("REST request to save EventVenue : {}", eventVenueDTO);
        if (eventVenueDTO.getId() != null) {
            throw new BadRequestAlertException("A new eventVenue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (!eventVenueDTO.getFiles().isEmpty()) {
            eventVenueDTO.setImages(imageService.createListImage(eventVenueDTO.getFiles()));
        }
        EventVenueDTO result = eventVenueService.save(eventVenueDTO);
        return ResponseEntity
            .created(new URI("/api/event-venues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /event-venues/:id} : Updates an existing eventVenue.
     *
     * @param id the id of the eventVenueDTO to save.
     * @param eventVenueDTO the eventVenueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated eventVenueDTO,
     * or with status {@code 400 (Bad Request)} if the eventVenueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the eventVenueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/event-venues/{id}")
    public ResponseEntity<EventVenueDTO> updateEventVenue(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody EventVenueDTO eventVenueDTO
    ) throws URISyntaxException {
        log.debug("REST request to update EventVenue : {}, {}", id, eventVenueDTO);
        if (eventVenueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, eventVenueDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!eventVenueDTO.getFiles().isEmpty()) {
            eventVenueDTO.getImages().addAll(imageService.createListImage(eventVenueDTO.getFiles()));
        }

        if (!eventVenueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EventVenueDTO result = eventVenueService.save(eventVenueDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, eventVenueDTO.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /event-venues/:id} : Partial updates given fields of an existing eventVenue, field will ignore if it is null
     *
     * @param id the id of the eventVenueDTO to save.
     * @param eventVenueDTO the eventVenueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated eventVenueDTO,
     * or with status {@code 400 (Bad Request)} if the eventVenueDTO is not valid,
     * or with status {@code 404 (Not Found)} if the eventVenueDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the eventVenueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/event-venues/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<EventVenueDTO> partialUpdateEventVenue(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody EventVenueDTO eventVenueDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update EventVenue partially : {}, {}", id, eventVenueDTO);
        if (eventVenueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, eventVenueDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!eventVenueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EventVenueDTO> result = eventVenueService.partialUpdate(eventVenueDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, eventVenueDTO.getId())
        );
    }

    /**
     * {@code GET  /event-venues} : get all the eventVenues.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of eventVenues in body.
     */
    @GetMapping("/event-venues")
    public ResponseEntity<List<EventVenueDTO>> getAllEventVenues(Pageable pageable) {
        log.debug("REST request to get a page of EventVenues");
        Page<EventVenueDTO> page = eventVenueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /event-venues/:id} : get the "id" eventVenue.
     *
     * @param id the id of the eventVenueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the eventVenueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/event-venues/{id}")
    public ResponseEntity<EventVenueDTO> getEventVenue(@PathVariable String id) {
        log.debug("REST request to get EventVenue : {}", id);
        Optional<EventVenueDTO> eventVenueDTO = eventVenueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(eventVenueDTO);
    }

    /**
     * {@code DELETE  /event-venues/:id} : delete the "id" eventVenue.
     *
     * @param id the id of the eventVenueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/event-venues/{id}")
    public ResponseEntity<Void> deleteEventVenue(@PathVariable String id) {
        log.debug("REST request to delete EventVenue : {}", id);
        eventVenueService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
