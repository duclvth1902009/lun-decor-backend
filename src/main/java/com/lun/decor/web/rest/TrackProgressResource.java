package com.lun.decor.web.rest;

import com.lun.decor.config.Constants;
import com.lun.decor.domain.Customer;
import com.lun.decor.domain.Otp;
import com.lun.decor.domain.Projct;
import com.lun.decor.domain.TrackProgress;
import com.lun.decor.googledrive.GoogleDriveFileService;
import com.lun.decor.repository.OtpRepository;
import com.lun.decor.repository.TrackProgressRepository;
import com.lun.decor.repository.UploadImageRepo;
import com.lun.decor.service.*;
import com.lun.decor.service.dto.*;
import com.lun.decor.service.mapper.TrackProgressMapper;
import com.lun.decor.web.rest.errors.BadRequestAlertException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.*;
import javax.sound.midi.Track;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.lun.decor.domain.TrackProgress}.
 */
@RestController
@RequestMapping("/api")
public class TrackProgressResource {

    private final Logger log = LoggerFactory.getLogger(TrackProgressResource.class);

    private static final String ENTITY_NAME = "trackProgress";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Value("${client.web.baseURL}")
    private String clientUrl;

    private final RequestOrderService requestOrderService;

    private final OtpRepository otpRepository;

    private final MailService mailService;

    private final TrackProgressService trackProgressService;

    private final ProjctService projctService;

    private final TrackProgressRepository trackProgressRepository;

    private final ImageService imageService;

    public TrackProgressResource(
        TrackProgressService trackProgressService,
        TrackProgressRepository trackProgressRepository,
        ProjctService projctService,
        MailService mailService,
        ImageService imageService,
        OtpRepository otpRepository,
        RequestOrderService requestOrderService
    ) {
        this.trackProgressService = trackProgressService;
        this.trackProgressRepository = trackProgressRepository;
        this.projctService = projctService;
        this.mailService = mailService;
        this.imageService = imageService;
        this.otpRepository = otpRepository;
        this.requestOrderService = requestOrderService;
    }

    /**
     * {@code POST  /track-progresses} : Create a new trackProgress.
     *
     * @param trackProgressDTO the trackProgressDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new trackProgressDTO, or with status {@code 400 (Bad Request)} if the trackProgress has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/track-progresses")
    public ResponseEntity<TrackProgressDTO> createTrackProgress(@RequestBody TrackProgressDTO trackProgressDTO) throws URISyntaxException {
        log.debug("REST request to save TrackProgress : {}", trackProgressDTO);
        if (trackProgressDTO.getId() != null) {
            throw new BadRequestAlertException("A new trackProgress cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (!trackProgressDTO.getFiles().isEmpty()) {
            trackProgressDTO.setImages(imageService.createListImage(trackProgressDTO.getFiles()));
        }
        TrackProgressDTO result = trackProgressService.save(trackProgressDTO);
        return ResponseEntity
            .created(new URI("/api/track-progresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /track-progresses/:id} : Updates an existing trackProgress.
     *
     * @param id the id of the trackProgressDTO to save.
     * @param trackProgressDTO the trackProgressDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated trackProgressDTO,
     * or with status {@code 400 (Bad Request)} if the trackProgressDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the trackProgressDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/track-progresses/{id}")
    public ResponseEntity<TrackProgressDTO> updateTrackProgress(
        @PathVariable(value = "id", required = false) final String id,
        @RequestBody TrackProgressDTO trackProgressDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TrackProgress : {}, {}", id, trackProgressDTO);
        if (trackProgressDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, trackProgressDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!trackProgressRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        TrackProgress oldTrack = trackProgressRepository.findById(id).orElse(null);
        assert oldTrack != null;
        Projct projct = oldTrack.getProjct();
        ProjctDTO projctDTO = projctService.findOne(projct.getId()).orElse(null);
        if (projctDTO == null) {
            throw new BadRequestAlertException("Khong tim thay du an ", ENTITY_NAME, "invalidProjctOfTrack");
        }
        if (!Constants.STATUS_ACTIVE.equals(projctDTO.getStatus())) {
            throw new BadRequestAlertException("Dự án đang không triển khai, không thể cập nhật ", ENTITY_NAME, "projectIsNotActive");
        }
        List<TrackProgress> lstTrackProgress = trackProgressRepository.findByProjctId(projct.getId());
        for (TrackProgress trackProgress : lstTrackProgress) {
            if (
                trackProgress.getStep() == null ||
                trackProgressDTO.getStep() == null ||
                trackProgress.getDeadLine() == null ||
                trackProgressDTO.getDeadLine() == null
            ) {
                System.out.println("nothing");
            } else if (
                (
                    trackProgress.getStep() < trackProgressDTO.getStep() &&
                    trackProgress.getDeadLine().isAfter(trackProgressDTO.getDeadLine())
                ) ||
                trackProgress.getStep() > trackProgressDTO.getStep() &&
                trackProgress.getDeadLine().isBefore(trackProgressDTO.getDeadLine())
            ) {
                throw new BadRequestAlertException("Deadline bước sau không thể nhỏ hơn bước trước ", ENTITY_NAME, "deadlineInvalid");
            }
        }

        if (trackProgressDTO.getStatus().equals(Constants.STATUS_DONE)) {
            Integer maxStep = 0;
            for (TrackProgress trackProgress : lstTrackProgress) {
                if (trackProgress.getStep() < trackProgressDTO.getStep() && !trackProgress.getStatus().equals(Constants.STATUS_DONE)) {
                    throw new BadRequestAlertException("Buoc truoc do chua hoan thanh", ENTITY_NAME, "invalidStatusTrack");
                }
                if (maxStep < trackProgress.getStep()) {
                    maxStep = trackProgress.getStep();
                }
            }

            projctDTO.setProgress((trackProgressDTO.getStep() * 1.0) / (maxStep * 1.0));
            projctService.save(projctDTO);
        }

        if (!trackProgressDTO.getFiles().isEmpty()) {
            trackProgressDTO.getImages().addAll(imageService.createListImage(trackProgressDTO.getFiles()));
        }

        TrackProgressDTO result = trackProgressService.save(trackProgressDTO);
        if (trackProgressDTO.getClientEnable()) {
            try {
                RequestOrderDTO requestOrderDTO = requestOrderService.findOne(projct.getRequestOrder().getId()).get();
                String otpCode = RandomStringUtils.randomNumeric(6);
                Otp otp = new Otp(otpCode);
                otp = otpRepository.save(otp);
                String url =
                    clientUrl +
                    "/tracking-order?id=" +
                    projct.getRequestOrder().getId() +
                    "&phone=" +
                    projct.getCustomer().getPhone() +
                    "&otp=" +
                    otpCode +
                    "&otp_id=" +
                    otp.getId();
                mailService.sendConfirmEmail(projct.getCustomer().getEmail(), requestOrderDTO, url);
            } catch (Exception ignored) {}
        }
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, trackProgressDTO.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /track-progresses/:id} : Partial updates given fields of an existing trackProgress, field will ignore if it is null
     *
     * @param id the id of the trackProgressDTO to save.
     * @param trackProgressDTO the trackProgressDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated trackProgressDTO,
     * or with status {@code 400 (Bad Request)} if the trackProgressDTO is not valid,
     * or with status {@code 404 (Not Found)} if the trackProgressDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the trackProgressDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/track-progresses/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<TrackProgressDTO> partialUpdateTrackProgress(
        @PathVariable(value = "id", required = false) final String id,
        @RequestBody TrackProgressDTO trackProgressDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update TrackProgress partially : {}, {}", id, trackProgressDTO);
        if (trackProgressDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, trackProgressDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!trackProgressRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TrackProgressDTO> result = trackProgressService.partialUpdate(trackProgressDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, trackProgressDTO.getId())
        );
    }

    /**
     * {@code GET  /track-progresses} : get all the trackProgresses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of trackProgresses in body.
     */
    @GetMapping("/track-progresses")
    public ResponseEntity<List<TrackProgressDTO>> getAllTrackProgresses(Pageable pageable) {
        log.debug("REST request to get a page of TrackProgresses");
        Page<TrackProgressDTO> page = trackProgressService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /track-progresses/:id} : get the "id" trackProgress.
     *
     * @param id the id of the trackProgressDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the trackProgressDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/track-progresses/{id}")
    public ResponseEntity<TrackProgressDTO> getTrackProgress(@PathVariable String id) {
        log.debug("REST request to get TrackProgress : {}", id);
        Optional<TrackProgressDTO> trackProgressDTO = trackProgressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(trackProgressDTO);
    }

    /**
     * {@code DELETE  /track-progresses/:id} : delete the "id" trackProgress.
     *
     * @param id the id of the trackProgressDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/track-progresses/{id}")
    public ResponseEntity<Void> deleteTrackProgress(@PathVariable String id) {
        log.debug("REST request to delete TrackProgress : {}", id);
        trackProgressService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    @GetMapping("/track-pregresses/projct/{projctId}")
    public ResponseEntity<List<TrackProgress>> findByProjctId(@PathVariable String projctId) {
        List<TrackProgress> trackProgress = trackProgressService.findALlByProjectId(projctId);
        return ResponseEntity.ok().body(trackProgress);
    }

    @PostMapping("/track-progresses/projct")
    public ResponseEntity<ProjctDTO> createTrackProgressInProject(@RequestBody TrackProgressDTO trackProgressDTO)
        throws URISyntaxException {
        log.debug("REST request to save TrackProgress in project : {}", trackProgressDTO);

        if (trackProgressDTO.getProjct() == null) {
            throw new BadRequestAlertException("Thieu thong tin projct", ENTITY_NAME, "projctnull");
        }
        Integer maxStep = 0;
        Integer maxStepDone = 0;
        List<TrackProgress> lstOldTrack = trackProgressService.findALlByProjectId(trackProgressDTO.getProjct().getId());
        if (lstOldTrack == null || lstOldTrack.size() == 0) {
            trackProgressDTO.setStep(1);
        } else {
            if (trackProgressDTO.getStep() < 0 || trackProgressDTO.getStep() > lstOldTrack.size()) {
                throw new BadRequestAlertException("Step khong hop le", ENTITY_NAME, "invalidStep");
            }

            for (TrackProgress progress : lstOldTrack) {
                if (progress.getStep() >= trackProgressDTO.getStep()) {
                    if (Constants.STATUS_DOING.equals(progress.getStatus()) || Constants.STATUS_DONE.equals(progress.getStatus())) {
                        throw new BadRequestAlertException("Bước sau đó đã hoàn thành", ENTITY_NAME, "nextStepIsDone");
                    }
                    progress.setStep(progress.getStep() + 1);
                    if (maxStep < progress.getStep()) {
                        maxStep = progress.getStep();
                    }
                } else {
                    if (Constants.STATUS_DONE.equals(progress.getStatus()) && maxStepDone < progress.getStep()) {
                        maxStepDone = progress.getStep();
                    }
                }
            }
            trackProgressRepository.saveAll(lstOldTrack);
        }

        ProjctDTO projctDTO = projctService.findOne(trackProgressDTO.getProjct().getId()).orElse(null);
        if (projctDTO == null) {
            throw new BadRequestAlertException("Thieu thong tin projct", ENTITY_NAME, "projctnull");
        }
        TrackProgressDTO result = trackProgressService.save(trackProgressDTO);
        if (projctDTO.getStages() != null && projctDTO.getStages().size() > 0) {
            projctDTO.getStages().add(result);
            projctDTO.setProgress(0D);
        } else {
            Set<TrackProgressDTO> trackProgresses = new HashSet<>();
            trackProgresses.add(result);
            projctDTO.setStages(trackProgresses);
            projctDTO.setProgress((maxStepDone * 1.0) / (maxStep * 1.0));
        }

        projctService.save(projctDTO);

        return ResponseEntity.ok().body(projctDTO);
    }
}
