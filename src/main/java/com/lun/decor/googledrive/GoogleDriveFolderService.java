package com.lun.decor.googledrive;

import com.google.api.services.drive.model.File;
import com.lun.decor.googledrive.GoogleFileManager;
import com.lun.decor.googledrive.IGoogleDriveFolder;
import com.lun.decor.service.dto.GoogleDriveFoldersDTO;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GoogleDriveFolderService implements IGoogleDriveFolder {

    @Autowired
    GoogleFileManager googleFileManager;

    @Value("${google.drive.prefix-folder}")
    private String PREFIX_FOLDER;

    @Override
    public List<GoogleDriveFoldersDTO> getAllFolder() throws IOException, GeneralSecurityException {
        List<File> files = googleFileManager.listFolderContent(googleFileManager.ROOT_FOLDER);
        List<GoogleDriveFoldersDTO> responseList = null;
        GoogleDriveFoldersDTO dto = null;

        if (files != null) {
            responseList = new ArrayList<>();
            for (File f : files) {
                dto = new GoogleDriveFoldersDTO();
                dto.setId(f.getId());
                dto.setName(f.getName());
                dto.setLink(PREFIX_FOLDER + f.getId());
                responseList.add(dto);
            }
        }
        return responseList;
    }

    @Override
    public void createFolder(String folderName) throws Exception {
        String folderId = googleFileManager.getFolderId(folderName);
        System.out.println(folderId);
    }

    @Override
    public void deleteFolder(String id) throws Exception {
        googleFileManager.deleteFileOrFolder(id);
    }
}
