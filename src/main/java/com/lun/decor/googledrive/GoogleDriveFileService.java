package com.lun.decor.googledrive;

import com.google.api.services.drive.model.File;
import com.lun.decor.googledrive.GoogleFileManager;
import com.lun.decor.googledrive.IGoogleDriveFile;
import com.lun.decor.service.dto.GoogleDriveFileDTO;
import com.lun.decor.utils.ConvertByteToMB;
import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GoogleDriveFileService implements IGoogleDriveFile {

    @Autowired
    GoogleFileManager googleFileManager;

    @Value("${google.drive.prefix-image}")
    private String PREFIX_IMAGE_VIEW;

    @Value("${google.drive.prefix-file}")
    private String PREFIX_FILE_VIEW;

    @Value("${google.drive.subfix-file}")
    private String SUBFIX_FILE_VIEW;

    @Override
    public List<GoogleDriveFileDTO> getAllFile() throws IOException, GeneralSecurityException {
        List<GoogleDriveFileDTO> responseList = null;
        List<File> files = googleFileManager.listEverything();
        GoogleDriveFileDTO dto = null;

        if (files != null) {
            responseList = new ArrayList<>();
            for (File f : files) {
                dto = new GoogleDriveFileDTO();
                if (f.getSize() != null) {
                    dto.setId(f.getId());
                    dto.setName(f.getName());
                    dto.setThumbnailLink(f.getThumbnailLink());
                    dto.setSize(ConvertByteToMB.getSize(f.getSize()));
                    dto.setLink(PREFIX_FILE_VIEW + f.getId() + SUBFIX_FILE_VIEW);
                    dto.setShared(f.getShared());

                    responseList.add(dto);
                }
            }
        }
        return responseList;
    }

    @Override
    public void deleteFile(String id) throws Exception {
        googleFileManager.deleteFileOrFolder(id);
    }

    @Override
    public String uploadFile(java.io.File file, String filePath, String fileContentType, boolean isPublic) {
        String type = "";
        String role = "";
        if (isPublic) {
            // Public file of folder for everyone
            type = "anyone";
            role = "reader";
        } else {
            // Private
            type = "private";
            role = "private";
        }
        String id = googleFileManager.uploadFile(file, filePath, type, role, fileContentType);
        return PREFIX_IMAGE_VIEW + id;
    }

    @Override
    public void downloadFile(String id, OutputStream outputStream) throws IOException, GeneralSecurityException {
        googleFileManager.downloadFile(id, outputStream);
    }
}
