package com.lun.decor.googledrive;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GoogleDriveConfig {

    @Value("${jhipster.clientApp.name}")
    private String APPLICATION_NAME; // Application name

    @Value("${google.drive.service.account}")
    private String GOOGLE_ACCOUNT; //  account

    @Value("${google.drive.service.key-file}")
    private String GOOGLE_KEY; //  KEY

    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    //private static final String TOKENS_DIRECTORY_PATH = "tokens";
    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);

    //private static final String CREDENTIALS_FILE_PATH = "/cred.json"; // path file Google Drive Service

    public Drive getInstance() throws GeneralSecurityException, IOException {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, googleCredential()).setApplicationName(APPLICATION_NAME).build();
        return service;
    }

    /**
     * Creates an authorized Credential object.
     *
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */

    public GoogleCredential googleCredential() throws GeneralSecurityException, IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory jsonFactory = new GsonFactory();
        return new GoogleCredential.Builder()
            .setTransport(httpTransport)
            .setJsonFactory(jsonFactory)
            .setServiceAccountId(GOOGLE_ACCOUNT)
            .setServiceAccountScopes(SCOPES)
            .setServiceAccountPrivateKeyFromP12File(GoogleDriveConfig.class.getResourceAsStream(GOOGLE_KEY))
            .build();
    }
}
