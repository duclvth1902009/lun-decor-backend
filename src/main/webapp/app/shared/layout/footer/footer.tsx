import './footer.scss';

import React from 'react';
import { Translate } from 'react-jhipster';
import { Col, Navbar, Row } from 'reactstrap';

const Footer = props => (
  <div className="bg-secondary footer page-content">
    <Row>
      <Col md="6">
        <div className="footer-copyright text-center py-3">
          <p>
            <Translate contentKey="footer">Your footer</Translate>
          </p>
        </div>
      </Col>
      <Col md="6">
        <div className="footer-copyright text-center py-3">
          © 2021 Copyright:
          <a href="/" style={{ color: '#fff' }}>
            {' '}
            LunDecor.com
          </a>
        </div>
      </Col>
    </Row>
  </div>
);

export default Footer;
