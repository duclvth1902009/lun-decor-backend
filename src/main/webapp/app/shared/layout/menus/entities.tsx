import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { Translate, translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.entities.main')}
    id="entity-menu"
    data-cy="entity"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="asterisk" to="/image">
      <Translate contentKey="global.menu.entities.image" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/partner">
      <Translate contentKey="global.menu.entities.partner" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/item">
      <Translate contentKey="global.menu.entities.item" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/customer">
      <Translate contentKey="global.menu.entities.customer" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/comment">
      <Translate contentKey="global.menu.entities.comment" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/design">
      <Translate contentKey="global.menu.entities.design" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/topic">
      <Translate contentKey="global.menu.entities.topic" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/product">
      <Translate contentKey="global.menu.entities.product" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/service-pkg">
      <Translate contentKey="global.menu.entities.servicePkg" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/pack">
      <Translate contentKey="global.menu.entities.pack" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/request-order">
      <Translate contentKey="global.menu.entities.requestOrder" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/event-type">
      <Translate contentKey="global.menu.entities.eventType" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/event-venue">
      <Translate contentKey="global.menu.entities.eventVenue" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/projct">
      <Translate contentKey="global.menu.entities.projct" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/track-progress">
      <Translate contentKey="global.menu.entities.trackProgress" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/task">
      <Translate contentKey="global.menu.entities.task" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/transactn">
      <Translate contentKey="global.menu.entities.transactn" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/otp">
      <Translate contentKey="global.menu.entities.otp" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
