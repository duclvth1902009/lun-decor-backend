import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { Translate, translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const ManageMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.manage.main')}
    id="manage-menu"
    data-cy="manage"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="asterisk" to="/manage/projct">
      <Translate contentKey="global.menu.manage.projct" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/manage/request-order">
      <Translate contentKey="global.menu.manage.request-order" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
