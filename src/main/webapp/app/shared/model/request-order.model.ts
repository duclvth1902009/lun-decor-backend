import dayjs from 'dayjs';
import { IPack } from 'app/shared/model/pack.model';
import { IImage } from 'app/shared/model/image.model';
import { ITopic } from 'app/shared/model/topic.model';
import { IProduct } from 'app/shared/model/product.model';
import { IServicePkg } from 'app/shared/model/service-pkg.model';
import { IEventType } from 'app/shared/model/event-type.model';
import { IEventVenue } from 'app/shared/model/event-venue.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface IRequestOrder {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  phone?: string;
  email?: string | null;
  cusName?: string | null;
  discount?: string | null;
  about?: string | null;
  holdTime?: string | null;
  budget?: string | null;
  address?: string | null;
  guestNum?: string | null;
  description?: string | null;
  status?: string | null;
  pack?: IPack | null;
  images?: IImage[] | null;
  topics?: ITopic[] | null;
  add_products?: IProduct[] | null;
  sub_products?: IProduct[] | null;
  add_services?: IServicePkg[] | null;
  sub_services?: IServicePkg[] | null;
  type?: IEventType | null;
  eventVenue?: IEventVenue | null;
  files?: IImageTransfer[] | null;
}

export const defaultValue: Readonly<IRequestOrder> = {};
