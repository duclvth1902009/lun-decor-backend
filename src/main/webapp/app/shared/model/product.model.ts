import { IImage } from 'app/shared/model/image.model';
import { IItem } from 'app/shared/model/item.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface IProduct {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  name?: string | null;
  code?: string;
  price?: number | null;
  description?: string | null;
  unit?: string | null;
  images?: IImage[] | null;
  items?: IItem[] | null;
  files?: IImageTransfer[] | null;
}

export const defaultValue: Readonly<IProduct> = {};
