export interface IEventType {
  id?: string;
  name?: string;
}

export const defaultValue: Readonly<IEventType> = {};
