import { IImage } from 'app/shared/model/image.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface IEventVenue {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  name?: string | null;
  address?: string | null;
  location?: string | null;
  price_from?: number | null;
  price_to?: number | null;
  phone?: string;
  email?: string | null;
  description?: string | null;
  quantity_max?: number | null;
  note?: string | null;
  images?: IImage[] | null;
  files?: IImageTransfer[] | null;
}

export const defaultValue: Readonly<IEventVenue> = {};
