import { IImage } from 'app/shared/model/image.model';
import { IPartner } from 'app/shared/model/partner.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface IItem {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  name?: string | null;
  code?: string;
  type?: number | null;
  price?: number | null;
  description?: string | null;
  unit?: string | null;
  images?: IImage[] | null;
  providers?: IPartner[] | null;
  files?: IImageTransfer[] | null;
}

export const defaultValue: Readonly<IItem> = {};
