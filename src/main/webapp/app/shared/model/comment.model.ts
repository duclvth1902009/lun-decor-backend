import { ICustomer } from 'app/shared/model/customer.model';
import { IProjct } from 'app/shared/model/projct.model';

export interface IComment {
  id?: string;
  content?: string | null;
  parent_id?: string | null;
  from?: ICustomer | null;
  parent?: IComment | null;
  projct?: IProjct | null;
}

export const defaultValue: Readonly<IComment> = {};
