import dayjs from 'dayjs';

export interface IOtp {
  id?: string;
  otp_code?: string | null;
  expired_time?: string | null;
  status?: number | null;
}

export const defaultValue: Readonly<IOtp> = {};
