export interface ICustomer {
  id?: string;
  name?: string | null;
  phone?: string;
  email?: string | null;
  username?: string | null;
  password?: string | null;
}

export const defaultValue: Readonly<ICustomer> = {};
