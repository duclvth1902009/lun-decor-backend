export interface IPartner {
  id?: string;
  name?: string | null;
  phone?: string;
  email?: string | null;
  location?: string | null;
  bank?: string | null;
  note?: string | null;
}

export const defaultValue: Readonly<IPartner> = {};
