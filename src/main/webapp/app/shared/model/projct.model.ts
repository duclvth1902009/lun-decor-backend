import dayjs from 'dayjs';
import { ITopic } from 'app/shared/model/topic.model';
import { IEventType } from 'app/shared/model/event-type.model';
import { IEventVenue } from 'app/shared/model/event-venue.model';
import { IRequestOrder } from 'app/shared/model/request-order.model';
import { ICustomer } from 'app/shared/model/customer.model';
import { IPack } from 'app/shared/model/pack.model';
import { IDesign } from 'app/shared/model/design.model';
import { IComment } from 'app/shared/model/comment.model';
import { IImage } from 'app/shared/model/image.model';
import { ITrackProgress } from 'app/shared/model/track-progress.model';
import { ITask } from 'app/shared/model/task.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface IProjct {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  holdTime?: string | null;
  location?: string | null;
  contractContentType?: string | null;
  contract?: string | null;
  rate?: number | null;
  preCapital?: number | null;
  preTotal?: number | null;
  finCapital?: number | null;
  finTotal?: number | null;
  rschangeCap?: string | null;
  rschangeTot?: string | null;
  status?: string | null;
  name?: string | null;
  topics?: ITopic[] | null;
  type?: IEventType | null;
  eventVenue?: IEventVenue | null;
  requestOrder?: IRequestOrder | null;
  customer?: ICustomer | null;
  packs?: IPack[] | null;
  final_design?: IDesign | null;
  designs?: IDesign[] | null;
  comments?: IComment[] | null;
  images?: IImage[] | null;
  stages?: ITrackProgress[] | null;
  tasks?: ITask[] | null;
  files?: IImageTransfer[] | null;
  progress?: number | null;
}

export const defaultValue: Readonly<IProjct> = {};
