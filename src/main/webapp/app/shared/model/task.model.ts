import dayjs from 'dayjs';
import { IProjct } from 'app/shared/model/projct.model';
import { ITrackProgress } from 'app/shared/model/track-progress.model';
import { IImage } from 'app/shared/model/image.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface ITask {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  name?: string | null;
  detail?: string | null;
  total?: number | null;
  note?: string | null;
  scope?: string | null;
  status?: string | null;
  startTime?: string | null;
  deadLine?: string | null;
  projct?: IProjct | null;
  stage?: ITrackProgress | null;
  images?: IImage[] | null;
  files?: IImageTransfer[] | null;
}

export const defaultValue: Readonly<ITask> = {};
