import { IProjct } from 'app/shared/model/projct.model';
import { ITrackProgress } from 'app/shared/model/track-progress.model';
import { ITask } from 'app/shared/model/task.model';
import { IPartner } from 'app/shared/model/partner.model';
import { ICustomer } from 'app/shared/model/customer.model';
import { IImage } from 'app/shared/model/image.model';

export interface ITransactn {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  detail?: string | null;
  total?: number | null;
  note?: string | null;
  status?: string | null;
  projct?: IProjct | null;
  stage?: ITrackProgress | null;
  atTask?: ITask | null;
  provider?: IPartner | null;
  customer?: ICustomer | null;
  image?: IImage | null;
}

export const defaultValue: Readonly<ITransactn> = {};
