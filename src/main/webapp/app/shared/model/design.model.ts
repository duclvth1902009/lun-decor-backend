import { IImage } from 'app/shared/model/image.model';
import { IProduct } from 'app/shared/model/product.model';
import { ITopic } from 'app/shared/model/topic.model';
import { IProjct } from 'app/shared/model/projct.model';

export interface IDesign {
  id?: string;
  fileContentType?: string;
  file?: string;
  imgQuickViewContentType?: string;
  imgQuickView?: string;
  quickView?: string;
  width?: number | null;
  height?: number | null;
  note?: string | null;
  images?: IImage[] | null;
  products?: IProduct[] | null;
  topics?: ITopic[] | null;
  of_prjct?: IProjct | null;
  fileUrl?: string;
}

export const defaultValue: Readonly<IDesign> = {};
