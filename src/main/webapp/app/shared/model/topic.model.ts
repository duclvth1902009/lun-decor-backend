import { IImage } from 'app/shared/model/image.model';
import { IEventType } from 'app/shared/model/event-type.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface ITopic {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  name?: string | null;
  tags?: string | null;
  images?: IImage[] | null;
  type?: IEventType | null;
  files?: IImageTransfer[] | null;
}

export const defaultValue: Readonly<ITopic> = {};
