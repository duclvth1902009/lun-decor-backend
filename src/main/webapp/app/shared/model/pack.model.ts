import { IImage } from 'app/shared/model/image.model';
import { IProduct } from 'app/shared/model/product.model';
import { IServicePkg } from 'app/shared/model/service-pkg.model';
import { IEventType } from 'app/shared/model/event-type.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface IPack {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  name?: string | null;
  price_from?: number | null;
  price_to?: number | null;
  description?: string | null;
  images?: IImage[] | null;
  products?: IProduct[] | null;
  services?: IServicePkg[] | null;
  type?: IEventType | null;
  files?: IImageTransfer[] | null;
}

export const defaultValue: Readonly<IPack> = {};
