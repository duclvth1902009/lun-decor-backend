export interface IImageTransfer {
  fileContentType?: string | null;
  file?: string | null;
}

export const defaultValue: Readonly<IImageTransfer> = {};
