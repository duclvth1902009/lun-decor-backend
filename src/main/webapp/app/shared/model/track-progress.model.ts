import dayjs from 'dayjs';
import { IProjct } from 'app/shared/model/projct.model';
import { ITask } from 'app/shared/model/task.model';
import { IImage } from 'app/shared/model/image.model';
import { IImageTransfer } from 'app/shared/model/image-transfer.model';

export interface ITrackProgress {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  name?: string | null;
  description?: string | null;
  clientEnable?: boolean | null;
  step?: number | null;
  deadLine?: string | null;
  status?: string | null;
  projct?: IProjct | null;
  tasks?: ITask[] | null;
  images?: IImage[] | null;
  files?: IImageTransfer[] | null;
}

export const defaultValue: Readonly<ITrackProgress> = {
  clientEnable: false,
};
