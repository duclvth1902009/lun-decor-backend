import { IEventVenue } from 'app/shared/model/event-venue.model';
import { IItem } from 'app/shared/model/item.model';
import { IProduct } from 'app/shared/model/product.model';
import { IServicePkg } from 'app/shared/model/service-pkg.model';
import { IPack } from 'app/shared/model/pack.model';
import { IDesign } from 'app/shared/model/design.model';
import { IRequestOrder } from 'app/shared/model/request-order.model';
import { IProjct } from 'app/shared/model/projct.model';
import { ITrackProgress } from 'app/shared/model/track-progress.model';
import { ITask } from 'app/shared/model/task.model';

export interface IImage {
  id?: string;
  fileContentType?: string | null;
  file?: string | null;
  url?: string | null;
  type?: string | null;
  feature?: string | null;
  note?: string | null;
  eventVenue?: IEventVenue | null;
  items?: IItem[] | null;
  products?: IProduct[] | null;
  servicePkgs?: IServicePkg[] | null;
  packs?: IPack[] | null;
  designs?: IDesign[] | null;
  orders?: IRequestOrder[] | null;
  projct?: IProjct | null;
  trackProgress?: ITrackProgress | null;
  task?: ITask | null;
}

export const defaultValue: Readonly<IImage> = {};
