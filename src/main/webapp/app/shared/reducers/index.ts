import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import image, {
  ImageState
} from 'app/entities/image/image.reducer';
// prettier-ignore
import partner, {
  PartnerState
} from 'app/entities/partner/partner.reducer';
// prettier-ignore
import item, {
  ItemState
} from 'app/entities/item/item.reducer';
// prettier-ignore
import customer, {
  CustomerState
} from 'app/entities/customer/customer.reducer';
// prettier-ignore
import comment, {
  CommentState
} from 'app/entities/comment/comment.reducer';
// prettier-ignore
import design, {
  DesignState
} from 'app/entities/design/design.reducer';
// prettier-ignore
import topic, {
  TopicState
} from 'app/entities/topic/topic.reducer';
// prettier-ignore
import product, {
  ProductState
} from 'app/entities/product/product.reducer';
// prettier-ignore
import servicePkg, {
  ServicePkgState
} from 'app/entities/service-pkg/service-pkg.reducer';
// prettier-ignore
import pack, {
  PackState
} from 'app/entities/pack/pack.reducer';
// prettier-ignore
import requestOrder, {
  RequestOrderState
} from 'app/entities/request-order/request-order.reducer';
// prettier-ignore
import eventType, {
  EventTypeState
} from 'app/entities/event-type/event-type.reducer';
// prettier-ignore
import eventVenue, {
  EventVenueState
} from 'app/entities/event-venue/event-venue.reducer';
// prettier-ignore
import projct, {
  ProjctState
} from 'app/entities/projct/projct.reducer';
// prettier-ignore
import trackProgress, {
  TrackProgressState
} from 'app/entities/track-progress/track-progress.reducer';
// prettier-ignore
import task, {
  TaskState
} from 'app/entities/task/task.reducer';
// prettier-ignore
import transactn, {
  TransactnState
} from 'app/entities/transactn/transactn.reducer';
// prettier-ignore
import otp, {
  OtpState
} from 'app/entities/otp/otp.reducer';
import manageProject, { ManageProjectState } from 'app/manage/projct/projct.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly image: ImageState;
  readonly partner: PartnerState;
  readonly item: ItemState;
  readonly customer: CustomerState;
  readonly comment: CommentState;
  readonly design: DesignState;
  readonly topic: TopicState;
  readonly product: ProductState;
  readonly servicePkg: ServicePkgState;
  readonly pack: PackState;
  readonly requestOrder: RequestOrderState;
  readonly eventType: EventTypeState;
  readonly eventVenue: EventVenueState;
  readonly projct: ProjctState;
  readonly trackProgress: TrackProgressState;
  readonly task: TaskState;
  readonly transactn: TransactnState;
  readonly otp: OtpState;
  readonly manageProject: ManageProjectState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  image,
  partner,
  item,
  customer,
  comment,
  design,
  topic,
  product,
  servicePkg,
  pack,
  requestOrder,
  eventType,
  eventVenue,
  projct,
  trackProgress,
  task,
  transactn,
  otp,
  manageProject,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
