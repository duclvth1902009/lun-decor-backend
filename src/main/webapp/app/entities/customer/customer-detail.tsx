import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './customer.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICustomerDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CustomerDetail = (props: ICustomerDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { customerEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="customerDetailsHeading">
          <Translate contentKey="lunBackendApp.customer.detail.title">Customer</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{customerEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.customer.name">Name</Translate>
                </span>
              </dt>
              <dd>{customerEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="phone">
                  <Translate contentKey="lunBackendApp.customer.phone">Phone</Translate>
                </span>
              </dt>
              <dd>{customerEntity.phone}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="email">
                  <Translate contentKey="lunBackendApp.customer.email">Email</Translate>
                </span>
              </dt>
              <dd>{customerEntity.email}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="username">
                  <Translate contentKey="lunBackendApp.customer.username">Username</Translate>
                </span>
              </dt>
              <dd>{customerEntity.username}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="password">
                  <Translate contentKey="lunBackendApp.customer.password">Password</Translate>
                </span>
              </dt>
              <dd>{customerEntity.password}</dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/customer" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/customer/${customerEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ customer }: IRootState) => ({
  customerEntity: customer.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CustomerDetail);
