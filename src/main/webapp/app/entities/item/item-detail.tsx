import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './item.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IItemDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ItemDetail = (props: IItemDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { itemEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="itemDetailsHeading">
          <Translate contentKey="lunBackendApp.item.detail.title">Item</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{itemEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.item.name">Name</Translate>
                </span>
              </dt>
              <dd>{itemEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="code">
                  <Translate contentKey="lunBackendApp.item.code">Code</Translate>
                </span>
              </dt>
              <dd>{itemEntity.code}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="type">
                  <Translate contentKey="lunBackendApp.item.type">Type</Translate>
                </span>
              </dt>
              <dd>{itemEntity.type}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="price">
                  <Translate contentKey="lunBackendApp.item.price">Price</Translate>
                </span>
              </dt>
              <dd>{itemEntity.price}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="description">
                  <Translate contentKey="lunBackendApp.item.description">Description</Translate>
                </span>
              </dt>
              <dd>{itemEntity.description}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="unit">
                  <Translate contentKey="lunBackendApp.item.unit">Unit</Translate>
                </span>
              </dt>
              <dd>{itemEntity.unit}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.item.images">Images</Translate>
              </dt>
              <dd>
                {itemEntity.images
                  ? itemEntity.images.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.url}</a>
                        {itemEntity.images && i === itemEntity.images.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.item.providers">Providers</Translate>
              </dt>
              <dd>
                {itemEntity.providers
                  ? itemEntity.providers.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.name}</a>
                        {itemEntity.providers && i === itemEntity.providers.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/item" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/item/${itemEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ item }: IRootState) => ({
  itemEntity: item.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetail);
