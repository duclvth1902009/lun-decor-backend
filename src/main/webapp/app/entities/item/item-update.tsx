import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getImages } from 'app/entities/image/image.reducer';
import { getEntities as getPartners } from 'app/entities/partner/partner.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './item.reducer';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IItemUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ItemUpdate = (props: IItemUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { itemEntity, partners, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/item' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getImages();
    props.getPartners();
  }, []);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...itemEntity,
        ...values,
        files: fileList,
        providers: mapIdList(values.providers),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.item.home.createOrEditLabel" data-cy="ItemCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.item.home.createOrEditLabel">Create or edit a Item</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : itemEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="item-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="item-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.item.file">File</Translate>
                  </Label>
                  <Row>{renderPhotos(fileList)}</Row>
                  <br />
                  <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
                </AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.item.images">File</Translate>
                  </Label>
                  <Row>
                    {itemEntity.images
                      ? itemEntity.images.map(otherEntity => (
                          <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                        ))
                      : null}
                  </Row>
                  <br />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="item-name">
                  <Translate contentKey="lunBackendApp.item.name">Name</Translate>
                </Label>
                <AvField id="item-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="codeLabel" for="item-code">
                  <Translate contentKey="lunBackendApp.item.code">Code</Translate>
                </Label>
                <AvField
                  id="item-code"
                  data-cy="code"
                  type="text"
                  name="code"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="typeLabel" for="item-type">
                  <Translate contentKey="lunBackendApp.item.type">Type</Translate>
                </Label>
                <AvField id="item-type" data-cy="type" type="string" className="form-control" name="type" />
              </AvGroup>
              <AvGroup>
                <Label id="priceLabel" for="item-price">
                  <Translate contentKey="lunBackendApp.item.price">Price</Translate>
                </Label>
                <AvField id="item-price" data-cy="price" type="string" className="form-control" name="price" />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="item-description">
                  <Translate contentKey="lunBackendApp.item.description">Description</Translate>
                </Label>
                <AvField id="item-description" data-cy="description" type="text" name="description" />
              </AvGroup>
              <AvGroup>
                <Label id="unitLabel" for="item-unit">
                  <Translate contentKey="lunBackendApp.item.unit">Unit</Translate>
                </Label>
                <AvField id="item-unit" data-cy="unit" type="text" name="unit" />
              </AvGroup>
              <AvGroup>
                <Label for="item-providers">
                  <Translate contentKey="lunBackendApp.item.providers">Providers</Translate>
                </Label>
                <AvInput
                  id="item-providers"
                  data-cy="providers"
                  type="select"
                  multiple
                  className="form-control"
                  name="providers"
                  value={!isNew && itemEntity.providers && itemEntity.providers.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {partners
                    ? partners.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/item" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  partners: storeState.partner.entities,
  itemEntity: storeState.item.entity,
  loading: storeState.item.loading,
  updating: storeState.item.updating,
  updateSuccess: storeState.item.updateSuccess,
});

const mapDispatchToProps = {
  getImages,
  getPartners,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ItemUpdate);
