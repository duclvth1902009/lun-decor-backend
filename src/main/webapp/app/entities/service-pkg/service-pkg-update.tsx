import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { setFileData, openFile, byteSize, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getImages } from 'app/entities/image/image.reducer';
import { getEntities as getPartners } from 'app/entities/partner/partner.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './service-pkg.reducer';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IServicePkgUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ServicePkgUpdate = (props: IServicePkgUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { servicePkgEntity, partners, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/service-pkg' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
    props.getPartners();
  }, []);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...servicePkgEntity,
        ...values,
        files: fileList,
        providers: mapIdList(values.providers),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.servicePkg.home.createOrEditLabel" data-cy="ServicePkgCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.servicePkg.home.createOrEditLabel">Create or edit a ServicePkg</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : servicePkgEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="service-pkg-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="service-pkg-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="fileLabel" for="file_file">
                  <Translate contentKey="lunBackendApp.servicePkg.file">File</Translate>
                </Label>
                <br />
                <Row>{renderPhotos(fileList)}</Row>
                <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
              </AvGroup>
              <AvGroup>
                <Label id="fileLabel" for="file">
                  <Translate contentKey="lunBackendApp.servicePkg.images">File</Translate>
                </Label>
                {servicePkgEntity.images
                  ? servicePkgEntity.images.map(otherEntity => (
                      <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                    ))
                  : null}
                <br />
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="service-pkg-name">
                  <Translate contentKey="lunBackendApp.servicePkg.name">Name</Translate>
                </Label>
                <AvField id="service-pkg-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="codeLabel" for="service-pkg-code">
                  <Translate contentKey="lunBackendApp.servicePkg.code">Code</Translate>
                </Label>
                <AvField
                  id="service-pkg-code"
                  data-cy="code"
                  type="text"
                  name="code"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="priceLabel" for="service-pkg-price">
                  <Translate contentKey="lunBackendApp.servicePkg.price">Price</Translate>
                </Label>
                <AvField id="service-pkg-price" data-cy="price" type="string" className="form-control" name="price" />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="service-pkg-description">
                  <Translate contentKey="lunBackendApp.servicePkg.description">Description</Translate>
                </Label>
                <AvField id="service-pkg-description" data-cy="description" type="text" name="description" />
              </AvGroup>
              <AvGroup>
                <Label for="service-pkg-providers">
                  <Translate contentKey="lunBackendApp.servicePkg.providers">Providers</Translate>
                </Label>
                <AvInput
                  id="service-pkg-providers"
                  data-cy="providers"
                  type="select"
                  multiple
                  className="form-control"
                  name="providers"
                  value={!isNew && servicePkgEntity.providers && servicePkgEntity.providers.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {partners
                    ? partners.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/service-pkg" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  partners: storeState.partner.entities,
  servicePkgEntity: storeState.servicePkg.entity,
  loading: storeState.servicePkg.loading,
  updating: storeState.servicePkg.updating,
  updateSuccess: storeState.servicePkg.updateSuccess,
});

const mapDispatchToProps = {
  getPartners,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ServicePkgUpdate);
