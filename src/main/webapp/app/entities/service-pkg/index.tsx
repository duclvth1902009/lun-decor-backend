import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ServicePkg from './service-pkg';
import ServicePkgDetail from './service-pkg-detail';
import ServicePkgUpdate from './service-pkg-update';
import ServicePkgDeleteDialog from './service-pkg-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ServicePkgUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ServicePkgUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ServicePkgDetail} />
      <ErrorBoundaryRoute path={match.url} component={ServicePkg} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ServicePkgDeleteDialog} />
  </>
);

export default Routes;
