import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './service-pkg.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IServicePkgDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ServicePkgDetail = (props: IServicePkgDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { servicePkgEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="servicePkgDetailsHeading">
          <Translate contentKey="lunBackendApp.servicePkg.detail.title">ServicePkg</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{servicePkgEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.servicePkg.name">Name</Translate>
                </span>
              </dt>
              <dd>{servicePkgEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="code">
                  <Translate contentKey="lunBackendApp.servicePkg.code">Code</Translate>
                </span>
              </dt>
              <dd>{servicePkgEntity.code}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="price">
                  <Translate contentKey="lunBackendApp.servicePkg.price">Price</Translate>
                </span>
              </dt>
              <dd>{servicePkgEntity.price}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="description">
                  <Translate contentKey="lunBackendApp.servicePkg.description">Description</Translate>
                </span>
              </dt>
              <dd>{servicePkgEntity.description}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.servicePkg.images">Images</Translate>
              </dt>
              <dd>
                {servicePkgEntity.images
                  ? servicePkgEntity.images.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.url}</a>
                        {servicePkgEntity.images && i === servicePkgEntity.images.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.servicePkg.providers">Providers</Translate>
              </dt>
              <dd>
                {servicePkgEntity.providers
                  ? servicePkgEntity.providers.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.name}</a>
                        <a href={`partner/${val.id}`}>{val.name}</a>
                        {servicePkgEntity.providers && i === servicePkgEntity.providers.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/service-pkg" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/service-pkg/${servicePkgEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ servicePkg }: IRootState) => ({
  servicePkgEntity: servicePkg.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ServicePkgDetail);
