import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './task.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITaskDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TaskDetail = (props: ITaskDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { taskEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="taskDetailsHeading">
          <Translate contentKey="lunBackendApp.task.detail_title.title">Task</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{taskEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.task.name">Name</Translate>
                </span>
              </dt>
              <dd>{taskEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="detail">
                  <Translate contentKey="lunBackendApp.task.detail">Detail</Translate>
                </span>
              </dt>
              <dd>{taskEntity.detail}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="total">
                  <Translate contentKey="lunBackendApp.task.total">Total</Translate>
                </span>
              </dt>
              <dd>{taskEntity.total}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="note">
                  <Translate contentKey="lunBackendApp.task.note">Note</Translate>
                </span>
              </dt>
              <dd>{taskEntity.note}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="scope">
                  <Translate contentKey="lunBackendApp.task.scope">Scope</Translate>
                </span>
              </dt>
              <dd>{taskEntity.scope}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="status">
                  <Translate contentKey="lunBackendApp.task.status">Status</Translate>
                </span>
              </dt>
              <dd>{taskEntity.status}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="startTime">
                  <Translate contentKey="lunBackendApp.task.startTime">Start Time</Translate>
                </span>
              </dt>
              <dd>{taskEntity.startTime ? <TextFormat value={taskEntity.startTime} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="deadLine">
                  <Translate contentKey="lunBackendApp.task.deadLine">Dead Line</Translate>
                </span>
              </dt>
              <dd>{taskEntity.deadLine ? <TextFormat value={taskEntity.deadLine} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.task.projct">Projct</Translate>
              </dt>
              <dd>{taskEntity.projct ? <a href={`projct/${taskEntity.projct.id}`}>{taskEntity.projct.id}</a> : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.task.stage">Stage</Translate>
              </dt>
              <dd>{taskEntity.stage ? <a href={`track-progress/${taskEntity.stage.id}`}>{taskEntity.stage.name}</a> : ''}</dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/task" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/task/${taskEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ task }: IRootState) => ({
  taskEntity: task.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetail);
