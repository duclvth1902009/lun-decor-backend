import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { openFile, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getProjcts } from 'app/entities/projct/projct.reducer';
import { getEntities as getTrackProgresses } from 'app/entities/track-progress/track-progress.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './task.reducer';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { getEntities as getImages } from 'app/entities/image/image.reducer';

export interface ITaskUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TaskUpdate = (props: ITaskUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { taskEntity, projcts, trackProgresses, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/task' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
    props.getImages();
    props.getProjcts();
    props.getTrackProgresses();
  }, []);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.startTime = convertDateTimeToServer(values.startTime);
    values.deadLine = convertDateTimeToServer(values.deadLine);

    if (errors.length === 0) {
      const entity = {
        ...taskEntity,
        ...values,
        projct: projcts.find(it => it.id.toString() === values.projctId.toString()),
        stage: trackProgresses.find(it => it.id.toString() === values.stageId.toString()),
        files: fileList,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.task.home.createOrEditLabel" data-cy="TaskCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.task.home.createOrEditLabel">Create or edit a Task</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : taskEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="task-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="task-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="fileLabel" for="file">
                  <Translate contentKey="lunBackendApp.task.file">File</Translate>
                </Label>
                <Row>{renderPhotos(fileList)}</Row>
                <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
              </AvGroup>
              <AvGroup>
                <Label id="fileLabel">
                  <Translate contentKey="lunBackendApp.image.file">File</Translate>
                </Label>
                {taskEntity.images
                  ? taskEntity.images.map(otherEntity => (
                      <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                    ))
                  : null}
                <br />
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="task-name">
                  <Translate contentKey="lunBackendApp.task.name">Name</Translate>
                </Label>
                <AvField id="task-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="detailLabel" for="task-detail">
                  <Translate contentKey="lunBackendApp.task.detail">Detail</Translate>
                </Label>
                <AvField id="task-detail" data-cy="detail" type="text" name="detail" />
              </AvGroup>
              <AvGroup>
                <Label id="totalLabel" for="task-total">
                  <Translate contentKey="lunBackendApp.task.total">Total</Translate>
                </Label>
                <AvField id="task-total" data-cy="total" type="string" className="form-control" name="total" />
              </AvGroup>
              <AvGroup>
                <Label id="noteLabel" for="task-note">
                  <Translate contentKey="lunBackendApp.task.note">Note</Translate>
                </Label>
                <AvField id="task-note" data-cy="note" type="text" name="note" />
              </AvGroup>
              <AvGroup>
                <Label id="scopeLabel" for="task-scope">
                  <Translate contentKey="lunBackendApp.task.scope">Scope</Translate>
                </Label>
                <AvField id="task-scope" data-cy="scope" type="text" name="scope" />
              </AvGroup>
              <AvGroup>
                <Label id="statusLabel" for="task-status">
                  <Translate contentKey="lunBackendApp.task.status">Status</Translate>
                </Label>
                <AvInput
                  id="task-status"
                  data-cy="status"
                  type="select"
                  className="form-control"
                  name="status"
                  value={!isNew && taskEntity.status}
                >
                  <option value="todo" key="todo">
                    todo
                  </option>
                  <option value="doing" key="doing">
                    doing
                  </option>
                  <option value="done" key="done">
                    done
                  </option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="startTimeLabel" for="task-startTime">
                  <Translate contentKey="lunBackendApp.task.startTime">Start Time</Translate>
                </Label>
                <AvInput
                  id="task-startTime"
                  data-cy="startTime"
                  type="datetime-local"
                  className="form-control"
                  name="startTime"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={!props.taskEntity.startTime ? displayDefaultDateTime() : convertDateTimeFromServer(props.taskEntity.startTime)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="deadLineLabel" for="task-deadLine">
                  <Translate contentKey="lunBackendApp.task.deadLine">Dead Line</Translate>
                </Label>
                <AvInput
                  id="task-deadLine"
                  data-cy="deadLine"
                  type="datetime-local"
                  className="form-control"
                  name="deadLine"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={!props.taskEntity.deadLine ? displayDefaultDateTime() : convertDateTimeFromServer(props.taskEntity.deadLine)}
                />
              </AvGroup>
              <AvGroup>
                <Label for="task-projct">
                  <Translate contentKey="lunBackendApp.task.projct">Projct</Translate>
                </Label>
                <AvInput
                  id="task-projct"
                  data-cy="projct"
                  type="select"
                  className="form-control"
                  name="projctId"
                  value={!isNew && taskEntity.projct && taskEntity.projct.id}
                >
                  <option value="" key="0" />
                  {projcts
                    ? projcts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="task-stage">
                  <Translate contentKey="lunBackendApp.task.stage">Stage</Translate>
                </Label>
                <AvInput
                  id="task-stage"
                  data-cy="stage"
                  type="select"
                  className="form-control"
                  name="stageId"
                  value={!isNew && taskEntity.stage && taskEntity.stage.id}
                >
                  <option value="" key="0" />
                  {trackProgresses
                    ? trackProgresses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/task" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  projcts: storeState.projct.entities,
  trackProgresses: storeState.trackProgress.entities,
  taskEntity: storeState.task.entity,
  loading: storeState.task.loading,
  updating: storeState.task.updating,
  updateSuccess: storeState.task.updateSuccess,
  images: storeState.image.entities,
});

const mapDispatchToProps = {
  getProjcts,
  getTrackProgresses,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
  getImages,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TaskUpdate);
