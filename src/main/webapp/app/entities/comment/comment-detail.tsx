import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './comment.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICommentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CommentDetail = (props: ICommentDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { commentEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="commentDetailsHeading">
          <Translate contentKey="lunBackendApp.comment.detail.title">Comment</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{commentEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="content">
                  <Translate contentKey="lunBackendApp.comment.content">Content</Translate>
                </span>
              </dt>
              <dd>{commentEntity.content}</dd>
            </Col>
            {/*<dt>*/}
            {/*  <span id="parent_id">*/}
            {/*    <Translate contentKey="lunBackendApp.comment.parent_id">Parent Id</Translate>*/}
            {/*  </span>*/}
            {/*</dt>*/}
            {/*<dd>{commentEntity.parent_id}</dd>
            </Col>*/}
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.comment.from">From</Translate>
              </dt>
              <dd>{commentEntity.from ? commentEntity.from.name : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.comment.parent">Parent</Translate>
              </dt>
              <dd>{commentEntity.parent ? commentEntity.parent.content : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.comment.projct">Projct</Translate>
              </dt>
              <dd>{commentEntity.projct ? commentEntity.projct.id : ''}</dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/comment" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/comment/${commentEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ comment }: IRootState) => ({
  commentEntity: comment.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CommentDetail);
