import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction, ICrudSearchAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProjct, defaultValue } from 'app/shared/model/projct.model';

export const ACTION_TYPES = {
  FETCH_PROJCT_LIST: 'projct/FETCH_PROJCT_LIST',
  FETCH_PROJCT: 'projct/FETCH_PROJCT',
  CREATE_PROJCT: 'projct/CREATE_PROJCT',
  UPDATE_PROJCT: 'projct/UPDATE_PROJCT',
  PARTIAL_UPDATE_PROJCT: 'projct/PARTIAL_UPDATE_PROJCT',
  DELETE_PROJCT: 'projct/DELETE_PROJCT',
  SET_BLOB: 'projct/SET_BLOB',
  RESET: 'projct/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProjct>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ProjctState = Readonly<typeof initialState>;

// Reducer

export default (state: ProjctState = initialState, action): ProjctState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROJCT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROJCT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PROJCT):
    case REQUEST(ACTION_TYPES.UPDATE_PROJCT):
    case REQUEST(ACTION_TYPES.DELETE_PROJCT):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_PROJCT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PROJCT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROJCT):
    case FAILURE(ACTION_TYPES.CREATE_PROJCT):
    case FAILURE(ACTION_TYPES.UPDATE_PROJCT):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_PROJCT):
    case FAILURE(ACTION_TYPES.DELETE_PROJCT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROJCT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROJCT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROJCT):
    case SUCCESS(ACTION_TYPES.UPDATE_PROJCT):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_PROJCT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROJCT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType,
        },
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/projcts';
const apiGetByStatus = 'api/projcts/status';
const apiSearch = 'api/projcts/search';

// Actions

export const getEntities: ICrudGetAllAction<IProjct> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROJCT_LIST,
    payload: axios.get<IProjct>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntitiesByStatus: ICrudSearchAction<IProjct> = (stt, page, size, sort) => {
  const requestUrl = `${apiGetByStatus}/${stt}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROJCT_LIST,
    payload: axios.get<IProjct>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const searchProjct: ICrudSearchAction<IProjct> = (searchStr, page, size, sort) => {
  const requestUrl = `${apiSearch}/${searchStr}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROJCT_LIST,
    payload: axios.get<IProjct>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const getEntity: ICrudGetAction<IProjct> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROJCT,
    payload: axios.get<IProjct>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IProjct> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROJCT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProjct> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROJCT,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IProjct> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_PROJCT,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProjct> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROJCT,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType,
  },
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
