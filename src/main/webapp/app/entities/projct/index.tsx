import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Projct from './projct';
import ProjctDetail from './projct-detail';
import ProjctUpdate from './projct-update';
import ProjctDeleteDialog from './projct-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProjctUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProjctUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProjctDetail} />
      <ErrorBoundaryRoute path={match.url} component={Projct} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ProjctDeleteDialog} />
  </>
);

export default Routes;
