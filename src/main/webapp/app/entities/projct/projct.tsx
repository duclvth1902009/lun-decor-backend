import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Input, Progress, Row, Table } from 'reactstrap';
import { openFile, byteSize, Translate, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ProgressBar from 'react-bootstrap/ProgressBar';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { IRootState } from 'app/shared/reducers';
import { getEntities, getEntitiesByStatus, searchProjct } from './projct.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { convertDateTimeFromServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import moment from 'moment';

export interface IProjctProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Projct = (props: IProjctProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'holdTime'), props.location.search)
  );

  const [totalMoney, setTotalMoney] = useState(0);

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},desc`);
  };

  const sortEntities = () => {
    getAllEntities;
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},desc`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth();
  const startDate = new Date(year, month, 1, 0);
  const searchObj = { start: startDate, end: date, status: ' ' };
  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get('sort');
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
  };

  const calTotal = (status, finTotal) => {
    let total = totalMoney;
    if (status === 'done') {
      total += finTotal;
    }
    setTotalMoney(total);
    return status;
  };

  const search = (event, errors, values) => {
    const searchStr = values.status + '/' + values.start + '/' + values.end;
    props.searchProjct(
      searchStr,
      paginationState.activePage - 1,
      paginationState.itemsPerPage,
      `${paginationState.sort},${paginationState.order}`
    );
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const handleSyncList = () => {
    sortEntities();
  };

  const APP_WHOLE_NUMBER_FORMAT = '0,0';

  const { projctList, match, loading, totalItems } = props;
  return (
    <div>
      <h2 id="projct-heading" data-cy="ProjctHeading">
        <Translate contentKey="lunBackendApp.projct.home.title">Projcts</Translate>

        <div className="d-flex justify-content-end">
          <AvForm model={searchObj} onSubmit={search}>
            <Row>
              <Col md="3">
                <AvInput
                  id="projct-start-time"
                  type="datetime-local"
                  className="form-control"
                  name="start"
                  placeholder={'Từ ngày'}
                  value={convertDateTimeFromServer(searchObj.start)}
                />
              </Col>
              <Col md="3">
                <AvInput
                  id="projct-end-time"
                  type="datetime-local"
                  className="form-control"
                  name="end"
                  placeholder={'Đến ngày'}
                  value={convertDateTimeFromServer(searchObj.end)}
                />
              </Col>
              <Col md="3">
                <AvInput id="projct-status" data-cy="status" type="select" className="form-control" name="status">
                  <option value=" " key="all">
                    getAll
                  </option>
                  <option value="pending" key="pending">
                    pending
                  </option>
                  <option value="active" key="active">
                    active
                  </option>
                  <option value="done" key="done">
                    done
                  </option>
                  <option value="cancel" key="cancel">
                    cancel
                  </option>
                </AvInput>
              </Col>
              <Col md="3">
                <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit">
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.search">search</Translate>
                </Button>
              </Col>
            </Row>
          </AvForm>
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="lunBackendApp.projct.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="lunBackendApp.projct.home.createLabel">Create new Projct</Translate>
          </Link>
        </div>
      </h2>
      <div></div>
      <div className="table-responsive">
        {projctList && projctList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                {/*<th className="hand" onClick={sort('id')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.id">ID</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th className="hand" onClick={sort('name')}>
                  <Translate contentKey="lunBackendApp.projct.name">Name</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('holdTime')}>
                  <Translate contentKey="lunBackendApp.projct.holdTime">Hold Time</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="lunBackendApp.projct.customer">Customer</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th className="hand" onClick={sort('location')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.location">Location</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('contract')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.contract">Contract</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('rate')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.rate">Rate</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('preCapital')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.preCapital">Pre Capital</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('preTotal')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.preTotal">Pre Total</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('finCapital')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.finCapital">Fin Capital</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('finTotal')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.finTotal">Fin Total</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('rschangeCap')}>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.rschangeCap">Rschange Cap</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th className="hand" onClick={sort('progress')}>
                  <Translate contentKey="lunBackendApp.projct.progress">Progress</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('status')}>
                  <Translate contentKey="lunBackendApp.projct.status">Status</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.type">Type</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.eventVenue">Event Venue</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.requestOrder">Request Order</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th>*/}
                {/*  <Translate contentKey="lunBackendApp.projct.final_design">Final Design</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th />
              </tr>
            </thead>
            <tbody>
              {projctList.map((projct, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  {/*<td style={{ maxWidth: 80 }}>*/}
                  {/*  <Link to={`${match.url}/${projct.id}`}>{projct.id}</Link>*/}
                  {/*</td>*/}
                  <td>
                    <Link to={`${match.url}/${projct.id}`}>{projct.name.substr(0, 15) + '...'}</Link>
                  </td>
                  <td>{projct.holdTime ? <TextFormat type="date" value={projct.holdTime} format={APP_DATE_FORMAT} /> : null}</td>
                  <td>{projct.customer ? <Link to={`customer/${projct.customer.id}`}>{projct.customer.name}</Link> : ''}</td>
                  {/*<td>{projct.location}</td>*/}
                  {/*<td>*/}
                  {/*  {projct.contract ? (*/}
                  {/*    <div>*/}
                  {/*      {projct.contractContentType ? (*/}
                  {/*        <a onClick={openFile(projct.contractContentType, projct.contract)}>*/}
                  {/*          <Translate contentKey="entity.action.open">Open</Translate>*/}
                  {/*          &nbsp;*/}
                  {/*        </a>*/}
                  {/*      ) : null}*/}
                  {/*      <span>*/}
                  {/*        {projct.contractContentType}, {byteSize(projct.contract)}*/}
                  {/*      </span>*/}
                  {/*    </div>*/}
                  {/*  ) : null}*/}
                  {/*</td>*/}
                  {/*<td>{projct.rate}</td>*/}
                  {/*<td>{projct.preCapital}</td>*/}
                  {/*<td>{projct.preTotal}</td>*/}
                  {/*<td>{projct.finCapital}</td>*/}
                  {/*<td>{projct.finTotal}</td>*/}
                  {/*<td>{projct.rschangeCap}</td>*/}
                  {/*<td>{projct.rschangeTot}</td>*/}
                  <td style={{ minWidth: 160 }}>
                    <Progress animated value={100 * projct.progress} color="success">
                      <span>
                        <TextFormat value={100 * projct.progress} type="number" format={APP_WHOLE_NUMBER_FORMAT} /> %
                      </span>
                    </Progress>
                  </td>
                  <td>{projct.status}</td>
                  {/*<td>{projct.type ? <Link to={`event-type/${projct.type.id}`}>{projct.type.name}</Link> : ''}</td>*/}
                  {/*<td>{projct.eventVenue ? <Link to={`event-venue/${projct.eventVenue.id}`}>{projct.eventVenue.name}</Link> : ''}</td>*/}
                  {/*<td style={{ maxWidth: 80 }}>*/}
                  {/*  {projct.requestOrder ? <Link to={`request-order/${projct.requestOrder.id}`}>{projct.requestOrder.id}</Link> : ''}*/}
                  {/*</td>                  */}
                  {/*<td>{projct.final_design ? <Link to={`design/${projct.final_design.id}`}>{projct.final_design.quickView}</Link> : ''}</td>*/}
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${projct.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${projct.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${projct.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="lunBackendApp.projct.home.notFound">No Projcts found</Translate>
            </div>
          )
        )}
      </div>
      {props.totalItems ? (
        <div className={projctList && projctList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      ) : (
        ''
      )}

      <div>
        <p>
          <span>Quản lý doanh thu</span>
        </p>
        <p>
          <span>Doanh thu </span> ({totalMoney}Đ)
        </p>
        <ProgressBar>
          <ProgressBar
            label={
              <span>
                Lợi nhuận: <TextFormat value={35000000} type="number" format={'0,0Đ'} />
              </span>
            }
            animated
            variant="success"
            now={35}
            key={1}
          />
          <ProgressBar
            label={
              <span>
                Phụ phí: <TextFormat value={15000000} type="number" format={'0,0Đ'} />
              </span>
            }
            animated
            variant="warning"
            now={15}
            key={2}
          />
          <ProgressBar
            label={
              <span>
                Chi phí: <TextFormat value={50000000} type="number" format={'0,0Đ'} />
              </span>
            }
            animated
            variant="danger"
            now={50}
            key={3}
          />
        </ProgressBar>
      </div>
    </div>
  );
};

const mapStateToProps = ({ projct }: IRootState) => ({
  projctList: projct.entities,
  loading: projct.loading,
  totalItems: projct.totalItems,
});

const mapDispatchToProps = {
  getEntities,
  getEntitiesByStatus,
  searchProjct,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Projct);
