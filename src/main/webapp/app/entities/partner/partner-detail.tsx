import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './partner.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPartnerDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PartnerDetail = (props: IPartnerDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { partnerEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="partnerDetailsHeading">
          <Translate contentKey="lunBackendApp.partner.detail.title">Partner</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{partnerEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.partner.name">Name</Translate>
                </span>
              </dt>
              <dd>{partnerEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="phone">
                  <Translate contentKey="lunBackendApp.partner.phone">Phone</Translate>
                </span>
              </dt>
              <dd>{partnerEntity.phone}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="email">
                  <Translate contentKey="lunBackendApp.partner.email">Email</Translate>
                </span>
              </dt>
              <dd>{partnerEntity.email}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="location">
                  <Translate contentKey="lunBackendApp.partner.location">Location</Translate>
                </span>
              </dt>
              <dd>{partnerEntity.location}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="bank">
                  <Translate contentKey="lunBackendApp.partner.bank">Bank</Translate>
                </span>
              </dt>
              <dd>{partnerEntity.bank}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="note">
                  <Translate contentKey="lunBackendApp.partner.note">Note</Translate>
                </span>
              </dt>
              <dd>{partnerEntity.note}</dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/partner" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/partner/${partnerEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ partner }: IRootState) => ({
  partnerEntity: partner.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PartnerDetail);
