import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './partner.reducer';
import { IPartner } from 'app/shared/model/partner.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPartnerUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PartnerUpdate = (props: IPartnerUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { partnerEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/partner' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...partnerEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.partner.home.createOrEditLabel" data-cy="PartnerCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.partner.home.createOrEditLabel">Create or edit a Partner</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : partnerEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="partner-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="partner-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="partner-name">
                  <Translate contentKey="lunBackendApp.partner.name">Name</Translate>
                </Label>
                <AvField id="partner-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="phoneLabel" for="partner-phone">
                  <Translate contentKey="lunBackendApp.partner.phone">Phone</Translate>
                </Label>
                <AvField
                  id="partner-phone"
                  data-cy="phone"
                  type="text"
                  name="phone"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    pattern: { value: '[0-9]+', errorMessage: translate('entity.validation.pattern', { pattern: '[0-9]+' }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="emailLabel" for="partner-email">
                  <Translate contentKey="lunBackendApp.partner.email">Email</Translate>
                </Label>
                <AvField id="partner-email" data-cy="email" type="text" name="email" />
              </AvGroup>
              <AvGroup>
                <Label id="locationLabel" for="partner-location">
                  <Translate contentKey="lunBackendApp.partner.location">Location</Translate>
                </Label>
                <AvField id="partner-location" data-cy="location" type="text" name="location" />
              </AvGroup>
              <AvGroup>
                <Label id="bankLabel" for="partner-bank">
                  <Translate contentKey="lunBackendApp.partner.bank">Bank</Translate>
                </Label>
                <AvField id="partner-bank" data-cy="bank" type="text" name="bank" />
              </AvGroup>
              <AvGroup>
                <Label id="noteLabel" for="partner-note">
                  <Translate contentKey="lunBackendApp.partner.note">Note</Translate>
                </Label>
                <AvField id="partner-note" data-cy="note" type="text" name="note" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/partner" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  partnerEntity: storeState.partner.entity,
  loading: storeState.partner.loading,
  updating: storeState.partner.updating,
  updateSuccess: storeState.partner.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PartnerUpdate);
