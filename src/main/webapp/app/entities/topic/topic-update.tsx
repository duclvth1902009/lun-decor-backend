import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getEventTypes } from 'app/entities/event-type/event-type.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './topic.reducer';

export interface ITopicUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TopicUpdate = (props: ITopicUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { topicEntity, eventTypes, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/topic' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getEventTypes();
  }, []);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...topicEntity,
        ...values,
        files: fileList,
        type: eventTypes.find(it => it.id.toString() === values.typeId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.topic.home.createOrEditLabel" data-cy="TopicCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.topic.home.createOrEditLabel">Create or edit a Topic</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : topicEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="topic-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="topic-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="fileLabel">
                  <Translate contentKey="lunBackendApp.topic.file">File</Translate>
                </Label>
                {topicEntity.images
                  ? topicEntity.images.map(otherEntity => (
                      <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                    ))
                  : null}
                <br />
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="topic-name">
                  <Translate contentKey="lunBackendApp.topic.name">Name</Translate>
                </Label>
                <AvField id="topic-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="tagsLabel" for="topic-tags">
                  <Translate contentKey="lunBackendApp.topic.tags">Tags</Translate>
                </Label>
                <AvField id="topic-tags" data-cy="tags" type="text" name="tags" />
              </AvGroup>
              <AvGroup>
                <Label>
                  <Translate contentKey="lunBackendApp.topic.images">Images</Translate>
                </Label>
                <Row>{renderPhotos(fileList)}</Row>
                <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
              </AvGroup>
              <AvGroup>
                <Label for="topic-type">
                  <Translate contentKey="lunBackendApp.topic.type">Type</Translate>
                </Label>
                <AvInput
                  id="topic-type"
                  data-cy="type"
                  type="select"
                  className="form-control"
                  name="typeId"
                  value={!isNew && topicEntity.type && topicEntity.type.id}
                >
                  <option value="" key="0" />
                  {eventTypes
                    ? eventTypes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/topic" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  eventTypes: storeState.eventType.entities,
  topicEntity: storeState.topic.entity,
  loading: storeState.topic.loading,
  updating: storeState.topic.updating,
  updateSuccess: storeState.topic.updateSuccess,
});

const mapDispatchToProps = {
  getEventTypes,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TopicUpdate);
