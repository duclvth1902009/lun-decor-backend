import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './topic.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITopicDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TopicDetail = (props: ITopicDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { topicEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="topicDetailsHeading">
          <Translate contentKey="lunBackendApp.topic.detail.title">Topic</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{topicEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.topic.name">Name</Translate>
                </span>
              </dt>
              <dd>{topicEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="tags">
                  <Translate contentKey="lunBackendApp.topic.tags">Tags</Translate>
                </span>
              </dt>
              <dd>{topicEntity.tags}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.topic.images">Images</Translate>
              </dt>
              <dd>
                {topicEntity.images
                  ? topicEntity.images.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.url}</a>
                        {/*<img src={`${val.url}`} style={{ maxHeight: '220px' }} />*/}
                        {topicEntity.images && i === topicEntity.images.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
          </Row>
          <Col md="6">
            <dt>
              <Translate contentKey="lunBackendApp.topic.type">Type</Translate>
            </dt>
            <dd>{topicEntity.type ? <a href={`event-type/${topicEntity.type.id}`}>{topicEntity.type.name}</a> : ''}</dd>
          </Col>
        </dl>
        <Button tag={Link} to="/topic" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/topic/${topicEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ topic }: IRootState) => ({
  topicEntity: topic.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TopicDetail);
