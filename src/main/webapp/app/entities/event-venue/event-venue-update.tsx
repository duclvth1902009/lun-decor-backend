import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { setFileData, openFile, byteSize, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, setBlob, reset } from './event-venue.reducer';
import { IEventVenue } from 'app/shared/model/event-venue.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { getEntities as getImages } from 'app/entities/image/image.reducer';

export interface IEventVenueUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const EventVenueUpdate = (props: IEventVenueUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { eventVenueEntity, loading, updating } = props;

  const { file, fileContentType } = eventVenueEntity;

  const handleClose = () => {
    props.history.push('/event-venue' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
    props.getImages();
  }, []);

  const [imageList, setImageList] = useState([]);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...eventVenueEntity,
        ...values,
        files: fileList,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.eventVenue.home.createOrEditLabel" data-cy="EventVenueCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.eventVenue.home.createOrEditLabel">Create or edit a EventVenue</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : eventVenueEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="event-venue-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="event-venue-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.eventVenue.file">File</Translate>
                  </Label>
                  <Row>{renderPhotos(fileList)}</Row>
                  <br />
                  <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
                </AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.eventVenue.images">File</Translate>
                  </Label>
                  <Row>
                    {eventVenueEntity.images
                      ? eventVenueEntity.images.map(otherEntity => (
                          <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                        ))
                      : null}
                  </Row>
                  <br />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="event-venue-name">
                  <Translate contentKey="lunBackendApp.eventVenue.name">Name</Translate>
                </Label>
                <AvField id="event-venue-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="addressLabel" for="event-venue-address">
                  <Translate contentKey="lunBackendApp.eventVenue.address">Address</Translate>
                </Label>
                <AvField id="event-venue-address" data-cy="address" type="text" name="address" />
              </AvGroup>
              <AvGroup>
                <Label id="locationLabel" for="event-venue-location">
                  <Translate contentKey="lunBackendApp.eventVenue.location">Location</Translate>
                </Label>
                <AvField id="event-venue-location" data-cy="location" type="text" name="location" />
              </AvGroup>
              <AvGroup>
                <Label id="price_fromLabel" for="event-venue-price_from">
                  <Translate contentKey="lunBackendApp.eventVenue.price_from">Price From</Translate>
                </Label>
                <AvField id="event-venue-price_from" data-cy="price_from" type="string" className="form-control" name="price_from" />
              </AvGroup>
              <AvGroup>
                <Label id="price_toLabel" for="event-venue-price_to">
                  <Translate contentKey="lunBackendApp.eventVenue.price_to">Price To</Translate>
                </Label>
                <AvField id="event-venue-price_to" data-cy="price_to" type="string" className="form-control" name="price_to" />
              </AvGroup>
              <AvGroup>
                <Label id="phoneLabel" for="event-venue-phone">
                  <Translate contentKey="lunBackendApp.eventVenue.phone">Phone</Translate>
                </Label>
                <AvField
                  id="event-venue-phone"
                  data-cy="phone"
                  type="text"
                  name="phone"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    pattern: { value: '[0-9]+', errorMessage: translate('entity.validation.pattern', { pattern: '[0-9]+' }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="emailLabel" for="event-venue-email">
                  <Translate contentKey="lunBackendApp.eventVenue.email">Email</Translate>
                </Label>
                <AvField id="event-venue-email" data-cy="email" type="text" name="email" />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="event-venue-description">
                  <Translate contentKey="lunBackendApp.eventVenue.description">Description</Translate>
                </Label>
                <AvField id="event-venue-description" data-cy="description" type="text" name="description" />
              </AvGroup>
              <AvGroup>
                <Label id="quantity_maxLabel" for="event-venue-quantity_max">
                  <Translate contentKey="lunBackendApp.eventVenue.quantity_max">Quantity Max</Translate>
                </Label>
                <AvField id="event-venue-quantity_max" data-cy="quantity_max" type="string" className="form-control" name="quantity_max" />
              </AvGroup>
              <AvGroup>
                <Label id="noteLabel" for="event-venue-note">
                  <Translate contentKey="lunBackendApp.eventVenue.note">Note</Translate>
                </Label>
                <AvField id="event-venue-note" data-cy="note" type="text" name="note" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/event-venue" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  eventVenueEntity: storeState.eventVenue.entity,
  loading: storeState.eventVenue.loading,
  updating: storeState.eventVenue.updating,
  updateSuccess: storeState.eventVenue.updateSuccess,
  images: storeState.image.entities,
});

const mapDispatchToProps = {
  getImages,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EventVenueUpdate);
