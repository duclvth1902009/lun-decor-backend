import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './event-venue.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEventVenueDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const EventVenueDetail = (props: IEventVenueDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { eventVenueEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="eventVenueDetailsHeading">
          <Translate contentKey="lunBackendApp.eventVenue.detail.title">EventVenue</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.eventVenue.name">Name</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="address">
                  <Translate contentKey="lunBackendApp.eventVenue.address">Address</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.address}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="location">
                  <Translate contentKey="lunBackendApp.eventVenue.location">Location</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.location}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="price_from">
                  <Translate contentKey="lunBackendApp.eventVenue.price_from">Price From</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.price_from}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="price_to">
                  <Translate contentKey="lunBackendApp.eventVenue.price_to">Price To</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.price_to}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="phone">
                  <Translate contentKey="lunBackendApp.eventVenue.phone">Phone</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.phone}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="email">
                  <Translate contentKey="lunBackendApp.eventVenue.email">Email</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.email}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="description">
                  <Translate contentKey="lunBackendApp.eventVenue.description">Description</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.description}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="quantity_max">
                  <Translate contentKey="lunBackendApp.eventVenue.quantity_max">Quantity Max</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.quantity_max}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="note">
                  <Translate contentKey="lunBackendApp.eventVenue.note">Note</Translate>
                </span>
              </dt>
              <dd>{eventVenueEntity.note}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.item.images">Images</Translate>
              </dt>
              <dd>
                {eventVenueEntity.images
                  ? eventVenueEntity.images.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.url}</a>
                        {eventVenueEntity.images && i === eventVenueEntity.images.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/event-venue" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/event-venue/${eventVenueEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ eventVenue }: IRootState) => ({
  eventVenueEntity: eventVenue.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EventVenueDetail);
