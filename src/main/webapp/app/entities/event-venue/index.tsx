import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EventVenue from './event-venue';
import EventVenueDetail from './event-venue-detail';
import EventVenueUpdate from './event-venue-update';
import EventVenueDeleteDialog from './event-venue-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EventVenueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EventVenueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EventVenueDetail} />
      <ErrorBoundaryRoute path={match.url} component={EventVenue} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={EventVenueDeleteDialog} />
  </>
);

export default Routes;
