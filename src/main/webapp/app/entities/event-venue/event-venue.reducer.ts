import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IEventVenue, defaultValue } from 'app/shared/model/event-venue.model';

export const ACTION_TYPES = {
  FETCH_EVENTVENUE_LIST: 'eventVenue/FETCH_EVENTVENUE_LIST',
  FETCH_EVENTVENUE: 'eventVenue/FETCH_EVENTVENUE',
  CREATE_EVENTVENUE: 'eventVenue/CREATE_EVENTVENUE',
  UPDATE_EVENTVENUE: 'eventVenue/UPDATE_EVENTVENUE',
  PARTIAL_UPDATE_EVENTVENUE: 'eventVenue/PARTIAL_UPDATE_EVENTVENUE',
  DELETE_EVENTVENUE: 'eventVenue/DELETE_EVENTVENUE',
  SET_BLOB: 'eventVenue/SET_BLOB',
  RESET: 'eventVenue/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IEventVenue>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type EventVenueState = Readonly<typeof initialState>;

// Reducer

export default (state: EventVenueState = initialState, action): EventVenueState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_EVENTVENUE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_EVENTVENUE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_EVENTVENUE):
    case REQUEST(ACTION_TYPES.UPDATE_EVENTVENUE):
    case REQUEST(ACTION_TYPES.DELETE_EVENTVENUE):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_EVENTVENUE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_EVENTVENUE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_EVENTVENUE):
    case FAILURE(ACTION_TYPES.CREATE_EVENTVENUE):
    case FAILURE(ACTION_TYPES.UPDATE_EVENTVENUE):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_EVENTVENUE):
    case FAILURE(ACTION_TYPES.DELETE_EVENTVENUE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_EVENTVENUE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_EVENTVENUE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_EVENTVENUE):
    case SUCCESS(ACTION_TYPES.UPDATE_EVENTVENUE):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_EVENTVENUE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_EVENTVENUE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType,
        },
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/event-venues';

// Actions

export const getEntities: ICrudGetAllAction<IEventVenue> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_EVENTVENUE_LIST,
    payload: axios.get<IEventVenue>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IEventVenue> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_EVENTVENUE,
    payload: axios.get<IEventVenue>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IEventVenue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_EVENTVENUE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IEventVenue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_EVENTVENUE,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IEventVenue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_EVENTVENUE,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IEventVenue> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_EVENTVENUE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType,
  },
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
