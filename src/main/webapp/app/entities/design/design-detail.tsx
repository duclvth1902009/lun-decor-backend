import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './design.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDesignDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DesignDetail = (props: IDesignDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { designEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="designDetailsHeading">
          <Translate contentKey="lunBackendApp.design.detail.title">Design</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{designEntity.id}</dd>
            </Col>
            {/*<Col md="6">*/}
            {/*  <dt>*/}
            {/*    <span id="file">*/}
            {/*      <Translate contentKey="lunBackendApp.design.file">File</Translate>*/}
            {/*    </span>*/}
            {/*  </dt>*/}
            {/*  <dd>*/}
            {/*    {designEntity.file ? (*/}
            {/*      <div>*/}
            {/*        {designEntity.fileContentType ? (*/}
            {/*          <a onClick={openFile(designEntity.fileContentType, designEntity.file)}>*/}
            {/*            <Translate contentKey="entity.action.open">Open</Translate>&nbsp;*/}
            {/*          </a>*/}
            {/*        ) : null}*/}
            {/*        <span>*/}
            {/*          {designEntity.fileContentType}, {byteSize(designEntity.file)}*/}
            {/*        </span>*/}
            {/*      </div>*/}
            {/*    ) : null}*/}
            {/*  </dd>*/}
            {/*</Col>*/}
            {/*<Col md="6">*/}
            {/*  <dt>*/}
            {/*    <span id="imgQuickView">*/}
            {/*      <Translate contentKey="lunBackendApp.design.imgQuickView">Img Quick View</Translate>*/}
            {/*    </span>*/}
            {/*  </dt>*/}
            {/*  <dd>*/}
            {/*    {designEntity.imgQuickView ? (*/}
            {/*      <div>*/}
            {/*        {designEntity.imgQuickViewContentType ? (*/}
            {/*          <a onClick={openFile(designEntity.imgQuickViewContentType, designEntity.imgQuickView)}>*/}
            {/*            <img*/}
            {/*              src={`data:${designEntity.imgQuickViewContentType};base64,${designEntity.imgQuickView}`}*/}
            {/*              style={{ maxHeight: '30px' }}*/}
            {/*            />*/}
            {/*          </a>*/}
            {/*        ) : null}*/}
            {/*        <span>*/}
            {/*          {designEntity.imgQuickViewContentType}, {byteSize(designEntity.imgQuickView)}*/}
            {/*        </span>*/}
            {/*      </div>*/}
            {/*    ) : null}*/}
            {/*  </dd>*/}
            {/*</Col>*/}
            <Col md="6">
              <dt>
                <span id="quickView">
                  <Translate contentKey="lunBackendApp.design.quickView">Quick View</Translate>
                </span>
              </dt>
              <img src={`${designEntity.quickView}`} style={{ maxHeight: '220px' }} />
            </Col>
            <Col md="6">
              <dt>
                <span id="width">
                  <Translate contentKey="lunBackendApp.design.width">Width</Translate>
                </span>
              </dt>
              <dd>{designEntity.width}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="height">
                  <Translate contentKey="lunBackendApp.design.height">Height</Translate>
                </span>
              </dt>
              <dd>{designEntity.height}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="note">
                  <Translate contentKey="lunBackendApp.design.note">Note</Translate>
                </span>
              </dt>
              <dd>{designEntity.note}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.design.images">Images</Translate>
              </dt>
              <dd>
                {designEntity.images
                  ? designEntity.images.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.url}</a>
                        {designEntity.images && i === designEntity.images.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.design.products">Products</Translate>
              </dt>
              <dd>
                {designEntity.products
                  ? designEntity.products.map((val, i) => (
                      <span key={val.id}>
                        <a href={`product/${val.id}`}>{val.name}</a>
                        {designEntity.products && i === designEntity.products.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.design.topics">Topics</Translate>
              </dt>
              <dd>
                {designEntity.topics
                  ? designEntity.topics.map((val, i) => (
                      <span key={val.id}>
                        <a href={`topic/${val.id}`}>{val.name}</a>
                        {designEntity.topics && i === designEntity.topics.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.design.of_prjct">Of Prjct</Translate>
              </dt>
              <dd>{designEntity.of_prjct ? <a href={`projct/${designEntity.of_prjct.id}`}>{designEntity.of_prjct.id}</a> : ''}</dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/design" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/design/${designEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ design }: IRootState) => ({
  designEntity: design.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DesignDetail);
