import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IDesign, defaultValue } from 'app/shared/model/design.model';

export const ACTION_TYPES = {
  FETCH_DESIGN_LIST: 'design/FETCH_DESIGN_LIST',
  FETCH_DESIGN: 'design/FETCH_DESIGN',
  CREATE_DESIGN: 'design/CREATE_DESIGN',
  UPDATE_DESIGN: 'design/UPDATE_DESIGN',
  PARTIAL_UPDATE_DESIGN: 'design/PARTIAL_UPDATE_DESIGN',
  DELETE_DESIGN: 'design/DELETE_DESIGN',
  SET_BLOB: 'design/SET_BLOB',
  RESET: 'design/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IDesign>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type DesignState = Readonly<typeof initialState>;

// Reducer

export default (state: DesignState = initialState, action): DesignState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_DESIGN_LIST):
    case REQUEST(ACTION_TYPES.FETCH_DESIGN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_DESIGN):
    case REQUEST(ACTION_TYPES.UPDATE_DESIGN):
    case REQUEST(ACTION_TYPES.DELETE_DESIGN):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_DESIGN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_DESIGN_LIST):
    case FAILURE(ACTION_TYPES.FETCH_DESIGN):
    case FAILURE(ACTION_TYPES.CREATE_DESIGN):
    case FAILURE(ACTION_TYPES.UPDATE_DESIGN):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_DESIGN):
    case FAILURE(ACTION_TYPES.DELETE_DESIGN):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DESIGN_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_DESIGN):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_DESIGN):
    case SUCCESS(ACTION_TYPES.UPDATE_DESIGN):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_DESIGN):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_DESIGN):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType,
        },
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/designs';

// Actions

export const getEntities: ICrudGetAllAction<IDesign> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_DESIGN_LIST,
    payload: axios.get<IDesign>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IDesign> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_DESIGN,
    payload: axios.get<IDesign>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IDesign> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_DESIGN,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IDesign> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_DESIGN,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IDesign> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_DESIGN,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IDesign> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_DESIGN,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType,
  },
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
