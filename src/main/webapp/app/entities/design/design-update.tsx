import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { setFileData, openFile, byteSize, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IImage } from 'app/shared/model/image.model';
import { getEntities as getImages } from 'app/entities/image/image.reducer';
import { IProduct } from 'app/shared/model/product.model';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { ITopic } from 'app/shared/model/topic.model';
import { getEntities as getTopics } from 'app/entities/topic/topic.reducer';
import { IProjct } from 'app/shared/model/projct.model';
import { getEntities as getProjcts } from 'app/entities/projct/projct.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './design.reducer';
import { IDesign } from 'app/shared/model/design.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IDesignUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DesignUpdate = (props: IDesignUpdateProps) => {
  const [idsimages, setIdsimages] = useState([]);
  const [idsproducts, setIdsproducts] = useState([]);
  const [idstopics, setIdstopics] = useState([]);
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { designEntity, images, products, topics, projcts, loading, updating } = props;

  const { file, fileContentType, imgQuickView, imgQuickViewContentType } = designEntity;

  const handleClose = () => {
    props.history.push('/design' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getImages();
    props.getProducts();
    props.getTopics();
    props.getProjcts();
  }, []);

  const onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => props.setBlob(name, data, contentType), isAnImage);
  };

  const clearBlob = name => () => {
    props.setBlob(name, undefined, undefined);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...designEntity,
        ...values,
        images: mapIdList(values.images),
        products: mapIdList(values.products),
        topics: mapIdList(values.topics),
        of_prjct: projcts.find(it => it.id.toString() === values.of_prjctId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.design.home.createOrEditLabel" data-cy="DesignCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.design.home.createOrEditLabel">Create or edit a Design</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : designEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="design-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="design-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.design.file">File</Translate>
                  </Label>
                  <br />
                  {file ? (
                    <div>
                      {fileContentType ? (
                        <a onClick={openFile(fileContentType, file)}>
                          <Translate contentKey="entity.action.open">Open</Translate>
                        </a>
                      ) : null}
                      <br />
                      <Row>
                        <Col md="11">
                          <span>
                            {fileContentType}, {byteSize(file)}
                          </span>
                        </Col>
                        <Col md="1">
                          <Button color="danger" onClick={clearBlob('file')}>
                            <FontAwesomeIcon icon="times-circle" />
                          </Button>
                        </Col>
                      </Row>
                    </div>
                  ) : null}
                  <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(false, 'file')} />
                  <AvInput type="hidden" name="file" value={file} />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <AvGroup>
                  <Label id="imgQuickViewLabel" for="imgQuickView">
                    <Translate contentKey="lunBackendApp.design.imgQuickView">Img Quick View</Translate>
                  </Label>
                  <br />
                  {imgQuickView ? (
                    <div>
                      {imgQuickViewContentType ? (
                        <a onClick={openFile(imgQuickViewContentType, imgQuickView)}>
                          <img src={`data:${imgQuickViewContentType};base64,${imgQuickView}`} style={{ maxHeight: '100px' }} />
                        </a>
                      ) : null}
                      <br />
                      <Row>
                        <Col md="11">
                          <span>
                            {imgQuickViewContentType}, {byteSize(imgQuickView)}
                          </span>
                        </Col>
                        <Col md="1">
                          <Button color="danger" onClick={clearBlob('imgQuickView')}>
                            <FontAwesomeIcon icon="times-circle" />
                          </Button>
                        </Col>
                      </Row>
                    </div>
                  ) : null}
                  <input
                    id="file_imgQuickView"
                    data-cy="imgQuickView"
                    type="file"
                    onChange={onBlobChange(true, 'imgQuickView')}
                    accept="image/*"
                  />
                  <AvInput type="hidden" name="imgQuickView" value={imgQuickView} />
                </AvGroup>
              </AvGroup>
              {/*<AvGroup>*/}
              {/*  <Label id="quickViewLabel" for="design-quickView">*/}
              {/*    <Translate contentKey="lunBackendApp.design.quickView">Quick View</Translate>*/}
              {/*  </Label>*/}
              {/*  <AvField*/}
              {/*    id="design-quickView"*/}
              {/*    data-cy="quickView"*/}
              {/*    type="text"*/}
              {/*    name="quickView"*/}
              {/*    validate={{*/}
              {/*      required: { value: true, errorMessage: translate('entity.validation.required') },*/}
              {/*    }}*/}
              {/*  />*/}
              {/*</AvGroup>*/}
              <AvGroup>
                <Label id="widthLabel" for="design-width">
                  <Translate contentKey="lunBackendApp.design.width">Width</Translate>
                </Label>
                <AvField id="design-width" data-cy="width" type="string" className="form-control" name="width" />
              </AvGroup>
              <AvGroup>
                <Label id="heightLabel" for="design-height">
                  <Translate contentKey="lunBackendApp.design.height">Height</Translate>
                </Label>
                <AvField id="design-height" data-cy="height" type="string" className="form-control" name="height" />
              </AvGroup>
              <AvGroup>
                <Label id="noteLabel" for="design-note">
                  <Translate contentKey="lunBackendApp.design.note">Note</Translate>
                </Label>
                <AvField id="design-note" data-cy="note" type="text" name="note" />
              </AvGroup>
              <AvGroup>
                <Label for="design-images">
                  <Translate contentKey="lunBackendApp.design.images">Images</Translate>
                </Label>
                <AvInput
                  id="design-images"
                  data-cy="images"
                  type="select"
                  multiple
                  className="form-control"
                  name="images"
                  value={!isNew && designEntity.images && designEntity.images.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {images
                    ? images.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.url}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="design-products">
                  <Translate contentKey="lunBackendApp.design.products">Products</Translate>
                </Label>
                <AvInput
                  id="design-products"
                  data-cy="products"
                  type="select"
                  multiple
                  className="form-control"
                  name="products"
                  value={!isNew && designEntity.products && designEntity.products.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {products
                    ? products.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="design-topics">
                  <Translate contentKey="lunBackendApp.design.topics">Topics</Translate>
                </Label>
                <AvInput
                  id="design-topics"
                  data-cy="topics"
                  type="select"
                  multiple
                  className="form-control"
                  name="topics"
                  value={!isNew && designEntity.topics && designEntity.topics.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {topics
                    ? topics.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="design-of_prjct">
                  <Translate contentKey="lunBackendApp.design.of_prjct">Of Prjct</Translate>
                </Label>
                <AvInput
                  id="design-of_prjct"
                  data-cy="of_prjct"
                  type="select"
                  className="form-control"
                  name="of_prjctId"
                  value={!isNew && designEntity.of_prjct && designEntity.of_prjct.id}
                >
                  <option value="" key="0" />
                  {projcts
                    ? projcts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/design" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  images: storeState.image.entities,
  products: storeState.product.entities,
  topics: storeState.topic.entities,
  projcts: storeState.projct.entities,
  designEntity: storeState.design.entity,
  loading: storeState.design.loading,
  updating: storeState.design.updating,
  updateSuccess: storeState.design.updateSuccess,
});

const mapDispatchToProps = {
  getImages,
  getProducts,
  getTopics,
  getProjcts,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DesignUpdate);
