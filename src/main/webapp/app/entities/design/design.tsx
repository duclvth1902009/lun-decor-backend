import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { openFile, byteSize, Translate, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './design.reducer';
import { IDesign } from 'app/shared/model/design.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';

export interface IDesignProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Design = (props: IDesignProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'id'), props.location.search)
  );

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get('sort');
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const handleSyncList = () => {
    sortEntities();
  };

  const { designList, match, loading, totalItems } = props;
  return (
    <div>
      <h2 id="design-heading" data-cy="DesignHeading">
        <Translate contentKey="lunBackendApp.design.home.title">Designs</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="lunBackendApp.design.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="lunBackendApp.design.home.createLabel">Create new Design</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {designList && designList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="lunBackendApp.design.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th className="hand" onClick={sort('file')}>*/}
                {/*  <Translate contentKey="lunBackendApp.design.file">File</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('imgQuickView')}>*/}
                {/*  <Translate contentKey="lunBackendApp.design.imgQuickView">Img Quick View</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th className="hand" onClick={sort('quickView')}>
                  <Translate contentKey="lunBackendApp.design.quickView">Quick View</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th className="hand" onClick={sort('width')}>*/}
                {/*  <Translate contentKey="lunBackendApp.design.width">Width</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('height')}>*/}
                {/*  <Translate contentKey="lunBackendApp.design.height">Height</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th className="hand" onClick={sort('note')}>
                  <Translate contentKey="lunBackendApp.design.note">Note</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th>*/}
                {/*  <Translate contentKey="lunBackendApp.design.of_prjct">Of Prjct</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th />
              </tr>
            </thead>
            <tbody>
              {designList.map((design, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${design.id}`} color="link" size="sm">
                      {design.id}
                    </Button>
                  </td>
                  {/*<td>*/}
                  {/*  {design.file ? (*/}
                  {/*    <div>*/}
                  {/*      {design.fileContentType ? (*/}
                  {/*        <a onClick={openFile(design.fileContentType, design.file)}>*/}
                  {/*          <Translate contentKey="entity.action.open">Open</Translate>*/}
                  {/*          &nbsp;*/}
                  {/*        </a>*/}
                  {/*      ) : null}*/}
                  {/*      <span>*/}
                  {/*        {design.fileContentType}, {byteSize(design.file)}*/}
                  {/*      </span>*/}
                  {/*    </div>*/}
                  {/*  ) : null}*/}
                  {/*</td>*/}
                  {/*<td>*/}
                  {/*  {design.imgQuickView ? (*/}
                  {/*    <div>*/}
                  {/*      {design.imgQuickViewContentType ? (*/}
                  {/*        <a onClick={openFile(design.imgQuickViewContentType, design.imgQuickView)}>*/}
                  {/*          <img*/}
                  {/*            src={`data:${design.imgQuickViewContentType};base64,${design.imgQuickView}`}*/}
                  {/*            style={{ maxHeight: '30px' }}*/}
                  {/*          />*/}
                  {/*          &nbsp;*/}
                  {/*        </a>*/}
                  {/*      ) : null}*/}
                  {/*      <span>*/}
                  {/*        {design.imgQuickViewContentType}, {byteSize(design.imgQuickView)}*/}
                  {/*      </span>*/}
                  {/*    </div>*/}
                  {/*  ) : null}*/}
                  {/*</td>*/}
                  <td>
                    <img src={`${design.quickView}`} style={{ maxHeight: '220px' }} />
                  </td>
                  {/*<td>{design.width}</td>*/}
                  {/*<td>{design.height}</td>*/}
                  <td>
                    <p>
                      <Translate contentKey="lunBackendApp.design.fileType">File Type: </Translate>
                      {design.note.split('-', 2)[0]}
                    </p>
                    <a href={`${design.note.split('-', 2)[1]}`}>Tải về</a>
                  </td>
                  {/*<td>{design.of_prjct ? <Link to={`projct/${design.of_prjct.id}`}>{design.projct.id ? design.projct.id.substr(0, 20) + '...' : ''}</Link> : ''}</td>*/}
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${design.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${design.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${design.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="lunBackendApp.design.home.notFound">No Designs found</Translate>
            </div>
          )
        )}
      </div>
      {props.totalItems ? (
        <div className={designList && designList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

const mapStateToProps = ({ design }: IRootState) => ({
  designList: design.entities,
  loading: design.loading,
  totalItems: design.totalItems,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Design);
