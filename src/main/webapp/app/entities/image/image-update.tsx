import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { setFileData, openFile, byteSize, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IEventVenue } from 'app/shared/model/event-venue.model';
import { getEntities as getEventVenues } from 'app/entities/event-venue/event-venue.reducer';
import { IItem } from 'app/shared/model/item.model';
import { getEntities as getItems } from 'app/entities/item/item.reducer';
import { IProduct } from 'app/shared/model/product.model';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { IServicePkg } from 'app/shared/model/service-pkg.model';
import { getEntities as getServicePkgs } from 'app/entities/service-pkg/service-pkg.reducer';
import { IPack } from 'app/shared/model/pack.model';
import { getEntities as getPacks } from 'app/entities/pack/pack.reducer';
import { IDesign } from 'app/shared/model/design.model';
import { getEntities as getDesigns } from 'app/entities/design/design.reducer';
import { IRequestOrder } from 'app/shared/model/request-order.model';
import { getEntities as getRequestOrders } from 'app/entities/request-order/request-order.reducer';
import { IProjct } from 'app/shared/model/projct.model';
import { getEntities as getProjcts } from 'app/entities/projct/projct.reducer';
import { ITrackProgress } from 'app/shared/model/track-progress.model';
import { getEntities as getTrackProgresses } from 'app/entities/track-progress/track-progress.reducer';
import { ITask } from 'app/shared/model/task.model';
import { getEntities as getTasks } from 'app/entities/task/task.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './image.reducer';
import { IImage } from 'app/shared/model/image.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IImageUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ImageUpdate = (props: IImageUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const {
    imageEntity,
    eventVenues,
    items,
    products,
    servicePkgs,
    packs,
    designs,
    requestOrders,
    projcts,
    trackProgresses,
    tasks,
    loading,
    updating,
  } = props;

  const { file, fileContentType } = imageEntity;

  const handleClose = () => {
    props.history.push('/image' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getEventVenues();
    props.getItems();
    props.getProducts();
    props.getServicePkgs();
    props.getPacks();
    props.getDesigns();
    props.getRequestOrders();
    props.getProjcts();
    props.getTrackProgresses();
    props.getTasks();
  }, []);

  const onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => props.setBlob(name, data, contentType), isAnImage);
  };

  const clearBlob = name => () => {
    props.setBlob(name, undefined, undefined);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...imageEntity,
        ...values,
        eventVenue: eventVenues.find(it => it.id.toString() === values.eventVenueId.toString()),
        projct: projcts.find(it => it.id.toString() === values.projctId.toString()),
        trackProgress: trackProgresses.find(it => it.id.toString() === values.trackProgressId.toString()),
        task: tasks.find(it => it.id.toString() === values.taskId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.image.home.createOrEditLabel" data-cy="ImageCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.image.home.createOrEditLabel">Create or edit a Image</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : imageEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="image-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="image-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.image.file">File</Translate>
                  </Label>
                  <br />
                  {file ? (
                    <div>
                      {fileContentType ? (
                        <a onClick={openFile(fileContentType, file)}>
                          <img src={`data:${fileContentType};base64,${file}`} style={{ maxHeight: '100px' }} />
                        </a>
                      ) : null}
                      <br />
                      <Row>
                        <Col md="11">
                          <span>
                            {fileContentType}, {byteSize(file)}
                          </span>
                        </Col>
                        <Col md="1">
                          <Button color="danger" onClick={clearBlob('file')}>
                            <FontAwesomeIcon icon="times-circle" />
                          </Button>
                        </Col>
                      </Row>
                    </div>
                  ) : null}
                  <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" />
                  <AvInput type="hidden" name="file" value={file} />
                </AvGroup>
              </AvGroup>
              {/*<AvGroup>*/}
              {/*  <Label id="urlLabel" for="image-url">*/}
              {/*    <Translate contentKey="lunBackendApp.image.url">Url</Translate>*/}
              {/*  </Label>*/}
              {/*  <AvField id="image-url" data-cy="url" type="text" name="url" />*/}
              {/*</AvGroup>*/}
              {/*<AvGroup>*/}
              {/*  <Label id="typeLabel" for="image-type">*/}
              {/*    <Translate contentKey="lunBackendApp.image.type">Type</Translate>*/}
              {/*  </Label>*/}
              {/*  <AvField id="image-type" data-cy="type" type="text" name="type" />*/}
              {/*</AvGroup>*/}
              {/*<AvGroup>*/}
              {/*  <Label id="featureLabel" for="image-feature">*/}
              {/*    <Translate contentKey="lunBackendApp.image.feature">Feature</Translate>*/}
              {/*  </Label>*/}
              {/*  <AvField id="image-feature" data-cy="feature" type="text" name="feature" />*/}
              {/*</AvGroup>*/}
              {/*<AvGroup>*/}
              {/*  <Label id="noteLabel" for="image-note">*/}
              {/*    <Translate contentKey="lunBackendApp.image.note">Note</Translate>*/}
              {/*  </Label>*/}
              {/*  <AvField id="image-note" data-cy="note" type="text" name="note" />*/}
              {/*</AvGroup>*/}
              <AvGroup>
                <Label for="image-eventVenue">
                  <Translate contentKey="lunBackendApp.image.eventVenue">Event Venue</Translate>
                </Label>
                <AvInput
                  id="image-eventVenue"
                  data-cy="eventVenue"
                  type="select"
                  className="form-control"
                  name="eventVenueId"
                  value={!isNew && imageEntity.eventVenue && imageEntity.eventVenue.id}
                >
                  <option value="" key="0" />
                  {eventVenues
                    ? eventVenues.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="image-projct">
                  <Translate contentKey="lunBackendApp.image.projct">Projct</Translate>
                </Label>
                <AvInput
                  id="image-projct"
                  data-cy="projct"
                  type="select"
                  className="form-control"
                  name="projctId"
                  value={!isNew && imageEntity.projct && imageEntity.projct.id}
                >
                  <option value="" key="0" />
                  {projcts
                    ? projcts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="image-trackProgress">
                  <Translate contentKey="lunBackendApp.image.trackProgress">Track Progress</Translate>
                </Label>
                <AvInput
                  id="image-trackProgress"
                  data-cy="trackProgress"
                  type="select"
                  className="form-control"
                  name="trackProgressId"
                  value={!isNew && imageEntity.trackProgress && imageEntity.trackProgress.id}
                >
                  <option value="" key="0" />
                  {trackProgresses
                    ? trackProgresses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="image-task">
                  <Translate contentKey="lunBackendApp.image.task">Task</Translate>
                </Label>
                <AvInput
                  id="image-task"
                  data-cy="task"
                  type="select"
                  className="form-control"
                  name="taskId"
                  value={!isNew && imageEntity.task && imageEntity.task.id}
                >
                  <option value="" key="0" />
                  {tasks
                    ? tasks.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/image" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  eventVenues: storeState.eventVenue.entities,
  items: storeState.item.entities,
  products: storeState.product.entities,
  servicePkgs: storeState.servicePkg.entities,
  packs: storeState.pack.entities,
  designs: storeState.design.entities,
  requestOrders: storeState.requestOrder.entities,
  projcts: storeState.projct.entities,
  trackProgresses: storeState.trackProgress.entities,
  tasks: storeState.task.entities,
  imageEntity: storeState.image.entity,
  loading: storeState.image.loading,
  updating: storeState.image.updating,
  updateSuccess: storeState.image.updateSuccess,
});

const mapDispatchToProps = {
  getEventVenues,
  getItems,
  getProducts,
  getServicePkgs,
  getPacks,
  getDesigns,
  getRequestOrders,
  getProjcts,
  getTrackProgresses,
  getTasks,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ImageUpdate);
