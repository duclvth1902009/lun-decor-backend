import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './image.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IImageDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ImageDetail = (props: IImageDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { imageEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="imageDetailsHeading">
          <Translate contentKey="lunBackendApp.image.detail.title">Image</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{imageEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="url">
                  <Translate contentKey="lunBackendApp.image.url">Url</Translate>
                </span>
              </dt>
              <dd>
                <img src={`${imageEntity.url}`} style={{ maxHeight: '200px' }} />
              </dd>
            </Col>
            {/*<Col md="6">*/}
            {/*  <dt>*/}
            {/*    <Translate contentKey="lunBackendApp.image.eventVenue">Event Venue</Translate>*/}
            {/*  </dt>*/}
            {/*  <dd>{imageEntity.eventVenue ? imageEntity.eventVenue.name : ''}</dd>*/}
            {/*</Col>*/}
            {/*<Col md="6">*/}
            {/*  <dt>*/}
            {/*    <Translate contentKey="lunBackendApp.image.projct">Projct</Translate>*/}
            {/*  </dt>*/}
            {/*  <dd>{imageEntity.projct ? imageEntity.projct.id : ''}</dd>*/}
            {/*</Col>*/}
            {/*<Col md="6">*/}
            {/*  <dt>*/}
            {/*    <Translate contentKey="lunBackendApp.image.trackProgress">Track Progress</Translate>*/}
            {/*  </dt>*/}
            {/*  <dd>{imageEntity.trackProgress ? imageEntity.trackProgress.name : ''}</dd>*/}
            {/*</Col>*/}
            {/*<Col md="6">*/}
            {/*  <dt>*/}
            {/*    <Translate contentKey="lunBackendApp.image.task">Task</Translate>*/}
            {/*  </dt>*/}
            {/*  <dd>{imageEntity.task ? imageEntity.task.name : ''}</dd>*/}
            {/*</Col>*/}
          </Row>
        </dl>
        <Button tag={Link} to="/image" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/image/${imageEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ image }: IRootState) => ({
  imageEntity: image.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ImageDetail);
