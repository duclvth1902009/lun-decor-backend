import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './otp.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOtpDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const OtpDetail = (props: IOtpDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { otpEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="otpDetailsHeading">
          <Translate contentKey="lunBackendApp.otp.detail.title">Otp</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{otpEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="otp_code">
                  <Translate contentKey="lunBackendApp.otp.otp_code">Otp Code</Translate>
                </span>
              </dt>
              <dd>{otpEntity.otp_code}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="expired_time">
                  <Translate contentKey="lunBackendApp.otp.expired_time">Expired Time</Translate>
                </span>
              </dt>
              <dd>{otpEntity.expired_time ? <TextFormat value={otpEntity.expired_time} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="status">
                  <Translate contentKey="lunBackendApp.otp.status">Status</Translate>
                </span>
              </dt>
              <dd>{otpEntity.status}</dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/otp" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/otp/${otpEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ otp }: IRootState) => ({
  otpEntity: otp.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OtpDetail);
