import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './otp.reducer';
import { IOtp } from 'app/shared/model/otp.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOtpProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Otp = (props: IOtpProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { otpList, match, loading } = props;
  return (
    <div>
      <h2 id="otp-heading" data-cy="OtpHeading">
        <Translate contentKey="lunBackendApp.otp.home.title">Otps</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="lunBackendApp.otp.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="lunBackendApp.otp.home.createLabel">Create new Otp</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {otpList && otpList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="lunBackendApp.otp.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="lunBackendApp.otp.otp_code">Otp Code</Translate>
                </th>
                <th>
                  <Translate contentKey="lunBackendApp.otp.expired_time">Expired Time</Translate>
                </th>
                <th>
                  <Translate contentKey="lunBackendApp.otp.status">Status</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {otpList.map((otp, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${otp.id}`} color="link" size="sm">
                      {otp.id}
                    </Button>
                  </td>
                  <td>{otp.otp_code}</td>
                  <td>{otp.expired_time ? <TextFormat type="date" value={otp.expired_time} format={APP_DATE_FORMAT} /> : null}</td>
                  <td>{otp.status}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${otp.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${otp.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${otp.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="lunBackendApp.otp.home.notFound">No Otps found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ otp }: IRootState) => ({
  otpList: otp.entities,
  loading: otp.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Otp);
