import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './otp.reducer';
import { IOtp } from 'app/shared/model/otp.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IOtpUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const OtpUpdate = (props: IOtpUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { otpEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/otp');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.expired_time = convertDateTimeToServer(values.expired_time);

    if (errors.length === 0) {
      const entity = {
        ...otpEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.otp.home.createOrEditLabel" data-cy="OtpCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.otp.home.createOrEditLabel">Create or edit a Otp</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : otpEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="otp-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="otp-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="otp_codeLabel" for="otp-otp_code">
                  <Translate contentKey="lunBackendApp.otp.otp_code">Otp Code</Translate>
                </Label>
                <AvField id="otp-otp_code" data-cy="otp_code" type="text" name="otp_code" />
              </AvGroup>
              <AvGroup>
                <Label id="expired_timeLabel" for="otp-expired_time">
                  <Translate contentKey="lunBackendApp.otp.expired_time">Expired Time</Translate>
                </Label>
                <AvInput
                  id="otp-expired_time"
                  data-cy="expired_time"
                  type="datetime-local"
                  className="form-control"
                  name="expired_time"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.otpEntity.expired_time)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="statusLabel" for="otp-status">
                  <Translate contentKey="lunBackendApp.otp.status">Status</Translate>
                </Label>
                <AvField id="otp-status" data-cy="status" type="string" className="form-control" name="status" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/otp" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  otpEntity: storeState.otp.entity,
  loading: storeState.otp.loading,
  updating: storeState.otp.updating,
  updateSuccess: storeState.otp.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OtpUpdate);
