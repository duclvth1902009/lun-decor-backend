import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Transactn from './transactn';
import TransactnDetail from './transactn-detail';
import TransactnUpdate from './transactn-update';
import TransactnDeleteDialog from './transactn-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={TransactnUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={TransactnUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TransactnDetail} />
      <ErrorBoundaryRoute path={match.url} component={Transactn} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={TransactnDeleteDialog} />
  </>
);

export default Routes;
