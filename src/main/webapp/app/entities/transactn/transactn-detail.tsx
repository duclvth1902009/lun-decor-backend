import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './transactn.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITransactnDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TransactnDetail = (props: ITransactnDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { transactnEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="transactnDetailsHeading">
          <Translate contentKey="lunBackendApp.transactn.detail_title.title">Transactn</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{transactnEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="detail">
                  <Translate contentKey="lunBackendApp.transactn.detail">Detail</Translate>
                </span>
              </dt>
              <dd>{transactnEntity.detail}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="total">
                  <Translate contentKey="lunBackendApp.transactn.total">Total</Translate>
                </span>
              </dt>
              <dd>{transactnEntity.total}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="note">
                  <Translate contentKey="lunBackendApp.transactn.note">Note</Translate>
                </span>
              </dt>
              <dd>{transactnEntity.note}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="status">
                  <Translate contentKey="lunBackendApp.transactn.status">Status</Translate>
                </span>
              </dt>
              <dd>{transactnEntity.status}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.transactn.projct">Projct</Translate>
              </dt>
              <dd>{transactnEntity.projct ? transactnEntity.projct.id : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.transactn.stage">Stage</Translate>
              </dt>
              <dd>{transactnEntity.stage ? transactnEntity.stage.name : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.transactn.atTask">At Task</Translate>
              </dt>
              <dd>{transactnEntity.atTask ? transactnEntity.atTask.name : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.transactn.provider">Provider</Translate>
              </dt>
              <dd>{transactnEntity.provider ? transactnEntity.provider.name : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.transactn.customer">Customer</Translate>
              </dt>
              <dd>{transactnEntity.customer ? transactnEntity.customer.name : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.transactn.image">Image</Translate>
              </dt>
              <dd>{transactnEntity.image ? transactnEntity.image.url : ''}</dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/transactn" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/transactn/${transactnEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ transactn }: IRootState) => ({
  transactnEntity: transactn.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TransactnDetail);
