import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ITransactn, defaultValue } from 'app/shared/model/transactn.model';

export const ACTION_TYPES = {
  FETCH_TRANSACTN_LIST: 'transactn/FETCH_TRANSACTN_LIST',
  FETCH_TRANSACTN: 'transactn/FETCH_TRANSACTN',
  CREATE_TRANSACTN: 'transactn/CREATE_TRANSACTN',
  UPDATE_TRANSACTN: 'transactn/UPDATE_TRANSACTN',
  PARTIAL_UPDATE_TRANSACTN: 'transactn/PARTIAL_UPDATE_TRANSACTN',
  DELETE_TRANSACTN: 'transactn/DELETE_TRANSACTN',
  SET_BLOB: 'transactn/SET_BLOB',
  RESET: 'transactn/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ITransactn>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type TransactnState = Readonly<typeof initialState>;

// Reducer

export default (state: TransactnState = initialState, action): TransactnState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TRANSACTN_LIST):
    case REQUEST(ACTION_TYPES.FETCH_TRANSACTN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_TRANSACTN):
    case REQUEST(ACTION_TYPES.UPDATE_TRANSACTN):
    case REQUEST(ACTION_TYPES.DELETE_TRANSACTN):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_TRANSACTN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_TRANSACTN_LIST):
    case FAILURE(ACTION_TYPES.FETCH_TRANSACTN):
    case FAILURE(ACTION_TYPES.CREATE_TRANSACTN):
    case FAILURE(ACTION_TYPES.UPDATE_TRANSACTN):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_TRANSACTN):
    case FAILURE(ACTION_TYPES.DELETE_TRANSACTN):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_TRANSACTN_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_TRANSACTN):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_TRANSACTN):
    case SUCCESS(ACTION_TYPES.UPDATE_TRANSACTN):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_TRANSACTN):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_TRANSACTN):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType,
        },
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/transactns';

// Actions

export const getEntities: ICrudGetAllAction<ITransactn> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_TRANSACTN_LIST,
    payload: axios.get<ITransactn>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<ITransactn> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TRANSACTN,
    payload: axios.get<ITransactn>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ITransactn> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TRANSACTN,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ITransactn> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TRANSACTN,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<ITransactn> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_TRANSACTN,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ITransactn> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TRANSACTN,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType,
  },
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
