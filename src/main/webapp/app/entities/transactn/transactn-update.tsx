import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { setFileData, openFile, byteSize, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProjct } from 'app/shared/model/projct.model';
import { getEntities as getProjcts } from 'app/entities/projct/projct.reducer';
import { ITrackProgress } from 'app/shared/model/track-progress.model';
import { getEntities as getTrackProgresses } from 'app/entities/track-progress/track-progress.reducer';
import { ITask } from 'app/shared/model/task.model';
import { getEntities as getTasks } from 'app/entities/task/task.reducer';
import { IPartner } from 'app/shared/model/partner.model';
import { getEntities as getPartners } from 'app/entities/partner/partner.reducer';
import { ICustomer } from 'app/shared/model/customer.model';
import { getEntities as getCustomers } from 'app/entities/customer/customer.reducer';
import { IImage } from 'app/shared/model/image.model';
import { getEntities as getImages } from 'app/entities/image/image.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './transactn.reducer';
import { ITransactn } from 'app/shared/model/transactn.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITransactnUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TransactnUpdate = (props: ITransactnUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { transactnEntity, projcts, trackProgresses, tasks, partners, customers, images, loading, updating } = props;

  const { file, fileContentType } = transactnEntity;

  const handleClose = () => {
    props.history.push('/transactn' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getProjcts();
    props.getTrackProgresses();
    props.getTasks();
    props.getPartners();
    props.getCustomers();
    props.getImages();
  }, []);

  const onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => props.setBlob(name, data, contentType), isAnImage);
  };

  const clearBlob = name => () => {
    props.setBlob(name, undefined, undefined);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...transactnEntity,
        ...values,
        projct: projcts.find(it => it.id.toString() === values.projctId.toString()),
        stage: trackProgresses.find(it => it.id.toString() === values.stageId.toString()),
        atTask: tasks.find(it => it.id.toString() === values.atTaskId.toString()),
        provider: partners.find(it => it.id.toString() === values.providerId.toString()),
        customer: customers.find(it => it.id.toString() === values.customerId.toString()),
        image: images.find(it => it.id.toString() === values.imageId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.transactn.home.createOrEditLabel" data-cy="TransactnCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.transactn.home.createOrEditLabel">Create or edit a Transactn</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : transactnEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="transactn-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="transactn-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.transactn.file">File</Translate>
                  </Label>
                  <br />
                  {file ? (
                    <div>
                      {fileContentType ? (
                        <a onClick={openFile(fileContentType, file)}>
                          <img src={`data:${fileContentType};base64,${file}`} style={{ maxHeight: '100px' }} />
                        </a>
                      ) : null}
                      <br />
                      <Row>
                        <Col md="11">
                          <span>
                            {fileContentType}, {byteSize(file)}
                          </span>
                        </Col>
                        <Col md="1">
                          <Button color="danger" onClick={clearBlob('file')}>
                            <FontAwesomeIcon icon="times-circle" />
                          </Button>
                        </Col>
                      </Row>
                    </div>
                  ) : null}
                  <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" />
                  <AvInput type="hidden" name="file" value={file} />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label id="detailLabel" for="transactn-detail">
                  <Translate contentKey="lunBackendApp.transactn.detail">Detail</Translate>
                </Label>
                <AvField id="transactn-detail" data-cy="detail" type="text" name="detail" />
              </AvGroup>
              <AvGroup>
                <Label id="totalLabel" for="transactn-total">
                  <Translate contentKey="lunBackendApp.transactn.total">Total</Translate>
                </Label>
                <AvField id="transactn-total" data-cy="total" type="string" className="form-control" name="total" />
              </AvGroup>
              <AvGroup>
                <Label id="noteLabel" for="transactn-note">
                  <Translate contentKey="lunBackendApp.transactn.note">Note</Translate>
                </Label>
                <AvField id="transactn-note" data-cy="note" type="text" name="note" />
              </AvGroup>
              <AvGroup>
                <Label id="statusLabel" for="transactn-status">
                  <Translate contentKey="lunBackendApp.transactn.status">Status</Translate>
                </Label>
                <AvField id="transactn-status" data-cy="status" type="text" name="status" />
              </AvGroup>
              <AvGroup>
                <Label for="transactn-projct">
                  <Translate contentKey="lunBackendApp.transactn.projct">Projct</Translate>
                </Label>
                <AvInput id="transactn-projct" data-cy="projct" type="select" className="form-control" name="projctId">
                  <option value="" key="0" />
                  {projcts
                    ? projcts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="transactn-stage">
                  <Translate contentKey="lunBackendApp.transactn.stage">Stage</Translate>
                </Label>
                <AvInput id="transactn-stage" data-cy="stage" type="select" className="form-control" name="stageId">
                  <option value="" key="0" />
                  {trackProgresses
                    ? trackProgresses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="transactn-atTask">
                  <Translate contentKey="lunBackendApp.transactn.atTask">At Task</Translate>
                </Label>
                <AvInput id="transactn-atTask" data-cy="atTask" type="select" className="form-control" name="atTaskId">
                  <option value="" key="0" />
                  {tasks
                    ? tasks.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="transactn-provider">
                  <Translate contentKey="lunBackendApp.transactn.provider">Provider</Translate>
                </Label>
                <AvInput id="transactn-provider" data-cy="provider" type="select" className="form-control" name="providerId">
                  <option value="" key="0" />
                  {partners
                    ? partners.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="transactn-customer">
                  <Translate contentKey="lunBackendApp.transactn.customer">Customer</Translate>
                </Label>
                <AvInput id="transactn-customer" data-cy="customer" type="select" className="form-control" name="customerId">
                  <option value="" key="0" />
                  {customers
                    ? customers.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="transactn-image">
                  <Translate contentKey="lunBackendApp.transactn.image">Image</Translate>
                </Label>
                <AvInput id="transactn-image" data-cy="image" type="select" className="form-control" name="imageId">
                  <option value="" key="0" />
                  {images
                    ? images.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.url}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/transactn" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  projcts: storeState.projct.entities,
  trackProgresses: storeState.trackProgress.entities,
  tasks: storeState.task.entities,
  partners: storeState.partner.entities,
  customers: storeState.customer.entities,
  images: storeState.image.entities,
  transactnEntity: storeState.transactn.entity,
  loading: storeState.transactn.loading,
  updating: storeState.transactn.updating,
  updateSuccess: storeState.transactn.updateSuccess,
});

const mapDispatchToProps = {
  getProjcts,
  getTrackProgresses,
  getTasks,
  getPartners,
  getCustomers,
  getImages,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TransactnUpdate);
