import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import Manage from 'app/manage';
import Image from './image';
import Partner from './partner';
import Item from './item';
import Customer from './customer';
import Comment from './comment';
import Design from './design';
import Topic from './topic';
import Product from './product';
import ServicePkg from './service-pkg';
import Pack from './pack';
import RequestOrder from './request-order';
import EventType from './event-type';
import EventVenue from './event-venue';
import Projct from './projct';
import TrackProgress from './track-progress';
import Task from './task';
import Transactn from './transactn';
import Otp from './otp';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}image`} component={Image} />
      <ErrorBoundaryRoute path={`${match.url}partner`} component={Partner} />
      <ErrorBoundaryRoute path={`${match.url}item`} component={Item} />
      <ErrorBoundaryRoute path={`${match.url}customer`} component={Customer} />
      <ErrorBoundaryRoute path={`${match.url}comment`} component={Comment} />
      <ErrorBoundaryRoute path={`${match.url}design`} component={Design} />
      <ErrorBoundaryRoute path={`${match.url}topic`} component={Topic} />
      <ErrorBoundaryRoute path={`${match.url}product`} component={Product} />
      <ErrorBoundaryRoute path={`${match.url}service-pkg`} component={ServicePkg} />
      <ErrorBoundaryRoute path={`${match.url}pack`} component={Pack} />
      <ErrorBoundaryRoute path={`${match.url}request-order`} component={RequestOrder} />
      <ErrorBoundaryRoute path={`${match.url}event-type`} component={EventType} />
      <ErrorBoundaryRoute path={`${match.url}event-venue`} component={EventVenue} />
      <ErrorBoundaryRoute path={`${match.url}projct`} component={Projct} />
      <ErrorBoundaryRoute path={`${match.url}track-progress`} component={TrackProgress} />
      <ErrorBoundaryRoute path={`${match.url}task`} component={Task} />
      <ErrorBoundaryRoute path={`${match.url}transactn`} component={Transactn} />
      <ErrorBoundaryRoute path={`${match.url}otp`} component={Otp} />
      <ErrorBoundaryRoute path={`${match.url}manage/`} component={Manage} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
