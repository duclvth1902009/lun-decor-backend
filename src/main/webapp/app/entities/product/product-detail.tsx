import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './product.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProductDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductDetail = (props: IProductDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { productEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="productDetailsHeading">
          <Translate contentKey="lunBackendApp.product.detail.title">Product</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{productEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.product.name">Name</Translate>
                </span>
              </dt>
              <dd>{productEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="code">
                  <Translate contentKey="lunBackendApp.product.code">Code</Translate>
                </span>
              </dt>
              <dd>{productEntity.code}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="price">
                  <Translate contentKey="lunBackendApp.product.price">Price</Translate>
                </span>
              </dt>
              <dd>{productEntity.price}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="description">
                  <Translate contentKey="lunBackendApp.product.description">Description</Translate>
                </span>
              </dt>
              <dd>{productEntity.description}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="unit">
                  <Translate contentKey="lunBackendApp.product.unit">Unit</Translate>
                </span>
              </dt>
              <dd>{productEntity.unit}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.product.images">Images</Translate>
              </dt>
              <dd>
                {productEntity.images
                  ? productEntity.images.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.url}</a>
                        {/*<img src={`${val.url}`} style={{ maxHeight: '220px' }} />*/}
                        {productEntity.images && i === productEntity.images.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.product.items">Items</Translate>
              </dt>
              <dd>
                {productEntity.items
                  ? productEntity.items.map((val, i) => (
                      <span key={val.id}>
                        <a href={`item/${val.id}`}>{val.name}</a>
                        {productEntity.items && i === productEntity.items.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/product" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/product/${productEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ product }: IRootState) => ({
  productEntity: product.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
