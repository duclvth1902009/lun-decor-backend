import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getItems } from 'app/entities/item/item.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './product.reducer';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProductUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductUpdate = (props: IProductUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { productEntity, items, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/product' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getItems();
  }, []);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...productEntity,
        ...values,
        files: fileList,
        items: mapIdList(values.items),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.product.home.createOrEditLabel" data-cy="ProductCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.product.home.createOrEditLabel">Create or edit a Product</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : productEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="product-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="product-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.product.file">File</Translate>
                  </Label>
                  <Row>{renderPhotos(fileList)}</Row>
                  <br />
                  <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
                </AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.product.images">File</Translate>
                  </Label>
                  <Row>
                    {productEntity.images
                      ? productEntity.images.map(otherEntity => (
                          <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                        ))
                      : null}
                  </Row>
                  <br />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="product-name">
                  <Translate contentKey="lunBackendApp.product.name">Name</Translate>
                </Label>
                <AvField id="product-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="codeLabel" for="product-code">
                  <Translate contentKey="lunBackendApp.product.code">Code</Translate>
                </Label>
                <AvField
                  id="product-code"
                  data-cy="code"
                  type="text"
                  name="code"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="priceLabel" for="product-price">
                  <Translate contentKey="lunBackendApp.product.price">Price</Translate>
                </Label>
                <AvField id="product-price" data-cy="price" type="string" className="form-control" name="price" />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="product-description">
                  <Translate contentKey="lunBackendApp.product.description">Description</Translate>
                </Label>
                <AvField id="product-description" data-cy="description" type="text" name="description" />
              </AvGroup>
              <AvGroup>
                <Label id="unitLabel" for="product-unit">
                  <Translate contentKey="lunBackendApp.product.unit">Unit</Translate>
                </Label>
                <AvField id="product-unit" data-cy="unit" type="text" name="unit" />
              </AvGroup>
              <AvGroup>
                <Label for="product-items">
                  <Translate contentKey="lunBackendApp.product.items">Items</Translate>
                </Label>
                <AvInput
                  id="product-items"
                  data-cy="items"
                  type="select"
                  multiple
                  className="form-control"
                  name="items"
                  value={!isNew && productEntity.items && productEntity.items.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {items
                    ? items.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/product" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  images: storeState.image.entities,
  items: storeState.item.entities,
  productEntity: storeState.product.entity,
  loading: storeState.product.loading,
  updating: storeState.product.updating,
  updateSuccess: storeState.product.updateSuccess,
});

const mapDispatchToProps = {
  getItems,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductUpdate);
