import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getImages } from 'app/entities/image/image.reducer';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { getEntities as getServicePkgs } from 'app/entities/service-pkg/service-pkg.reducer';

import { getEntities as getEventTypes } from 'app/entities/event-type/event-type.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './pack.reducer';

import { mapIdList } from 'app/shared/util/entity-utils';
import { CKEditor } from 'ckeditor4-react';

export interface IPackUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PackUpdate = (props: IPackUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { packEntity, images, products, servicePkgs, eventTypes, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/pack' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getImages();
    props.getProducts();
    props.getServicePkgs();
    props.getEventTypes();
  }, []);

  useEffect(() => {
    setImgList(packEntity.images);
  }, [packEntity]);

  const [fileList, setFileList] = useState([]);
  const [imgList, setImgList] = useState([]);

  const renderFiles = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };

  const renderPhotos = source => {
    return source.map(img => {
      return (
        <Col
          key={img.id}
          md="2"
          style={{
            backgroundImage: `url("${img.url}")`,
            height: 100,
            width: 'auto',
            backgroundSize: 100,
            backgroundRepeat: 'no-repeat',
          }}
        >
          <Button color="danger" onClick={removeImageMapping(img)}>
            <FontAwesomeIcon icon="times-circle" />
          </Button>
        </Col>
      );
    });
  };

  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };

  const removeImageMapping = img => () => {
    const arrayImage = [...imgList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setImgList(arrayImage);
    }
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...packEntity,
        ...values,
        description: values.description.editor ? values.description.editor.document.$.body.innerHTML : packEntity.description,
        files: fileList,
        images: imgList,
        products: mapIdList(values.products),
        services: mapIdList(values.services),
        type: eventTypes.find(it => it.id.toString() === values.typeId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.pack.home.createOrEditLabel" data-cy="PackCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.pack.home.createOrEditLabel">Create or edit a Pack</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : packEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="pack-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="pack-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.pack.file">File</Translate>
                  </Label>
                  <Row>{renderFiles(fileList)}</Row>
                  <br />
                  <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
                </AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.pack.images">File</Translate>
                  </Label>
                  <Row>{imgList && renderPhotos(imgList)}</Row>
                  <br />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="pack-name">
                  <Translate contentKey="lunBackendApp.pack.name">Name</Translate>
                </Label>
                <AvField id="pack-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="price_fromLabel" for="pack-price_from">
                  <Translate contentKey="lunBackendApp.pack.price_from">Price From</Translate>
                </Label>
                {/*<CurrencyInput*/}
                {/*  id="pack-price_from"*/}
                {/*  name="price_from"*/}
                {/*  placeholder="Please enter a number"*/}
                {/*  value={packEntity.price_from ? packEntity.price_from : 0}*/}
                {/*  data-cy="price_from"*/}
                {/*  className="form-control"*/}
                {/*  suffix="đ"*/}
                {/*/>*/}
                <AvField id="pack-price_from" data-cy="price_from" type="string" className="form-control" name="price_from" />
              </AvGroup>
              <AvGroup>
                <Label id="price_toLabel" for="pack-price_to">
                  <Translate contentKey="lunBackendApp.pack.price_to">Price To</Translate>
                </Label>
                {/*<CurrencyInput*/}
                {/*  id="pack-price_to"*/}
                {/*  name="price_to"*/}
                {/*  placeholder="Please enter a number"*/}
                {/*  value={packEntity.price_to}*/}
                {/*  value={packEntity.price_to ? packEntity.price_to : 0}*/}
                {/*  data-cy="price_to"*/}
                {/*  className="form-control"*/}
                {/*  suffix="đ"*/}
                {/*/>*/}
                <AvField id="pack-price_to" data-cy="price_to" type="string" className="form-control" name="price_to" />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="pack-description">
                  <Translate contentKey="lunBackendApp.pack.description">Description</Translate>
                </Label>
                <AvField
                  tag={CKEditor}
                  id="pack-description"
                  data-cy="description"
                  type="classic"
                  name="description"
                  initData={packEntity.description ? packEntity.description : '<p>Hãy mô tả về gói tiệc này</p>'}
                />
              </AvGroup>
              {/*<AvGroup className="image-select">*/}
              {/*  <Label for="pack-images">*/}
              {/*    <Translate contentKey="lunBackendApp.pack.images">Images</Translate>*/}
              {/*  </Label>*/}
              {/*  <AvInput*/}
              {/*    id="pack-images"*/}
              {/*    data-cy="images"*/}
              {/*    type="select"*/}
              {/*    multiple*/}
              {/*    className="form-control"*/}
              {/*    name="images"*/}
              {/*    value={!isNew && packEntity.images && packEntity.images.map(e => e.id)}*/}
              {/*  >*/}
              {/*    <option value="" key="0" style={{ height: 10, width: 150 }} />*/}
              {/*    {packEntity.images*/}
              {/*      ? packEntity.images.map(otherEntity => (*/}
              {/*        <option*/}
              {/*          value={otherEntity.id}*/}
              {/*          key={otherEntity.id}*/}
              {/*          style={{*/}
              {/*            backgroundImage: `url(${otherEntity.url})`,*/}
              {/*            height: 150,*/}
              {/*            width: 170,*/}
              {/*            backgroundSize: 150,*/}
              {/*            backgroundRepeat: 'no-repeat',*/}
              {/*          }}*/}
              {/*        />*/}
              {/*      ))*/}
              {/*      : null}*/}
              {/*  </AvInput>*/}
              {/*</AvGroup>*/}
              <AvGroup>
                <Label for="pack-products">
                  <Translate contentKey="lunBackendApp.pack.products">Products</Translate>
                </Label>
                <AvInput
                  id="pack-products"
                  data-cy="products"
                  type="select"
                  multiple
                  className="form-control"
                  name="products"
                  value={!isNew && packEntity.products && packEntity.products.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {products
                    ? products.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="pack-services">
                  <Translate contentKey="lunBackendApp.pack.services">Services</Translate>
                </Label>
                <AvInput
                  id="pack-services"
                  data-cy="services"
                  type="select"
                  multiple
                  className="form-control"
                  name="services"
                  value={!isNew && packEntity.services && packEntity.services.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {servicePkgs
                    ? servicePkgs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="pack-type">
                  <Translate contentKey="lunBackendApp.pack.type">Type</Translate>
                </Label>
                <AvInput
                  id="pack-type"
                  data-cy="type"
                  type="select"
                  className="form-control"
                  name="typeId"
                  value={!isNew && packEntity.type && packEntity.type.id}
                >
                  <option value="" key="0" />
                  {eventTypes
                    ? eventTypes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/pack" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
      {/*<CKEditor*/}
      {/*  data="<p>Hello from CKEditor 4!</p>"*/}
      {/*  initData="<p>Hello from CKEditor 4!</p>"*/}
      {/*/>*/}
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  images: storeState.image.entities,
  products: storeState.product.entities,
  servicePkgs: storeState.servicePkg.entities,
  eventTypes: storeState.eventType.entities,
  packEntity: storeState.pack.entity,
  loading: storeState.pack.loading,
  updating: storeState.pack.updating,
  updateSuccess: storeState.pack.updateSuccess,
});

const mapDispatchToProps = {
  getImages,
  getProducts,
  getServicePkgs,
  getEventTypes,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PackUpdate);
