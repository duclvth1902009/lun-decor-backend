import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './pack.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPackDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PackDetail = (props: IPackDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { packEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="packDetailsHeading">
          <Translate contentKey="lunBackendApp.pack.detail.title">Pack</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{packEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.pack.name">Name</Translate>
                </span>
              </dt>
              <dd>{packEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="price_from">
                  <Translate contentKey="lunBackendApp.pack.price_from">Price From</Translate>
                </span>
              </dt>
              <dd>{packEntity.price_from}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="price_to">
                  <Translate contentKey="lunBackendApp.pack.price_to">Price To</Translate>
                </span>
              </dt>
              <dd>{packEntity.price_to}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="description">
                  <Translate contentKey="lunBackendApp.pack.description">Description</Translate>
                </span>
              </dt>
              <dd>{packEntity.description}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.pack.images">Images</Translate>
              </dt>
              <dd>
                {packEntity.images
                  ? packEntity.images.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.url}</a>
                        {packEntity.images && i === packEntity.images.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.pack.products">Products</Translate>
              </dt>
              <dd>
                {packEntity.products
                  ? packEntity.products.map((val, i) => (
                      <span key={val.id}>
                        <a href={`product/${val.id}`}>{val.name}</a>
                        {packEntity.products && i === packEntity.products.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.pack.services">Services</Translate>
              </dt>
              <dd>
                {packEntity.services
                  ? packEntity.services.map((val, i) => (
                      <span key={val.id}>
                        <a href={`service-pkg/${val.id}`}>{val.name}</a>
                        {packEntity.services && i === packEntity.services.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.pack.type">Type</Translate>
              </dt>
              <dd>{packEntity.type ? <a href={`event-type/${packEntity.type.id}`}>{packEntity.type.name}</a> : ''}</dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/pack" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/pack/${packEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ pack }: IRootState) => ({
  packEntity: pack.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PackDetail);
