import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './track-progress.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITrackProgressDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TrackProgressDetail = (props: ITrackProgressDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { trackProgressEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="trackProgressDetailsHeading">
          <Translate contentKey="lunBackendApp.trackProgress.detail.title">TrackProgress</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{trackProgressEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.trackProgress.name">Name</Translate>
                </span>
              </dt>
              <dd>{trackProgressEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="description">
                  <Translate contentKey="lunBackendApp.trackProgress.description">Description</Translate>
                </span>
              </dt>
              <dd>{trackProgressEntity.description}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="clientEnable">
                  <Translate contentKey="lunBackendApp.trackProgress.clientEnable">Client Enable</Translate>
                </span>
              </dt>
              <dd>{trackProgressEntity.clientEnable ? 'true' : 'false'}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="step">
                  <Translate contentKey="lunBackendApp.trackProgress.step">Step</Translate>
                </span>
              </dt>
              <dd>{trackProgressEntity.step}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="deadLine">
                  <Translate contentKey="lunBackendApp.trackProgress.deadLine">Dead Line</Translate>
                </span>
              </dt>
              <dd>
                {trackProgressEntity.deadLine ? (
                  <TextFormat value={trackProgressEntity.deadLine} type="date" format={APP_DATE_FORMAT} />
                ) : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="status">
                  <Translate contentKey="lunBackendApp.trackProgress.status">Status</Translate>
                </span>
              </dt>
              <dd>{trackProgressEntity.status}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.trackProgress.projct">Projct</Translate>
              </dt>
              <dd>
                {trackProgressEntity.projct ? <a href={`projct/${trackProgressEntity.projct.id}`}>{trackProgressEntity.projct.id}</a> : ''}
              </dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/track-progress" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/track-progress/${trackProgressEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ trackProgress }: IRootState) => ({
  trackProgressEntity: trackProgress.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TrackProgressDetail);
