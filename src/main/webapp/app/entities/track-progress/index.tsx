import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import TrackProgress from './track-progress';
import TrackProgressDetail from './track-progress-detail';
import TrackProgressUpdate from './track-progress-update';
import TrackProgressDeleteDialog from './track-progress-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={TrackProgressUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={TrackProgressUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TrackProgressDetail} />
      <ErrorBoundaryRoute path={match.url} component={TrackProgress} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={TrackProgressDeleteDialog} />
  </>
);

export default Routes;
