import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ITrackProgress, defaultValue } from 'app/shared/model/track-progress.model';

export const ACTION_TYPES = {
  FETCH_TRACKPROGRESS_LIST: 'trackProgress/FETCH_TRACKPROGRESS_LIST',
  FETCH_TRACKPROGRESS: 'trackProgress/FETCH_TRACKPROGRESS',
  CREATE_TRACKPROGRESS: 'trackProgress/CREATE_TRACKPROGRESS',
  UPDATE_TRACKPROGRESS: 'trackProgress/UPDATE_TRACKPROGRESS',
  PARTIAL_UPDATE_TRACKPROGRESS: 'trackProgress/PARTIAL_UPDATE_TRACKPROGRESS',
  DELETE_TRACKPROGRESS: 'trackProgress/DELETE_TRACKPROGRESS',
  SET_BLOB: 'trackProgress/SET_BLOB',
  RESET: 'trackProgress/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ITrackProgress>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type TrackProgressState = Readonly<typeof initialState>;

// Reducer

export default (state: TrackProgressState = initialState, action): TrackProgressState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TRACKPROGRESS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_TRACKPROGRESS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_TRACKPROGRESS):
    case REQUEST(ACTION_TYPES.UPDATE_TRACKPROGRESS):
    case REQUEST(ACTION_TYPES.DELETE_TRACKPROGRESS):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_TRACKPROGRESS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_TRACKPROGRESS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_TRACKPROGRESS):
    case FAILURE(ACTION_TYPES.CREATE_TRACKPROGRESS):
    case FAILURE(ACTION_TYPES.UPDATE_TRACKPROGRESS):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_TRACKPROGRESS):
    case FAILURE(ACTION_TYPES.DELETE_TRACKPROGRESS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_TRACKPROGRESS_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_TRACKPROGRESS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_TRACKPROGRESS):
    case SUCCESS(ACTION_TYPES.UPDATE_TRACKPROGRESS):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_TRACKPROGRESS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_TRACKPROGRESS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType,
        },
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/track-progresses';

// Actions

export const getEntities: ICrudGetAllAction<ITrackProgress> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_TRACKPROGRESS_LIST,
    payload: axios.get<ITrackProgress>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getTrackByProject: ICrudGetAction<ITrackProgress> = projctId => {
  const requestUrl = `api/track-progresses/projct/${projctId}`;
  return {
    type: ACTION_TYPES.FETCH_TRACKPROGRESS,
    payload: axios.get<ITrackProgress>(requestUrl),
  };
};

export const getEntity: ICrudGetAction<ITrackProgress> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TRACKPROGRESS,
    payload: axios.get<ITrackProgress>(requestUrl),
  };
};

export const createEntityForProject: ICrudPutAction<ITrackProgress> = entity => async dispatch => {
  const requestUrl = `api/track-progresses/projct/`;
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TRACKPROGRESS,
    payload: axios.post(requestUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const createEntity: ICrudPutAction<ITrackProgress> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TRACKPROGRESS,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ITrackProgress> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TRACKPROGRESS,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<ITrackProgress> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_TRACKPROGRESS,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ITrackProgress> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TRACKPROGRESS,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType,
  },
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
