import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Input, Row, Table } from 'reactstrap';
import { Translate, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './request-order.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { getEntitiesByStatus } from 'app/manage/request-order/request-order.reducer';

export interface IRequestOrderProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const RequestOrder = (props: IRequestOrderProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'id'), props.location.search)
  );

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get('sort');
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const handleSyncList = () => {
    sortEntities();
  };

  const search = stt => {
    props.getEntitiesByStatus(
      stt.target.value,
      paginationState.activePage - 1,
      paginationState.itemsPerPage,
      `${paginationState.sort},${paginationState.order}`
    );
  };

  const { requestOrderList, match, loading, totalItems } = props;
  return (
    <div>
      <h2 id="request-order-heading" data-cy="RequestOrderHeading">
        <Translate contentKey="lunBackendApp.requestOrder.home.title">Request Orders</Translate>
        <div className="d-flex justify-content-end">
          <Col md="4">
            <Input st id="projct-status" data-cy="status" onChange={search} type="select" className="form-control" name="status">
              <option value=" " key="all">
                getAll
              </option>
              <option value="pending" key="pending">
                pending
              </option>
              <option value="active" key="active">
                active
              </option>
              <option value="done" key="done">
                done
              </option>
              <option value="cancel" key="cancel">
                cancel
              </option>
            </Input>
          </Col>
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="lunBackendApp.requestOrder.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="lunBackendApp.requestOrder.home.createLabel">Create new Request Order</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {requestOrderList && requestOrderList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                {/*<th className="hand" onClick={sort('id')}>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.id">ID</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th className="hand" onClick={sort('phone')}>
                  <Translate contentKey="lunBackendApp.requestOrder.phone">Phone</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th className="hand" onClick={sort('email')}>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.email">Email</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th className="hand" onClick={sort('cusName')}>
                  <Translate contentKey="lunBackendApp.requestOrder.cusName">Cus Name</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th className="hand" onClick={sort('discount')}>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.discount">Discount</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('about')}>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.about">About</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th className="hand" onClick={sort('holdTime')}>
                  <Translate contentKey="lunBackendApp.requestOrder.holdTime">Hold Time</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th className="hand" onClick={sort('budget')}>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.budget">Budget</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('address')}>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.address">Address</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('guestNum')}>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.guestNum">Guest Num</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                {/*<th className="hand" onClick={sort('description')}>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.description">Description</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th className="hand" onClick={sort('status')}>
                  <Translate contentKey="lunBackendApp.requestOrder.status">Status</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="lunBackendApp.requestOrder.pack">Pack</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="lunBackendApp.requestOrder.type">Type</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                {/*<th>*/}
                {/*  <Translate contentKey="lunBackendApp.requestOrder.eventVenue">Event Venue</Translate> <FontAwesomeIcon icon="sort" />*/}
                {/*</th>*/}
                <th />
              </tr>
            </thead>
            <tbody>
              {requestOrderList.map((requestOrder, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  {/*<td>*/}
                  {/*  <Button tag={Link} to={`${match.url}/${requestOrder.id}`} color="link" size="sm">*/}
                  {/*    {requestOrder.id}*/}
                  {/*  </Button>*/}
                  {/*</td>*/}
                  <td>{requestOrder.phone}</td>
                  {/*<td>{requestOrder.email}</td>*/}
                  <td>{requestOrder.cusName}</td>
                  {/*<td>{requestOrder.discount}</td>*/}
                  {/*<td>{requestOrder.about}</td>*/}
                  <td>
                    {requestOrder.holdTime ? <TextFormat type="date" value={requestOrder.holdTime} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  {/*<td>{requestOrder.budget}</td>*/}
                  {/*<td>{requestOrder.address}</td>*/}
                  {/*<td>{requestOrder.guestNum}</td>*/}
                  {/*<td>{requestOrder.description}</td>*/}
                  <td>{requestOrder.status}</td>
                  <td>{requestOrder.pack ? <Link to={`pack/${requestOrder.pack.id}`}>{requestOrder.pack.name}</Link> : ''}</td>
                  <td>{requestOrder.type ? <Link to={`event-type/${requestOrder.type.id}`}>{requestOrder.type.name}</Link> : ''}</td>
                  {/*<td>*/}
                  {/*  {requestOrder.eventVenue ? (*/}
                  {/*    <Link to={`event-venue/${requestOrder.eventVenue.id}`}>{requestOrder.eventVenue.name}</Link>*/}
                  {/*  ) : (*/}
                  {/*    ''*/}
                  {/*  )}*/}
                  {/*</td>*/}
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${requestOrder.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${requestOrder.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${requestOrder.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="lunBackendApp.requestOrder.home.notFound">No Request Orders found</Translate>
            </div>
          )
        )}
      </div>
      {props.totalItems ? (
        <div className={requestOrderList && requestOrderList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

const mapStateToProps = ({ requestOrder }: IRootState) => ({
  requestOrderList: requestOrder.entities,
  loading: requestOrder.loading,
  totalItems: requestOrder.totalItems,
});

const mapDispatchToProps = {
  getEntities,
  getEntitiesByStatus,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RequestOrder);
