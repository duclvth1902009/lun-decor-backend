import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import RequestOrder from './request-order';
import RequestOrderDetail from './request-order-detail';
import RequestOrderUpdate from './request-order-update';
import RequestOrderDeleteDialog from './request-order-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={RequestOrderUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={RequestOrderUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={RequestOrderDetail} />
      <ErrorBoundaryRoute path={match.url} component={RequestOrder} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={RequestOrderDeleteDialog} />
  </>
);

export default Routes;
