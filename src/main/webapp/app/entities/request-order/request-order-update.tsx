import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { setFileData, openFile, byteSize, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getPacks } from 'app/entities/pack/pack.reducer';
import { getEntities as getTopics } from 'app/entities/topic/topic.reducer';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { getEntities as getServicePkgs } from 'app/entities/service-pkg/service-pkg.reducer';
import { getEntities as getEventTypes } from 'app/entities/event-type/event-type.reducer';
import { getEntities as getEventVenues } from 'app/entities/event-venue/event-venue.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './request-order.reducer';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import CurrencyInput from 'react-currency-input-field';

export interface IRequestOrderUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RequestOrderUpdate = (props: IRequestOrderUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { requestOrderEntity, packs, topics, products, servicePkgs, eventTypes, eventVenues, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/request-order' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getPacks();
    props.getTopics();
    props.getProducts();
    props.getServicePkgs();
    props.getEventTypes();
    props.getEventVenues();
  }, []);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.holdTime = convertDateTimeToServer(values.holdTime);

    if (errors.length === 0) {
      const entity = {
        ...requestOrderEntity,
        ...values,
        files: fileList,
        topics: mapIdList(values.topics),
        add_products: mapIdList(values.add_products),
        sub_products: mapIdList(values.sub_products),
        add_services: mapIdList(values.add_services),
        sub_services: mapIdList(values.sub_services),
        pack: packs.find(it => it.id.toString() === values.packId.toString()),
        type: eventTypes.find(it => it.id.toString() === values.typeId.toString()),
        eventVenue: eventVenues.find(it => it.id.toString() === values.eventVenueId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.requestOrder.home.createOrEditLabel" data-cy="RequestOrderCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.requestOrder.home.createOrEditLabel">Create or edit a RequestOrder</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : requestOrderEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="request-order-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="request-order-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="fileLabel" for="file">
                  <Translate contentKey="lunBackendApp.requestOrder.file">File</Translate>
                </Label>
                <Row>{renderPhotos(fileList)}</Row>
                <br />
                <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
              </AvGroup>
              <AvGroup>
                <Label id="fileLabel" for="file">
                  <Translate contentKey="lunBackendApp.requestOrder.images">File</Translate>
                </Label>
                <Row>
                  {requestOrderEntity.images
                    ? requestOrderEntity.images.map(otherEntity => (
                        <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                      ))
                    : null}
                </Row>
                <br />
              </AvGroup>
              <AvGroup>
                <Label id="phoneLabel" for="request-order-phone">
                  <Translate contentKey="lunBackendApp.requestOrder.phone">Phone</Translate>
                </Label>
                <AvField
                  id="request-order-phone"
                  data-cy="phone"
                  type="text"
                  name="phone"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    pattern: { value: '[0-9]+', errorMessage: translate('entity.validation.pattern', { pattern: '[0-9]+' }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="emailLabel" for="request-order-email">
                  <Translate contentKey="lunBackendApp.requestOrder.email">Email</Translate>
                </Label>
                <AvField id="request-order-email" data-cy="email" type="text" name="email" />
              </AvGroup>
              <AvGroup>
                <Label id="cusNameLabel" for="request-order-cusName">
                  <Translate contentKey="lunBackendApp.requestOrder.cusName">Cus Name</Translate>
                </Label>
                <AvField id="request-order-cusName" data-cy="cusName" type="text" name="cusName" />
              </AvGroup>
              <AvGroup>
                <Label id="discountLabel" for="request-order-discount">
                  <Translate contentKey="lunBackendApp.requestOrder.discount">Discount</Translate>
                </Label>
                <AvField id="request-order-discount" data-cy="discount" type="text" name="discount" />
              </AvGroup>
              <AvGroup>
                <Label id="aboutLabel" for="request-order-about">
                  <Translate contentKey="lunBackendApp.requestOrder.about">About</Translate>
                </Label>
                <AvField id="request-order-about" data-cy="about" type="text" name="about" />
              </AvGroup>
              <AvGroup>
                <Label id="holdTimeLabel" for="request-order-holdTime">
                  <Translate contentKey="lunBackendApp.requestOrder.holdTime">Hold Time</Translate>
                </Label>
                <AvInput
                  id="request-order-holdTime"
                  data-cy="holdTime"
                  type="datetime-local"
                  className="form-control"
                  name="holdTime"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={
                    !props.requestOrderEntity.holdTime
                      ? displayDefaultDateTime()
                      : convertDateTimeFromServer(props.requestOrderEntity.holdTime)
                  }
                />
              </AvGroup>
              <AvGroup>
                <Label id="budgetLabel" for="request-order-budget">
                  <Translate contentKey="lunBackendApp.requestOrder.budget">Budget</Translate>
                </Label>
                {/*<CurrencyInput*/}
                {/*  id="request-order-budget"*/}
                {/*  name="budget"*/}
                {/*  placeholder="Please enter a number"*/}
                {/*  value={requestOrderEntity.budget}*/}
                {/*  data-cy="budget"*/}
                {/*  className="form-control"*/}
                {/*  suffix="đ"*/}
                {/*/>*/}
                <AvField id="request-order-budget" data-cy="budget" type="text" name="budget" />
              </AvGroup>
              <AvGroup>
                <Label id="addressLabel" for="request-order-address">
                  <Translate contentKey="lunBackendApp.requestOrder.address">Address</Translate>
                </Label>
                <AvField id="request-order-address" data-cy="address" type="text" name="address" />
              </AvGroup>
              <AvGroup>
                <Label id="guestNumLabel" for="request-order-guestNum">
                  <Translate contentKey="lunBackendApp.requestOrder.guestNum">Guest Num</Translate>
                </Label>
                <AvField id="request-order-guestNum" data-cy="guestNum" type="text" name="guestNum" />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="request-order-description">
                  <Translate contentKey="lunBackendApp.requestOrder.description">Description</Translate>
                </Label>
                <AvField id="request-order-description" data-cy="description" type="text" name="description" />
              </AvGroup>
              <AvGroup>
                <Label id="statusLabel" for="request-order-status">
                  <Translate contentKey="lunBackendApp.requestOrder.status">Status</Translate>
                </Label>
                <AvInput id="request-order-status" data-cy="status" type="select" className="form-control" name="status">
                  <option value="active" key="active">
                    active
                  </option>
                  <option value="waitActive" key="waitActive">
                    waitActive
                  </option>
                  <option value="inActive" key="inActive">
                    inActive
                  </option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="request-order-pack">
                  <Translate contentKey="lunBackendApp.requestOrder.pack">Pack</Translate>
                </Label>
                <AvInput
                  id="request-order-pack"
                  data-cy="pack"
                  type="select"
                  className="form-control"
                  name="packId"
                  value={!isNew && requestOrderEntity.pack && requestOrderEntity.pack.id}
                >
                  <option value="" key="0" />
                  {packs
                    ? packs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="request-order-topics">
                  <Translate contentKey="lunBackendApp.requestOrder.topics">Topics</Translate>
                </Label>
                <AvInput
                  id="request-order-topics"
                  data-cy="topics"
                  type="select"
                  multiple
                  className="form-control"
                  name="topics"
                  value={!isNew && requestOrderEntity.topics && requestOrderEntity.topics.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {topics
                    ? topics.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="request-order-add_products">
                  <Translate contentKey="lunBackendApp.requestOrder.add_products">Add Products</Translate>
                </Label>
                <AvInput
                  id="request-order-add_products"
                  data-cy="add_products"
                  type="select"
                  multiple
                  className="form-control"
                  name="add_products"
                  value={!isNew && requestOrderEntity.add_products && requestOrderEntity.add_products.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {products
                    ? products.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="request-order-sub_products">
                  <Translate contentKey="lunBackendApp.requestOrder.sub_products">Sub Products</Translate>
                </Label>
                <AvInput
                  id="request-order-sub_products"
                  data-cy="sub_products"
                  type="select"
                  multiple
                  className="form-control"
                  name="sub_products"
                  value={!isNew && requestOrderEntity.sub_products && requestOrderEntity.sub_products.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {products
                    ? products.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="request-order-add_services">
                  <Translate contentKey="lunBackendApp.requestOrder.add_services">Add Services</Translate>
                </Label>
                <AvInput
                  id="request-order-add_services"
                  data-cy="add_services"
                  type="select"
                  multiple
                  className="form-control"
                  name="add_services"
                  value={!isNew && requestOrderEntity.add_services && requestOrderEntity.add_services.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {servicePkgs
                    ? servicePkgs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="request-order-sub_services">
                  <Translate contentKey="lunBackendApp.requestOrder.sub_services">Sub Services</Translate>
                </Label>
                <AvInput
                  id="request-order-sub_services"
                  data-cy="sub_services"
                  type="select"
                  multiple
                  className="form-control"
                  name="sub_services"
                  value={!isNew && requestOrderEntity.sub_services && requestOrderEntity.sub_services.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {servicePkgs
                    ? servicePkgs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="request-order-type">
                  <Translate contentKey="lunBackendApp.requestOrder.type">Type</Translate>
                </Label>
                <AvInput id="request-order-type" data-cy="type" type="select" className="form-control" name="typeId">
                  <option value="" key="0" />
                  {eventTypes
                    ? eventTypes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="request-order-eventVenue">
                  <Translate contentKey="lunBackendApp.requestOrder.eventVenue">Event Venue</Translate>
                </Label>
                <AvInput id="request-order-eventVenue" data-cy="eventVenue" type="select" className="form-control" name="eventVenueId">
                  <option value="" key="0" />
                  {eventVenues
                    ? eventVenues.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/request-order" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  packs: storeState.pack.entities,
  topics: storeState.topic.entities,
  products: storeState.product.entities,
  servicePkgs: storeState.servicePkg.entities,
  eventTypes: storeState.eventType.entities,
  eventVenues: storeState.eventVenue.entities,
  requestOrderEntity: storeState.requestOrder.entity,
  loading: storeState.requestOrder.loading,
  updating: storeState.requestOrder.updating,
  updateSuccess: storeState.requestOrder.updateSuccess,
});

const mapDispatchToProps = {
  getPacks,
  getTopics,
  getProducts,
  getServicePkgs,
  getEventTypes,
  getEventVenues,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RequestOrderUpdate);
