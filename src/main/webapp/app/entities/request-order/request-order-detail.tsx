import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './request-order.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRequestOrderDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RequestOrderDetail = (props: IRequestOrderDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { requestOrderEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="requestOrderDetailsHeading">
          <Translate contentKey="lunBackendApp.requestOrder.detail.title">RequestOrder</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="phone">
                  <Translate contentKey="lunBackendApp.requestOrder.phone">Phone</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.phone}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="email">
                  <Translate contentKey="lunBackendApp.requestOrder.email">Email</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.email}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="cusName">
                  <Translate contentKey="lunBackendApp.requestOrder.cusName">Cus Name</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.cusName}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="discount">
                  <Translate contentKey="lunBackendApp.requestOrder.discount">Discount</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.discount}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="about">
                  <Translate contentKey="lunBackendApp.requestOrder.about">About</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.about}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="holdTime">
                  <Translate contentKey="lunBackendApp.requestOrder.holdTime">Hold Time</Translate>
                </span>
              </dt>
              <dd>
                {requestOrderEntity.holdTime ? (
                  <TextFormat value={requestOrderEntity.holdTime} type="date" format={APP_DATE_FORMAT} />
                ) : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="budget">
                  <Translate contentKey="lunBackendApp.requestOrder.budget">Budget</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.budget}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="address">
                  <Translate contentKey="lunBackendApp.requestOrder.address">Address</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.address}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="guestNum">
                  <Translate contentKey="lunBackendApp.requestOrder.guestNum">Guest Num</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.guestNum}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="description">
                  <Translate contentKey="lunBackendApp.requestOrder.description">Description</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.description}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="status">
                  <Translate contentKey="lunBackendApp.requestOrder.status">Status</Translate>
                </span>
              </dt>
              <dd>{requestOrderEntity.status}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.pack">Pack</Translate>
              </dt>
              <dd>{requestOrderEntity.pack ? <a href={`pack/${requestOrderEntity.pack.id}`}>{requestOrderEntity.pack.name}</a> : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.images">Images</Translate>
              </dt>
              <dd>
                {requestOrderEntity.images
                  ? requestOrderEntity.images.map((val, i) => (
                      <span key={val.id}>
                        <a>{val.url}</a>
                        {requestOrderEntity.images && i === requestOrderEntity.images.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.topics">Topics</Translate>
              </dt>
              <dd>
                {requestOrderEntity.topics
                  ? requestOrderEntity.topics.map((val, i) => (
                      <span key={val.id}>
                        <a href={`topic/${val.id}`}>{val.name}</a>
                        {requestOrderEntity.topics && i === requestOrderEntity.topics.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.add_products">Add Products</Translate>
              </dt>
              <dd>
                {requestOrderEntity.add_products
                  ? requestOrderEntity.add_products.map((val, i) => (
                      <span key={val.id}>
                        <a href={`product/${val.id}`}>{val.name}</a>
                        {requestOrderEntity.add_products && i === requestOrderEntity.add_products.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.sub_products">Sub Products</Translate>
              </dt>
              <dd>
                {requestOrderEntity.sub_products
                  ? requestOrderEntity.sub_products.map((val, i) => (
                      <span key={val.id}>
                        <a href={`product/${val.id}`}>{val.name}</a>
                        {requestOrderEntity.sub_products && i === requestOrderEntity.sub_products.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.add_services">Add Services</Translate>
              </dt>
              <dd>
                {requestOrderEntity.add_services
                  ? requestOrderEntity.add_services.map((val, i) => (
                      <span key={val.id}>
                        <a href={`service-pkg/${val.id}`}>{val.name}</a>
                        {requestOrderEntity.add_services && i === requestOrderEntity.add_services.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.sub_services">Sub Services</Translate>
              </dt>
              <dd>
                {requestOrderEntity.sub_services
                  ? requestOrderEntity.sub_services.map((val, i) => (
                      <span key={val.id}>
                        <a href={`service-pkg/${val.id}`}>{val.name}</a>
                        {requestOrderEntity.sub_services && i === requestOrderEntity.sub_services.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.type">Type</Translate>
              </dt>
              <dd>
                {requestOrderEntity.type ? <a href={`event-type/${requestOrderEntity.type.id}`}>{requestOrderEntity.type.name}</a> : ''}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.requestOrder.eventVenue">Event Venue</Translate>
              </dt>
              <dd>
                {requestOrderEntity.eventVenue ? (
                  <a href={`event-venue/${requestOrderEntity.eventVenue.id}`}>{requestOrderEntity.eventVenue.name}</a>
                ) : (
                  ''
                )}
              </dd>
            </Col>
          </Row>
        </dl>
        <Button tag={Link} to="/request-order" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/request-order/${requestOrderEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ requestOrder }: IRootState) => ({
  requestOrderEntity: requestOrder.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RequestOrderDetail);
