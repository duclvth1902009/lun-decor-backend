import './home.scss';

import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { TextFormat, Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Row, Col, Alert } from 'reactstrap';
import { Card } from 'react-bootstrap';
import ProgressBar from 'react-bootstrap/ProgressBar';
import { getEntitiesWaiting, getWaitActiveEntities } from 'app/manage/request-order/request-order.reducer';
import { IRootState } from 'app/shared/reducers';

// export type IHomeProp = StateProps;
export interface IHomeProp extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}
export const Home = (props: IHomeProp) => {
  const { account, requests } = props;
  const getAllEntities = () => {
    props.getWaitActiveEntities();
  };
  useEffect(() => {
    if (account && account.login) {
      getAllEntities();
    }
  }, []);

  return (
    <Row>
      <Col md="3" className="pad">
        <span className="hipster rounded" />
      </Col>
      <Col md="9">
        <h2>
          <Translate contentKey="home.title">Welcome, Java Hipster!</Translate>
        </h2>
        <p className="lead">
          <Translate contentKey="home.subtitle">This is your homepage</Translate>
        </p>
        {account && account.login ? (
          <div>
            <Alert color="success">
              <Translate contentKey="home.logged.message" interpolate={{ username: account.login }}>
                You are logged in as user {account.login}.
              </Translate>
            </Alert>
            {requests && requests.length > 0 ? (
              <div>
                {requests.map((requestOrder, i) => (
                  <div key={requestOrder.id}>
                    <Alert color="danger">
                      Đơn hàng mới của khách hàng <Link to={`/manage/request-order/${requestOrder.id}/edit`}>{requestOrder.phone}</Link>{' '}
                      đang chờ duyệt
                    </Alert>
                  </div>
                ))}
              </div>
            ) : (
              <div></div>
            )}
            <Row>
              <Card style={{ width: '15rem', backgroundColor: '#12b103', margin: '1rem' }} className="mb-2">
                <Card.Header style={{ color: 'white' }}>Doanh thu</Card.Header>
                <Card.Body>
                  <Card.Title style={{ color: 'white' }}> Tổng doanh thu trong tháng</Card.Title>
                  <Card.Text style={{ color: 'white' }}>50,000,000 Đ</Card.Text>
                </Card.Body>
              </Card>

              <Card style={{ width: '15rem', backgroundColor: '#006dc2', margin: '1rem' }} className="mb-2">
                <Card.Header style={{ color: 'white' }}>Đơn hàng</Card.Header>
                <Card.Body>
                  <Card.Title style={{ color: 'white' }}> Số đơn đặt hàng </Card.Title>
                  <Card.Text style={{ color: 'white' }}>8</Card.Text>
                </Card.Body>
              </Card>

              <Card style={{ width: '15rem', backgroundColor: '#ce0c40', margin: '1rem' }} className="mb-2">
                <Card.Header style={{ color: 'white' }}>Chi phí</Card.Header>
                <Card.Body>
                  <Card.Title style={{ color: 'white' }}> Tổng chi phí </Card.Title>
                  <Card.Text style={{ color: 'white' }}>10,000,000 Đ</Card.Text>
                </Card.Body>
              </Card>

              {/*<Card style={{ width: '15rem', backgroundColor:'#ded900' , margin:'1rem'}} className="mb-2">*/}
              {/*  <Card.Header  style={{color:'white'}}>Lợi nhuận</Card.Header>*/}
              {/*  <Card.Body>*/}
              {/*    <Card.Title  style={{color:'white'}}> Tổng lợi nhuận trong tháng</Card.Title>*/}
              {/*    <Card.Text  style={{color:'white'}}>*/}
              {/*      40,000,000 Đ*/}
              {/*    </Card.Text>*/}
              {/*  </Card.Body>*/}
              {/*</Card>*/}
            </Row>
            Dự án trong tháng
            <Row>
              <ProgressBar style={{ width: '90%', height: '40px', marginLeft: '1rem' }}>
                <ProgressBar
                  label={
                    <span>
                      Đã hoàn thành: <TextFormat value={4} type="number" format={'0,0'} />
                    </span>
                  }
                  animated
                  variant="success"
                  now={50}
                  key={1}
                />
                <ProgressBar
                  label={
                    <span>
                      Đang triển khai: <TextFormat value={2} type="number" format={'0,0'} />
                    </span>
                  }
                  animated
                  variant="danger"
                  now={25}
                  key={2}
                />
                <ProgressBar
                  label={
                    <span>
                      Đơn hàng mới: <TextFormat value={2} type="number" format={'0,0'} />
                    </span>
                  }
                  animated
                  variant="warning"
                  now={25}
                  key={3}
                />
              </ProgressBar>
            </Row>
          </div>
        ) : (
          <div>
            <Alert color="warning">
              <Translate contentKey="global.messages.info.authenticated.prefix">If you want to </Translate>

              <Link to="/login" className="alert-link">
                <Translate contentKey="global.messages.info.authenticated.link"> sign in</Translate>
              </Link>
              {/*<Translate contentKey="global.messages.info.authenticated.suffix">*/}
              {/*  , you can try the default accounts:*/}
              {/*  <br />- Administrator (login=&quot;admin&quot; and password=&quot;admin&quot;)*/}
              {/*  <br />- User (login=&quot;user&quot; and password=&quot;user&quot;).*/}
              {/*</Translate>*/}
            </Alert>

            {/*<Alert color="warning">*/}
            {/*  <Translate contentKey="global.messages.info.register.noaccount">You do not have an account yet?</Translate>&nbsp;*/}
            {/*  <Link to="/account/register" className="alert-link">*/}
            {/*    <Translate contentKey="global.messages.info.register.link">Register a new account</Translate>*/}
            {/*  </Link>*/}
            {/*</Alert>*/}
          </div>
        )}
        {/*<p>*/}
        {/*  <Translate contentKey="home.question">If you have any question on JHipster:</Translate>*/}
        {/*</p>*/}

        {/*<ul>*/}
        {/*  <li>*/}
        {/*    <a href="https://www.jhipster.tech/" target="_blank" rel="noopener noreferrer">*/}
        {/*      <Translate contentKey="home.link.homepage">JHipster homepage</Translate>*/}
        {/*    </a>*/}
        {/*  </li>*/}
        {/*  <li>*/}
        {/*    <a href="http://stackoverflow.com/tags/jhipster/info" target="_blank" rel="noopener noreferrer">*/}
        {/*      <Translate contentKey="home.link.stackoverflow">JHipster on Stack Overflow</Translate>*/}
        {/*    </a>*/}
        {/*  </li>*/}
        {/*  <li>*/}
        {/*    <a href="https://github.com/jhipster/generator-jhipster/issues?state=open" target="_blank" rel="noopener noreferrer">*/}
        {/*      <Translate contentKey="home.link.bugtracker">JHipster bug tracker</Translate>*/}
        {/*    </a>*/}
        {/*  </li>*/}
        {/*  <li>*/}
        {/*    <a href="https://gitter.im/jhipster/generator-jhipster" target="_blank" rel="noopener noreferrer">*/}
        {/*      <Translate contentKey="home.link.chat">JHipster public chat room</Translate>*/}
        {/*    </a>*/}
        {/*  </li>*/}
        {/*  <li>*/}
        {/*    <a href="https://twitter.com/jhipster" target="_blank" rel="noopener noreferrer">*/}
        {/*      <Translate contentKey="home.link.follow">follow @jhipster on Twitter</Translate>*/}
        {/*    </a>*/}
        {/*  </li>*/}
        {/*</ul>*/}

        {/*<p>*/}
        {/*  <Translate contentKey="home.like">If you like JHipster, do not forget to give us a star on</Translate>{' '}*/}
        {/*  <a href="https://github.com/jhipster/generator-jhipster" target="_blank" rel="noopener noreferrer">*/}
        {/*    GitHub*/}
        {/*  </a>*/}
        {/*  !*/}
        {/*</p>*/}
      </Col>
    </Row>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  requests: storeState.requestOrder.entities,
});
const mapDispatchToProps = {
  getEntitiesWaiting,
  getWaitActiveEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Home);
