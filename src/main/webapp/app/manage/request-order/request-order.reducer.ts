import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction, ICrudSearchAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IRequestOrder, defaultValue } from 'app/shared/model/request-order.model';
import { IProjct } from 'app/shared/model/projct.model';

export const ACTION_TYPES = {
  FETCH_REQUESTORDER_LIST: 'requestOrder/FETCH_REQUESTORDER_LIST',
  FETCH_REQUESTORDER: 'requestOrder/FETCH_REQUESTORDER',
  CREATE_REQUESTORDER: 'requestOrder/CREATE_REQUESTORDER',
  UPDATE_REQUESTORDER: 'requestOrder/UPDATE_REQUESTORDER',
  PARTIAL_UPDATE_REQUESTORDER: 'requestOrder/PARTIAL_UPDATE_REQUESTORDER',
  DELETE_REQUESTORDER: 'requestOrder/DELETE_REQUESTORDER',
  RESET: 'requestOrder/RESET',
  SET_BLOB: 'requestOrder/SET_BLOB',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IRequestOrder>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type RequestOrderState = Readonly<typeof initialState>;

// Reducer

export default (state: RequestOrderState = initialState, action): RequestOrderState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_REQUESTORDER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_REQUESTORDER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_REQUESTORDER):
    case REQUEST(ACTION_TYPES.UPDATE_REQUESTORDER):
    case REQUEST(ACTION_TYPES.DELETE_REQUESTORDER):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_REQUESTORDER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_REQUESTORDER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_REQUESTORDER):
    case FAILURE(ACTION_TYPES.CREATE_REQUESTORDER):
    case FAILURE(ACTION_TYPES.UPDATE_REQUESTORDER):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_REQUESTORDER):
    case FAILURE(ACTION_TYPES.DELETE_REQUESTORDER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_REQUESTORDER_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_REQUESTORDER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_REQUESTORDER):
    case SUCCESS(ACTION_TYPES.UPDATE_REQUESTORDER):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_REQUESTORDER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_REQUESTORDER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/request-orders';
const apiWaitActive = 'api/request-orders-waitActive';
const apiGetByStatus = 'api/request-orders/status';
const apiWaiting = 'api/request-orders/waiting';

// Actions

export const getEntities: ICrudGetAllAction<IRequestOrder> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_REQUESTORDER_LIST,
    payload: axios.get<IRequestOrder>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getWaitActiveEntities: ICrudGetAllAction<IRequestOrder> = (page, size, sort) => {
  const requestUrl = `${apiWaitActive}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_REQUESTORDER_LIST,
    payload: axios.get<IRequestOrder>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntitiesByStatus: ICrudSearchAction<IProjct> = (stt, page, size, sort) => {
  const requestUrl = `${apiGetByStatus}/${stt}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_REQUESTORDER_LIST,
    payload: axios.get<IProjct>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntitiesWaiting: ICrudGetAllAction<IRequestOrder> = (page, size, sort) => {
  const requestUrl = `${apiWaiting}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_REQUESTORDER_LIST,
    payload: axios.get<IRequestOrder>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IRequestOrder> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_REQUESTORDER,
    payload: axios.get<IRequestOrder>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IRequestOrder> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_REQUESTORDER,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getWaitActiveEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IRequestOrder> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_REQUESTORDER,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IRequestOrder> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_REQUESTORDER,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IRequestOrder> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_REQUESTORDER,
    payload: axios.delete(requestUrl),
  });
  dispatch(getWaitActiveEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});

export const setRequestBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType,
  },
});
