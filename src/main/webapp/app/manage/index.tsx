import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Projct from './projct';
import RequestOrder from './request-order';

/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}

      <ErrorBoundaryRoute path={`${match.url}/projct`} component={Projct} />
      <ErrorBoundaryRoute path={`${match.url}/request-order`} component={RequestOrder} />

      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
