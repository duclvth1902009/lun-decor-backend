import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { Translate, openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './projct.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProjctDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProjctDetail = (props: IProjctDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { projctEntity } = props;
  return (
    <Row>
      <Col md="12">
        <h2 data-cy="projctDetailsHeading">
          <Translate contentKey="lunBackendApp.projct.detail.title">Projct</Translate>
        </h2>
        <dl className="jh-entity-details">
          <Row>
            <Col md="6">
              <dt>
                <span id="id">
                  <Translate contentKey="global.field.id">ID</Translate>
                </span>
              </dt>
              <dd>{projctEntity.id}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="holdTime">
                  <Translate contentKey="lunBackendApp.projct.holdTime">Hold Time</Translate>
                </span>
              </dt>
              <dd>{projctEntity.holdTime ? <TextFormat value={projctEntity.holdTime} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="location">
                  <Translate contentKey="lunBackendApp.projct.location">Location</Translate>
                </span>
              </dt>
              <dd>{projctEntity.location}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="contract">
                  <Translate contentKey="lunBackendApp.projct.contract">Contract</Translate>
                </span>
              </dt>
              <dd>
                {projctEntity.contract ? (
                  <div>
                    {projctEntity.contractContentType ? (
                      <a onClick={openFile(projctEntity.contractContentType, projctEntity.contract)}>
                        <Translate contentKey="entity.action.open">Open</Translate>&nbsp;
                      </a>
                    ) : null}
                    <span>
                      {projctEntity.contractContentType}, {byteSize(projctEntity.contract)}
                    </span>
                  </div>
                ) : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="rate">
                  <Translate contentKey="lunBackendApp.projct.rate">Rate</Translate>
                </span>
              </dt>
              <dd>{projctEntity.rate}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="preCapital">
                  <Translate contentKey="lunBackendApp.projct.preCapital">Pre Capital</Translate>
                </span>
              </dt>
              <dd>{projctEntity.preCapital}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="preTotal">
                  <Translate contentKey="lunBackendApp.projct.preTotal">Pre Total</Translate>
                </span>
              </dt>
              <dd>{projctEntity.preTotal}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="finCapital">
                  <Translate contentKey="lunBackendApp.projct.finCapital">Fin Capital</Translate>
                </span>
              </dt>
              <dd>{projctEntity.finCapital}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="finTotal">
                  <Translate contentKey="lunBackendApp.projct.finTotal">Fin Total</Translate>
                </span>
              </dt>
              <dd>{projctEntity.finTotal}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="rschangeCap">
                  <Translate contentKey="lunBackendApp.projct.rschangeCap">Rschange Cap</Translate>
                </span>
              </dt>
              <dd>{projctEntity.rschangeCap}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="rschangeTot">
                  <Translate contentKey="lunBackendApp.projct.rschangeTot">Rschange Tot</Translate>
                </span>
              </dt>
              <dd>{projctEntity.rschangeTot}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="status">
                  <Translate contentKey="lunBackendApp.projct.status">Status</Translate>
                </span>
              </dt>
              <dd>{projctEntity.status}</dd>
            </Col>
            <Col md="6">
              <dt>
                <span id="name">
                  <Translate contentKey="lunBackendApp.projct.name">Name</Translate>
                </span>
              </dt>
              <dd>{projctEntity.name}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.projct.topics">Topics</Translate>
              </dt>
              <dd>
                {projctEntity.topics
                  ? projctEntity.topics.map((val, i) => (
                      <span key={val.id}>
                        <a href={`topic/${val.id}`}>{val.name}</a>
                        {projctEntity.topics && i === projctEntity.topics.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.projct.type">Type</Translate>
              </dt>
              <dd>{projctEntity.type ? <a href={`event-type/${projctEntity.type.id}`}>{projctEntity.type.name}</a> : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.projct.eventVenue">Event Venue</Translate>
              </dt>
              <dd>
                {projctEntity.eventVenue ? <a href={`event-venue/${projctEntity.eventVenue.id}`}>{projctEntity.eventVenue.name}</a> : ''}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.projct.requestOrder">Request Order</Translate>
              </dt>
              <dd>
                {projctEntity.requestOrder ? (
                  <a href={`request-order/${projctEntity.requestOrder.id}`}>{projctEntity.requestOrder.id}</a>
                ) : (
                  ''
                )}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.projct.customer">Customer</Translate>
              </dt>
              <dd>{projctEntity.customer ? <a href={`customer/${projctEntity.customer.id}`}>{projctEntity.customer.name}</a> : ''}</dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.projct.packs">Packs</Translate>
              </dt>
              <dd>
                {projctEntity.packs
                  ? projctEntity.packs.map((val, i) => (
                      <span key={val.id}>
                        <a href={`pack/${val.id}`}>{val.name}</a>
                        {projctEntity.packs && i === projctEntity.packs.length - 1 ? '' : ', '}
                      </span>
                    ))
                  : null}
              </dd>
            </Col>
            <Col md="6">
              <dt>
                <Translate contentKey="lunBackendApp.projct.final_design">Final Design</Translate>
              </dt>
              <dd>
                {projctEntity.final_design ? <a href={`design/${projctEntity.final_design.id}`}>{projctEntity.final_design.id}</a> : ''}
              </dd>
            </Col>
          </Row>
        </dl>
        <Label>
          <Translate contentKey="lunBackendApp.projct.track-progress">Các bước triển khai</Translate>
        </Label>
        {projctEntity.stages
          ? projctEntity.stages
              .sort((a, b) => a.step - b.step)
              .map(otherEntity => (
                <div key={otherEntity.id}>
                  {/*<Link to={`/manage/projct/${projctEntity.id}/editTrack/${otherEntity.id}`}>*/}
                  {otherEntity.step} {otherEntity.name} {/*</Link>*/}
                </div>
              ))
          : null}
        <Button tag={Link} to="/manage/projct" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/manage/projct/${projctEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ manageProject }: IRootState) => ({
  projctEntity: manageProject.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProjctDetail);
