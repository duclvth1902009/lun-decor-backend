import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { byteSize, openFile, setFileData, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getProjcts } from 'app/entities/projct/projct.reducer';
import { getEntity, updateEntity, createEntity, reset } from './track-progress.reducer';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { getEntities as getImages, setBlob } from 'app/entities/image/image.reducer';

export interface ITrackProgressUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string; trackId: string }> {}

export const TrackProgressUpdate = (props: ITrackProgressUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.trackId);

  const { trackProgressEntity, loading, updating, image } = props;
  const { file, fileContentType } = image;

  const handleClose = () => {
    props.history.push(`/manage/projct/${props.match.params.id}/edit`);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.trackId);
    }

    props.getImages();
    props.getProjcts();
  }, []);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };
  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.deadLine = convertDateTimeToServer(values.deadLine);

    if (errors.length === 0) {
      const entity = {
        ...trackProgressEntity,
        ...values,
        files: fileList,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.trackProgress.home.updateLabel" data-cy="TrackProgressCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.trackProgress.home.updateLabel">Create a TrackProgress</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : trackProgressEntity} onSubmit={saveEntity}>
              <AvGroup>
                <Label id="nameLabel" for="track-progress-name">
                  <Translate contentKey="lunBackendApp.trackProgress.name">Name</Translate>
                </Label>
                <AvField id="track-progress-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.image.file">File</Translate>
                  </Label>
                  <Row>{renderPhotos(fileList)}</Row>
                  <br />
                  <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
                  <AvInput type="hidden" name="file" value={file} />
                </AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="lunBackendApp.projct.images">File</Translate>
                  </Label>
                  <Row>
                    {trackProgressEntity.images
                      ? trackProgressEntity.images.map(otherEntity => (
                          <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                        ))
                      : null}
                  </Row>
                  <br />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="track-progress-description">
                  <Translate contentKey="lunBackendApp.trackProgress.description">Description</Translate>
                </Label>
                <AvField id="track-progress-description" data-cy="description" type="text" name="description" />
              </AvGroup>
              <AvGroup check>
                <Label id="clientEnableLabel">
                  <AvInput
                    id="track-progress-clientEnable"
                    data-cy="clientEnable"
                    type="checkbox"
                    className="form-check-input"
                    name="clientEnable"
                  />
                  <Translate contentKey="lunBackendApp.trackProgress.clientEnable">Client Enable</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="stepLabel" for="track-progress-step">
                  <Translate contentKey="lunBackendApp.trackProgress.step">Step</Translate>
                </Label>
                <AvField id="track-progress-step" data-cy="step" type="string" readOnly className="form-control" name="step" />
              </AvGroup>
              <AvGroup>
                <Label id="deadLineLabel" for="track-progress-deadLine">
                  <Translate contentKey="lunBackendApp.trackProgress.deadLine">Dead Line</Translate>
                </Label>
                <AvInput
                  id="track-progress-deadLine"
                  data-cy="deadLine"
                  type="datetime-local"
                  className="form-control"
                  name="deadLine"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.trackProgressEntity.deadLine)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="statusLabel" for="track-progress-status">
                  <Translate contentKey="lunBackendApp.trackProgress.status">Status</Translate>
                </Label>
                {/*<AvField id="track-progress-status" data-cy="status" type="text" name="status" />*/}
                <AvInput id="track-progress-status" data-cy="status" type="select" className="form-control" name="status">
                  <option value="todo" key="todo">
                    todo
                  </option>
                  <option value="doing" key="doing">
                    doing
                  </option>
                  <option value="done" key="done">
                    done
                  </option>
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to={`/manage/projct/${props.match.params.id}/edit`} replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  projcts: storeState.projct.entities,
  trackProgressEntity: storeState.trackProgress.entity,
  loading: storeState.trackProgress.loading,
  updating: storeState.trackProgress.updating,
  updateSuccess: storeState.trackProgress.updateSuccess,
  images: storeState.image.entities,
  image: storeState.image.entity,
});

const mapDispatchToProps = {
  getProjcts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
  setBlob,
  getImages,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TrackProgressUpdate);
