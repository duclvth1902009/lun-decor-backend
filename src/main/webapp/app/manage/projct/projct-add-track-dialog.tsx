import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Label } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { IRootState } from 'app/shared/reducers';
import { getEntity, getEntities, createTrackForProject } from './projct.reducer';
import { reset } from './track-progress.reducer';
import { convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProjctAddTrackDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProjctAddTrackDialog = (props: IProjctAddTrackDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
    props.reset();
  }, []);
  const { projctEntity, trackEntity } = props;
  const handleClose = () => {
    props.history.push(`/manage/projct/${projctEntity.id}/edit`);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...trackEntity,
        ...values,
        projct: projctEntity,
      };
      props.createTrackForProject(entity);
    }
  };
  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose} data-cy="projctDeleteDialogHeading">
        <Translate contentKey="lunBackendApp.projct.add-track">Confirm delete operation</Translate>
      </ModalHeader>
      <AvForm model={{}} onSubmit={saveEntity}>
        <ModalBody id="lunBackendApp.projct.delete.question">
          <AvGroup>
            <Label id="locationLabel" for="track-name">
              <Translate contentKey="lunBackendApp.projct.track-name">Tên bước</Translate>
            </Label>
            <AvField id="track-name" data-cy="name" type="text" name="name" />
          </AvGroup>
          <AvGroup>
            <Label id="locationLabel" for="track-step">
              <Translate contentKey="lunBackendApp.projct.step">Thứ tự triển khai</Translate>
            </Label>
            <AvField id="track-step" data-cy="step" type="text" name="step" />
          </AvGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={handleClose}>
            <FontAwesomeIcon icon="ban" />
            &nbsp;
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit">
            <FontAwesomeIcon icon="save" />
            &nbsp;
            <Translate contentKey="entity.action.save">Save</Translate>
          </Button>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  projctEntity: storeState.projct.entity,
  trackEntity: storeState.trackProgress.entity,
  updateSuccess: storeState.projct.updateSuccess,
});

const mapDispatchToProps = { getEntity, createTrackForProject, reset, getEntities };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProjctAddTrackDialog);
