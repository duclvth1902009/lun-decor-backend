import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { byteSize, openFile, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getTopics } from 'app/entities/topic/topic.reducer';
import { getEntities as getEventTypes } from 'app/entities/event-type/event-type.reducer';
import { getEntities as getEventVenues } from 'app/entities/event-venue/event-venue.reducer';
import { getEntities as getRequestOrders } from 'app/entities/request-order/request-order.reducer';
import { getEntities as getCustomers } from 'app/entities/customer/customer.reducer';
import { getEntities as getPacks } from 'app/entities/pack/pack.reducer';
import { getEntities as getDesigns } from 'app/entities/design/design.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './projct.reducer';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProjctUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProjctUpdate = (props: IProjctUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { projctEntity, topics, eventTypes, eventVenues, requestOrders, customers, packs, designs, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/manage/projct' + props.location.search);
  };

  const { contract, contractContentType } = projctEntity;

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getTopics();
    props.getEventTypes();
    props.getEventVenues();
    props.getRequestOrders();
    props.getCustomers();
    props.getPacks();
    props.getDesigns();
  }, []);

  const [fileList, setFileList] = useState([]);

  const renderPhotos = source => {
    return source.map(photo => {
      return (
        <Col key={photo.file} md="2">
          <div
            style={{
              backgroundImage: `url(data:${photo.fileContentType};base64,${photo.file})`,
              height: 100,
              width: 100,
              backgroundSize: 100,
              backgroundRepeat: 'no-repeat',
            }}
          >
            <Button color="danger" onClick={clearBlob(photo)}>
              <FontAwesomeIcon icon="times-circle" />
            </Button>
          </div>
        </Col>
      );
    });
  };
  const onBlobChange = (isAnImage, name) => event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fileReader: FileReader = new FileReader();
      const filei = event.target.files[i];
      fileReader.readAsDataURL(filei);
      fileReader.onload = e => {
        const base64Data = e.target['result'].toString().substr(e.target['result'].toString().indexOf('base64,') + 'base64,'.length);
        const obj = { file: base64Data, fileContentType: filei.type };
        setFileList(prevFile => prevFile.concat(obj));
      };
    }
  };

  const clearBlob = img => () => {
    const arrayImage = [...fileList];
    const index = arrayImage.indexOf(img);
    if (index !== -1) {
      arrayImage.splice(index, 1);
      setFileList(arrayImage);
    }
  };
  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.holdTime = convertDateTimeToServer(values.holdTime);

    if (errors.length === 0) {
      const entity = {
        ...projctEntity,
        ...values,
        files: fileList,
        topics: mapIdList(values.topics),
        packs: mapIdList(values.packs),
        type: eventTypes.find(it => it.id.toString() === values.typeId.toString()),
        eventVenue: eventVenues.find(it => it.id.toString() === values.eventVenueId.toString()),
        requestOrder: requestOrders.find(it => it.id.toString() === values.requestOrderId.toString()),
        customer: customers.find(it => it.id.toString() === values.customerId.toString()),
        final_design: designs.find(it => it.id.toString() === values.final_designId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lunBackendApp.projct.home.createOrEditLabel" data-cy="ProjctCreateUpdateHeading">
            <Translate contentKey="lunBackendApp.projct.home.createOrEditLabel">Create or edit a Projct</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : projctEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="projct-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="projct-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="holdTimeLabel" for="projct-holdTime">
                  <Translate contentKey="lunBackendApp.projct.holdTime">Hold Time</Translate>
                </Label>
                <AvInput
                  id="projct-holdTime"
                  data-cy="holdTime"
                  type="datetime-local"
                  className="form-control"
                  name="holdTime"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.projctEntity.holdTime)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="locationLabel" for="projct-location">
                  <Translate contentKey="lunBackendApp.projct.location">Location</Translate>
                </Label>
                <AvField id="projct-location" data-cy="location" type="text" name="location" />
              </AvGroup>
              {/*<AvGroup>*/}
              {/*  <AvGroup>*/}
              {/*    <Label id="contractLabel" for="contract">*/}
              {/*      <Translate contentKey="lunBackendApp.projct.contract">Contract</Translate>*/}
              {/*    </Label>*/}
              {/*    <br />*/}
              {/*    {contract ? (*/}
              {/*      <div>*/}
              {/*        {contractContentType ? (*/}
              {/*          <a onClick={openFile(contractContentType, contract)}>*/}
              {/*            <Translate contentKey="entity.action.open">Open</Translate>*/}
              {/*          </a>*/}
              {/*        ) : null}*/}
              {/*        <br />*/}
              {/*        <Row>*/}
              {/*          <Col md="11">*/}
              {/*            <span>*/}
              {/*              {contractContentType}, {byteSize(contract)}*/}
              {/*            </span>*/}
              {/*          </Col>*/}
              {/*          <Col md="1">*/}
              {/*            <Button color="danger" onClick={clearBlob('contract')}>*/}
              {/*              <FontAwesomeIcon icon="times-circle" />*/}
              {/*            </Button>*/}
              {/*          </Col>*/}
              {/*        </Row>*/}
              {/*      </div>*/}
              {/*    ) : null}*/}
              {/*    <input id="file_contract" data-cy="contract" type="file" onChange={onBlobChange(false, 'contract')} />*/}
              {/*    <AvInput type="hidden" name="contract" value={contract} />*/}
              {/*  </AvGroup>*/}
              {/*</AvGroup>*/}
              <AvGroup>
                <Label id="rateLabel" for="projct-rate">
                  <Translate contentKey="lunBackendApp.projct.rate">Rate</Translate>
                </Label>
                <AvField id="projct-rate" data-cy="rate" type="string" className="form-control" name="rate" />
              </AvGroup>
              <AvGroup>
                <Label id="preCapitalLabel" for="projct-preCapital">
                  <Translate contentKey="lunBackendApp.projct.preCapital">Pre Capital</Translate>
                </Label>
                <AvField id="projct-preCapital" data-cy="preCapital" type="string" className="form-control" name="preCapital" />
              </AvGroup>
              <AvGroup>
                <Label id="preTotalLabel" for="projct-preTotal">
                  <Translate contentKey="lunBackendApp.projct.preTotal">Pre Total</Translate>
                </Label>
                <AvField id="projct-preTotal" data-cy="preTotal" type="currency" className="form-control" name="preTotal" />
              </AvGroup>
              <AvGroup>
                <Label id="finCapitalLabel" for="projct-finCapital">
                  <Translate contentKey="lunBackendApp.projct.finCapital">Fin Capital</Translate>
                </Label>
                <AvField id="projct-finCapital" data-cy="finCapital" type="string" className="form-control" name="finCapital" />
              </AvGroup>
              <AvGroup>
                <Label id="finTotalLabel" for="projct-finTotal">
                  <Translate contentKey="lunBackendApp.projct.finTotal">Fin Total</Translate>
                </Label>
                <AvField id="projct-finTotal" data-cy="finTotal" type="string" className="form-control" name="finTotal" />
              </AvGroup>
              <AvGroup>
                <Label id="rschangeCapLabel" for="projct-rschangeCap">
                  <Translate contentKey="lunBackendApp.projct.rschangeCap">Rschange Cap</Translate>
                </Label>
                <AvField id="projct-rschangeCap" data-cy="rschangeCap" type="text" name="rschangeCap" />
              </AvGroup>
              <AvGroup>
                <Label id="rschangeTotLabel" for="projct-rschangeTot">
                  <Translate contentKey="lunBackendApp.projct.rschangeTot">Rschange Tot</Translate>
                </Label>
                <AvField id="projct-rschangeTot" data-cy="rschangeTot" type="text" name="rschangeTot" />
              </AvGroup>
              <AvGroup>
                <Label id="statusLabel" for="projct-status">
                  <Translate contentKey="lunBackendApp.projct.status">Status</Translate>
                </Label>
                <AvInput
                  id="projct-status"
                  data-cy="status"
                  type="select"
                  className="form-control"
                  name="status"
                  value={!isNew && projctEntity.status}
                >
                  <option value="pending" key="pending">
                    pending
                  </option>
                  <option value="active" key="active">
                    active
                  </option>
                  <option value="done" key="done">
                    done
                  </option>
                  <option value="cancel" key="cancel">
                    cancel
                  </option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="projct-name">
                  <Translate contentKey="lunBackendApp.projct.name">Name</Translate>
                </Label>
                <AvField id="projct-name" data-cy="name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label for="projct-topics">
                  <Translate contentKey="lunBackendApp.projct.topics">Topics</Translate>
                </Label>
                <AvInput
                  id="projct-topics"
                  data-cy="topics"
                  type="select"
                  multiple
                  className="form-control"
                  name="topics"
                  value={!isNew && projctEntity.topics && projctEntity.topics.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {topics
                    ? topics.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="projct-type">
                  <Translate contentKey="lunBackendApp.projct.type">Type</Translate>
                </Label>
                <AvInput
                  id="projct-type"
                  data-cy="type"
                  type="select"
                  className="form-control"
                  name="typeId"
                  value={!isNew && projctEntity.type && projctEntity.type.id}
                >
                  <option value="" key="0" />
                  {eventTypes
                    ? eventTypes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="projct-eventVenue">
                  <Translate contentKey="lunBackendApp.projct.eventVenue">Event Venue</Translate>
                </Label>
                <AvInput
                  id="projct-eventVenue"
                  data-cy="eventVenue"
                  type="select"
                  className="form-control"
                  name="eventVenueId"
                  value={!isNew && projctEntity.eventVenue && projctEntity.eventVenue.id}
                >
                  <option value="" key="0" />
                  {eventVenues
                    ? eventVenues.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="projct-requestOrder">
                  <Translate contentKey="lunBackendApp.projct.requestOrder">Request Order</Translate>
                </Label>
                <AvInput
                  id="projct-requestOrder"
                  data-cy="requestOrder"
                  type="select"
                  className="form-control"
                  name="requestOrderId"
                  value={!isNew && projctEntity.requestOrder && projctEntity.requestOrder.id}
                >
                  <option value="" key="0" />
                  {requestOrders
                    ? requestOrders.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="projct-customer">
                  <Translate contentKey="lunBackendApp.projct.customer">Customer</Translate>
                </Label>
                <AvInput
                  id="projct-customer"
                  data-cy="customer"
                  type="select"
                  className="form-control"
                  name="customerId"
                  value={!isNew && projctEntity.customer && projctEntity.customer.id}
                >
                  <option value="" key="0" />
                  {customers
                    ? customers.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="projct-packs">
                  <Translate contentKey="lunBackendApp.projct.packs">Packs</Translate>
                </Label>
                <AvInput
                  id="projct-packs"
                  data-cy="packs"
                  type="select"
                  multiple
                  className="form-control"
                  name="packs"
                  value={!isNew && projctEntity.packs && projctEntity.packs.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {packs
                    ? packs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="projct-final_design">
                  <Translate contentKey="lunBackendApp.projct.final_design">Final Design</Translate>
                </Label>
                <AvInput
                  id="projct-final_design"
                  data-cy="final_design"
                  type="select"
                  className="form-control"
                  name="final_designId"
                  value={!isNew && projctEntity.final_design && projctEntity.final_design.id}
                >
                  <option value="" key="0" />
                  {designs
                    ? designs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.quickView}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="fileLabel" for="file_file">
                  <Translate contentKey="lunBackendApp.projct.file">File</Translate>
                </Label>
                <Row>{renderPhotos(fileList)}</Row>
                <br />
                <input id="file_file" data-cy="file" type="file" onChange={onBlobChange(true, 'file')} accept="image/*" multiple />
              </AvGroup>
              <AvGroup>
                <Label id="fileLabel" for="file">
                  <Translate contentKey="lunBackendApp.projct.images">File</Translate>
                </Label>
                <Row>
                  {projctEntity.images
                    ? projctEntity.images.map(otherEntity => (
                        <img src={`${otherEntity.url}`} style={{ maxHeight: '150px' }} key={otherEntity.id} />
                      ))
                    : null}
                </Row>
                <br />
              </AvGroup>
              <AvGroup>
                <Label>
                  <Translate contentKey="lunBackendApp.projct.track-progress">Các bước triển khai</Translate>
                </Label>
                {projctEntity.stages
                  ? projctEntity.stages
                      .sort((a, b) => a.step - b.step)
                      .map(otherEntity => (
                        <div key={otherEntity.id}>
                          <Link to={`/manage/projct/${projctEntity.id}/editTrack/${otherEntity.id}`}>
                            {otherEntity.step} {otherEntity.name}{' '}
                          </Link>
                        </div>
                      ))
                  : null}
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/manage/projct" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
              &nbsp;
              <Button tag={Link} id="cancel-save" to={`/manage/projct/${projctEntity.id}/edit/addTrack`} replace color="info">
                <FontAwesomeIcon icon="eye" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.addTrack">Add Track</Translate>
                </span>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  topics: storeState.topic.entities,
  eventTypes: storeState.eventType.entities,
  eventVenues: storeState.eventVenue.entities,
  requestOrders: storeState.requestOrder.entities,
  customers: storeState.customer.entities,
  packs: storeState.pack.entities,
  designs: storeState.design.entities,
  projctEntity: storeState.manageProject.entity,
  loading: storeState.manageProject.loading,
  updating: storeState.manageProject.updating,
  updateSuccess: storeState.manageProject.updateSuccess,
  tracks: storeState.trackProgress.entities,
});

const mapDispatchToProps = {
  getTopics,
  getEventTypes,
  getEventVenues,
  getRequestOrders,
  getCustomers,
  getPacks,
  getDesigns,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProjctUpdate);
