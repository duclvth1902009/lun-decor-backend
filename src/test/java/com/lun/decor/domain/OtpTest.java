package com.lun.decor.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OtpTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Otp.class);
        Otp otp1 = new Otp();
        otp1.setId("id1");
        Otp otp2 = new Otp();
        otp2.setId(otp1.getId());
        assertThat(otp1).isEqualTo(otp2);
        otp2.setId("id2");
        assertThat(otp1).isNotEqualTo(otp2);
        otp1.setId(null);
        assertThat(otp1).isNotEqualTo(otp2);
    }
}
