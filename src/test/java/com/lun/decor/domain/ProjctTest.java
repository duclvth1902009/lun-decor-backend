package com.lun.decor.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ProjctTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Projct.class);
        Projct projct1 = new Projct();
        projct1.setId("id1");
        Projct projct2 = new Projct();
        projct2.setId(projct1.getId());
        assertThat(projct1).isEqualTo(projct2);
        projct2.setId("id2");
        assertThat(projct1).isNotEqualTo(projct2);
        projct1.setId(null);
        assertThat(projct1).isNotEqualTo(projct2);
    }
}
