package com.lun.decor.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RequestOrderTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequestOrder.class);
        RequestOrder requestOrder1 = new RequestOrder();
        requestOrder1.setId("id1");
        RequestOrder requestOrder2 = new RequestOrder();
        requestOrder2.setId(requestOrder1.getId());
        assertThat(requestOrder1).isEqualTo(requestOrder2);
        requestOrder2.setId("id2");
        assertThat(requestOrder1).isNotEqualTo(requestOrder2);
        requestOrder1.setId(null);
        assertThat(requestOrder1).isNotEqualTo(requestOrder2);
    }
}
