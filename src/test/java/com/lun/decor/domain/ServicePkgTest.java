package com.lun.decor.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ServicePkgTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ServicePkg.class);
        ServicePkg servicePkg1 = new ServicePkg();
        servicePkg1.setId("id1");
        ServicePkg servicePkg2 = new ServicePkg();
        servicePkg2.setId(servicePkg1.getId());
        assertThat(servicePkg1).isEqualTo(servicePkg2);
        servicePkg2.setId("id2");
        assertThat(servicePkg1).isNotEqualTo(servicePkg2);
        servicePkg1.setId(null);
        assertThat(servicePkg1).isNotEqualTo(servicePkg2);
    }
}
