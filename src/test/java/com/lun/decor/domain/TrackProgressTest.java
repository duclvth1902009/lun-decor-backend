package com.lun.decor.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TrackProgressTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrackProgress.class);
        TrackProgress trackProgress1 = new TrackProgress();
        trackProgress1.setId("id1");
        TrackProgress trackProgress2 = new TrackProgress();
        trackProgress2.setId(trackProgress1.getId());
        assertThat(trackProgress1).isEqualTo(trackProgress2);
        trackProgress2.setId("id2");
        assertThat(trackProgress1).isNotEqualTo(trackProgress2);
        trackProgress1.setId(null);
        assertThat(trackProgress1).isNotEqualTo(trackProgress2);
    }
}
