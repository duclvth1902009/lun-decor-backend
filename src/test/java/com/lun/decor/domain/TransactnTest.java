package com.lun.decor.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TransactnTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Transactn.class);
        Transactn transactn1 = new Transactn();
        transactn1.setId("id1");
        Transactn transactn2 = new Transactn();
        transactn2.setId(transactn1.getId());
        assertThat(transactn1).isEqualTo(transactn2);
        transactn2.setId("id2");
        assertThat(transactn1).isNotEqualTo(transactn2);
        transactn1.setId(null);
        assertThat(transactn1).isNotEqualTo(transactn2);
    }
}
