package com.lun.decor.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DesignTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Design.class);
        Design design1 = new Design();
        design1.setId("id1");
        Design design2 = new Design();
        design2.setId(design1.getId());
        assertThat(design1).isEqualTo(design2);
        design2.setId("id2");
        assertThat(design1).isNotEqualTo(design2);
        design1.setId(null);
        assertThat(design1).isNotEqualTo(design2);
    }
}
