package com.lun.decor.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EventVenueTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventVenue.class);
        EventVenue eventVenue1 = new EventVenue();
        eventVenue1.setId("id1");
        EventVenue eventVenue2 = new EventVenue();
        eventVenue2.setId(eventVenue1.getId());
        assertThat(eventVenue1).isEqualTo(eventVenue2);
        eventVenue2.setId("id2");
        assertThat(eventVenue1).isNotEqualTo(eventVenue2);
        eventVenue1.setId(null);
        assertThat(eventVenue1).isNotEqualTo(eventVenue2);
    }
}
