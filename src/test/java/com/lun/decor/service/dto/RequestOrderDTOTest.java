package com.lun.decor.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RequestOrderDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequestOrderDTO.class);
        RequestOrderDTO requestOrderDTO1 = new RequestOrderDTO();
        requestOrderDTO1.setId("id1");
        RequestOrderDTO requestOrderDTO2 = new RequestOrderDTO();
        assertThat(requestOrderDTO1).isNotEqualTo(requestOrderDTO2);
        requestOrderDTO2.setId(requestOrderDTO1.getId());
        assertThat(requestOrderDTO1).isEqualTo(requestOrderDTO2);
        requestOrderDTO2.setId("id2");
        assertThat(requestOrderDTO1).isNotEqualTo(requestOrderDTO2);
        requestOrderDTO1.setId(null);
        assertThat(requestOrderDTO1).isNotEqualTo(requestOrderDTO2);
    }
}
