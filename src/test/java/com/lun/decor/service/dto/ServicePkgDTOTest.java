package com.lun.decor.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ServicePkgDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ServicePkgDTO.class);
        ServicePkgDTO servicePkgDTO1 = new ServicePkgDTO();
        servicePkgDTO1.setId("id1");
        ServicePkgDTO servicePkgDTO2 = new ServicePkgDTO();
        assertThat(servicePkgDTO1).isNotEqualTo(servicePkgDTO2);
        servicePkgDTO2.setId(servicePkgDTO1.getId());
        assertThat(servicePkgDTO1).isEqualTo(servicePkgDTO2);
        servicePkgDTO2.setId("id2");
        assertThat(servicePkgDTO1).isNotEqualTo(servicePkgDTO2);
        servicePkgDTO1.setId(null);
        assertThat(servicePkgDTO1).isNotEqualTo(servicePkgDTO2);
    }
}
