package com.lun.decor.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EventVenueDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventVenueDTO.class);
        EventVenueDTO eventVenueDTO1 = new EventVenueDTO();
        eventVenueDTO1.setId("id1");
        EventVenueDTO eventVenueDTO2 = new EventVenueDTO();
        assertThat(eventVenueDTO1).isNotEqualTo(eventVenueDTO2);
        eventVenueDTO2.setId(eventVenueDTO1.getId());
        assertThat(eventVenueDTO1).isEqualTo(eventVenueDTO2);
        eventVenueDTO2.setId("id2");
        assertThat(eventVenueDTO1).isNotEqualTo(eventVenueDTO2);
        eventVenueDTO1.setId(null);
        assertThat(eventVenueDTO1).isNotEqualTo(eventVenueDTO2);
    }
}
