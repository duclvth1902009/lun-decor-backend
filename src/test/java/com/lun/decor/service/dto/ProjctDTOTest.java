package com.lun.decor.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ProjctDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProjctDTO.class);
        ProjctDTO projctDTO1 = new ProjctDTO();
        projctDTO1.setId("id1");
        ProjctDTO projctDTO2 = new ProjctDTO();
        assertThat(projctDTO1).isNotEqualTo(projctDTO2);
        projctDTO2.setId(projctDTO1.getId());
        assertThat(projctDTO1).isEqualTo(projctDTO2);
        projctDTO2.setId("id2");
        assertThat(projctDTO1).isNotEqualTo(projctDTO2);
        projctDTO1.setId(null);
        assertThat(projctDTO1).isNotEqualTo(projctDTO2);
    }
}
