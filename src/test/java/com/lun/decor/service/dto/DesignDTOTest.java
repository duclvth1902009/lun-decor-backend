package com.lun.decor.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DesignDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DesignDTO.class);
        DesignDTO designDTO1 = new DesignDTO();
        designDTO1.setId("id1");
        DesignDTO designDTO2 = new DesignDTO();
        assertThat(designDTO1).isNotEqualTo(designDTO2);
        designDTO2.setId(designDTO1.getId());
        assertThat(designDTO1).isEqualTo(designDTO2);
        designDTO2.setId("id2");
        assertThat(designDTO1).isNotEqualTo(designDTO2);
        designDTO1.setId(null);
        assertThat(designDTO1).isNotEqualTo(designDTO2);
    }
}
