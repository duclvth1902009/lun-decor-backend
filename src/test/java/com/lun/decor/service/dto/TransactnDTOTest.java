package com.lun.decor.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TransactnDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactnDTO.class);
        TransactnDTO transactnDTO1 = new TransactnDTO();
        transactnDTO1.setId("id1");
        TransactnDTO transactnDTO2 = new TransactnDTO();
        assertThat(transactnDTO1).isNotEqualTo(transactnDTO2);
        transactnDTO2.setId(transactnDTO1.getId());
        assertThat(transactnDTO1).isEqualTo(transactnDTO2);
        transactnDTO2.setId("id2");
        assertThat(transactnDTO1).isNotEqualTo(transactnDTO2);
        transactnDTO1.setId(null);
        assertThat(transactnDTO1).isNotEqualTo(transactnDTO2);
    }
}
