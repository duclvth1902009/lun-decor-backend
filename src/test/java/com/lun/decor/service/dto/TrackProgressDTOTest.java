package com.lun.decor.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.lun.decor.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TrackProgressDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrackProgressDTO.class);
        TrackProgressDTO trackProgressDTO1 = new TrackProgressDTO();
        trackProgressDTO1.setId("id1");
        TrackProgressDTO trackProgressDTO2 = new TrackProgressDTO();
        assertThat(trackProgressDTO1).isNotEqualTo(trackProgressDTO2);
        trackProgressDTO2.setId(trackProgressDTO1.getId());
        assertThat(trackProgressDTO1).isEqualTo(trackProgressDTO2);
        trackProgressDTO2.setId("id2");
        assertThat(trackProgressDTO1).isNotEqualTo(trackProgressDTO2);
        trackProgressDTO1.setId(null);
        assertThat(trackProgressDTO1).isNotEqualTo(trackProgressDTO2);
    }
}
