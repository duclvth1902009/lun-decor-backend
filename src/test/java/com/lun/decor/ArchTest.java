package com.lun.decor;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.lun.decor");

        noClasses()
            .that()
            .resideInAnyPackage("com.lun.decor.service..")
            .or()
            .resideInAnyPackage("com.lun.decor.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.lun.decor.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
