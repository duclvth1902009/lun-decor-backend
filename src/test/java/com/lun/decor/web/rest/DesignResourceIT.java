package com.lun.decor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.Design;
import com.lun.decor.repository.DesignRepository;
import com.lun.decor.service.DesignService;
import com.lun.decor.service.dto.DesignDTO;
import com.lun.decor.service.mapper.DesignMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link DesignResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class DesignResourceIT {

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_IMG_QUICK_VIEW = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMG_QUICK_VIEW = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMG_QUICK_VIEW_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMG_QUICK_VIEW_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_QUICK_VIEW = "AAAAAAAAAA";
    private static final String UPDATED_QUICK_VIEW = "BBBBBBBBBB";

    private static final Long DEFAULT_WIDTH = 1L;
    private static final Long UPDATED_WIDTH = 2L;

    private static final Long DEFAULT_HEIGHT = 1L;
    private static final Long UPDATED_HEIGHT = 2L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/designs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private DesignRepository designRepository;

    @Mock
    private DesignRepository designRepositoryMock;

    @Autowired
    private DesignMapper designMapper;

    @Mock
    private DesignService designServiceMock;

    @Autowired
    private MockMvc restDesignMockMvc;

    private Design design;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Design createEntity() {
        Design design = new Design()
            .imgQuickView(DEFAULT_IMG_QUICK_VIEW)
            .imgQuickViewContentType(DEFAULT_IMG_QUICK_VIEW_CONTENT_TYPE)
            .quickView(DEFAULT_QUICK_VIEW)
            .width(DEFAULT_WIDTH)
            .height(DEFAULT_HEIGHT)
            .note(DEFAULT_NOTE);
        return design;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Design createUpdatedEntity() {
        Design design = new Design()
            .imgQuickView(UPDATED_IMG_QUICK_VIEW)
            .imgQuickViewContentType(UPDATED_IMG_QUICK_VIEW_CONTENT_TYPE)
            .quickView(UPDATED_QUICK_VIEW)
            .width(UPDATED_WIDTH)
            .height(UPDATED_HEIGHT)
            .note(UPDATED_NOTE);
        return design;
    }

    @BeforeEach
    public void initTest() {
        designRepository.deleteAll();
        design = createEntity();
    }

    @Test
    void createDesign() throws Exception {
        int databaseSizeBeforeCreate = designRepository.findAll().size();
        // Create the Design
        DesignDTO designDTO = designMapper.toDto(design);
        restDesignMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(designDTO)))
            .andExpect(status().isCreated());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeCreate + 1);
        Design testDesign = designList.get(designList.size() - 1);
        assertThat(testDesign.getFile()).isEqualTo(DEFAULT_FILE);
        assertThat(testDesign.getFileContentType()).isEqualTo(DEFAULT_FILE_CONTENT_TYPE);
        assertThat(testDesign.getImgQuickView()).isEqualTo(DEFAULT_IMG_QUICK_VIEW);
        assertThat(testDesign.getImgQuickViewContentType()).isEqualTo(DEFAULT_IMG_QUICK_VIEW_CONTENT_TYPE);
        assertThat(testDesign.getQuickView()).isEqualTo(DEFAULT_QUICK_VIEW);
        assertThat(testDesign.getWidth()).isEqualTo(DEFAULT_WIDTH);
        assertThat(testDesign.getHeight()).isEqualTo(DEFAULT_HEIGHT);
        assertThat(testDesign.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    void createDesignWithExistingId() throws Exception {
        // Create the Design with an existing ID
        design.setId("existing_id");
        DesignDTO designDTO = designMapper.toDto(design);

        int databaseSizeBeforeCreate = designRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDesignMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(designDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkQuickViewIsRequired() throws Exception {
        int databaseSizeBeforeTest = designRepository.findAll().size();
        // set the field null
        design.setQuickView(null);

        // Create the Design, which fails.
        DesignDTO designDTO = designMapper.toDto(design);

        restDesignMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(designDTO)))
            .andExpect(status().isBadRequest());

        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllDesigns() throws Exception {
        // Initialize the database
        designRepository.save(design);

        // Get all the designList
        restDesignMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(design.getId())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].imgQuickViewContentType").value(hasItem(DEFAULT_IMG_QUICK_VIEW_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imgQuickView").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMG_QUICK_VIEW))))
            .andExpect(jsonPath("$.[*].quickView").value(hasItem(DEFAULT_QUICK_VIEW)))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.intValue())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllDesignsWithEagerRelationshipsIsEnabled() throws Exception {
        when(designServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restDesignMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(designServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllDesignsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(designServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restDesignMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(designServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getDesign() throws Exception {
        // Initialize the database
        designRepository.save(design);

        // Get the design
        restDesignMockMvc
            .perform(get(ENTITY_API_URL_ID, design.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(design.getId()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.imgQuickViewContentType").value(DEFAULT_IMG_QUICK_VIEW_CONTENT_TYPE))
            .andExpect(jsonPath("$.imgQuickView").value(Base64Utils.encodeToString(DEFAULT_IMG_QUICK_VIEW)))
            .andExpect(jsonPath("$.quickView").value(DEFAULT_QUICK_VIEW))
            .andExpect(jsonPath("$.width").value(DEFAULT_WIDTH.intValue()))
            .andExpect(jsonPath("$.height").value(DEFAULT_HEIGHT.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE));
    }

    @Test
    void getNonExistingDesign() throws Exception {
        // Get the design
        restDesignMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewDesign() throws Exception {
        // Initialize the database
        designRepository.save(design);

        int databaseSizeBeforeUpdate = designRepository.findAll().size();

        // Update the design
        Design updatedDesign = designRepository.findById(design.getId()).get();
        updatedDesign
            .imgQuickView(UPDATED_IMG_QUICK_VIEW)
            .imgQuickViewContentType(UPDATED_IMG_QUICK_VIEW_CONTENT_TYPE)
            .quickView(UPDATED_QUICK_VIEW)
            .width(UPDATED_WIDTH)
            .height(UPDATED_HEIGHT)
            .note(UPDATED_NOTE);
        DesignDTO designDTO = designMapper.toDto(updatedDesign);

        restDesignMockMvc
            .perform(
                put(ENTITY_API_URL_ID, designDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(designDTO))
            )
            .andExpect(status().isOk());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
        Design testDesign = designList.get(designList.size() - 1);
        assertThat(testDesign.getFile()).isEqualTo(UPDATED_FILE);
        assertThat(testDesign.getFileContentType()).isEqualTo(UPDATED_FILE_CONTENT_TYPE);
        assertThat(testDesign.getImgQuickView()).isEqualTo(UPDATED_IMG_QUICK_VIEW);
        assertThat(testDesign.getImgQuickViewContentType()).isEqualTo(UPDATED_IMG_QUICK_VIEW_CONTENT_TYPE);
        assertThat(testDesign.getQuickView()).isEqualTo(UPDATED_QUICK_VIEW);
        assertThat(testDesign.getWidth()).isEqualTo(UPDATED_WIDTH);
        assertThat(testDesign.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testDesign.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    void putNonExistingDesign() throws Exception {
        int databaseSizeBeforeUpdate = designRepository.findAll().size();
        design.setId(UUID.randomUUID().toString());

        // Create the Design
        DesignDTO designDTO = designMapper.toDto(design);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDesignMockMvc
            .perform(
                put(ENTITY_API_URL_ID, designDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(designDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchDesign() throws Exception {
        int databaseSizeBeforeUpdate = designRepository.findAll().size();
        design.setId(UUID.randomUUID().toString());

        // Create the Design
        DesignDTO designDTO = designMapper.toDto(design);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDesignMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(designDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamDesign() throws Exception {
        int databaseSizeBeforeUpdate = designRepository.findAll().size();
        design.setId(UUID.randomUUID().toString());

        // Create the Design
        DesignDTO designDTO = designMapper.toDto(design);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDesignMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(designDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateDesignWithPatch() throws Exception {
        // Initialize the database
        designRepository.save(design);

        int databaseSizeBeforeUpdate = designRepository.findAll().size();

        // Update the design using partial update
        Design partialUpdatedDesign = new Design();
        partialUpdatedDesign.setId(design.getId());

        partialUpdatedDesign.quickView(UPDATED_QUICK_VIEW).width(UPDATED_WIDTH).height(UPDATED_HEIGHT).note(UPDATED_NOTE);

        restDesignMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDesign.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDesign))
            )
            .andExpect(status().isOk());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
        Design testDesign = designList.get(designList.size() - 1);
        assertThat(testDesign.getFile()).isEqualTo(DEFAULT_FILE);
        assertThat(testDesign.getFileContentType()).isEqualTo(DEFAULT_FILE_CONTENT_TYPE);
        assertThat(testDesign.getImgQuickView()).isEqualTo(DEFAULT_IMG_QUICK_VIEW);
        assertThat(testDesign.getImgQuickViewContentType()).isEqualTo(DEFAULT_IMG_QUICK_VIEW_CONTENT_TYPE);
        assertThat(testDesign.getQuickView()).isEqualTo(UPDATED_QUICK_VIEW);
        assertThat(testDesign.getWidth()).isEqualTo(UPDATED_WIDTH);
        assertThat(testDesign.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testDesign.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    void fullUpdateDesignWithPatch() throws Exception {
        // Initialize the database
        designRepository.save(design);

        int databaseSizeBeforeUpdate = designRepository.findAll().size();

        // Update the design using partial update
        Design partialUpdatedDesign = new Design();
        partialUpdatedDesign.setId(design.getId());

        partialUpdatedDesign
            .imgQuickView(UPDATED_IMG_QUICK_VIEW)
            .imgQuickViewContentType(UPDATED_IMG_QUICK_VIEW_CONTENT_TYPE)
            .quickView(UPDATED_QUICK_VIEW)
            .width(UPDATED_WIDTH)
            .height(UPDATED_HEIGHT)
            .note(UPDATED_NOTE);

        restDesignMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDesign.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDesign))
            )
            .andExpect(status().isOk());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
        Design testDesign = designList.get(designList.size() - 1);
        assertThat(testDesign.getFile()).isEqualTo(UPDATED_FILE);
        assertThat(testDesign.getFileContentType()).isEqualTo(UPDATED_FILE_CONTENT_TYPE);
        assertThat(testDesign.getImgQuickView()).isEqualTo(UPDATED_IMG_QUICK_VIEW);
        assertThat(testDesign.getImgQuickViewContentType()).isEqualTo(UPDATED_IMG_QUICK_VIEW_CONTENT_TYPE);
        assertThat(testDesign.getQuickView()).isEqualTo(UPDATED_QUICK_VIEW);
        assertThat(testDesign.getWidth()).isEqualTo(UPDATED_WIDTH);
        assertThat(testDesign.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testDesign.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    void patchNonExistingDesign() throws Exception {
        int databaseSizeBeforeUpdate = designRepository.findAll().size();
        design.setId(UUID.randomUUID().toString());

        // Create the Design
        DesignDTO designDTO = designMapper.toDto(design);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDesignMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, designDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(designDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchDesign() throws Exception {
        int databaseSizeBeforeUpdate = designRepository.findAll().size();
        design.setId(UUID.randomUUID().toString());

        // Create the Design
        DesignDTO designDTO = designMapper.toDto(design);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDesignMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(designDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamDesign() throws Exception {
        int databaseSizeBeforeUpdate = designRepository.findAll().size();
        design.setId(UUID.randomUUID().toString());

        // Create the Design
        DesignDTO designDTO = designMapper.toDto(design);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDesignMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(designDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Design in the database
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteDesign() throws Exception {
        // Initialize the database
        designRepository.save(design);

        int databaseSizeBeforeDelete = designRepository.findAll().size();

        // Delete the design
        restDesignMockMvc
            .perform(delete(ENTITY_API_URL_ID, design.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Design> designList = designRepository.findAll();
        assertThat(designList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
