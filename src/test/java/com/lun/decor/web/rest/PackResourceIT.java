package com.lun.decor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.Pack;
import com.lun.decor.repository.PackRepository;
import com.lun.decor.service.PackService;
import com.lun.decor.service.dto.PackDTO;
import com.lun.decor.service.mapper.PackMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link PackResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PackResourceIT {

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_PRICE_FROM = 1L;
    private static final Long UPDATED_PRICE_FROM = 2L;

    private static final Long DEFAULT_PRICE_TO = 1L;
    private static final Long UPDATED_PRICE_TO = 2L;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/packs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private PackRepository packRepository;

    @Mock
    private PackRepository packRepositoryMock;

    @Autowired
    private PackMapper packMapper;

    @Mock
    private PackService packServiceMock;

    @Autowired
    private MockMvc restPackMockMvc;

    private Pack pack;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pack createEntity() {
        Pack pack = new Pack()
            .name(DEFAULT_NAME)
            .price_from(DEFAULT_PRICE_FROM)
            .price_to(DEFAULT_PRICE_TO)
            .description(DEFAULT_DESCRIPTION);
        return pack;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pack createUpdatedEntity() {
        Pack pack = new Pack()
            .name(UPDATED_NAME)
            .price_from(UPDATED_PRICE_FROM)
            .price_to(UPDATED_PRICE_TO)
            .description(UPDATED_DESCRIPTION);
        return pack;
    }

    @BeforeEach
    public void initTest() {
        packRepository.deleteAll();
        pack = createEntity();
    }

    @Test
    void createPack() throws Exception {
        int databaseSizeBeforeCreate = packRepository.findAll().size();
        // Create the Pack
        PackDTO packDTO = packMapper.toDto(pack);
        restPackMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(packDTO)))
            .andExpect(status().isCreated());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeCreate + 1);
        Pack testPack = packList.get(packList.size() - 1);
        assertThat(testPack.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPack.getPrice_from()).isEqualTo(DEFAULT_PRICE_FROM);
        assertThat(testPack.getPrice_to()).isEqualTo(DEFAULT_PRICE_TO);
        assertThat(testPack.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    void createPackWithExistingId() throws Exception {
        // Create the Pack with an existing ID
        pack.setId("existing_id");
        PackDTO packDTO = packMapper.toDto(pack);

        int databaseSizeBeforeCreate = packRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPackMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(packDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllPacks() throws Exception {
        // Initialize the database
        packRepository.save(pack);

        // Get all the packList
        restPackMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pack.getId())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].price_from").value(hasItem(DEFAULT_PRICE_FROM.intValue())))
            .andExpect(jsonPath("$.[*].price_to").value(hasItem(DEFAULT_PRICE_TO.intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPacksWithEagerRelationshipsIsEnabled() throws Exception {
        when(packServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPackMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(packServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPacksWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(packServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPackMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(packServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getPack() throws Exception {
        // Initialize the database
        packRepository.save(pack);

        // Get the pack
        restPackMockMvc
            .perform(get(ENTITY_API_URL_ID, pack.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pack.getId()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.price_from").value(DEFAULT_PRICE_FROM.intValue()))
            .andExpect(jsonPath("$.price_to").value(DEFAULT_PRICE_TO.intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    void getNonExistingPack() throws Exception {
        // Get the pack
        restPackMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewPack() throws Exception {
        // Initialize the database
        packRepository.save(pack);

        int databaseSizeBeforeUpdate = packRepository.findAll().size();

        // Update the pack
        Pack updatedPack = packRepository.findById(pack.getId()).get();
        updatedPack.name(UPDATED_NAME).price_from(UPDATED_PRICE_FROM).price_to(UPDATED_PRICE_TO).description(UPDATED_DESCRIPTION);
        PackDTO packDTO = packMapper.toDto(updatedPack);

        restPackMockMvc
            .perform(
                put(ENTITY_API_URL_ID, packDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(packDTO))
            )
            .andExpect(status().isOk());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
        Pack testPack = packList.get(packList.size() - 1);
        assertThat(testPack.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPack.getPrice_from()).isEqualTo(UPDATED_PRICE_FROM);
        assertThat(testPack.getPrice_to()).isEqualTo(UPDATED_PRICE_TO);
        assertThat(testPack.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    void putNonExistingPack() throws Exception {
        int databaseSizeBeforeUpdate = packRepository.findAll().size();
        pack.setId(UUID.randomUUID().toString());

        // Create the Pack
        PackDTO packDTO = packMapper.toDto(pack);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPackMockMvc
            .perform(
                put(ENTITY_API_URL_ID, packDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(packDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPack() throws Exception {
        int databaseSizeBeforeUpdate = packRepository.findAll().size();
        pack.setId(UUID.randomUUID().toString());

        // Create the Pack
        PackDTO packDTO = packMapper.toDto(pack);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPackMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(packDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPack() throws Exception {
        int databaseSizeBeforeUpdate = packRepository.findAll().size();
        pack.setId(UUID.randomUUID().toString());

        // Create the Pack
        PackDTO packDTO = packMapper.toDto(pack);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPackMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(packDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePackWithPatch() throws Exception {
        // Initialize the database
        packRepository.save(pack);

        int databaseSizeBeforeUpdate = packRepository.findAll().size();

        // Update the pack using partial update
        Pack partialUpdatedPack = new Pack();
        partialUpdatedPack.setId(pack.getId());

        partialUpdatedPack.name(UPDATED_NAME).price_from(UPDATED_PRICE_FROM).price_to(UPDATED_PRICE_TO).description(UPDATED_DESCRIPTION);

        restPackMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPack.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPack))
            )
            .andExpect(status().isOk());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
        Pack testPack = packList.get(packList.size() - 1);
        assertThat(testPack.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPack.getPrice_from()).isEqualTo(UPDATED_PRICE_FROM);
        assertThat(testPack.getPrice_to()).isEqualTo(UPDATED_PRICE_TO);
        assertThat(testPack.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    void fullUpdatePackWithPatch() throws Exception {
        // Initialize the database
        packRepository.save(pack);

        int databaseSizeBeforeUpdate = packRepository.findAll().size();

        // Update the pack using partial update
        Pack partialUpdatedPack = new Pack();
        partialUpdatedPack.setId(pack.getId());

        partialUpdatedPack.name(UPDATED_NAME).price_from(UPDATED_PRICE_FROM).price_to(UPDATED_PRICE_TO).description(UPDATED_DESCRIPTION);

        restPackMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPack.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPack))
            )
            .andExpect(status().isOk());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
        Pack testPack = packList.get(packList.size() - 1);
        assertThat(testPack.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPack.getPrice_from()).isEqualTo(UPDATED_PRICE_FROM);
        assertThat(testPack.getPrice_to()).isEqualTo(UPDATED_PRICE_TO);
        assertThat(testPack.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    void patchNonExistingPack() throws Exception {
        int databaseSizeBeforeUpdate = packRepository.findAll().size();
        pack.setId(UUID.randomUUID().toString());

        // Create the Pack
        PackDTO packDTO = packMapper.toDto(pack);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPackMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, packDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(packDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPack() throws Exception {
        int databaseSizeBeforeUpdate = packRepository.findAll().size();
        pack.setId(UUID.randomUUID().toString());

        // Create the Pack
        PackDTO packDTO = packMapper.toDto(pack);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPackMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(packDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPack() throws Exception {
        int databaseSizeBeforeUpdate = packRepository.findAll().size();
        pack.setId(UUID.randomUUID().toString());

        // Create the Pack
        PackDTO packDTO = packMapper.toDto(pack);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPackMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(packDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Pack in the database
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePack() throws Exception {
        // Initialize the database
        packRepository.save(pack);

        int databaseSizeBeforeDelete = packRepository.findAll().size();

        // Delete the pack
        restPackMockMvc
            .perform(delete(ENTITY_API_URL_ID, pack.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pack> packList = packRepository.findAll();
        assertThat(packList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
