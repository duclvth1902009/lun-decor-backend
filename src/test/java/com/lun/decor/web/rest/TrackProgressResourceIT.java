package com.lun.decor.web.rest;

import static com.lun.decor.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.TrackProgress;
import com.lun.decor.repository.TrackProgressRepository;
import com.lun.decor.service.dto.TrackProgressDTO;
import com.lun.decor.service.mapper.TrackProgressMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link TrackProgressResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TrackProgressResourceIT {

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CLIENT_ENABLE = false;
    private static final Boolean UPDATED_CLIENT_ENABLE = true;

    private static final Integer DEFAULT_STEP = 1;
    private static final Integer UPDATED_STEP = 2;

    private static final ZonedDateTime DEFAULT_DEAD_LINE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DEAD_LINE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/track-progresses";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private TrackProgressRepository trackProgressRepository;

    @Autowired
    private TrackProgressMapper trackProgressMapper;

    @Autowired
    private MockMvc restTrackProgressMockMvc;

    private TrackProgress trackProgress;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrackProgress createEntity() {
        TrackProgress trackProgress = new TrackProgress()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .clientEnable(DEFAULT_CLIENT_ENABLE)
            .step(DEFAULT_STEP)
            .deadLine(DEFAULT_DEAD_LINE)
            .status(DEFAULT_STATUS);
        return trackProgress;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrackProgress createUpdatedEntity() {
        TrackProgress trackProgress = new TrackProgress()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .clientEnable(UPDATED_CLIENT_ENABLE)
            .step(UPDATED_STEP)
            .deadLine(UPDATED_DEAD_LINE)
            .status(UPDATED_STATUS);
        return trackProgress;
    }

    @BeforeEach
    public void initTest() {
        trackProgressRepository.deleteAll();
        trackProgress = createEntity();
    }

    @Test
    void createTrackProgress() throws Exception {
        int databaseSizeBeforeCreate = trackProgressRepository.findAll().size();
        // Create the TrackProgress
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(trackProgress);
        restTrackProgressMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isCreated());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeCreate + 1);
        TrackProgress testTrackProgress = trackProgressList.get(trackProgressList.size() - 1);
        assertThat(testTrackProgress.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTrackProgress.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTrackProgress.getClientEnable()).isEqualTo(DEFAULT_CLIENT_ENABLE);
        assertThat(testTrackProgress.getStep()).isEqualTo(DEFAULT_STEP);
        assertThat(testTrackProgress.getDeadLine()).isEqualTo(DEFAULT_DEAD_LINE);
        assertThat(testTrackProgress.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void createTrackProgressWithExistingId() throws Exception {
        // Create the TrackProgress with an existing ID
        trackProgress.setId("existing_id");
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(trackProgress);

        int databaseSizeBeforeCreate = trackProgressRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrackProgressMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllTrackProgresses() throws Exception {
        // Initialize the database
        trackProgressRepository.save(trackProgress);

        // Get all the trackProgressList
        restTrackProgressMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trackProgress.getId())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].clientEnable").value(hasItem(DEFAULT_CLIENT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].step").value(hasItem(DEFAULT_STEP)))
            .andExpect(jsonPath("$.[*].deadLine").value(hasItem(sameInstant(DEFAULT_DEAD_LINE))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    void getTrackProgress() throws Exception {
        // Initialize the database
        trackProgressRepository.save(trackProgress);

        // Get the trackProgress
        restTrackProgressMockMvc
            .perform(get(ENTITY_API_URL_ID, trackProgress.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(trackProgress.getId()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.clientEnable").value(DEFAULT_CLIENT_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.step").value(DEFAULT_STEP))
            .andExpect(jsonPath("$.deadLine").value(sameInstant(DEFAULT_DEAD_LINE)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    void getNonExistingTrackProgress() throws Exception {
        // Get the trackProgress
        restTrackProgressMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewTrackProgress() throws Exception {
        // Initialize the database
        trackProgressRepository.save(trackProgress);

        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();

        // Update the trackProgress
        TrackProgress updatedTrackProgress = trackProgressRepository.findById(trackProgress.getId()).get();
        updatedTrackProgress
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .clientEnable(UPDATED_CLIENT_ENABLE)
            .step(UPDATED_STEP)
            .deadLine(UPDATED_DEAD_LINE)
            .status(UPDATED_STATUS);
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(updatedTrackProgress);

        restTrackProgressMockMvc
            .perform(
                put(ENTITY_API_URL_ID, trackProgressDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isOk());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
        TrackProgress testTrackProgress = trackProgressList.get(trackProgressList.size() - 1);
        assertThat(testTrackProgress.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTrackProgress.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTrackProgress.getClientEnable()).isEqualTo(UPDATED_CLIENT_ENABLE);
        assertThat(testTrackProgress.getStep()).isEqualTo(UPDATED_STEP);
        assertThat(testTrackProgress.getDeadLine()).isEqualTo(UPDATED_DEAD_LINE);
        assertThat(testTrackProgress.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void putNonExistingTrackProgress() throws Exception {
        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();
        trackProgress.setId(UUID.randomUUID().toString());

        // Create the TrackProgress
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(trackProgress);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrackProgressMockMvc
            .perform(
                put(ENTITY_API_URL_ID, trackProgressDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchTrackProgress() throws Exception {
        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();
        trackProgress.setId(UUID.randomUUID().toString());

        // Create the TrackProgress
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(trackProgress);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTrackProgressMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamTrackProgress() throws Exception {
        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();
        trackProgress.setId(UUID.randomUUID().toString());

        // Create the TrackProgress
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(trackProgress);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTrackProgressMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateTrackProgressWithPatch() throws Exception {
        // Initialize the database
        trackProgressRepository.save(trackProgress);

        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();

        // Update the trackProgress using partial update
        TrackProgress partialUpdatedTrackProgress = new TrackProgress();
        partialUpdatedTrackProgress.setId(trackProgress.getId());

        partialUpdatedTrackProgress.step(UPDATED_STEP);

        restTrackProgressMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTrackProgress.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTrackProgress))
            )
            .andExpect(status().isOk());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
        TrackProgress testTrackProgress = trackProgressList.get(trackProgressList.size() - 1);
        assertThat(testTrackProgress.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTrackProgress.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTrackProgress.getClientEnable()).isEqualTo(DEFAULT_CLIENT_ENABLE);
        assertThat(testTrackProgress.getStep()).isEqualTo(UPDATED_STEP);
        assertThat(testTrackProgress.getDeadLine()).isEqualTo(DEFAULT_DEAD_LINE);
        assertThat(testTrackProgress.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void fullUpdateTrackProgressWithPatch() throws Exception {
        // Initialize the database
        trackProgressRepository.save(trackProgress);

        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();

        // Update the trackProgress using partial update
        TrackProgress partialUpdatedTrackProgress = new TrackProgress();
        partialUpdatedTrackProgress.setId(trackProgress.getId());

        partialUpdatedTrackProgress
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .clientEnable(UPDATED_CLIENT_ENABLE)
            .step(UPDATED_STEP)
            .deadLine(UPDATED_DEAD_LINE)
            .status(UPDATED_STATUS);

        restTrackProgressMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTrackProgress.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTrackProgress))
            )
            .andExpect(status().isOk());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
        TrackProgress testTrackProgress = trackProgressList.get(trackProgressList.size() - 1);
        assertThat(testTrackProgress.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTrackProgress.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTrackProgress.getClientEnable()).isEqualTo(UPDATED_CLIENT_ENABLE);
        assertThat(testTrackProgress.getStep()).isEqualTo(UPDATED_STEP);
        assertThat(testTrackProgress.getDeadLine()).isEqualTo(UPDATED_DEAD_LINE);
        assertThat(testTrackProgress.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void patchNonExistingTrackProgress() throws Exception {
        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();
        trackProgress.setId(UUID.randomUUID().toString());

        // Create the TrackProgress
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(trackProgress);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrackProgressMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, trackProgressDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchTrackProgress() throws Exception {
        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();
        trackProgress.setId(UUID.randomUUID().toString());

        // Create the TrackProgress
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(trackProgress);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTrackProgressMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamTrackProgress() throws Exception {
        int databaseSizeBeforeUpdate = trackProgressRepository.findAll().size();
        trackProgress.setId(UUID.randomUUID().toString());

        // Create the TrackProgress
        TrackProgressDTO trackProgressDTO = trackProgressMapper.toDto(trackProgress);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTrackProgressMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(trackProgressDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TrackProgress in the database
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteTrackProgress() throws Exception {
        // Initialize the database
        trackProgressRepository.save(trackProgress);

        int databaseSizeBeforeDelete = trackProgressRepository.findAll().size();

        // Delete the trackProgress
        restTrackProgressMockMvc
            .perform(delete(ENTITY_API_URL_ID, trackProgress.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TrackProgress> trackProgressList = trackProgressRepository.findAll();
        assertThat(trackProgressList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
