package com.lun.decor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.ServicePkg;
import com.lun.decor.repository.ServicePkgRepository;
import com.lun.decor.service.ServicePkgService;
import com.lun.decor.service.dto.ServicePkgDTO;
import com.lun.decor.service.mapper.ServicePkgMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link ServicePkgResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ServicePkgResourceIT {

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Long DEFAULT_PRICE = 1L;
    private static final Long UPDATED_PRICE = 2L;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/service-pkgs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ServicePkgRepository servicePkgRepository;

    @Mock
    private ServicePkgRepository servicePkgRepositoryMock;

    @Autowired
    private ServicePkgMapper servicePkgMapper;

    @Mock
    private ServicePkgService servicePkgServiceMock;

    @Autowired
    private MockMvc restServicePkgMockMvc;

    private ServicePkg servicePkg;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServicePkg createEntity() {
        ServicePkg servicePkg = new ServicePkg()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .price(DEFAULT_PRICE)
            .description(DEFAULT_DESCRIPTION);
        return servicePkg;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServicePkg createUpdatedEntity() {
        ServicePkg servicePkg = new ServicePkg()
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .price(UPDATED_PRICE)
            .description(UPDATED_DESCRIPTION);
        return servicePkg;
    }

    @BeforeEach
    public void initTest() {
        servicePkgRepository.deleteAll();
        servicePkg = createEntity();
    }

    @Test
    void createServicePkg() throws Exception {
        int databaseSizeBeforeCreate = servicePkgRepository.findAll().size();
        // Create the ServicePkg
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);
        restServicePkgMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(servicePkgDTO)))
            .andExpect(status().isCreated());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeCreate + 1);
        ServicePkg testServicePkg = servicePkgList.get(servicePkgList.size() - 1);
        assertThat(testServicePkg.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testServicePkg.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testServicePkg.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testServicePkg.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    void createServicePkgWithExistingId() throws Exception {
        // Create the ServicePkg with an existing ID
        servicePkg.setId("existing_id");
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);

        int databaseSizeBeforeCreate = servicePkgRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restServicePkgMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(servicePkgDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = servicePkgRepository.findAll().size();
        // set the field null
        servicePkg.setCode(null);

        // Create the ServicePkg, which fails.
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);

        restServicePkgMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(servicePkgDTO)))
            .andExpect(status().isBadRequest());

        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllServicePkgs() throws Exception {
        // Initialize the database
        servicePkgRepository.save(servicePkg);

        // Get all the servicePkgList
        restServicePkgMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(servicePkg.getId())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllServicePkgsWithEagerRelationshipsIsEnabled() throws Exception {
        when(servicePkgServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restServicePkgMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(servicePkgServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllServicePkgsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(servicePkgServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restServicePkgMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(servicePkgServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getServicePkg() throws Exception {
        // Initialize the database
        servicePkgRepository.save(servicePkg);

        // Get the servicePkg
        restServicePkgMockMvc
            .perform(get(ENTITY_API_URL_ID, servicePkg.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(servicePkg.getId()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    void getNonExistingServicePkg() throws Exception {
        // Get the servicePkg
        restServicePkgMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewServicePkg() throws Exception {
        // Initialize the database
        servicePkgRepository.save(servicePkg);

        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();

        // Update the servicePkg
        ServicePkg updatedServicePkg = servicePkgRepository.findById(servicePkg.getId()).get();
        updatedServicePkg.name(UPDATED_NAME).code(UPDATED_CODE).price(UPDATED_PRICE).description(UPDATED_DESCRIPTION);
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(updatedServicePkg);

        restServicePkgMockMvc
            .perform(
                put(ENTITY_API_URL_ID, servicePkgDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(servicePkgDTO))
            )
            .andExpect(status().isOk());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
        ServicePkg testServicePkg = servicePkgList.get(servicePkgList.size() - 1);
        assertThat(testServicePkg.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testServicePkg.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testServicePkg.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testServicePkg.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    void putNonExistingServicePkg() throws Exception {
        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();
        servicePkg.setId(UUID.randomUUID().toString());

        // Create the ServicePkg
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restServicePkgMockMvc
            .perform(
                put(ENTITY_API_URL_ID, servicePkgDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(servicePkgDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchServicePkg() throws Exception {
        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();
        servicePkg.setId(UUID.randomUUID().toString());

        // Create the ServicePkg
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServicePkgMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(servicePkgDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamServicePkg() throws Exception {
        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();
        servicePkg.setId(UUID.randomUUID().toString());

        // Create the ServicePkg
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServicePkgMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(servicePkgDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateServicePkgWithPatch() throws Exception {
        // Initialize the database
        servicePkgRepository.save(servicePkg);

        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();

        // Update the servicePkg using partial update
        ServicePkg partialUpdatedServicePkg = new ServicePkg();
        partialUpdatedServicePkg.setId(servicePkg.getId());

        partialUpdatedServicePkg.price(UPDATED_PRICE).description(UPDATED_DESCRIPTION);

        restServicePkgMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedServicePkg.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedServicePkg))
            )
            .andExpect(status().isOk());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
        ServicePkg testServicePkg = servicePkgList.get(servicePkgList.size() - 1);
        assertThat(testServicePkg.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testServicePkg.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testServicePkg.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testServicePkg.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    void fullUpdateServicePkgWithPatch() throws Exception {
        // Initialize the database
        servicePkgRepository.save(servicePkg);

        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();

        // Update the servicePkg using partial update
        ServicePkg partialUpdatedServicePkg = new ServicePkg();
        partialUpdatedServicePkg.setId(servicePkg.getId());

        partialUpdatedServicePkg.name(UPDATED_NAME).code(UPDATED_CODE).price(UPDATED_PRICE).description(UPDATED_DESCRIPTION);

        restServicePkgMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedServicePkg.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedServicePkg))
            )
            .andExpect(status().isOk());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
        ServicePkg testServicePkg = servicePkgList.get(servicePkgList.size() - 1);
        assertThat(testServicePkg.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testServicePkg.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testServicePkg.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testServicePkg.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    void patchNonExistingServicePkg() throws Exception {
        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();
        servicePkg.setId(UUID.randomUUID().toString());

        // Create the ServicePkg
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restServicePkgMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, servicePkgDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(servicePkgDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchServicePkg() throws Exception {
        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();
        servicePkg.setId(UUID.randomUUID().toString());

        // Create the ServicePkg
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServicePkgMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(servicePkgDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamServicePkg() throws Exception {
        int databaseSizeBeforeUpdate = servicePkgRepository.findAll().size();
        servicePkg.setId(UUID.randomUUID().toString());

        // Create the ServicePkg
        ServicePkgDTO servicePkgDTO = servicePkgMapper.toDto(servicePkg);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServicePkgMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(servicePkgDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ServicePkg in the database
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteServicePkg() throws Exception {
        // Initialize the database
        servicePkgRepository.save(servicePkg);

        int databaseSizeBeforeDelete = servicePkgRepository.findAll().size();

        // Delete the servicePkg
        restServicePkgMockMvc
            .perform(delete(ENTITY_API_URL_ID, servicePkg.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ServicePkg> servicePkgList = servicePkgRepository.findAll();
        assertThat(servicePkgList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
