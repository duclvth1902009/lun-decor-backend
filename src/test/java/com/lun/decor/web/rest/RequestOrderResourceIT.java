package com.lun.decor.web.rest;

import static com.lun.decor.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.RequestOrder;
import com.lun.decor.repository.RequestOrderRepository;
import com.lun.decor.service.RequestOrderService;
import com.lun.decor.service.dto.RequestOrderDTO;
import com.lun.decor.service.mapper.RequestOrderMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link RequestOrderResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class RequestOrderResourceIT {

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_PHONE = "501200";
    private static final String UPDATED_PHONE = "03287";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_CUS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CUS_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DISCOUNT = "AAAAAAAAAA";
    private static final String UPDATED_DISCOUNT = "BBBBBBBBBB";

    private static final String DEFAULT_ABOUT = "AAAAAAAAAA";
    private static final String UPDATED_ABOUT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_HOLD_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_HOLD_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_BUDGET = "AAAAAAAAAA";
    private static final String UPDATED_BUDGET = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_GUEST_NUM = "AAAAAAAAAA";
    private static final String UPDATED_GUEST_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/request-orders";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private RequestOrderRepository requestOrderRepository;

    @Mock
    private RequestOrderRepository requestOrderRepositoryMock;

    @Autowired
    private RequestOrderMapper requestOrderMapper;

    @Mock
    private RequestOrderService requestOrderServiceMock;

    @Autowired
    private MockMvc restRequestOrderMockMvc;

    private RequestOrder requestOrder;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequestOrder createEntity() {
        RequestOrder requestOrder = new RequestOrder()
            .phone(DEFAULT_PHONE)
            .email(DEFAULT_EMAIL)
            .cusName(DEFAULT_CUS_NAME)
            .discount(DEFAULT_DISCOUNT)
            .about(DEFAULT_ABOUT)
            .holdTime(DEFAULT_HOLD_TIME)
            .budget(DEFAULT_BUDGET)
            .address(DEFAULT_ADDRESS)
            .guestNum(DEFAULT_GUEST_NUM)
            .description(DEFAULT_DESCRIPTION)
            .status(DEFAULT_STATUS);
        return requestOrder;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequestOrder createUpdatedEntity() {
        RequestOrder requestOrder = new RequestOrder()
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .cusName(UPDATED_CUS_NAME)
            .discount(UPDATED_DISCOUNT)
            .about(UPDATED_ABOUT)
            .holdTime(UPDATED_HOLD_TIME)
            .budget(UPDATED_BUDGET)
            .address(UPDATED_ADDRESS)
            .guestNum(UPDATED_GUEST_NUM)
            .description(UPDATED_DESCRIPTION)
            .status(UPDATED_STATUS);
        return requestOrder;
    }

    @BeforeEach
    public void initTest() {
        requestOrderRepository.deleteAll();
        requestOrder = createEntity();
    }

    @Test
    void createRequestOrder() throws Exception {
        int databaseSizeBeforeCreate = requestOrderRepository.findAll().size();
        // Create the RequestOrder
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);
        restRequestOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isCreated());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeCreate + 1);
        RequestOrder testRequestOrder = requestOrderList.get(requestOrderList.size() - 1);
        assertThat(testRequestOrder.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testRequestOrder.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRequestOrder.getCusName()).isEqualTo(DEFAULT_CUS_NAME);
        assertThat(testRequestOrder.getDiscount()).isEqualTo(DEFAULT_DISCOUNT);
        assertThat(testRequestOrder.getAbout()).isEqualTo(DEFAULT_ABOUT);
        assertThat(testRequestOrder.getHoldTime()).isEqualTo(DEFAULT_HOLD_TIME);
        assertThat(testRequestOrder.getBudget()).isEqualTo(DEFAULT_BUDGET);
        assertThat(testRequestOrder.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testRequestOrder.getGuestNum()).isEqualTo(DEFAULT_GUEST_NUM);
        assertThat(testRequestOrder.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRequestOrder.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void createRequestOrderWithExistingId() throws Exception {
        // Create the RequestOrder with an existing ID
        requestOrder.setId("existing_id");
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);

        int databaseSizeBeforeCreate = requestOrderRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRequestOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = requestOrderRepository.findAll().size();
        // set the field null
        requestOrder.setPhone(null);

        // Create the RequestOrder, which fails.
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);

        restRequestOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isBadRequest());

        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllRequestOrders() throws Exception {
        // Initialize the database
        requestOrderRepository.save(requestOrder);

        // Get all the requestOrderList
        restRequestOrderMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requestOrder.getId())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].cusName").value(hasItem(DEFAULT_CUS_NAME)))
            .andExpect(jsonPath("$.[*].discount").value(hasItem(DEFAULT_DISCOUNT)))
            .andExpect(jsonPath("$.[*].about").value(hasItem(DEFAULT_ABOUT)))
            .andExpect(jsonPath("$.[*].holdTime").value(hasItem(sameInstant(DEFAULT_HOLD_TIME))))
            .andExpect(jsonPath("$.[*].budget").value(hasItem(DEFAULT_BUDGET)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].guestNum").value(hasItem(DEFAULT_GUEST_NUM)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllRequestOrdersWithEagerRelationshipsIsEnabled() throws Exception {
        when(requestOrderServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restRequestOrderMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(requestOrderServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllRequestOrdersWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(requestOrderServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restRequestOrderMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(requestOrderServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getRequestOrder() throws Exception {
        // Initialize the database
        requestOrderRepository.save(requestOrder);

        // Get the requestOrder
        restRequestOrderMockMvc
            .perform(get(ENTITY_API_URL_ID, requestOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(requestOrder.getId()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.cusName").value(DEFAULT_CUS_NAME))
            .andExpect(jsonPath("$.discount").value(DEFAULT_DISCOUNT))
            .andExpect(jsonPath("$.about").value(DEFAULT_ABOUT))
            .andExpect(jsonPath("$.holdTime").value(sameInstant(DEFAULT_HOLD_TIME)))
            .andExpect(jsonPath("$.budget").value(DEFAULT_BUDGET))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.guestNum").value(DEFAULT_GUEST_NUM))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    void getNonExistingRequestOrder() throws Exception {
        // Get the requestOrder
        restRequestOrderMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewRequestOrder() throws Exception {
        // Initialize the database
        requestOrderRepository.save(requestOrder);

        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();

        // Update the requestOrder
        RequestOrder updatedRequestOrder = requestOrderRepository.findById(requestOrder.getId()).get();
        updatedRequestOrder
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .cusName(UPDATED_CUS_NAME)
            .discount(UPDATED_DISCOUNT)
            .about(UPDATED_ABOUT)
            .holdTime(UPDATED_HOLD_TIME)
            .budget(UPDATED_BUDGET)
            .address(UPDATED_ADDRESS)
            .guestNum(UPDATED_GUEST_NUM)
            .description(UPDATED_DESCRIPTION)
            .status(UPDATED_STATUS);
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(updatedRequestOrder);

        restRequestOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, requestOrderDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isOk());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
        RequestOrder testRequestOrder = requestOrderList.get(requestOrderList.size() - 1);
        assertThat(testRequestOrder.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testRequestOrder.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRequestOrder.getCusName()).isEqualTo(UPDATED_CUS_NAME);
        assertThat(testRequestOrder.getDiscount()).isEqualTo(UPDATED_DISCOUNT);
        assertThat(testRequestOrder.getAbout()).isEqualTo(UPDATED_ABOUT);
        assertThat(testRequestOrder.getHoldTime()).isEqualTo(UPDATED_HOLD_TIME);
        assertThat(testRequestOrder.getBudget()).isEqualTo(UPDATED_BUDGET);
        assertThat(testRequestOrder.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testRequestOrder.getGuestNum()).isEqualTo(UPDATED_GUEST_NUM);
        assertThat(testRequestOrder.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRequestOrder.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void putNonExistingRequestOrder() throws Exception {
        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();
        requestOrder.setId(UUID.randomUUID().toString());

        // Create the RequestOrder
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRequestOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, requestOrderDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchRequestOrder() throws Exception {
        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();
        requestOrder.setId(UUID.randomUUID().toString());

        // Create the RequestOrder
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRequestOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamRequestOrder() throws Exception {
        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();
        requestOrder.setId(UUID.randomUUID().toString());

        // Create the RequestOrder
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRequestOrderMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateRequestOrderWithPatch() throws Exception {
        // Initialize the database
        requestOrderRepository.save(requestOrder);

        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();

        // Update the requestOrder using partial update
        RequestOrder partialUpdatedRequestOrder = new RequestOrder();
        partialUpdatedRequestOrder.setId(requestOrder.getId());

        partialUpdatedRequestOrder
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .cusName(UPDATED_CUS_NAME)
            .discount(UPDATED_DISCOUNT)
            .holdTime(UPDATED_HOLD_TIME)
            .budget(UPDATED_BUDGET)
            .description(UPDATED_DESCRIPTION);

        restRequestOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRequestOrder.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRequestOrder))
            )
            .andExpect(status().isOk());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
        RequestOrder testRequestOrder = requestOrderList.get(requestOrderList.size() - 1);
        assertThat(testRequestOrder.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testRequestOrder.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRequestOrder.getCusName()).isEqualTo(UPDATED_CUS_NAME);
        assertThat(testRequestOrder.getDiscount()).isEqualTo(UPDATED_DISCOUNT);
        assertThat(testRequestOrder.getAbout()).isEqualTo(DEFAULT_ABOUT);
        assertThat(testRequestOrder.getHoldTime()).isEqualTo(UPDATED_HOLD_TIME);
        assertThat(testRequestOrder.getBudget()).isEqualTo(UPDATED_BUDGET);
        assertThat(testRequestOrder.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testRequestOrder.getGuestNum()).isEqualTo(DEFAULT_GUEST_NUM);
        assertThat(testRequestOrder.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRequestOrder.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void fullUpdateRequestOrderWithPatch() throws Exception {
        // Initialize the database
        requestOrderRepository.save(requestOrder);

        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();

        // Update the requestOrder using partial update
        RequestOrder partialUpdatedRequestOrder = new RequestOrder();
        partialUpdatedRequestOrder.setId(requestOrder.getId());

        partialUpdatedRequestOrder
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .cusName(UPDATED_CUS_NAME)
            .discount(UPDATED_DISCOUNT)
            .about(UPDATED_ABOUT)
            .holdTime(UPDATED_HOLD_TIME)
            .budget(UPDATED_BUDGET)
            .address(UPDATED_ADDRESS)
            .guestNum(UPDATED_GUEST_NUM)
            .description(UPDATED_DESCRIPTION)
            .status(UPDATED_STATUS);

        restRequestOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRequestOrder.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRequestOrder))
            )
            .andExpect(status().isOk());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
        RequestOrder testRequestOrder = requestOrderList.get(requestOrderList.size() - 1);
        assertThat(testRequestOrder.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testRequestOrder.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRequestOrder.getCusName()).isEqualTo(UPDATED_CUS_NAME);
        assertThat(testRequestOrder.getDiscount()).isEqualTo(UPDATED_DISCOUNT);
        assertThat(testRequestOrder.getAbout()).isEqualTo(UPDATED_ABOUT);
        assertThat(testRequestOrder.getHoldTime()).isEqualTo(UPDATED_HOLD_TIME);
        assertThat(testRequestOrder.getBudget()).isEqualTo(UPDATED_BUDGET);
        assertThat(testRequestOrder.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testRequestOrder.getGuestNum()).isEqualTo(UPDATED_GUEST_NUM);
        assertThat(testRequestOrder.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRequestOrder.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void patchNonExistingRequestOrder() throws Exception {
        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();
        requestOrder.setId(UUID.randomUUID().toString());

        // Create the RequestOrder
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRequestOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, requestOrderDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchRequestOrder() throws Exception {
        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();
        requestOrder.setId(UUID.randomUUID().toString());

        // Create the RequestOrder
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRequestOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamRequestOrder() throws Exception {
        int databaseSizeBeforeUpdate = requestOrderRepository.findAll().size();
        requestOrder.setId(UUID.randomUUID().toString());

        // Create the RequestOrder
        RequestOrderDTO requestOrderDTO = requestOrderMapper.toDto(requestOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRequestOrderMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(requestOrderDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RequestOrder in the database
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteRequestOrder() throws Exception {
        // Initialize the database
        requestOrderRepository.save(requestOrder);

        int databaseSizeBeforeDelete = requestOrderRepository.findAll().size();

        // Delete the requestOrder
        restRequestOrderMockMvc
            .perform(delete(ENTITY_API_URL_ID, requestOrder.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RequestOrder> requestOrderList = requestOrderRepository.findAll();
        assertThat(requestOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
