package com.lun.decor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.EventVenue;
import com.lun.decor.repository.EventVenueRepository;
import com.lun.decor.service.dto.EventVenueDTO;
import com.lun.decor.service.mapper.EventVenueMapper;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link EventVenueResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EventVenueResourceIT {

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final Long DEFAULT_PRICE_FROM = 1L;
    private static final Long UPDATED_PRICE_FROM = 2L;

    private static final Long DEFAULT_PRICE_TO = 1L;
    private static final Long UPDATED_PRICE_TO = 2L;

    private static final String DEFAULT_PHONE = "60";
    private static final String UPDATED_PHONE = "939";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Long DEFAULT_QUANTITY_MAX = 1L;
    private static final Long UPDATED_QUANTITY_MAX = 2L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/event-venues";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private EventVenueRepository eventVenueRepository;

    @Autowired
    private EventVenueMapper eventVenueMapper;

    @Autowired
    private MockMvc restEventVenueMockMvc;

    private EventVenue eventVenue;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventVenue createEntity() {
        EventVenue eventVenue = new EventVenue()
            .name(DEFAULT_NAME)
            .address(DEFAULT_ADDRESS)
            .location(DEFAULT_LOCATION)
            .price_from(DEFAULT_PRICE_FROM)
            .price_to(DEFAULT_PRICE_TO)
            .phone(DEFAULT_PHONE)
            .email(DEFAULT_EMAIL)
            .description(DEFAULT_DESCRIPTION)
            .quantity_max(DEFAULT_QUANTITY_MAX)
            .note(DEFAULT_NOTE);
        return eventVenue;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventVenue createUpdatedEntity() {
        EventVenue eventVenue = new EventVenue()
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .location(UPDATED_LOCATION)
            .price_from(UPDATED_PRICE_FROM)
            .price_to(UPDATED_PRICE_TO)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .description(UPDATED_DESCRIPTION)
            .quantity_max(UPDATED_QUANTITY_MAX)
            .note(UPDATED_NOTE);
        return eventVenue;
    }

    @BeforeEach
    public void initTest() {
        eventVenueRepository.deleteAll();
        eventVenue = createEntity();
    }

    @Test
    void createEventVenue() throws Exception {
        int databaseSizeBeforeCreate = eventVenueRepository.findAll().size();
        // Create the EventVenue
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);
        restEventVenueMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(eventVenueDTO)))
            .andExpect(status().isCreated());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeCreate + 1);
        EventVenue testEventVenue = eventVenueList.get(eventVenueList.size() - 1);
        assertThat(testEventVenue.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEventVenue.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testEventVenue.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testEventVenue.getPrice_from()).isEqualTo(DEFAULT_PRICE_FROM);
        assertThat(testEventVenue.getPrice_to()).isEqualTo(DEFAULT_PRICE_TO);
        assertThat(testEventVenue.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testEventVenue.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testEventVenue.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEventVenue.getQuantity_max()).isEqualTo(DEFAULT_QUANTITY_MAX);
        assertThat(testEventVenue.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    void createEventVenueWithExistingId() throws Exception {
        // Create the EventVenue with an existing ID
        eventVenue.setId("existing_id");
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);

        int databaseSizeBeforeCreate = eventVenueRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEventVenueMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(eventVenueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventVenueRepository.findAll().size();
        // set the field null
        eventVenue.setPhone(null);

        // Create the EventVenue, which fails.
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);

        restEventVenueMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(eventVenueDTO)))
            .andExpect(status().isBadRequest());

        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllEventVenues() throws Exception {
        // Initialize the database
        eventVenueRepository.save(eventVenue);

        // Get all the eventVenueList
        restEventVenueMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eventVenue.getId())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].price_from").value(hasItem(DEFAULT_PRICE_FROM.intValue())))
            .andExpect(jsonPath("$.[*].price_to").value(hasItem(DEFAULT_PRICE_TO.intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].quantity_max").value(hasItem(DEFAULT_QUANTITY_MAX.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)));
    }

    @Test
    void getEventVenue() throws Exception {
        // Initialize the database
        eventVenueRepository.save(eventVenue);

        // Get the eventVenue
        restEventVenueMockMvc
            .perform(get(ENTITY_API_URL_ID, eventVenue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(eventVenue.getId()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION))
            .andExpect(jsonPath("$.price_from").value(DEFAULT_PRICE_FROM.intValue()))
            .andExpect(jsonPath("$.price_to").value(DEFAULT_PRICE_TO.intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.quantity_max").value(DEFAULT_QUANTITY_MAX.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE));
    }

    @Test
    void getNonExistingEventVenue() throws Exception {
        // Get the eventVenue
        restEventVenueMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewEventVenue() throws Exception {
        // Initialize the database
        eventVenueRepository.save(eventVenue);

        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();

        // Update the eventVenue
        EventVenue updatedEventVenue = eventVenueRepository.findById(eventVenue.getId()).get();
        updatedEventVenue
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .location(UPDATED_LOCATION)
            .price_from(UPDATED_PRICE_FROM)
            .price_to(UPDATED_PRICE_TO)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .description(UPDATED_DESCRIPTION)
            .quantity_max(UPDATED_QUANTITY_MAX)
            .note(UPDATED_NOTE);
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(updatedEventVenue);

        restEventVenueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, eventVenueDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(eventVenueDTO))
            )
            .andExpect(status().isOk());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
        EventVenue testEventVenue = eventVenueList.get(eventVenueList.size() - 1);
        assertThat(testEventVenue.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEventVenue.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testEventVenue.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testEventVenue.getPrice_from()).isEqualTo(UPDATED_PRICE_FROM);
        assertThat(testEventVenue.getPrice_to()).isEqualTo(UPDATED_PRICE_TO);
        assertThat(testEventVenue.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testEventVenue.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testEventVenue.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEventVenue.getQuantity_max()).isEqualTo(UPDATED_QUANTITY_MAX);
        assertThat(testEventVenue.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    void putNonExistingEventVenue() throws Exception {
        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();
        eventVenue.setId(UUID.randomUUID().toString());

        // Create the EventVenue
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEventVenueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, eventVenueDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(eventVenueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchEventVenue() throws Exception {
        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();
        eventVenue.setId(UUID.randomUUID().toString());

        // Create the EventVenue
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEventVenueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(eventVenueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamEventVenue() throws Exception {
        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();
        eventVenue.setId(UUID.randomUUID().toString());

        // Create the EventVenue
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEventVenueMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(eventVenueDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateEventVenueWithPatch() throws Exception {
        // Initialize the database
        eventVenueRepository.save(eventVenue);

        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();

        // Update the eventVenue using partial update
        EventVenue partialUpdatedEventVenue = new EventVenue();
        partialUpdatedEventVenue.setId(eventVenue.getId());

        partialUpdatedEventVenue.address(UPDATED_ADDRESS).location(UPDATED_LOCATION).description(UPDATED_DESCRIPTION);

        restEventVenueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEventVenue.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEventVenue))
            )
            .andExpect(status().isOk());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
        EventVenue testEventVenue = eventVenueList.get(eventVenueList.size() - 1);
        assertThat(testEventVenue.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEventVenue.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testEventVenue.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testEventVenue.getPrice_from()).isEqualTo(DEFAULT_PRICE_FROM);
        assertThat(testEventVenue.getPrice_to()).isEqualTo(DEFAULT_PRICE_TO);
        assertThat(testEventVenue.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testEventVenue.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testEventVenue.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEventVenue.getQuantity_max()).isEqualTo(DEFAULT_QUANTITY_MAX);
        assertThat(testEventVenue.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    void fullUpdateEventVenueWithPatch() throws Exception {
        // Initialize the database
        eventVenueRepository.save(eventVenue);

        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();

        // Update the eventVenue using partial update
        EventVenue partialUpdatedEventVenue = new EventVenue();
        partialUpdatedEventVenue.setId(eventVenue.getId());

        partialUpdatedEventVenue
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .location(UPDATED_LOCATION)
            .price_from(UPDATED_PRICE_FROM)
            .price_to(UPDATED_PRICE_TO)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .description(UPDATED_DESCRIPTION)
            .quantity_max(UPDATED_QUANTITY_MAX)
            .note(UPDATED_NOTE);

        restEventVenueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEventVenue.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEventVenue))
            )
            .andExpect(status().isOk());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
        EventVenue testEventVenue = eventVenueList.get(eventVenueList.size() - 1);
        assertThat(testEventVenue.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEventVenue.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testEventVenue.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testEventVenue.getPrice_from()).isEqualTo(UPDATED_PRICE_FROM);
        assertThat(testEventVenue.getPrice_to()).isEqualTo(UPDATED_PRICE_TO);
        assertThat(testEventVenue.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testEventVenue.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testEventVenue.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEventVenue.getQuantity_max()).isEqualTo(UPDATED_QUANTITY_MAX);
        assertThat(testEventVenue.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    void patchNonExistingEventVenue() throws Exception {
        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();
        eventVenue.setId(UUID.randomUUID().toString());

        // Create the EventVenue
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEventVenueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, eventVenueDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(eventVenueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchEventVenue() throws Exception {
        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();
        eventVenue.setId(UUID.randomUUID().toString());

        // Create the EventVenue
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEventVenueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(eventVenueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamEventVenue() throws Exception {
        int databaseSizeBeforeUpdate = eventVenueRepository.findAll().size();
        eventVenue.setId(UUID.randomUUID().toString());

        // Create the EventVenue
        EventVenueDTO eventVenueDTO = eventVenueMapper.toDto(eventVenue);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEventVenueMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(eventVenueDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EventVenue in the database
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteEventVenue() throws Exception {
        // Initialize the database
        eventVenueRepository.save(eventVenue);

        int databaseSizeBeforeDelete = eventVenueRepository.findAll().size();

        // Delete the eventVenue
        restEventVenueMockMvc
            .perform(delete(ENTITY_API_URL_ID, eventVenue.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EventVenue> eventVenueList = eventVenueRepository.findAll();
        assertThat(eventVenueList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
