package com.lun.decor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.Transactn;
import com.lun.decor.repository.TransactnRepository;
import com.lun.decor.service.dto.TransactnDTO;
import com.lun.decor.service.mapper.TransactnMapper;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link TransactnResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TransactnResourceIT {

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_DETAIL = "BBBBBBBBBB";

    private static final Long DEFAULT_TOTAL = 1L;
    private static final Long UPDATED_TOTAL = 2L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/transactns";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private TransactnRepository transactnRepository;

    @Autowired
    private TransactnMapper transactnMapper;

    @Autowired
    private MockMvc restTransactnMockMvc;

    private Transactn transactn;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transactn createEntity() {
        Transactn transactn = new Transactn().detail(DEFAULT_DETAIL).total(DEFAULT_TOTAL).note(DEFAULT_NOTE).status(DEFAULT_STATUS);
        return transactn;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transactn createUpdatedEntity() {
        Transactn transactn = new Transactn().detail(UPDATED_DETAIL).total(UPDATED_TOTAL).note(UPDATED_NOTE).status(UPDATED_STATUS);
        return transactn;
    }

    @BeforeEach
    public void initTest() {
        transactnRepository.deleteAll();
        transactn = createEntity();
    }

    @Test
    void createTransactn() throws Exception {
        int databaseSizeBeforeCreate = transactnRepository.findAll().size();
        // Create the Transactn
        TransactnDTO transactnDTO = transactnMapper.toDto(transactn);
        restTransactnMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(transactnDTO)))
            .andExpect(status().isCreated());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeCreate + 1);
        Transactn testTransactn = transactnList.get(transactnList.size() - 1);
        assertThat(testTransactn.getFile()).isEqualTo(DEFAULT_FILE);
        assertThat(testTransactn.getFileContentType()).isEqualTo(DEFAULT_FILE_CONTENT_TYPE);
        assertThat(testTransactn.getDetail()).isEqualTo(DEFAULT_DETAIL);
        assertThat(testTransactn.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testTransactn.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testTransactn.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void createTransactnWithExistingId() throws Exception {
        // Create the Transactn with an existing ID
        transactn.setId("existing_id");
        TransactnDTO transactnDTO = transactnMapper.toDto(transactn);

        int databaseSizeBeforeCreate = transactnRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactnMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(transactnDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllTransactns() throws Exception {
        // Initialize the database
        transactnRepository.save(transactn);

        // Get all the transactnList
        restTransactnMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactn.getId())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].detail").value(hasItem(DEFAULT_DETAIL)))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    void getTransactn() throws Exception {
        // Initialize the database
        transactnRepository.save(transactn);

        // Get the transactn
        restTransactnMockMvc
            .perform(get(ENTITY_API_URL_ID, transactn.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(transactn.getId()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.detail").value(DEFAULT_DETAIL))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    void getNonExistingTransactn() throws Exception {
        // Get the transactn
        restTransactnMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewTransactn() throws Exception {
        // Initialize the database
        transactnRepository.save(transactn);

        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();

        // Update the transactn
        Transactn updatedTransactn = transactnRepository.findById(transactn.getId()).get();
        updatedTransactn.detail(UPDATED_DETAIL).total(UPDATED_TOTAL).note(UPDATED_NOTE).status(UPDATED_STATUS);
        TransactnDTO transactnDTO = transactnMapper.toDto(updatedTransactn);

        restTransactnMockMvc
            .perform(
                put(ENTITY_API_URL_ID, transactnDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactnDTO))
            )
            .andExpect(status().isOk());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
        Transactn testTransactn = transactnList.get(transactnList.size() - 1);
        assertThat(testTransactn.getFile()).isEqualTo(UPDATED_FILE);
        assertThat(testTransactn.getFileContentType()).isEqualTo(UPDATED_FILE_CONTENT_TYPE);
        assertThat(testTransactn.getDetail()).isEqualTo(UPDATED_DETAIL);
        assertThat(testTransactn.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testTransactn.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testTransactn.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void putNonExistingTransactn() throws Exception {
        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();
        transactn.setId(UUID.randomUUID().toString());

        // Create the Transactn
        TransactnDTO transactnDTO = transactnMapper.toDto(transactn);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactnMockMvc
            .perform(
                put(ENTITY_API_URL_ID, transactnDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactnDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchTransactn() throws Exception {
        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();
        transactn.setId(UUID.randomUUID().toString());

        // Create the Transactn
        TransactnDTO transactnDTO = transactnMapper.toDto(transactn);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactnMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactnDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamTransactn() throws Exception {
        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();
        transactn.setId(UUID.randomUUID().toString());

        // Create the Transactn
        TransactnDTO transactnDTO = transactnMapper.toDto(transactn);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactnMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(transactnDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateTransactnWithPatch() throws Exception {
        // Initialize the database
        transactnRepository.save(transactn);

        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();

        // Update the transactn using partial update
        Transactn partialUpdatedTransactn = new Transactn();
        partialUpdatedTransactn.setId(transactn.getId());

        partialUpdatedTransactn.detail(UPDATED_DETAIL).total(UPDATED_TOTAL).note(UPDATED_NOTE);

        restTransactnMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTransactn.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTransactn))
            )
            .andExpect(status().isOk());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
        Transactn testTransactn = transactnList.get(transactnList.size() - 1);
        assertThat(testTransactn.getFile()).isEqualTo(UPDATED_FILE);
        assertThat(testTransactn.getFileContentType()).isEqualTo(UPDATED_FILE_CONTENT_TYPE);
        assertThat(testTransactn.getDetail()).isEqualTo(UPDATED_DETAIL);
        assertThat(testTransactn.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testTransactn.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testTransactn.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void fullUpdateTransactnWithPatch() throws Exception {
        // Initialize the database
        transactnRepository.save(transactn);

        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();

        // Update the transactn using partial update
        Transactn partialUpdatedTransactn = new Transactn();
        partialUpdatedTransactn.setId(transactn.getId());

        partialUpdatedTransactn.detail(UPDATED_DETAIL).total(UPDATED_TOTAL).note(UPDATED_NOTE).status(UPDATED_STATUS);

        restTransactnMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTransactn.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTransactn))
            )
            .andExpect(status().isOk());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
        Transactn testTransactn = transactnList.get(transactnList.size() - 1);
        assertThat(testTransactn.getFile()).isEqualTo(UPDATED_FILE);
        assertThat(testTransactn.getFileContentType()).isEqualTo(UPDATED_FILE_CONTENT_TYPE);
        assertThat(testTransactn.getDetail()).isEqualTo(UPDATED_DETAIL);
        assertThat(testTransactn.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testTransactn.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testTransactn.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void patchNonExistingTransactn() throws Exception {
        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();
        transactn.setId(UUID.randomUUID().toString());

        // Create the Transactn
        TransactnDTO transactnDTO = transactnMapper.toDto(transactn);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactnMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, transactnDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(transactnDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchTransactn() throws Exception {
        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();
        transactn.setId(UUID.randomUUID().toString());

        // Create the Transactn
        TransactnDTO transactnDTO = transactnMapper.toDto(transactn);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactnMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(transactnDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamTransactn() throws Exception {
        int databaseSizeBeforeUpdate = transactnRepository.findAll().size();
        transactn.setId(UUID.randomUUID().toString());

        // Create the Transactn
        TransactnDTO transactnDTO = transactnMapper.toDto(transactn);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactnMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(transactnDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Transactn in the database
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteTransactn() throws Exception {
        // Initialize the database
        transactnRepository.save(transactn);

        int databaseSizeBeforeDelete = transactnRepository.findAll().size();

        // Delete the transactn
        restTransactnMockMvc
            .perform(delete(ENTITY_API_URL_ID, transactn.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Transactn> transactnList = transactnRepository.findAll();
        assertThat(transactnList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
