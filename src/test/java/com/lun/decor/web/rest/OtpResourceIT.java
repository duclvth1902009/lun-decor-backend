package com.lun.decor.web.rest;

import static com.lun.decor.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.Otp;
import com.lun.decor.repository.OtpRepository;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Integration tests for the {@link OtpResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OtpResourceIT {

    private static final String DEFAULT_OTP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_OTP_CODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_EXPIRED_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EXPIRED_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_STATUS = 1L;
    private static final Long UPDATED_STATUS = 2L;

    private static final String ENTITY_API_URL = "/api/otps";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private OtpRepository otpRepository;

    @Autowired
    private MockMvc restOtpMockMvc;

    private Otp otp;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Otp createEntity() {
        Otp otp = new Otp().otp_code(DEFAULT_OTP_CODE).expired_time(DEFAULT_EXPIRED_TIME).status(DEFAULT_STATUS);
        return otp;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Otp createUpdatedEntity() {
        Otp otp = new Otp().otp_code(UPDATED_OTP_CODE).expired_time(UPDATED_EXPIRED_TIME).status(UPDATED_STATUS);
        return otp;
    }

    @BeforeEach
    public void initTest() {
        otpRepository.deleteAll();
        otp = createEntity();
    }

    @Test
    void createOtp() throws Exception {
        int databaseSizeBeforeCreate = otpRepository.findAll().size();
        // Create the Otp
        restOtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(otp)))
            .andExpect(status().isCreated());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeCreate + 1);
        Otp testOtp = otpList.get(otpList.size() - 1);
        assertThat(testOtp.getOtp_code()).isEqualTo(DEFAULT_OTP_CODE);
        assertThat(testOtp.getExpired_time()).isEqualTo(DEFAULT_EXPIRED_TIME);
        assertThat(testOtp.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void createOtpWithExistingId() throws Exception {
        // Create the Otp with an existing ID
        otp.setId("existing_id");

        int databaseSizeBeforeCreate = otpRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(otp)))
            .andExpect(status().isBadRequest());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllOtps() throws Exception {
        // Initialize the database
        otpRepository.save(otp);

        // Get all the otpList
        restOtpMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otp.getId())))
            .andExpect(jsonPath("$.[*].otp_code").value(hasItem(DEFAULT_OTP_CODE)))
            .andExpect(jsonPath("$.[*].expired_time").value(hasItem(sameInstant(DEFAULT_EXPIRED_TIME))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.intValue())));
    }

    @Test
    void getOtp() throws Exception {
        // Initialize the database
        otpRepository.save(otp);

        // Get the otp
        restOtpMockMvc
            .perform(get(ENTITY_API_URL_ID, otp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(otp.getId()))
            .andExpect(jsonPath("$.otp_code").value(DEFAULT_OTP_CODE))
            .andExpect(jsonPath("$.expired_time").value(sameInstant(DEFAULT_EXPIRED_TIME)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.intValue()));
    }

    @Test
    void getNonExistingOtp() throws Exception {
        // Get the otp
        restOtpMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewOtp() throws Exception {
        // Initialize the database
        otpRepository.save(otp);

        int databaseSizeBeforeUpdate = otpRepository.findAll().size();

        // Update the otp
        Otp updatedOtp = otpRepository.findById(otp.getId()).get();
        updatedOtp.otp_code(UPDATED_OTP_CODE).expired_time(UPDATED_EXPIRED_TIME).status(UPDATED_STATUS);

        restOtpMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOtp.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedOtp))
            )
            .andExpect(status().isOk());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
        Otp testOtp = otpList.get(otpList.size() - 1);
        assertThat(testOtp.getOtp_code()).isEqualTo(UPDATED_OTP_CODE);
        assertThat(testOtp.getExpired_time()).isEqualTo(UPDATED_EXPIRED_TIME);
        assertThat(testOtp.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void putNonExistingOtp() throws Exception {
        int databaseSizeBeforeUpdate = otpRepository.findAll().size();
        otp.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtpMockMvc
            .perform(
                put(ENTITY_API_URL_ID, otp.getId()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(otp))
            )
            .andExpect(status().isBadRequest());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchOtp() throws Exception {
        int databaseSizeBeforeUpdate = otpRepository.findAll().size();
        otp.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtpMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(otp))
            )
            .andExpect(status().isBadRequest());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamOtp() throws Exception {
        int databaseSizeBeforeUpdate = otpRepository.findAll().size();
        otp.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtpMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(otp)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateOtpWithPatch() throws Exception {
        // Initialize the database
        otpRepository.save(otp);

        int databaseSizeBeforeUpdate = otpRepository.findAll().size();

        // Update the otp using partial update
        Otp partialUpdatedOtp = new Otp();
        partialUpdatedOtp.setId(otp.getId());

        partialUpdatedOtp.otp_code(UPDATED_OTP_CODE);

        restOtpMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOtp.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOtp))
            )
            .andExpect(status().isOk());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
        Otp testOtp = otpList.get(otpList.size() - 1);
        assertThat(testOtp.getOtp_code()).isEqualTo(UPDATED_OTP_CODE);
        assertThat(testOtp.getExpired_time()).isEqualTo(DEFAULT_EXPIRED_TIME);
        assertThat(testOtp.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void fullUpdateOtpWithPatch() throws Exception {
        // Initialize the database
        otpRepository.save(otp);

        int databaseSizeBeforeUpdate = otpRepository.findAll().size();

        // Update the otp using partial update
        Otp partialUpdatedOtp = new Otp();
        partialUpdatedOtp.setId(otp.getId());

        partialUpdatedOtp.otp_code(UPDATED_OTP_CODE).expired_time(UPDATED_EXPIRED_TIME).status(UPDATED_STATUS);

        restOtpMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOtp.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOtp))
            )
            .andExpect(status().isOk());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
        Otp testOtp = otpList.get(otpList.size() - 1);
        assertThat(testOtp.getOtp_code()).isEqualTo(UPDATED_OTP_CODE);
        assertThat(testOtp.getExpired_time()).isEqualTo(UPDATED_EXPIRED_TIME);
        assertThat(testOtp.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void patchNonExistingOtp() throws Exception {
        int databaseSizeBeforeUpdate = otpRepository.findAll().size();
        otp.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtpMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, otp.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otp))
            )
            .andExpect(status().isBadRequest());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchOtp() throws Exception {
        int databaseSizeBeforeUpdate = otpRepository.findAll().size();
        otp.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtpMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(otp))
            )
            .andExpect(status().isBadRequest());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamOtp() throws Exception {
        int databaseSizeBeforeUpdate = otpRepository.findAll().size();
        otp.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOtpMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(otp)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteOtp() throws Exception {
        // Initialize the database
        otpRepository.save(otp);

        int databaseSizeBeforeDelete = otpRepository.findAll().size();

        // Delete the otp
        restOtpMockMvc.perform(delete(ENTITY_API_URL_ID, otp.getId()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
