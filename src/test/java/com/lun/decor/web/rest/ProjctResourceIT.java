package com.lun.decor.web.rest;

import static com.lun.decor.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lun.decor.IntegrationTest;
import com.lun.decor.domain.Projct;
import com.lun.decor.repository.ProjctRepository;
import com.lun.decor.service.ProjctService;
import com.lun.decor.service.dto.ProjctDTO;
import com.lun.decor.service.mapper.ProjctMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link ProjctResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ProjctResourceIT {

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final ZonedDateTime DEFAULT_HOLD_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_HOLD_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final byte[] DEFAULT_CONTRACT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_CONTRACT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_CONTRACT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_CONTRACT_CONTENT_TYPE = "image/png";

    private static final Double DEFAULT_RATE = 1D;
    private static final Double UPDATED_RATE = 2D;

    private static final Long DEFAULT_PRE_CAPITAL = 1L;
    private static final Long UPDATED_PRE_CAPITAL = 2L;

    private static final Long DEFAULT_PRE_TOTAL = 1L;
    private static final Long UPDATED_PRE_TOTAL = 2L;

    private static final Long DEFAULT_FIN_CAPITAL = 1L;
    private static final Long UPDATED_FIN_CAPITAL = 2L;

    private static final Long DEFAULT_FIN_TOTAL = 1L;
    private static final Long UPDATED_FIN_TOTAL = 2L;

    private static final String DEFAULT_RSCHANGE_CAP = "AAAAAAAAAA";
    private static final String UPDATED_RSCHANGE_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_RSCHANGE_TOT = "AAAAAAAAAA";
    private static final String UPDATED_RSCHANGE_TOT = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/projcts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ProjctRepository projctRepository;

    @Mock
    private ProjctRepository projctRepositoryMock;

    @Autowired
    private ProjctMapper projctMapper;

    @Mock
    private ProjctService projctServiceMock;

    @Autowired
    private MockMvc restProjctMockMvc;

    private Projct projct;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Projct createEntity() {
        Projct projct = new Projct()
            .holdTime(DEFAULT_HOLD_TIME)
            .location(DEFAULT_LOCATION)
            .contract(DEFAULT_CONTRACT)
            .contractContentType(DEFAULT_CONTRACT_CONTENT_TYPE)
            .rate(DEFAULT_RATE)
            .preCapital(DEFAULT_PRE_CAPITAL)
            .preTotal(DEFAULT_PRE_TOTAL)
            .finCapital(DEFAULT_FIN_CAPITAL)
            .finTotal(DEFAULT_FIN_TOTAL)
            .rschangeCap(DEFAULT_RSCHANGE_CAP)
            .rschangeTot(DEFAULT_RSCHANGE_TOT)
            .status(DEFAULT_STATUS)
            .name(DEFAULT_NAME);
        return projct;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Projct createUpdatedEntity() {
        Projct projct = new Projct()
            .holdTime(UPDATED_HOLD_TIME)
            .location(UPDATED_LOCATION)
            .contract(UPDATED_CONTRACT)
            .contractContentType(UPDATED_CONTRACT_CONTENT_TYPE)
            .rate(UPDATED_RATE)
            .preCapital(UPDATED_PRE_CAPITAL)
            .preTotal(UPDATED_PRE_TOTAL)
            .finCapital(UPDATED_FIN_CAPITAL)
            .finTotal(UPDATED_FIN_TOTAL)
            .rschangeCap(UPDATED_RSCHANGE_CAP)
            .rschangeTot(UPDATED_RSCHANGE_TOT)
            .status(UPDATED_STATUS)
            .name(UPDATED_NAME);
        return projct;
    }

    @BeforeEach
    public void initTest() {
        projctRepository.deleteAll();
        projct = createEntity();
    }

    @Test
    void createProjct() throws Exception {
        int databaseSizeBeforeCreate = projctRepository.findAll().size();
        // Create the Projct
        ProjctDTO projctDTO = projctMapper.toDto(projct);
        restProjctMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(projctDTO)))
            .andExpect(status().isCreated());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeCreate + 1);
        Projct testProjct = projctList.get(projctList.size() - 1);
        assertThat(testProjct.getHoldTime()).isEqualTo(DEFAULT_HOLD_TIME);
        assertThat(testProjct.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testProjct.getContract()).isEqualTo(DEFAULT_CONTRACT);
        assertThat(testProjct.getContractContentType()).isEqualTo(DEFAULT_CONTRACT_CONTENT_TYPE);
        assertThat(testProjct.getRate()).isEqualTo(DEFAULT_RATE);
        assertThat(testProjct.getPreCapital()).isEqualTo(DEFAULT_PRE_CAPITAL);
        assertThat(testProjct.getPreTotal()).isEqualTo(DEFAULT_PRE_TOTAL);
        assertThat(testProjct.getFinCapital()).isEqualTo(DEFAULT_FIN_CAPITAL);
        assertThat(testProjct.getFinTotal()).isEqualTo(DEFAULT_FIN_TOTAL);
        assertThat(testProjct.getRschangeCap()).isEqualTo(DEFAULT_RSCHANGE_CAP);
        assertThat(testProjct.getRschangeTot()).isEqualTo(DEFAULT_RSCHANGE_TOT);
        assertThat(testProjct.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testProjct.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    void createProjctWithExistingId() throws Exception {
        // Create the Projct with an existing ID
        projct.setId("existing_id");
        ProjctDTO projctDTO = projctMapper.toDto(projct);

        int databaseSizeBeforeCreate = projctRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjctMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(projctDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllProjcts() throws Exception {
        // Initialize the database
        projctRepository.save(projct);

        // Get all the projctList
        restProjctMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projct.getId())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].holdTime").value(hasItem(sameInstant(DEFAULT_HOLD_TIME))))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].contractContentType").value(hasItem(DEFAULT_CONTRACT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].contract").value(hasItem(Base64Utils.encodeToString(DEFAULT_CONTRACT))))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].preCapital").value(hasItem(DEFAULT_PRE_CAPITAL.intValue())))
            .andExpect(jsonPath("$.[*].preTotal").value(hasItem(DEFAULT_PRE_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].finCapital").value(hasItem(DEFAULT_FIN_CAPITAL.intValue())))
            .andExpect(jsonPath("$.[*].finTotal").value(hasItem(DEFAULT_FIN_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].rschangeCap").value(hasItem(DEFAULT_RSCHANGE_CAP)))
            .andExpect(jsonPath("$.[*].rschangeTot").value(hasItem(DEFAULT_RSCHANGE_TOT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllProjctsWithEagerRelationshipsIsEnabled() throws Exception {
        when(projctServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restProjctMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(projctServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllProjctsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(projctServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restProjctMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(projctServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getProjct() throws Exception {
        // Initialize the database
        projctRepository.save(projct);

        // Get the projct
        restProjctMockMvc
            .perform(get(ENTITY_API_URL_ID, projct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(projct.getId()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.holdTime").value(sameInstant(DEFAULT_HOLD_TIME)))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION))
            .andExpect(jsonPath("$.contractContentType").value(DEFAULT_CONTRACT_CONTENT_TYPE))
            .andExpect(jsonPath("$.contract").value(Base64Utils.encodeToString(DEFAULT_CONTRACT)))
            .andExpect(jsonPath("$.rate").value(DEFAULT_RATE.doubleValue()))
            .andExpect(jsonPath("$.preCapital").value(DEFAULT_PRE_CAPITAL.intValue()))
            .andExpect(jsonPath("$.preTotal").value(DEFAULT_PRE_TOTAL.intValue()))
            .andExpect(jsonPath("$.finCapital").value(DEFAULT_FIN_CAPITAL.intValue()))
            .andExpect(jsonPath("$.finTotal").value(DEFAULT_FIN_TOTAL.intValue()))
            .andExpect(jsonPath("$.rschangeCap").value(DEFAULT_RSCHANGE_CAP))
            .andExpect(jsonPath("$.rschangeTot").value(DEFAULT_RSCHANGE_TOT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    void getNonExistingProjct() throws Exception {
        // Get the projct
        restProjctMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewProjct() throws Exception {
        // Initialize the database
        projctRepository.save(projct);

        int databaseSizeBeforeUpdate = projctRepository.findAll().size();

        // Update the projct
        Projct updatedProjct = projctRepository.findById(projct.getId()).get();
        updatedProjct
            .holdTime(UPDATED_HOLD_TIME)
            .location(UPDATED_LOCATION)
            .contract(UPDATED_CONTRACT)
            .contractContentType(UPDATED_CONTRACT_CONTENT_TYPE)
            .rate(UPDATED_RATE)
            .preCapital(UPDATED_PRE_CAPITAL)
            .preTotal(UPDATED_PRE_TOTAL)
            .finCapital(UPDATED_FIN_CAPITAL)
            .finTotal(UPDATED_FIN_TOTAL)
            .rschangeCap(UPDATED_RSCHANGE_CAP)
            .rschangeTot(UPDATED_RSCHANGE_TOT)
            .status(UPDATED_STATUS)
            .name(UPDATED_NAME);
        ProjctDTO projctDTO = projctMapper.toDto(updatedProjct);

        restProjctMockMvc
            .perform(
                put(ENTITY_API_URL_ID, projctDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(projctDTO))
            )
            .andExpect(status().isOk());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
        Projct testProjct = projctList.get(projctList.size() - 1);
        assertThat(testProjct.getHoldTime()).isEqualTo(UPDATED_HOLD_TIME);
        assertThat(testProjct.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testProjct.getContract()).isEqualTo(UPDATED_CONTRACT);
        assertThat(testProjct.getContractContentType()).isEqualTo(UPDATED_CONTRACT_CONTENT_TYPE);
        assertThat(testProjct.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testProjct.getPreCapital()).isEqualTo(UPDATED_PRE_CAPITAL);
        assertThat(testProjct.getPreTotal()).isEqualTo(UPDATED_PRE_TOTAL);
        assertThat(testProjct.getFinCapital()).isEqualTo(UPDATED_FIN_CAPITAL);
        assertThat(testProjct.getFinTotal()).isEqualTo(UPDATED_FIN_TOTAL);
        assertThat(testProjct.getRschangeCap()).isEqualTo(UPDATED_RSCHANGE_CAP);
        assertThat(testProjct.getRschangeTot()).isEqualTo(UPDATED_RSCHANGE_TOT);
        assertThat(testProjct.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testProjct.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    void putNonExistingProjct() throws Exception {
        int databaseSizeBeforeUpdate = projctRepository.findAll().size();
        projct.setId(UUID.randomUUID().toString());

        // Create the Projct
        ProjctDTO projctDTO = projctMapper.toDto(projct);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjctMockMvc
            .perform(
                put(ENTITY_API_URL_ID, projctDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(projctDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchProjct() throws Exception {
        int databaseSizeBeforeUpdate = projctRepository.findAll().size();
        projct.setId(UUID.randomUUID().toString());

        // Create the Projct
        ProjctDTO projctDTO = projctMapper.toDto(projct);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProjctMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(projctDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamProjct() throws Exception {
        int databaseSizeBeforeUpdate = projctRepository.findAll().size();
        projct.setId(UUID.randomUUID().toString());

        // Create the Projct
        ProjctDTO projctDTO = projctMapper.toDto(projct);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProjctMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(projctDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateProjctWithPatch() throws Exception {
        // Initialize the database
        projctRepository.save(projct);

        int databaseSizeBeforeUpdate = projctRepository.findAll().size();

        // Update the projct using partial update
        Projct partialUpdatedProjct = new Projct();
        partialUpdatedProjct.setId(projct.getId());

        partialUpdatedProjct
            .location(UPDATED_LOCATION)
            .contract(UPDATED_CONTRACT)
            .contractContentType(UPDATED_CONTRACT_CONTENT_TYPE)
            .rate(UPDATED_RATE)
            .preCapital(UPDATED_PRE_CAPITAL)
            .finCapital(UPDATED_FIN_CAPITAL)
            .rschangeTot(UPDATED_RSCHANGE_TOT)
            .name(UPDATED_NAME);

        restProjctMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProjct.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProjct))
            )
            .andExpect(status().isOk());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
        Projct testProjct = projctList.get(projctList.size() - 1);
        assertThat(testProjct.getHoldTime()).isEqualTo(DEFAULT_HOLD_TIME);
        assertThat(testProjct.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testProjct.getContract()).isEqualTo(UPDATED_CONTRACT);
        assertThat(testProjct.getContractContentType()).isEqualTo(UPDATED_CONTRACT_CONTENT_TYPE);
        assertThat(testProjct.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testProjct.getPreCapital()).isEqualTo(UPDATED_PRE_CAPITAL);
        assertThat(testProjct.getPreTotal()).isEqualTo(DEFAULT_PRE_TOTAL);
        assertThat(testProjct.getFinCapital()).isEqualTo(UPDATED_FIN_CAPITAL);
        assertThat(testProjct.getFinTotal()).isEqualTo(DEFAULT_FIN_TOTAL);
        assertThat(testProjct.getRschangeCap()).isEqualTo(DEFAULT_RSCHANGE_CAP);
        assertThat(testProjct.getRschangeTot()).isEqualTo(UPDATED_RSCHANGE_TOT);
        assertThat(testProjct.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testProjct.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    void fullUpdateProjctWithPatch() throws Exception {
        // Initialize the database
        projctRepository.save(projct);

        int databaseSizeBeforeUpdate = projctRepository.findAll().size();

        // Update the projct using partial update
        Projct partialUpdatedProjct = new Projct();
        partialUpdatedProjct.setId(projct.getId());

        partialUpdatedProjct
            .holdTime(UPDATED_HOLD_TIME)
            .location(UPDATED_LOCATION)
            .contract(UPDATED_CONTRACT)
            .contractContentType(UPDATED_CONTRACT_CONTENT_TYPE)
            .rate(UPDATED_RATE)
            .preCapital(UPDATED_PRE_CAPITAL)
            .preTotal(UPDATED_PRE_TOTAL)
            .finCapital(UPDATED_FIN_CAPITAL)
            .finTotal(UPDATED_FIN_TOTAL)
            .rschangeCap(UPDATED_RSCHANGE_CAP)
            .rschangeTot(UPDATED_RSCHANGE_TOT)
            .status(UPDATED_STATUS)
            .name(UPDATED_NAME);

        restProjctMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProjct.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProjct))
            )
            .andExpect(status().isOk());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
        Projct testProjct = projctList.get(projctList.size() - 1);
        assertThat(testProjct.getHoldTime()).isEqualTo(UPDATED_HOLD_TIME);
        assertThat(testProjct.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testProjct.getContract()).isEqualTo(UPDATED_CONTRACT);
        assertThat(testProjct.getContractContentType()).isEqualTo(UPDATED_CONTRACT_CONTENT_TYPE);
        assertThat(testProjct.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testProjct.getPreCapital()).isEqualTo(UPDATED_PRE_CAPITAL);
        assertThat(testProjct.getPreTotal()).isEqualTo(UPDATED_PRE_TOTAL);
        assertThat(testProjct.getFinCapital()).isEqualTo(UPDATED_FIN_CAPITAL);
        assertThat(testProjct.getFinTotal()).isEqualTo(UPDATED_FIN_TOTAL);
        assertThat(testProjct.getRschangeCap()).isEqualTo(UPDATED_RSCHANGE_CAP);
        assertThat(testProjct.getRschangeTot()).isEqualTo(UPDATED_RSCHANGE_TOT);
        assertThat(testProjct.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testProjct.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    void patchNonExistingProjct() throws Exception {
        int databaseSizeBeforeUpdate = projctRepository.findAll().size();
        projct.setId(UUID.randomUUID().toString());

        // Create the Projct
        ProjctDTO projctDTO = projctMapper.toDto(projct);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjctMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, projctDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(projctDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchProjct() throws Exception {
        int databaseSizeBeforeUpdate = projctRepository.findAll().size();
        projct.setId(UUID.randomUUID().toString());

        // Create the Projct
        ProjctDTO projctDTO = projctMapper.toDto(projct);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProjctMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(projctDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamProjct() throws Exception {
        int databaseSizeBeforeUpdate = projctRepository.findAll().size();
        projct.setId(UUID.randomUUID().toString());

        // Create the Projct
        ProjctDTO projctDTO = projctMapper.toDto(projct);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProjctMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(projctDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Projct in the database
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteProjct() throws Exception {
        // Initialize the database
        projctRepository.save(projct);

        int databaseSizeBeforeDelete = projctRepository.findAll().size();

        // Delete the projct
        restProjctMockMvc
            .perform(delete(ENTITY_API_URL_ID, projct.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Projct> projctList = projctRepository.findAll();
        assertThat(projctList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
